"""
This module is the entry point to the stylometry calculations

Requirements:
   pip2.7 install nltk
   pip2.7 install pdfminer
   pip2.7 install scikit-learn
   pip2.7 install pydot
   
   from interactive console:
       nltk.download('punkt')
"""
#import stylometry
import sys
import os

def classify():
    sys.path.insert(0, '..')
    sys.path.append(os.path.dirname(__file__) + "/myStylo")


    import myStylo.extract
    import myStylo.classify
    import myStylo.cluster
    import myStylo.pdfRead

    #convert pdfs to txt files
    root_dir = '../stylometry-students/'

    fname = myStylo.pdfRead.convert(root_dir)


    Terrill = myStylo.StyloDocument(root_dir + fname)
    Terrill.text_output()


    '''
    # Single Author Corpus

    Terrill_corpus = myStylo.StyloCorpus.from_glob_pattern('../stylometry-data/Terrill/*.txt')
    Terrill_corpus.output_csv('terrill.csv')

    # All authors
    novel_corpus = myStylo.StyloCorpus.from_glob_pattern('../stylometry-data/*/*.txt')
    novel_corpus.output_csv('novels.csv')


    # splits data into validation and training default 80% train 20% validation
    dtree = myStylo.StyloDecisionTree('novels.csv')
    # fit the decision tree to the data
    dtree.fit()
    # predict the authorship of the validation set
    dtree.predict()
    # Show the confusion matrix and accuracy of the validation prediction
    matrix = dtree.confusion_matrix()
    print matrix
    # Write the decision tree to an image file
    dtree.output_image('tree.png')


    # Create a KMeans clusterer and run PCA on the data
    kmeans = myStylo.StyloKMeans('novels.csv')
    # Cluster the PCA'd data using K-means
    kmeans.fit()
    # Shot the plot of explained variance per principle component
    #kmeans.stylo_pca.plot_explained_variance()
    # Show the plot of the PCA'd data with the cluster centroids
    kmeans.plot_clusters()

    return "it worked"'
    '''

    '''
    def convert(fname, pages=None):
        if not pages:
            pagenums = set()
        else:
            pagenums = set(pages)

        output = StringIO()
        manager = PDFResourceManager()
        converter = TextConverter(manager, output, laparams=LAParams())
        interpreter = PDFPageInterpreter(manager, converter)

        infile = file(fname, 'rb')
        for page in myStylo.pdfRead.PDFPage.get_pages(infile, pagenums):
            interpreter.process_page(page)
        infile.close()
        converter.close()
        text = output.getvalue()
        output.close
        return text
    '''

if __name__ == "__main__":
    classify()