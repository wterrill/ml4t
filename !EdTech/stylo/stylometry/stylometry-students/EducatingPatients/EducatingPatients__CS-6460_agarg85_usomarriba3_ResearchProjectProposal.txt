 

 

 

 

 

Educational Technology Project Proposal 

 
  
 
 
 
 
 
 
 

 

A Research Project Proposal submitted by  

Ashish Garg (agarg85) and Uvernes Somarriba Sr. (usomarriba3) 

 

CS-6460 – Education Technology 

 
 

Empowering Patients 

By  

 

 

Using Educational Technology in Educating Patients 

 
 
 
 
 
 
 
 
 
 

Georgia Institute of Technology 

Spring 2016 

 

Using Educational Technology in Educating Patients 

 

 

 

Table of Contents 

Contents 
Educational Technology Project Proposal ...................................................................................... 1 

Using Educational Technology in Educating Patients ..................................................................... 1 

Table of Contents ............................................................................................................................ 2 

Synopsis........................................................................................................................................... 3 

Section A ......................................................................................................................................... 3 

Section B ......................................................................................................................................... 3 

Section C ......................................................................................................................................... 4 

Section D ......................................................................................................................................... 5 

Section E .......................................................................................................................................... 5 

Section F .......................................................................................................................................... 6 

Section G ......................................................................................................................................... 6 

Section H ......................................................................................................................................... 7 

Section I ........................................................................................................................................... 7 

Calendar .......................................................................................................................................... 8 

References .................................................................................................................................... 10 

 

 
 

 

2       
 

Using Educational Technology in Educating Patients 

 

 

Synopsis 
All project proposals you should cover the following information: 
1.  The members of the project team: 

 

i. 
ii. 

Ashish Garg (agarg85) 
Uvernes Somarriba (usomarriba3) 
 

2.  A task list of the tasks that must be completed to execute and deliver the project. Make sure to include 

the required tasks as well, such as the trailer, progress reports, and final paper. 
 

Today, meaningful patient education programming is an important part of the treatment, but only when 
it offers compelling, evidence-based, and actionable results. 

Here we take a look at how effective patient education makes a difference in meeting the significant cost, 
quality and care challenges health providers face every day. 
 
Intent of this research is to explore existing EdTech tools and define an EdTech framework for the effective 
and efficient implementation of Patient Learning Systems. 

 

Section A 
Introduction, objective and background of the project 
Patient education is important for patients for: 

i. 

ii. 
iii. 

Better patient outcomes – Patients more likely to respond well to their treatment plan – fewer 
complications. 
Informed Consent – Patients feel you’ve provided the information they need. 
Satisfaction and referrals – Patients more likely to stay with a medical practice and refer other 
patients. 
Risk Management – Lower risk of malpractice when patients have realistic expectations. 
Reduced hospital readmissions – Better after care that may reduce hospital readmissions. 
 
Action plan: 
Tasks List: 

iv. 
v. 

Complete this section. 
Proposal revision and resubmission based on the feedback received. 
CITI certification. 
IRB approval planning. 
First weekly status report 

i. 
ii. 
iii. 
iv. 
v. 
 

Division of responsibilities: 
Ashish: Overall patient education write-up. IRB report. 
Uvernes: Specific to hospital patient discharge and the readmission. CITI certification. IRB report. 

Section B 
Challenges 

 

i. 

Demographics – investigate how patient demographics affect the patient education 

 

3       
 

Using Educational Technology in Educating Patients 

 

 

 

iv. 

ii. 
iii. 

Cultural – do cultural and ethnicity have impact on the patient education? 
Education – Low literacy level and low FHL (Functional Health Level) affects patient’s ability 
to retain knowledge and understand instructions. 
Government  policies,  laws  and  regulations  –  more  and  more  Government  policies  are 
encouraging to promote patient education, within the patient safety and security laws. 
Ethics – what are people ethics related to patient education? 
 
Action plan: 
Tasks List: 

v. 

i. 
ii. 
iii. 

Complete this section. 
Second weekly status report 
Intermediate milestone – Progress report 1. 

 

Division of responsibilities: 
Ashish: Demographic, Education, Government policies, laws and regulations related to patients education. 
Fall back plan if IRB is not approved. 
Uvernes: Government policies, laws and regulations and Ethics in treatment and rehabilitation of patients. 
Fall back plan if IRB is not approved. 

Section C 

 

Current solutions in the marketspace and their limitations.  

i.  Web sites – Websites are good sources of information. But they are one sided and are not 

patient centric. 

iii. 

iv. 

ii.  Multimedia Kits – Multimedia kits include integrated and interactive packages of print and 
electronic media that help patients with chronic diseases. Multimedia Kits are usually not very 
interactive in nature, though some products are available that try to mock a classroom setup. 
Printed publications – Numerous books and journals are published for both consumers and 
healthcare professionals. Books and Journals provide excellent source of patient education. 
However, they lack the ability to be interactive and provide feedback mechanism. 
Audio  and  video  tapes/disks  –  Audio  tapes  and  CDs  provide  patient  education  in  very 
traditional way, where a patient can listen and learn. Video Tapes, Videos CDs and DVDs also 
have a prominent role in patient education and are trying to use advances in the educational 
technology to enhance patient’s knowledge. 
Existing applications/portals – X-plain patient education system from The Patient Education 
Institute  (PEI)  and  Vocera  Good  Go  Patient  Dischager  Communication  from  Vocera 
Communication, Inc. are two noticeable efforts in using education technology for educating 
patients.  Each product uses its own architectural models. Architectural model for different 
products are significantly different from each other.  None of the existing product seems to 
be a complete solution.  This makes evident the need of a standard framework. 
 
Action plan: 
Tasks List: 

v. 

i. 

ii. 

Investigate various systems and their sources. Identify the gaps and shortcomings in the 
current systems. 
Incorporate feedback received on Milestone 1. 

 

4       
 

Using Educational Technology in Educating Patients 

 

 

iii. 

Third Weekly Status Report. 
 

 

Division of responsibilities: 
Ashish: Web Sites, Multimedia kits which include Audio and Video.  
Uvernes:  Printed publications and Existing applications/portals.  

Section D 
Vision of a comprehensive solution to empower patients through patient education. 

 

i. 

ii. 

Benefits – better management of care plan, public health, reduced relapses and readmission 
and overall cost reduction. 
Patient engagement/participation in their own health – when patients are engaged in their 
health,  then  better  educated  patients  improve  –  physician  communication,  improve 
medication administration, diet and physical exercise. 
Providers’ involvement in the community health efforts – the benefit of this being part of the 
community  providers  belong  also  reduces  malpractices  and  increases  social  awareness 
among healthcare professional community. 
 
Action plan: 
Tasks List: 

iii. 

i. 
ii. 
iii. 

Justify time and effort in educating patients. 
Fourth Weekly Status Report. 
Intermediate milestone – Progress Report 2. 

 

Division of responsibilities: 
Ashish: Benefits and Providers’ involvement. 
Uvernes: Benefits and Patient engagement. 

Section E 
Application of educational technology in Patient Education and high-level architectural view. 

 

i. 

ii. 

Exploration of education technologies – Learning Management Systems, Intelligent Tutoring 
Systems,  MOOCs,  Computer-Supported  Collaborative  Work,  Simulation-Based  Learning, 
Mobile Devices. 
Selection  of  education  technologies  -  Which  of  the  existing  EdTech  components  are  best 
suitable  for  an  integrated  solution?    Is  there  any  functional  requirement  that  cannot  be 
mapped to an existing component? 
High-level Business, Data, Technology and Application architectural views - How the different 
components  should  be  integrated?  What  kind  of  interaction  is  required  between  those 
components? 
Application Security – protect the data that is consumed by the application. 
 
Action plan: 
Tasks List: 

iv. 

iii. 

i. 
ii. 
iii. 

Explore education technology and its application in educating patients. 
Incorporate feedback received on the Milestone 2. 
Fifth weekly status report. 

 

5       
 

Using Educational Technology in Educating Patients 

 

 

 
Division of responsibilities: 
Ashish: High-level architecture view and security 
Uvernes: Exploration and selection of technology 

 

Section F 
High-level design of the administration module of the solution. 

 

i. 

ii. 

Functional  requirements  –  Define  the  functional  requirements  of  the  system  and  its 
administration module. 
Create process flow – come up with the process flow to show how the process will work from 
the administration module perspective. 
Define user interaction points – User interaction points. 
Define  data  persistence  requirements  –  Data  that  needs  to  be  persisted  for  analytics  and 
future use. 
 
Action plan: 
Tasks List: 

iii. 
iv. 

i. 
ii. 
iii. 

Define the administration module of the system. 
Sixth weekly status report. 
Intermediate Milestone 3 – Trailer. 

 

Division of responsibilities: 
Ashish: Functional requirements and process flow. 
Uvernes: User interaction points and data persistence requirements. 

Section G 
Recommendation for content and feedback mechanism. 

 

i. 

ii. 

iii. 

Content creation – who should create the content. What contents formats are better suitable 
for educating patients?  
Content  management  –  how  and  what  the  content should  be  managed,  stored,  accessed, 
retrieved. 
Content delivery – which media, format the content should be delivered in. How the content 
should be delivered? Is adaptive learning suitable for educating patients? Should the system 
be able to change the speed and content format in adapting the different learning capacities 
of different patients? 
Feedback mechanism – email, chats, forums, SMS, phone calls are ways to get feedback from 
patients. 
 
Action plan: 
Tasks List: 

iv. 

i. 
ii. 
iii. 

Incorporate feedback received on the Milestone 3. 
Define and delivery of content. 
Seventh weekly status report. 

 

Division of responsibilities: 

 

6       
 

Using Educational Technology in Educating Patients 

 

 

Ashish: Functional requirements and process flow. 
Uvernes: User interaction points and data persistence requirements. 

 

Section H 
Integration of the solution with the current EHR/EMR systems.  

 

i. 

ii. 

Interaction with the provider systems – provide in and out integration points for information 
communication. 
Intervention by the providers and by patients – provide a mechanism to intervene patient 
education plan. Ability for patients to adjust speed and level of content delivery. 
Communication  mechanism  between  the  patients  and  their  providers  –  patient  portals, 
emails, refill requests, appointment scheduling etc. 
 
Action plan: 
Tasks List: 

iii. 

i. 

ii. 
iii. 

Use  of  APIs,  integration  technologies  like  Web  Services/XML/OSA  to  provide  robust 
integration features in the application. 
Eighth weekly status report. 
Intermediate Milestone 4 – Video Presentation. 

 

Division of responsibilities: 
Ashish: Interaction and integration with provider systems. 
Uvernes: Communication mechanism. 

Section I 
Summarization of the research paper. 

 

i. 
ii. 
iii. 
iv. 
v. 

Abstract of the final outcome – create the abstract of the final outcome of the research. 
References/Sources – assemble all the references/resources into the document. 
Assemble the pieces into one document – finalize the document. 
Final presentation and report. 
Final project submission. 
 
Action plan: 
Tasks List: 

i. 
ii. 
iii. 

Finalize project submission. 
Finalize presentation and report. 
Ninth weekly report. 

 

Division of responsibilities: 
Ashish: Abstract and References. 
Uvernes: Assembling and Final presentation. 
 

 

 

7       
 

Using Educational Technology in Educating Patients 

 

 

 

Calendar 
A calendar describing weekly milestones from February 22nd to May 1st. 
 
# 

Milestone 

1 

Proposal submission 

Week 1 Activities 

2 

3 

4 

5 

6 

Proposal revision and resubmission based on the feedback 
received 
Complete Section A 

Weekly Status Report 1 

CITI Certification - Uvernes 

Prepare for IRB review 

Week 2 Activities 

7 

8 

9 

Complete Section B 

Weekly Status Report 2 

Milestone 1: Progress Report – 1  

Week 3 Activities 

10 

Incorporate feedback received on Milestone 1 

11 

Complete Section C 

12  Weekly Status Report 3 

Week 4 Activities 

13 

Complete Section D 

14  Weekly Status Report 4 

15 

Milestone 2: Progress Report – 2  

Week 5 Activities 

Intermediate 
Milestone 
No 

Deliverable Date 
(MM/DD/YYYY) 
02/22/2016 

No 

No 

No 

No 

No 

No 

No 

Yes 

No 

No 

No 

No 

No 

Yes 

02/28/2016 

02/28/2016 

02/28/2016 

02/28/2016 

02/28/2016 

03/07/2016 

03/07/2016 

03/07/2016 

03/14/2016 

03/14/2016 

03/14/2016 

03/20/2016 

03/20/2016 

03/20/2016 

16 

Incorporate feedback received on Milestone 2 

No 

03/28/2016 

 

8       
 

Using Educational Technology in Educating Patients 

 

No 

No 

No 

No 

Yes 

No 

No 

No 

No 

No 

Yes 

No 

No 

No 

No 

03/28/2016 

03/28/2016 

04/04/2016 

04/04/2016 

04/04/2016 

04/11/2016 

04/11/2016 

04/11/2016 

04/18/2016 

04/18/2016 

04/18/2016 

04/25/2016 

04/25/2016 

04/25/2016 

05/01/2016 

 

 

17 

Complete Section E 

18  Weekly Status Report 5 

Week 6 Activities 

19 

Complete Section F 

20  Weekly Status Report 6 

21 

Milestone 3: Trailer 

Week 7 Activities 

22 

Incorporate feedback received on Milestone 3 

23 

Complete Section G 

24  Weekly Status Report 7 

Week 8 Activities 

25 

Complete Section H 

26  Weekly Status Report 8 

27 

Milestone 4: Video Presentation 

Week 9 Activities 

28 

Incorporate feedback received on Milestone 4 

29 

Complete Section I 

30  Weekly Status Report 9 

31 

Final Presentation and Project Report 

 

 
 

 

9       
 

Using Educational Technology in Educating Patients 

 

 

 

References 
[1] Patient Education Institute (PEI) – X-Plain -  http://www.patient-education.com/ 
[2] Vocera Communications, Inc.  -  http://www.vocera.com 
[3] Significance of Patient Education for Healthcare and Rehabilitation: 
http://www.jblearning.com/samples/0763755443/55447_CH01_Dreeben.pdf 
[4] The Right Patient Education Tools: Strategies to Improve Health Outcomes and Meet Regulatory 
Goals HealthOutcomesWhitePaper.pdf: http://www.milner-
fenwick.com/Portals/0/Docs/PDFs/HealthOutcomesWhitePaper.pdf?ver=2015-10-09-145455-357 
[5] Jama Network ( http://archinte.jamanetwork.com/article.aspx?articleid=214905%20 ) 
[6] Sentara Virginia Beach Hospital case study (2009): 
http://cdn.medicexchange.com/images/whitepaper/11%20-%20Getwellnetwork%20-
%20Improving%20Heart%20Failure%20Outcomes%20Through%20Interactive%20Patient%20care-
%20The%20sentara%20Virginia%20Beach%20General%20Hospital%20Experience.pdf?1265977678 
[7] CMS Guidelines: 
http://www.cms.gov/Regulations-and-
Guidance/Legislation/EHRIncentivePrograms/downloads/6_Patient-Specifi c_Education_Resources.pdf 
[8] Stanford University Patient Education Center: http://patienteducation.stanford.edu/materials/ 
[9] Elsvier (http://www.journals.elsevier.com/patient-education-and-counseling/) 
[10] Wikipedia: (https://en.wikipedia.org/wiki/Patient_education) 
[11] Healthcare Design Magazine: http://www.healthcaredesignmagazine.com/article/designing-
evidence-based-research-project-childrens-hospital-collaborative-approach 
[12] M. D. Roblyer: Educational Technology Research That Makes a Difference: Series Introduction 
(http://citejournal.org/articles/v5i2seminal1.pdf) 
[13] Meaningful Use Regulations: http://www.cms.gov/Regulations-and-Guidance/Legislation/ 
EHRIncentivePrograms/Downloads/Stage2Overview_Tipsheet.pdf 
 
 
 

 

10       

 

