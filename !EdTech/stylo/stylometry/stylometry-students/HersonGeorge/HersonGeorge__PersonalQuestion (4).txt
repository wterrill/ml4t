https://www.udacity.com/wiki/ud915/Spring2016/Personal_Question 

Personal Question 

CS6460 Educational Technology 

 Spring 2016 

by George B. Herson  

(​gherson3@gatech.edu​) 

February 14, 2016 

What are the knowledge representation languages related to Attempto? What are its 
"competitors" and what are their benefits and downsides?  In other words, why would you pick 
one language over another. Consider ​agent communication​.  How does it relate to formalized 
human communication? 
 

Competing with Attempto in the knowledge representation by controlled natural language space 

is another formal language called Gellish.  Unlike Attempto, it comes with defined concepts and 

relation types.  Gellish also differentiates concepts and the names referring to them, making 

synonyms in e.g. different natural languages simple. [1] Gellish allows subject­predicate­object 

specification.  Relations such as “is a specialization of” and “is valid in the context of” are ready 

to use. However, Gellish has no mapping to logic beyond if­then rules. There is no general way 

to express universal quantification over several variables, for example. [2]  

The standard semantic web choice for knowledge representation is Web Ontology Language 

(OWL).  Its job is to add semantics to the subject­predicate­object triples of another semantic 

web standard, Resource Description Framework (RDF).  RDF, by itself, only allows 

specification of Is­A relationships and object properties. [3] 

Agent communication abstracts content language, however, so my hypothesis is that a variety of 

knowledge representation formalisms can be supported in a single reasoning system offering 

contradiction­detection and semantic query.  A principal virtue of logic is that it is directly and 

unambiguously machine translatable.  It would only cost increased complexity to include agents 

that spoke Attempto, Gellish, OWL, etc, to translate those specifications into the representation 

used internally by the system’s reasoner. 

Agents can be understood by comparing them to the Web and Web services. 

“The constructs used to describe and encode a page — the Hypertext Markup Language 

(html) — describe the page’s appearance, but not its contents. By contrast, software 

agents don’t care about the appearance, but only the contents. 

Some agents, however, use the Web as it is now. Take the shopbot, an agent that visits 

online retailer catalogs and returns the prices for items users might want to buy. Shopbots 

operate by a form of screen­scraping, in which they download catalog pages and search 

first for the name of an item of interest and then for the nearest set of characters that has a 

dollar sign, which presumably is the item’s price. The shopbots also might submit the 

same forms that a human would likely submit and then parse the returned pages that 

merchants expect humans are viewing. The Semantic Web will make the Web more 

accessible to agents by making use of semantic constructs, such as ontologies represented 

in well­established languages, so that agents can understand what is on a page.” p2 [4] 

In fact, “UDDI is itself a Web service that is based on XML and SOAP. It provides both 

white­pages and yellow­pages services, but not a brokering or facilitating service.” p3 [4] 

Agents extend Web services in important ways p3 [4]: 

● Agents are self­aware, and through learning and model­building during interactions gain 

awareness of other agents.  Such awareness is used to provide improved services to 

repeat customers and capitalize on new capabilities in its environment.  

● Agents are designed to use and reconcile ontologies, freeing the clients and providers of 

the agent to use individually preferred ontology.  

● Agents pro­actively communicate updates and alerts while Web services are passive until 

invoked, e.g., lack a subscription mechanism for periodic updates.  

● Agents are autonomous, which appears to mean that their behavior retains some mystery, 

as in, e.g., the circumstances that trigger commitments and coordination with other 

agents. 

● “Agents are cooperative, and by forming teams and coalitions can provide higher­level 

and more comprehensive services”, e.g., a production notification system that works with 

the shipper’s Web services to offer customers real time purchase status. “Web services 

currently involve a single client accessing a single server” without composing 

functionality, “but soon applications will demand federated servers with multiple clients 

sharing results.” p4 [4] 

● Agents can interact as peers while Web services are strictly servers to their clients. [5] p5 

● Agents can be mobile [5] p5, e.g., reside on a smartphone, whereas Web services are 

usually cloud­based and ever­ready. 

To give the flavor of an Agent Communication Language, a sample KQML message: 

(tell :sender 

bhkAgent 
:receiver fininBot 
:in­reply­toid7.24.97.45391 
:ontology ecbk12 
:language Prolog 
:content

“price(ISBN3429459, 24.95)”) 

[6] slide 16.  The slots for ontology and language of content demonstrate that those may vary. 

As far as relating agent communication to formalized human communication we can note, firstly, 

that humans qualify as intelligent agents: 

● Intelligent agents​ (also known as ​rational agents​) are not just computer programs: 
they may also be machines, human beings, communities of human beings (such as 
firms​) or anything that is capable of goal directed behavior. ­­ 
https://en.wikipedia.org/wiki/Software_agent#Distinguishing_intelligent_software_ag
ents_from_intelligent_agents_in_artificial_intelligence  

Secondly, though humans can accomplish goal oriented behaviors more flexibly than software, 

submitting human interaction to formal analysis may prove cost effective where preservation of 

human life is involved.  E.g. [7]: “Using this language [(Enhanced Operator Function Model)], 

we modeled the behavior of the human [air traffic control] operators in the procedure to effect a 

change of heading clearance, and sketched a proof of a basic safety property. Further proofs 

along these lines could help guarantee the safety of the protocol’s task model, or find errors in 

the definition of the procedure.” p7  

References 

[1] ​Gellish. (2015, April 20). In ​Wikipedia, The Free Encyclopedia​. Retrieved 11:50, February 15, 
2016, from ​https://en.wikipedia.org/w/index.php?title=Gellish&oldid=657351296 
[2] ​Kuhn, T. (2014). A survey and classification of controlled natural languages.​Computational 
Linguistics​, ​40​(1), 121­170.  ​http://attempto.ifi.uzh.ch/site/pubs/papers/kuhn2013cl.pdf 

[3] ​Knowledge representation and reasoning. (2016, February 13). In ​Wikipedia, The Free 
Encyclopedia​. Retrieved 11:51, February 15, 2016, 
from​https://en.wikipedia.org/w/index.php?title=Knowledge_representation_and_reasoning&oldid=70
4764821 

[4] Huhns, M. N. (2002). Agents as Web services. ​IEEE Internet computing​,​6​(4), 93. ​url  

[5] Liu, D. (n.d.). Agents and Web Services. 

http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.9.4830&rep=rep1&type=pdf  

[6] Finin, T. (2007). A Brief Overview of Agent Communication in the Semantic Web Era and 
Beyond. ​http://ebiquity.umbc.edu/resource/html/id/220/ 
[7] ​Bass, E. J., Bolton, M. L., Feigh, K., Griffith, D., Gunter, E., Mansky, W., & Rushby, J. 
(2011, October). Toward a multi­method approach to formalizing human­automation interaction 
and human­human communications. In ​Systems, Man, and Cybernetics (SMC), 2011 IEEE 
International Conference on​ (pp. 1817­1824). IEEE. ​url  
 

