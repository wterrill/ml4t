The area of Educational Technology that I am most interested in exploring does not seem to be 

 
a well-researched area, especially not in the course library. Online tutoring is something that I have been 
attracted to for quite some time, and I would really love to create a tool/startup in this space. As I 
elaborated on in my previous assignment, when I was in college, I tutored a high school boy in AP 
Computer Science, and it was a very rewarding experience for me, both educationally and financially. 
There seems to be a market for online tutoring, and I think that if there was a solid online tool to 
facilitate these tutoring sessions many students and tutors could benefit. The focus of educational 
technology today seems to be in taking the live interaction out of the equation. Dr. Joyner talks a lot 
about trying to make up for the lack of live classrooms in OMSCS classes, and he says that the constant 
flow of information on Piazza is a wonderful substitute for classroom discussions. I definitely agree with 
him on this (and I think it’s even better because I rarely raise my hand during lectures) but I still think 
that live interaction is necessary for some students to really learn material well. The boy that I tutored in 
Computer Science had ADD, and there’s no way he would have been able to function in a Piazza forum 
to learn the material. Piazza is great for a Master level course, but I’m not sure if it could work as well for 
a high school class, and maybe not even for some college students who need more help.  

While everything today is moving online, we need to figure out how to keep live interaction 

 
going over the web. There are a number of companies that do offer online tutoring, but none of them 
seem to be too overwhelmingly popular, especially for younger kids. I would guess that this is because 
people don’t feel comfortable talking to strangers on the internet, and even more so parents don’t feel 
comfortable letting their kids talk with strangers on the internet. I definitely understand the idea of 
online tutoring rubbing parents the wrong way. That is why my tool, YeshivaTutors.com would focus on 
the Jewish community (at least at first). We would only hire Jewish tutors to teach Judaic subjects, such 
as Bible and Talmud. Jewish parents would love for their children to learn these subjects with older, 
more experienced students. I think keeping the website focused on the Jewish community would create 
a built-in sense of trust. The Jewish nation is like one big family, and it’s not the same as talking to a 
complete stranger online. 

Focusing a little more on the actual functionality of the online tutoring sites, there seems to be a 

 
variety of models out there. Wyzant seems to be a fairly open site, where anyone can sign up to be a 
tutor. They put the responsibility of ensuring safety on the students, recommending that you run a 
background check of your own before hiring a tutor on the site. That’s a scary thing for me, and I would 
never go that route if my child needed a tutor. The Princeton Review’s Tutor.com looks like it’s a lot 
harder to become a tutor. You need to pass a subject matter test, do a mock tutoring session, and pass a 
background check. I would be much more likely to hire a tutor from this site, as the company has done 
more work to ensure that they are hiring legitimate, qualified tutors. I think it’s important that the site 
stands behind their tutors and can confidently guarantee students’ safety, as well as their tutors’ 
knowledge and teaching abilities. 

Something that I noticed in all of these sites (Wyzant, Tutor.com, Chegg, etc.) is that they focus 
 
the site in facilitating the actual tutoring session. So many of them have built in to their site video chat 
capabilities and synchronized document editing features. When I originally wanted to create 
YeshivaTutors.com, I had thought that the site would help pair up students and tutors, and then they 
would use Skype and Google Docs to actually host the tutoring session. The site would help them 
maintain their relationship in scheduling sessions, tracking progress, and providing feedback to parents. 
The sites that are out there aren’t necessarily looking to maintain recurring sessions with the same 

tutor, they’re more on an as-needed basis. When you need help, log on to our site, and we’ll find 
someone available to help you out. I think it is beneficial to pair up with the same tutor throughout a 
semester so that the tutor and student get to know each other a bit and get more productive as time 
goes on. It’s always easier to learn with someone that you feel comfortable with. I think that involving 
the parents in the process is also very important, making sure that they feel comfortable with the tutor 
and are in the loop regarding their child’s progress. This would likely increase parent’s likelihood to hire 
an online tutor for their children, which could be a large market to tap into on top of the existing 
independent college students who are the primary users of these online tutoring sites. 

