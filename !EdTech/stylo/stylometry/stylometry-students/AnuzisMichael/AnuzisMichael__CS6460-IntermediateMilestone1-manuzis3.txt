CS6460: Intermediate Milestone 1 

Michael Anuzis (​o@gatech.edu​) 
 

 

 

 

Contents 

Intermediate Milestone 1 
Executive Summary 
Project Team 
Research Focus 

Differentiators for a New Approach 

Primary Question 1 
Primary Question 2 
Secondary (Stretch) Question 

Research Methodology 

Intermediate Milestone 1 
 
This intermediate milestone is a screencast of our interactive tutoring system content thus far. 
The content is still a work in progress, but this screencast gives an overview of Oppia and how 
we’re using its interactive tutoring capabilities to teach Python as part of a narrative of 
humanity’s colonization of Mars. 
 

● https://youtu.be/NqtaVJN5c4o​ (~20 mins, I recommend watching at a higher speed) 

 
For reference, an abbreviated version of the project proposal is below. (for reviewers who may 
want more background) 

Executive Summary 
 
Computer Science education is increasingly important with president Obama approving $4 
billion in funding for K12 CS education in Feb’16 , yet most CS educational resources can be 
significantly improved. Typical courses at Codecademy.com, Codeschool.com, Udacity.com, 
and Code.org are 1­size­fits­all approaches that ignore a learner’s prior knowledge and bring 
learners on a linear path through a given topic. Much of the educational content is also rather 
dry, having learners program things like tip calculators or fictional grocery store software. 
 
Aspiring to challenge the status quo and explore new practices in educational technology, 

1

1 
http://www.informationweek.com/government/leadership/computer­science­for­all­obama­unveils­initiative­for­us­students/d/d­id/132
4136 

this project will create a Narrative­centered Learning Environment (NLE) that teaches 
fundamentals of Python programming through an immersive story of humanity’s colonization of 
Mars. We’re doing this via Oppia.org’s open source Intelligent Tutoring System (ITS). 
 

Project Team 

● Michael Anuzis: working alone within CS6460, partnering with two volunteers on content. 
● Sean Lip: Oppia.org founder, volunteering to help review content and excited to see how 

many students Oppia’s ITS­style learning can scale to via AppEngine. 

● Zhan Xiong: CS undergrad at UPenn, an Oppia volunteer interested in the project. 

 
 
 

2

Research Focus 
 
Leveraging ITS to teach computer programming is not new, with systems like LISPITS, 
PROUST, and CHIRON  being used as early as the 1980s. These early systems were 
significant breakthroughs in ITS, analyzing student programs relative to a model implementation 
and analyzing discrepancies to provide personalized insights for improvement. Students 
encountering bugs in CHIRON could ask why and receive a list of hints for why specific bugs 
may be occurring at various stages of their assignment. 
 
In recent years there has been an influx of new interactive resources teaching computer 
programming including Codecademy.com, Codeschool.com, Udacity.com, and Code.org, 
among others. Most contemporary solutions provide personalized insight into programming 
bugs and errors, looking for the absence of variable names or functions that produce incorrect 
outputs and warning appropriately. Programming solutions that result in no errors but still return 
incorrect values also receive personalized hints based on the range of incorrect values seen. 
 

Differentiators for a New Approach 
 
Our research has two primary questions and one secondary question. 
 

Primary Question 1 
How much do learners value an experience that adapts to prior knowledge? 
 

2 ​Corbett, A.T., & Anderson, J. R. (1992). LISP Intelligent Tutoring System Research in Skill Acquisition. In Larkin, J. & Chabay, R. 
(Eds.) Computer assisted instruction and intelligent tutoring systems: shared goals and complementary approaches (pp.73­110) 
Englewood Cliffs, New Jersey: Prentice­Hall Inc. 

Most MOOCs and online CS resources today start learners at the same point and force them 
through a linear path that ignores prior knowledge. Some MOOCs evaluate a learner’s 
proficiency as they progress, but many are simply online equivalents of lecture material. 
 
Bloom’s 2 Sigma Problem research from the 1980s found that learning outcomes improve by 
one standard deviation shifting from lectures to mastery­based learning that verifies proficiency 
before allowing students to proceed, and by a second standard deviation shifting to individual 
tutoring. 
 

 

 
 
Utilizing Oppia’s interactive branching, we will emulate a 1­on­1 tutor by probing for prior 
knowledge and helping learners hone in on their own learning path starting with problems 
appropriate for their skill level. 
 
To measure success, students with prior programming experience, but infamiliarity with OOP 
will be invited to try the content and surveyed for its ability to hone in on an appropriate learning 
step relative to status quo systems. 
 

Primary Question 2 
 
How well can a Narrative­based Learning Environment (NLE) meet or exceed the effectiveness 
of existing CS resources when factoring in proficiencies achieved and fun factor? 
 
Existing resources like Codecademy.com claim 2.5M+ learners for an introduction to Python 
that implements tip calculators and fictional grocery store software. This research aims to 
explore how an NLE can meet or exceed learning outcomes achieved on status quo systems. 
 
To measure success, we will invite a group of learners with limited programming experience to 
complete both the Codecademy tutorial and our NLE. Learners will be evaluated on their 
proficiency after each platform, and asked to rate each experience for its learning effectiveness 
and fun factor. 
 

Secondary (Stretch) Question 
 
How important is an ITS’ branching ability to route students to supplementary practice material? 
 
If learners get stuck on a concept with a typical linear experience they’re often out of luck. Links 
to supplementary resources can be provided, but such links take the student outside the 
learning experience where their struggles are harder to observe and address. 
 
Oppia’s branching ability provides an opportunity to support learners end­to­end, inclusive of 
supplementary exercises as part of the experience. This technique is rarely seen in today’s CS 
educational resources, and it’s difficult to anticipate for certain whether there will be an 
opportunity to apply it in this project. 
 
Through analyzing learner submissions and where they get stuck most frequently, if we are able 
to identify an opportunity for supplemental practice we will attempt to incorporate it in the 
project. 
 
An example using nested data structures might ask learners to utilize lists inside dictionaries 
inside lists. If learners struggle, it may be effective to route to a simpler warmup that reduces 1 
layer of depth. 
 
To measure success, students will be surveyed for the relevance of feedback they receive 
reflecting the ITS’ ability to correctly identify misconceptions and provide appropriate solutions. 
Students who follow supplementary support­branches will be surveyed on the effectiveness of 
these activities. 
 

 

 

Research Methodology 
 
This project will seek IRB approval to evaluate learning via surveys for the beta testing group. 
The alpha testing group will be informal volunteers interested in learning programming. 
 
In the event IRB rejects the project, or approval takes too long to acquire, the project and 
surveys will proceed as planned and the scope of publication will be kept within CS6460. 
 
Beyond the research scope, this project aspires to reach 100,000 unique learners in 2016. Upon 
reaching critical mass, we are also interested in seeing how Oppia’s wiki­inspired maintenance 
of ITS content allows for the expansion and support of this content via a community of 
volunteers. 

