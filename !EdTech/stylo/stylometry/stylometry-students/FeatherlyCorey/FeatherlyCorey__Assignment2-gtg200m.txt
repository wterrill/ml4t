 
 

Assignment 2: Exploring Your Area  

 

Corey Featherly 
gtg200m 

1  Planned Area of Focus: Project Based Learning 

Based on exploration of the course library, as well as research into educational technology in general, 
tools and technologies related to project based learning has been selected as the planned area of focus 
for this project. This decision was made after considering the documented educational technology areas 
listed in the course library at a high level, and then looking more closely through select areas of interest 
including project based learning, game-based learning, and intelligent tutors. The principal driver for this 
decision was personal interest and the hope that this project will result in the creation of a useful tool. 

2  Current Problems in the Project Based Learning Area 

Project based learning is not a new concept. Projects may be considered the original learning 
methodology in the sense that long before there were classrooms, new hunters were educated in the 
ways of hunting by participating in a hunt (i.e., a hunting project). In light of this, it is intuitive to 
consider project learning to be a more natural approach for humans as opposed to the more contrived 
traditional classroom learning. In terms of documented considerations on project based learning versus 
traditional classroom learning the origins are considered to go as far back as John Dewey per his writing 
of “My Pedagogic Creed” in 1897 where he states: 

The teacher is not in the school to impose certain ideas or to form certain 
habits in the child, but is there as a member of the community to select 
the influences which shall affect the child and to assist him in properly 
responding to these.......I believe, therefore, in the so-called expressive 
or constructive activities as the centre of correlation. 

(Dewey, 1897) 

Since 1897 significant research has been done to measure the effectiveness of project based learning. 
That body of research generally indicates that project based learning is effective. For example on article 
stated “Compared to students taught traditionally, students taught in a manner that incorporates small-
group learning achieve higher grades, learn at a deeper level, retain information longer, are less likely to 
drop out of school, acquire greater communication and teamwork skills, and gain a better 
understanding of the environment in which they will be working as professionals” (Oakley, Felder, Brent, 
& Elhajj, 2004). 

As another example Hernández-Ramos and De La Paz performed research in which “Results from 
content knowledge measures showed significant gains for students in the project-based learning 
condition as compared to students in the comparison school” (Hernández-Ramos & Paz, 2009). Based on 
research such as this evaluating the effectiveness of project based learning is not considered a current 
problem, as it is generally considered an effective educational methodology. 

In their article Hernández-Ramos and De La Paz indicated that their results point to several areas in need 
of further research including “…integrat(ing) technology in ways that promote disciplinary thinking” 
(Hernández-Ramos & Paz, 2009) and “…devis(ing) learning projects that are more meaningful for 
students” (Hernández-Ramos & Paz, 2009). 

Spring 2016 

CS 6460 Educational Technology 

Page 1 of 3 

 
 

Assignment 2: Exploring Your Area  

 

Corey Featherly 
gtg200m 

Let’s consider for a moment the real world results of skills developed during education that includes 
project based learning by examining project management as a component of business success. Project 
management is generally considered a critical component for business success. What causes business 
projects to fail and why? Can the root cause be addressed by more effective project based learning 
during the development of individuals that will go on to be involved in business projects? 

Suvi Elonen and Karlos A. Artto performed a study aimed at identifying the problem areas in managing 
multiple internal development projects. Their results indicated six problem areas: 

(1) Inadequate project level activities 

(2) Lacking resources, competencies and methods 

(3) Lacking commitment, unclear roles and responsibilities 

(4) Inadequate portfolio level activities 

(5) Inadequate information management 

(6) Inadequate management of project-oriented organisation. 

(Elonen & Artto, 2003) 

The importance of facilitating and guiding a project are underscored by the study performed by 
Brigid Barron, which resulted in data that “ …provide evidence that shows that learning 
outcomes as well as concurrent joint problem-solving outcomes are influenced by qualities of 
interaction” (Barron, 2003). This suggests that there is an opportunity to improve joint problem-
solving outcomes by providing a tool that improves the quality of interaction. 

Based on this research, implementing technology to provide tools for project based learning 
that guide students to be successful project participants would not only benefit project based 
learning in educational environments, but would also benefit businesses in two ways: 

1)  By creating a larger pool of human resources that are experienced with being successful project 

contributors 

2)  Making that same tool available to businesses for project management, which has the added 

benefit of being familiar to individuals that used it during their education. 

Such a tool is considered a current problem in the project based learning area. This tool could in theory 
reduce business costs related to project management and improve the success rate of business projects. 
This would be accomplished by making the tool available for project based learning during education, as 
well as having a tool that is effective in this space which is a significant challenge in and of itself. 

3  Existing Tools and Major Players Analysis 

There are a number of existing tools in this space, two popular business tools include Asana and Trello. 
One education focused PBL tool is PBLU.org. 

The Asana website states that “Asana's mission is to help humanity thrive by enabling all teams to work 
together effortlessly” (Asana, n.d.). Asana started as an internal tool at the company Facebook, and is 
currently a web application focused on improving team collaboration within companies. 

Spring 2016 

CS 6460 Educational Technology 

Page 2 of 3 

 
 

Assignment 2: Exploring Your Area  

 

Corey Featherly 
gtg200m 

Trello explains itself as “Trello is the easy, free, flexible, and visual way to manage your projects and 
organize anything” (Trello, n.d.). Trello is designed after traditional collaboration tools such as a white 
board. Like Asana, it is also a web application that helps project teams stay organized by using virtual 
“cards” that can be organized on virtual “boards”. 

The PBLU.org website offers tools to educators aimed at simplifying the implementation of project 
based learning in classrooms ((BIE), n.d.). 

These tools are freely available, although Asana has a price wall after a certain number of users are 
collaborating on the same project. Further analysis of these and other similar tools is planned. 

4  Works Cited 

(BIE), B. I. (n.d.). PBLU.org. Retrieved from PBLU.org: http://pblu.org/ 

Asana. (n.d.). Asana. Retrieved from Asana: https://asana.com/company 

Barron, B. (2003). When Smart Groups Fail. THE JOURNAL OF THE LEARNING SCIENCES, 307–359. 

Dewey, J. (1897). My Pedagogic Creed. 

Elonen, S., & Artto, K. A. (2003). Problems in Managing Internal Development Projects in Multi-Project 

Environments. International Journal of Project Management, 395–402. 

Hernández-Ramos, P., & Paz, S. D. (2009). Learning History in Middle School by. Journal of Research on 

Technology in Education, 151-173. 

Oakley, B., Felder, R. M., Brent, R., & Elhajj, I. (2004). Turning Student Groups into Effective Teams. 

Journal of Student Centered Learning. 

Trello. (n.d.). Trello. Retrieved from Trello: https://trello.com/about 

 

 

Spring 2016 

CS 6460 Educational Technology 

Page 3 of 3 

