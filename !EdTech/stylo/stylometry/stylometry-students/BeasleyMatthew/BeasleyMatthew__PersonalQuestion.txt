Personal Question

Matthew Beasley
mbeasley9@gatech.edu

The Question
"When the 'teacher-student relationship is often viewed as power-wielder vs powerless' and 'the 
hip-hop belief system that students ascribe to includes a positioning of oneself as superior to 
situations at hand in order to mask one’s vulnerability and afﬁrm one’s power in situations where 
one is challenged,' how does this power struggle get resolved in a manner that's mutually 
beneﬁcial to both parties? Find some real-world strategies that have been effective in the 
classroom and describe what techniques were used to balance the power struggle.”
Remember, the goal of the personal question is to prove you're ready to contribute to this 
community. So, you'll want your answer to be a bit longer and deeper, and especially well-
referenced.

Background and Introduction
The personal question I was given was derived from Assignment 3 in which I highlighted a few 
of the issues inhibiting educational progress, particularly in technical ﬁelds, in inner-city public 
schools. Most students in urban public schools in the United States are from minority cultures 
and are economically poor. Most educators on the other hand, are upper-middle class 
economically and from the majority (white) culture. Furthermore, the most common educational 
structures and pedagogical methods in urban schools all cater to the dominant culture often to 
the detriment of the students in those schools. As I wrote in Assignment 3, most educational 
issues are rooted in signiﬁcant cultural disconnects between educators and students, and these 
disconnects are often displayed in the form of student/teacher power struggles within the 
classroom. 
In researching the answer to my personal question, a few dominant themes surfaced with 
regard to resolving these power struggles for the beneﬁt of all: 
• Educators from the majority culture should recognize the power they inherently have by 
simply being from the majority 
• They should also be careful to guard against any inherited, generally false, assumptions 
about the communities they are serving 
• From this place of cultural cognizance, educators should incorporate relevant cultural and 
community connections familiar to their students within their classroom structures, teaching 
styles, and assignments. 

Below I’ll highlight a few examples of educators who have exercised these strategies and the 
beneﬁts they and their students have seen.

Understanding False Assumptions and Investing in Local Communities
In her book Confronting Racism, Poverty, and Power, Catherine Compton-Lilly gives several 
accounts of the poorly formed, inaccurate, assumptions educators from suburban communities 
make about the urban communities where they teach. Most of these assumptions are along the 
lines of viewing a student’s struggles in the classroom as being the fault of their upbringing and 

life outside of school. Unfortunately, these assumptions commonly lead teachers to “give up” on 
their students, labeling them and their families as hopelessly incompetent and highly unlikely 
(aka “at risk”) to break out of the cycles of poverty synonymous with their community. Compton-
Lilly, a white teacher coming from a suburban middle-class school, admits to bringing in many of 
these same assumptions in her ﬁrst year teaching in an urban school system. However, she 
was confronted with the inaccuracies of those assumptions as she got involved in the lives of 
her students and their families as part of her doctoral research. She discovered that the parents 
of her students who were categorically marginalized in teacher’s lounge discussions, actually 
took great interest in their children’s education, were available to provide ample support for 
homework, and were excellently postured to help their children grow in literacy training. 
Engagement with her students families also opened her eyes to the harsh realities of the 
neighborhoods her students are from, the hardships their parents face being economically poor, 
and the differences in language and communication between herself and the urban community 
she was teaching in. 
These newfound perspectives inspired Compton-Lilly to “build on what [her] children bring” 
when forming assignments and presenting materials.  In her book she details several “critical 
literacy” projects which are composed of four components: 

1. They address a student-selected issue
2. They are collectively relevant to students
3. They require students to use their developing literacy skills
4. They are designed to promote change in the students community

I’ll brieﬂy highlight one successful project which involved using the jump-rope rhymes her ﬁrst 
grade students knew as the foundation for teaching early reading skills. In contrast to her 
colleagues disparaging comments about how the students were “language deprived” or 
“language deﬁcient”, Compton-Lilly found the students actually have “rich linguistic resources… 
though they may not [be] the traditional nursery rhymes the teachers expect and value.” Her 
students were extremely enthusiastic that rhymes they knew were incorporated into the 
educational training. In their enthusiasm and ability to relate to the content, they quickly picked 
up word-letter associations. Additionally, these rhymes provided the basis for developing 
concepts about print, practicing early reading strategies, and building a strong classroom 
community (Compton-Lilly, 2004).  
Compton-Lilly’s perspectives and culturally relevant critical literacy projects are very much in 
line with much of the academic literature on resolving power struggles in urban educational 
settings. Christopher Emdin discusses at length the need for employing “reality pedagogy,” that 
is, relating educational concepts to students with people and things from their realities outside of 
school. He writes “The amalgamation of these concepts (reality and pedagogy) is the ﬁrst step 
towards creating an approach to the teaching of science that beneﬁts  populations who are 
immersed in a reality different from the culture of the classroom.” In Emdin’s experience the 
application of reality pedagogy has signiﬁcantly increased his ability to relate to his students as 
well as drastically improve his students’ engagement and desire to learn science. (Emdin, 2010; 
p. 101 - 104) 
Additional support for these practices is echoed by Jim Cummins in his essay, Empowering 
Minority Students: A Framework for Intervention. In it he discusses the need to include minority 
group cultural features in instructional patters, and he cites the “dramatic improvements” seen at 

the Kamehameha Early Education Program in Hawaii. “When reading instruction was changed 
to permit students to collaborate in discussing and interpreting texts dramatic improvements 
were found in both reading and verbal intellectual abilities.”  (Cummins, 1986)
Sensitivity to Social Power Structures
The above references primarily support the need for incorporating culturally relevant context, 
communication, and relationship practices in the classroom and the beneﬁts gained by both 
students and educators when those practices are exercised. This need has been widely 
identiﬁed and well documented for decades (Paris & Alim, 2014), and implicit in those examples, 
is an understanding from the educator that they cannot wield their culture’s dominance as a 
weapon in the classroom if they hope to have success educating their students. However, if 
majority-culture educators do not also fully understand the larger social power-dynamics at play 
when teaching minority students, there is the potential for destructive employment of well-
intentioned, culturally considerate, pedagogy. 
Lisa Delpit warns teachers not to shy away from their instructional power under the supposition 
that displaying power disempowers their students. She gives examples of poorly executed 
methods where teachers don’t teach, but instead, they have students teach each other the 
entire time to maximize cultural consideration. Doing so robs the students of the opportunity to 
learn how to communicate in the dominant culture’s language, and further perpetuates the 
sense of betrayal among minorities towards white educators. Educators need to respect and 
uphold the students’ expert knowledge and teach as an authoritative voice from the dominant 
culture. A great example Delpit provides in support of this argument is of a white teacher at a 
majority-black Alabama school having students analyze rap songs as basis for understanding 
Shakespearean structure (Delpit, 1988). 
This need for individualized instruction while clearly deﬁning lines of authority is further 
supported in Dave Brown’s qualitative study of thirteen well-respected educators in urban 
schools from across the country. In his 2004 article in Urban Education, one overwhelming 
sentiment from the educators was the necessity of connecting with their students personally 
while continuing to teach with assertiveness and clearly stated expectations (Brown, 2004). 
Authoritative avoidance can lead to a plethora of issues in the classroom since authority is 
communicated and recognized in signiﬁcantly different ways between that of white middle-class 
cultures and many of the minority cultures found in United States urban centers (Delpit, 1988). 
In Brown’s study, the teachers he interviewed repeatedly emphasized improvement to their’s 
and their students’ experiences when those lines are clearly understood but not executed in 
such a way that the students voices are silenced. 

Conclusion
After the research and compilation of my thoughts in response to this question, I’m left with a 
lingering burning question of my own: If there is such a large body of evidence to support the 
need for incorporating cultural context in the presentation of educational materials and the 
structuring of classroom environments, why are Catherine Compton-Lilly and Christopher Emdin 
pleading for the same application of these methods in the mid-2000s that Jim Cummins and 
Lisa Delpit were pleading for in the mid-1980s? Cummins’ take in 1986 was that the dominant 
culture explicitly and implicitly ignores and dismisses evidence for these practices because it 
threatens the power they hold. I desperately hope that is not the case, but my own experience 
and intuition tell me he is probably on to something. Regardless, I think there are clearly 

identiﬁed needs for promoting these cultural considerations in educational practices at the very 
least for the sake of the learners. My hope is that a tool like I proposed in Assignment 4 will 
encourage and facilitate educators to more deeply engage in the communities in which they 
teach, making material more applicable for their students and inherently resolving destructive 
struggles for power in the classroom. 

Post-Script: An Institutionalized Approach - TechBoston Academy
TechBoston Academy in the Boston inner-city community of Dorchester is a ‘pilot’ public school 
that has been given the ﬂexibility to institute and ﬁnance their educational structure in non-
typical ways. Students are selected to attend TechBoston academy via a lottery system, and 
demographically, the student body is comprised of 90% minorities, and 70% of the students 
qualify for free and reduced-price lunch (a common poverty metric). A 2008 case study by 
Education Resource Strategies reveals that TechBoston Academy has dedicated its ﬁnancial 
resources and structured its entire educational program for its high school students (grades 
9-12) on core principles of personalizing education for students. Speciﬁcally, they ensure small 
class sizes and low student-to-teacher ratios. They build in a Project Room as part of the school 
day for students to get extra time to work with educators and other students in a free-range 
setting to work on assignments. They also build in a signiﬁcant amount of time for other student 
“enrichment” activities such as tutoring, learning center, state-testing support, and seminar 
classes. These are all in addition to the students’ normal core classes. The statistical data given 
in the ERS report shows that TechBoston’s students compared favorably to the rest of Boston 
Public schools on state-mandated testing, out performing in a number of subjects. Notably, their 
school is 22% higher than average for Boston Public Schools for college-going rate (77% vs. 
55%). While the report doesn’t give speciﬁc insights to interpersonal student/teacher classroom 
relationships (like those cited above), I thought it was an interesting example of a system-wide, 
institutional, and cultural commitment to personalizing education and empowering students with 
ownership of their educational experience.

References

Brown, D. F. (2004). Urban teachers’ professed classroom management strategies: 
Reﬂections of culturally responsive teaching. Urban Education, 39, 266-289.
Compton-Lilly, C. (2004). Confronting racism, poverty, and power: Classroom strategies to 
change the world. New Hampshire: Heinemann.
Cummins, J. (1986). Empowering minority students: A framework for intervention. In In Weis, L., 
& Fine, M. (1993). Beyond Silenced Voices : Class, Race, and Gender in United States 
Schools. Albany: State University of New York Press
Delpit, L. (1988). The Silenced Dialogue: Power and Pedagogy in Educating Other People’s 
Children. In Weis, L., & Fine, M. (1993). Beyond Silenced Voices : Class, Race, and 
Gender in United States Schools. Albany: State University of New York Press
Emdin, C. (2010). Urban science education for the hip-hop generation: Essential tools for the 
urban science educator and researcher. New York: Sense.
Paris, D., & Alim, S. (2014). What are we seeking to sustain through culturally sustaining
pedagogy? A loving critique forward. Harvard Educational Review, 84, 85–100.
Sheilds, R. A., & Miles, K. H. (2008). Relevance strategic designs: TechBoston academy. 
https://www.erstrategies.org/cms/ﬁles/872-techboston-academy.pdf
VanSciver, J. H. (1989). Not a gray issue. Educational Leadership, 47, 80-81

