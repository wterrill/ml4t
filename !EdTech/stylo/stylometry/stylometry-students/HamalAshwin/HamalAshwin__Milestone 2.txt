Project Wittgenstein 
CS 6460 ­ Educational Technology 
Intermediate Milestone 2 
Ashwin Hamal 
 
For this milestone, following tasks were completed 
 

●  Setup mongodb. 
●  Implement database schemas using mongoose. 
●  Write boilerplate and tests APIs on socket.io server. 
●  Write boilerplate code for the front­end app. 
●  Write APIs on the server side for login/logout and updating user’s profile. 
●  Add web views on the client side for Login/Logout. 

 
The task of updating user’s profile, is still in progress. 
 
Source code could be found at: 
https://github.com/ahamal/witt 
 
A live public development website is at. 
http://witt.ashwinhamal.com/ 

Issue tracker is at: 
https://github.com/ahamal/witt/issues/1 
 
 
Note: The API is written in a socket pub­sub way. As opposed to AJAX/REST way. So it’s a whole 
different system. 
 
Snapshots: 
 
 

 

 
 

 

 

