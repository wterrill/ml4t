Assignment 1 - Personal Statement

Ashwin Hamal

My name is Ashwin. I am originally from Nepal, residing in New York. I came the the US 9 years ago seeking
undergraduate education. And I have been working at startups ever since I graduated building varieties of applications.
I also have been teaching three 12-14 year olds computer programming, CSS, JavaScript and HTML once a week for
about a year and a half.

I am also an avid student of Philosophy and Cognitive Science, and more specifically into Zen, which in Sanskrit means
'to focus'. Most of what we do has to do with what we learn, especially pleasures and pain. So I spend a lot of time
thinking what 'learning' really is, and how we 'learn'.

I'd like to approach this course from a Philosophical point of view. For example, in my opinion, education can be broken
down into 4 faculties - Skill, Knowledge, Experience and Enlightenment. Courses like this one is catered more towards
'enlightenment'. A language course would be more about the 'skill' at first and 'experience' later. Whereas a history
course is about 'knowledge' and 'enlightenment'. I'd like to learn more about these faculties of education, and would like
to dive in deeper on how a human mind treats them.

To elaborate more, gaining a skill like a language, is mostly about practice over and over again, until the point where it's
automatic, there's a strong connection between the neurons in the brain and we don't have to think about it anymore
and it comes natural to us. But I believe there's a lot more involved with what makes someone an effective
communicator, and it has to do with the person understanding the culture and context well, and having a strong shared
notions of words and their meaning with other people. Otherwise there's a disconnect. These are also the topics I spend a
lot of time thinking about.

My expertise is in building front-end web applications, so I would like use the skill in this course. I am also taking
Knowledge Based AI class at the moment, which I believe would be helpful.

After GATech I've been seriously considering to pursue Masters in Education, Cognitive Science/Learning. I hope to one
day go back and modernize the education system in Nepal, so the country can produce strong leaders, engineers,
thinkers and innovators, and people no longer have to suffer as much.

