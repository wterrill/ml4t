Project Wittgenstein 
CS 6460 ­ Educational Technology 
Intermediate Milestone 1 
Ashwin Hamal 
 
 
For this milestone, 
 
A github repository where we store all the code, a simple hello world server was added.  
https://github.com/ahamal/witt 
 
A public development environment was created the code is running. 
http://witt.ashwinhamal.com/ 

A tracker that keeps track of what has been finished and what remains has been created 
https://github.com/ahamal/witt/issues/1 
 
The wireframe and database schema for the project was completed. You could find them below, or on 
the public project folder:  
https://drive.google.com/folderview?id=0B2A_EyY9pdvjdjV1ZnYyOV9Ybk0&usp=sharing 

 
 

 

 

 

