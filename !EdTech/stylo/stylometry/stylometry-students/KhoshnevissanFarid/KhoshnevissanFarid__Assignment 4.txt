 
 
 

 
 
 

Name: Farid Khoshnevissan 
Class: CS 6460 
Date: 2/7/2016 

Question and Homework Based App to Increase Student Engagement in the Classroom 

From the research I conducted over the past 3 weeks and my own personal experiences, I 

The app would have multiple modules built in, designed around what the instructor would like 

 
 
decided that I’m going to work on a way to tackle the issue of getting teachers and professors to 
recognize the value of mobile devices in the classroom. I am going to focus on showing how mobile 
devices can be utilized for a more engaging experience, causing students to be more interested in the 
material, regardless of subject matter or content. Specifically, I will design and create an app that will act 
as a question/quiz/homework app to be used both in the classroom and at home. The app will act as an 
additional way for instructors to reach students, and vice versa. The app will contribute to the 
community by reducing the negative image associated with mobile devices in the classroom, by trying to 
outweigh the bad with the good. 
 
to teach or discuss. One module would involve a quiz platform, not intended for actual quizzes that 
impact grades but more for quick questions to test student knowledge in the middle of class regarding 
the current topic The instructor could see the data coming in and how students are responding. Another 
module would be a forum, similar to Piazza where students can ask questions and get answers from 
fellow students and the instructor, with the main benefit of not having to need everyone’s contact info 
or ask them in a private message on Facebook or other platform. A similar module would be a form of 
direct communication from students to the instructor and vice versa, useful when one is sick or the 
instructor found something interesting or felt like needed to share something before the next class 
period. A homework module would also exist, allowing instructors to give quick homework assignments; 
the module would be similar to the quiz module but meant for outside of class. Another module would 
be one that tracks to see if the user has the app open and active, confirming that a student isn’t doing 
other activities and instead is focused on the current topic. Such a design would allow for modules to be 
added to fit instructor’s needs. 
 
i>clickers1, a product I discussed in assignment 3. The main limitation in i>clickers from personal 
experience was they were costly and the interface wasn’t well received.4 The company required a 
subscription from students (in addition to the cost of the physical device if they bought one), or required 
the school/university to make a large purchase or enterprise subscription. Another app is Class Dojo, 
which targets instructors specifically for classroom student management, storing student information 
and ability to award points to students. Another app is ‘The Answer Pad’, similar to the i>clicker 
equipment but designed for apps.5 I would draw inspiration from The Answer Pad for my own app, as 
my app would be designed to be all encompassing, to make it a tool seen almost a necessity like the 
calculator. 
 
that is a combination of various ideas, and I believe this combination of ideas is necessary to sway the 
image of mobile devices in the classroom. 
Citations 

There are some competitors in this area, with some existing products. One product in the area is 

Overall, there has been a lot of products designed to increase student engagement, but not one 

1.  "Clicker & Audience Response Systems - IClicker." https://www1.iclicker.com/  
2.  http://er.educause.edu/articles/2015/3/engaging-students-with-a-mobile-app  
3.  http://edtechteacher.org/apps/#Begin  
4.  https://play.google.com/store/apps/details?id=com.mnv.reef#details-reviews 
5.  http://theanswerpad.com/   

