Assignment 5

Personal Statement

The Question posed by my mentor was: How do you expect that using your system will contribute to 
the learning goals of the courses that use it?  

Executive Summary
The value proposition of my proposed system (adding peer review to NBGrader) is that it will increase 
the number of learning objectives that are assessed per assignment, improve the quality and timeliness 
of these assessments, and facilitate the scaling of class size. 

To address the question of “how will the proposed system contribute to the learning goals of 

Introduction 
 
courses that use it?”  it will first be helpful to examine the definition of, and thinking around, the topic 
of learning goals.  Then it will be helpful to highlight changes in today's learning  that might impact the
ability to achieve these learning goals and which may necessitate new learning tools and approaches.

Learning Objectives

“An essential first step for faculty in preparing any program is to identify explicit learning 

outcomes what students should know and be able to do at the end of each course or instructional unit 
and the program.”1 This was one of several summary statements from a 2003 report on a Workshop 
sponsored by the US National Research council.  The report continued with “Learning outcomes 
should not be limited to a list of content terms, but should comprise a mutually supportive framework 
of facts, central concepts, reasoning skills, and competencies in three areas of learning: content, 
scientific process, and application (learning how to learn).”

While this workshop was in 2003, a very similar conclusion had been reached in 1956 by 

Bloom et al2, who published the Taxonomy of educational objectives. These objectives for cognitive 
learning became Knowledge, Comprehension, Application,Analysis, Synthesis, and Evaluation.  “The 

1 McCray, Richard, Robert L DeHaan, Julie Anne Schuck, National Research Council (U.S.), Steering Committee on 
Criteria and Benchmarks for Increased Learning from Undergraduate STEM Instruction, National Research Council 
(U.S.), Committee on Undergraduate Science Education, et al. Improving Undergraduate Instruction in Science, 
Technology, Engineering, and Mathematics Report of a Workshop. Washington, DC: National Academies Press, 2003. 
http://site.ebrary.com/id/10046838.

2 Bloom, B.S. (Ed.), Engelhart, M.D., Furst, E.J., Hill, W.H., & Krathwohl, D.R. (1956). Taxonomy of educational 

objectives: The classification of educational goals. Handbook 1: Cognitive domain. New York

scheme has been translated into every language known to man, and laminated posters of Bloom's 
Taxonomy adorn schoolrooms from Oklahoma City to Osaka, Tallahassee to Tajikistan.”3  An Internet 
image search on “Bloom's Pryamid” will come back with a multitude of hits.   Figure 1 is just one 
example.  

Figure 1. Blooms Taxonomy as as wheel4

In 2003, Krathwohl revised Blooms taxonomy, essentially moving from one dimension to two 
dimensions.5, adding a cognitive process dimension to Bloom's Knowledge dimension, as shown in the 
Figure 2.  This approach separated out the “ knowledge nouns” (facts, concepts, procedures, meta-

3 Wineburg, S., & Schneider, J. (2010). Was bloom's taxonomy pointed in the wrong direction? Phi Delta Kappan, 91(4), 

56-61. Retrieved from http://search.proquest.com.prx.library.gatech.edu/docview/218522065?accountid=11107 

4 Author: Doug Belshaw, Author URL: https://www.flickr.com/people/dougbelshaw/

Title: Bloom's Taxonomy as a wheel,Year: 2009, Source: Flickr
Source URL: https://www.flickr.com; License: Creative Commons Attribution-ShareAlike License

5 Krathwohl, David R. “A Revision of Bloom’s Taxonomy: An Overview.” Theory Into Practice 41, no. 4 (September 

2002): 212.

cognitive” from the  “cognitive verbs' “remember, Understand, Apply, Analyze, Evaluate, Create.  It 
also permits the establishment of objectives as elements that fill in the matrix, as illustrated in Figure 2 
as well. Here,  the first objective might be to memorize some historical facts.  The next objective is to 
place those facts in context.  The third object is to create new thinking based on those facts and 
understanding.  The final objective might be to critically evaluate the work of other classmates.  

Figure 2.  From Krathwohl, 2002.  

While the Blooms taxonomy and derivations/revisions thereof has been the dominate construct in 
education circles, there are other alternatives.  For example 6

“Biggs and Collis (1982) describe the growth of competence in terms of, first, a quantitative accrual of the
components of a task, which then become qualitatively restructured. SOLO, which stands for the Structure
of the Observed Learning Outcome, provides a systematic way of describing how a learner's performance 
grows in complexity when mastering many academic tasks. Five levels may be distinguished: 

1. Pre-structural. The task is not attacked appropriately; the student hasn't understood the point.

2. Uni-structural. One or a few aspects of the task are picked up and used (understanding as nominal). 

3. Multi-structural. Several aspects of the task are learned but are treated separately (understanding as 
knowing about).

4. Relational. The components are integrated into a coherent whole, with each part contributing to the 
overall meaning (understanding as appreciating relationships).

5. Extended abstract. The integrated whole at the relational level is reconceptualised at a higher level of 
abstraction, which enables generalization to a new topic or area, or is turned reflexively on oneself 
(understanding as far transfer, and as involving metacognition). Levels of understanding such as these 
may be used for structuring curriculum objectives hierarchical”

6 Biggs, John. “Enhancing Teaching through Constructive Alignment.” Higher Education 32, no. 3 (1996): 347–64.

Based on my experiences as both a student and teacher, and for the purpose of this discussion, the 
taxonomy of Figure 2  provides a good and useful foundation for this work.

The Learning Environment of Today

Since the phrase was first used in 2008,  the topic of MOOC's (Massive Open Online Courses) 
have generated a substantial amount of discussion, both in the peer reviewed literature7 as well in the 
open press, e.g.8  While a comprehensive literature survey, is beyond scope, a few key points can be 
made.  In addition to the discussion, an entire international industry has been created, with both non-
profit (eg. Khan Academy, edX), commercial (Coursera, Udacity, AT&T) and academic institutions 
(GaTech, Stanford, MIT) providing or partnering to offer an substantial on-line offering.    The OMSCS
website9 captures the promise of its on-line program  succinctly as “Accredited, Affordable, and 
Accessible”.  As with any novel disruptive technology, there have been some legitimate criticisms, 
among them that there are very low completion rates1, there is a lack of geographic diversity, and they 
the could actually increase educational inequality by creating a tiered system of haves (rich students 
with teachers) and have-nots (poor students with video tapes)10  Yousef et al.1 have categorized the 
dimensions of MOOCS peer review literature nicely as: Concept; Design; Learning Theory; Case 
Studies; Business Models; Target Groups; and Assessment.  

I will suggest that today's learning environment in general, and massive online classes in 

particular, face significant challenges characterized by the triplet of Scale, Depth, and Speed that are 
driving a paradigm shift in the learning process and will necessitate innovation along the dimension of 
Assessment.

Scale- Not surprisingly, massive on-line classes face significant challenges in dealing with the 
rapidly expanding numbers of students per class.  Unfortunately, the tradition methods of teaching that 
rely on faculty and teaching assistants do not scale at the same pace that enrollments are increasing. 
New tools and methods will need to be developed to address the growing gap between online 
classroom size and faculty bandwidth.

Depth-The amount of knowledge and cognitive processing ability required to be successful at 
the college (both undergraduate and graduate) level is also increasing.  While difficult to quantify, a 
review the OMS syllabus reveals an impressive list of prerequisite knowledge (multiple languages, R, 

7 Yousef, Ahmed Mohamed Fahmy, Mohamed Amine Chatti, Ulrik Schroeder, and Marold Wosnitza andHarald Jakobs. 

“A Review of the State-of-the-Art,” CSEDU International Conference on Computer Supported Education 2014. 
Barcelona, Spain: 

8

9

“An Early Report Card on MOOCs - WSJ.” Accessed January 25, 2016. 
http://www.wsj.com/articles/SB10001424052702303759604579093400834738972.
“Why OMS CS? | OMSCS | Georgia Institute of Technology | Atlanta, GA.” Accessed January 25, 2016. 
http://www.omscs.gatech.edu/explore-oms-cs.

10 “Massive Open Online Course - Wikipedia, the Free Encyclopedia.” Accessed January 25, 2016. 

https://en.wikipedia.org/wiki/Massive_open_online_course#Notable_providers.

Python, Java), operating systems (unix, Windows, Android), tools (git hub, visualization) etc.  The 
likely hood that faculty and assistant staff will be subject matter experts in all the areas that students 
could use detailed feedback is decreasing, and represents a growing bandwidth gap.

Speed- As the pace of technological evolution increases, the pressure to deliver more precise 

learning outcomes in shorter amounts of time is also increasing.   Unfortunately, the ability to deliver at
a faster pace is opposed by the previous two trends.  Increasing student body size coupled with limited 
faculty bandwidth create significant bottlenecks, and impact the ability to deliver assessment feedback 
as quickly as is needed for the pace of the course. 

Bases on these challenges of Size, Depth and Speed, I am suggesting that there is a fundamental
paradigm shift occurring in the adult learning environment.   I believe this shift can be characterized by
by an analogy to the shift from linear Waterfall Development to Agile Development.  In linear Waterfall
development, elements are executed serially and progress made sequentially.  Similarly in the existing 
learning model (Blooms Pyramid) first some facts are learned, and then placed in context, and then 
analyzed etc, as diagrammed in Figure 2. In the new paradigm, I think all elements in the matrix in 
Figure 2 are addressed not linearly, but in very rapid cycles (potentially in parallel).  For example, a 
typically OMS class might require the knowledge of programing language, but in the midst of creating 
a new application to data analysis, facts like new api call, might need to be added while analyzing the 
outcome of multiple experimental runs.  Entire code bases might need to be re factored to improve 
performance etc.  

This Agile like learning process, where multiple elements from matrix in Figure 2 are addressed
simultaneously presents a significant assessment problem.  For the field of computer science eduction 
specifically, I believe peer review combined with auto grading will be significant advancement in 
addressing this assessment problem

Peer Assessment for Computer Science Education

There is quite a bit of academic literature on the topic of peer assessment11.  The reviewed 

literature seems to agree that, when correctly implemented, Peer Assessment is effective in increasing 

11 C¸evik, Y.D. “Assessor or Assessee? Investigating the Differential Effects of Online Peer Assessment Roles in the 

Development of Students’ Problem-Solving Skills.” Computers in Human Behavior 52, no. 52 (November 1, 2015): 
250–58.

Lewis, Stuart, and Phil Davies. “Automated Peer-Assisted Assessment of Programming Skills.” In Information Technology:

Research and Education, 2004. ITRE 2004. 2nd International Conference on, 84–86. IEEE, 2004. 
http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=1393651.

Sadler, Philip M., and Eddie Good. “The Impact of Self-and Peer-Grading on Student Learning.” Educational Assessment 

11, no. 1 (2006): 1–31.

2004. http://eprints.dcs.warwick.ac.uk/51/.

Sitthiworachart, Jirarat, and M. S. Joy. “Web-Based Peer Assessment System with an Anonymous Communication Tool,” 

Wang, Yanqing, Hang Li, Yuqiang Feng, Yu Jiang, and Ying Liu. “Assessment of Programming Language Learning Based 
on Peer Code Review Model: Implementation and Experience Report.” Computers & Education 59, no. 2 (September 
2012): 412–22. doi:10.1016/j.compedu.2012.01.007.

Wing-Shui, N. G. “The Impact of Peer Assessment and Feedback Strategy in Learning Computer Programming in Higher 

Education.” Issues in Informing Science & Information Technology, Volume 9 (2012) 9 (2012): 17.

positive educational outcomes, and that this applies to the specialty of Computer Science as well.  
(Criticism of Peer Assessment exists primarily in personal web posts12, and this still needs to reconciled
with the generally positive outcomes reported in the academic literature)

Figure 3, taken from Wang et al (2012)  typifies the activity flow for peer assessment process.  I 

believe this process, which focuses on code review, can be improved upon in several ways

1) It could be coupled with autograding  (this will cover basics knowledge facts)

2) It should allow for mixed narrative and programming language coding in the manuscript (extending 
beyond fact to evaluate conceptual knowledge)

3) The manuscript should be interactive and executable by reviewer  (meta-cognitive)

Figure 3.  Activity Diagram for EduPCR system (V2.) from Wang et al (2012)13

12 “Essays on the Flaws of Peer Grading in MOOCs.” Accessed January 24, 2016. 

https://www.insidehighered.com/views/2013/03/05/essays-flaws-peer-grading-moocs.

13 Wang, Yanqing, Hang Li, Yuqiang Feng, Yu Jiang, and Ying Liu. “Assessment of Programming Language Learning 

Based on Peer Code Review Model: Implementation and Experience Report.” Computers & Education 59, no. 2 
(September 2012): 412–22. doi:10.1016/j.compedu.2012.01.007.

Evaluation Criteria
Existing research into the effectiveness of peer review tends to focus on either an individual matrix 
element of Figure 2 (i.e. code review Wang 2012)  or on correlating peer assessment grades with 
teacher evaluated grades (Sadler and Good 2006)  I have not yet come across work that maps peer 
assessment to the effectiveness against multiple learning objectives simultaneously.   This may be due 
to novelty of viewing the matrix from a parallel (Agile) viewpoint.   This also suggest that matrix of 
Figure 2 should be considered as a starting point for the peer review guidance rubric.   While 
quantitative aspects of an peer assessment system, such as average time to complete, or student 
satisfaction, can be measured, new evaluation methods may need to be developed to validate the value 
proposition hypothesis.

