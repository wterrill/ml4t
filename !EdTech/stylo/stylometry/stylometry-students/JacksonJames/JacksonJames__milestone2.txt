bandwidth efficient 

online learning

Milestone #2, CS-6460, Spring 2016

James Jackson

Georgia Tech

next steps from milestone #1

● determine slide timing (presentation-based lectures)
● determine hand pointer position/timing (presentation-based lectures)
● look at options for talking heads ex. avatars

 

convert lectures: assumptions

● presentation content (slides) is available
● video content (slides w/o hand pointer) is available separately
● video content (floating hand pointer) is available separately

convert lectures: workflow

video → JPG (ffmpeg, one image/s)

1. PPT → JPG
2. PPT → PDF → SVG (pdf2svg) 
3.
4. AAC audio → Opus (ffmpeg + opusenc)
5. AAC audio → mp3 (ffmpeg) as Safari does not support Opus
6.
7.
8. display SVGs from #2 according to timing from #6, embed audio
9. overlay animated hand according to timing/position from #7

find timing of slides (for each JPG in #1, compare image to set in #3)
find timing/position of hand pointer (identify object within JPG set in #3)

STEPS 1-8 successfully prototyped !

step 6: find timing of slides

● images are nearly identical, need an efficient similarity check
● enter pHash (perceptual hash), Radial method: http://www.phash.org/ 
● nice Python bindings: https://github.com/polachok/py-phash 
● compute pHash of the images from the video (one-time operation)
● compute pHash of the images from the presentation (one-time operation)
● compute cross correlation of presentation and video pHash (fast !!!)

step 6: find timing of slides (pHash example)

Reference image
from PowerPoint

Video Img #52

Similarity: 0.908

Video Img #27

Similarity: 0.995

Match !!!

Video Img #148

Similarity: 0.737

step 7: find timing/position of hand pointer

● pointer can occupy a significant area, 

identify key area pointed to

● template matching in OpenCV with 

“fingertip” template
○

http://opencv-python-tutroals.readthedocs.
org/en/latest/py_tutorials/py_imgproc/py_te
mplate_matching/py_template_matching.
html 

○ working methods: TM_CCOEFF_NORMED, 

TM_CCORR_NORMED, TM_SQDIFF, 
TM_SQDIFF_NORMED
failing methods: TM_CCOEFF, TM_CCORR

○

Template (fingertip)

step 8: slide animation

● avoid authoring tools, focus on programmatic environments
● enter GSAP (GreenSock Animation Platform)

○

http://greensock.com/gsap 

● result: http://jamesjackson.github.io/autoconvert/ 

○

floating hand not implemented

step 8: slide animation (download profile)

● original video size: 31.5MB
● animated vector graphics: 628KB
○ workflow supports auto-conversion !
○
○

hand animation not included
resolution agnostic - vector graphics always 
look great !

next steps...

● look at options for talking heads ex. avatars
● investigate video compression alternatives
● investigate audio codecs, audio/video sync, streaming vs. download
● start testing offline caching mechanisms

 

