bandwidth efficient 

online learning

Milestone #3, CS-6460, Spring 2016

James Jackson

Georgia Tech

next steps from milestone #2

● look at options for talking heads ex. avatars
● investigate video compression alternatives
● investigate audio codecs, audio/video sync, streaming vs. download
● start testing offline caching mechanisms

 

but first… floating hand implementation

●

●

●

use floating hand location 
(previously determined via OpenCV) 
to drive GSAP animation

○

http://greensock.com/gsap

updated demo: http://jamesjackson.
github.io/autoconvert/ 
proof-of-concept implemented over 
short period starting @ 32s

talking heads - avatars

● avatar wishlist

JavaScript animated SVG avatar library
avatar appearance generated from real photos
lip-sync with HTML5 audio 

● avatar reality

SitePal is an example that addresses some aspects, http://www.sitepal.com/api/ 
implementations are relatively immature (area for future research)
inaccuracies in lip-sync create more of a distraction
expressions/movements are difficult to emulate

● recommend using static images for talking heads

○
○
○

○
○
○
○

 

video compression - a step back

Description/URL

Codecs

Size

Scale Invariant

original high quality video, https://www.
dropbox.com/s/ed7i8qx9x7gg2hz/original.
mp4?dl=0 

ffmpeg transcode, lower quality, https:
//www.dropbox.
com/s/xnxacawr02nmksg/new2.mp4?dl=0 

ffmpeg transcode, lower quality, https:
//www.dropbox.
com/s/tlo90ozs0r16agh/new.webm?dl=0 

animated vector graphics, http:
//jamesjackson.github.io/autoconvert/ 

MPEG-4/AAC

31.5MB

No

MPEG-4/AAC

3.8MB

No

VP9/Opus @ 16kbps

2.3MB

No

Opus @ 16kbps

628KB

Yes

video compression - review

● aggressive compression provides significant bandwidth savings, at the 

expense of quality

● animated vector graphics provide the lowest bandwidth, along with excellent 

quality (also scale invariant)

 

audio codecs - HTML5

Source: https://en.wikipedia.org/wiki/HTML5_Audio  

Format

Chrome

Internet 
Explorer

Edge

Firefox

Opera

Safari

MP3

MP4 with AAC audio

Ogg with Vorbis 

audio

Yes

Yes

Yes, in IE9

Yes

From OS[a]

From OS[b]

Yes, in v3.1

Yes, in IE9

Yes

From OS[a]

From OS[b]

Yes

Yes, in v9

No

No

Yes, in v3.5

Yes, in v10.

50

With Xiph QuickTime Components

Ogg with Opus audio

Yes, in v25

(in v31 for 
Windows)

No

No

Yes, in v15.0

Yes, in v14

No

audio codecs - quality vs. bitrate

● Opus is the only HTML5 audio codec that 

performs well at low bitrates
choose Opus if supported, otherwise AAC 
or MP3 (prototype uses MP3)

●

 

Source: http://opus-codec.org/comparison/ 

audio codecs - testing

Description/URL

original, https://www.dropbox.
com/s/63bcleers33eetl/audio123.m4a?
dl=0 

ffmpeg transcode, https://www.dropbox.
com/s/60krwuliqlbiow9/audio123b.m4a?
dl=0 

ffmpeg transcode, https://www.dropbox.
com/s/yuwu20xn6wbqjtp/audio123.mp3?
dl=0 

opusenc transcode, https://www.dropbox.
com/s/rdfcuk23uot6ifi/audio123.ogg?dl=0 

Codec

AAC, 96kbps

Size

2.8MB

AAC, 57kbps (VBR)

1.6MB

MP3, 64kbps (VBR)

1.2MB

Opus (within Ogg), 16kbps

465KB

Audio/video sync, audio streaming/download

● prototype uses HTML5 audio preload=auto directive to trigger immediate file 

download

● browsers start rendering audio before it is downloaded

○ HTML5 audio progress event can provide additional flexibility
○
○

separate routine can be used to download the entire file before playing
animation can be paused in the event of an audio buffer underrun while streaming

● HTML5 audio currentTime directive allows seeking to a specific location, but 

the server must support HTTP byte-range requests

● prototype uses GSAP TimelineMax library for animation timing control

○

https://greensock.com/docs/#/HTML5/GSAP/TimelineMax/  

 

offline access

● HTML5 Application Cache allows controlled offline caching of files

○
○
○

https://developer.mozilla.org/en-US/docs/Web/HTML/Using_the_application_cache 
suitable for lecture content and logic (HTML, JavaScript, audio etc.)
not suitable for user data ex. lecture progress, answers to quizzes
■ Web Storage/Local Storage for user data
■

http://diveintohtml5.info/storage.html 

● new prototype on Heroku using HTML5 Application Cache

○ moved from GitHub Pages to Heroku to leverage PHP for returning different HTML pages 

depending on the browser

 

offline access - appcache manifest

●
appcache manifest controls offline caching
● HTML page references appcache manifest

● ← this manifest includes Opus audio, as the 

request came from Chrome

●

○ <html manifest="offline_ogg.appcache">

PHP trick to return unique HTML file (unique 
manifest, unique audio codec) depending on 
browser

offline access - Chrome example

1.
2.
3.
4.

●

visit http://offlineapp.herokuapp.com/ 
disable Internet connection
visit http://offlineapp.herokuapp.com/ 
still working !

view browser appcache:

○

chrome://appcache-internals/ 

next steps...

● evaluate offline access limitations
● evaluate browser compatibility for overall solution
● research linkage of smart content with interactive learning, gamification etc.
● research linkage of smart content with localization and accessibility

 

