Assignment 1:  Introducing Yourself 

 

My name is Victor Hartgraves, and I live in Fort Worth, Texas.  I am married and have 

two sons, ages ten and 15.  I currently work in Dallas, Texas for the Federal Reserve as a 

collaboration systems engineer.  I earned my BSCS from Texas Tech University in 1993 and 

have worked since then mainly for telecommunications and financial services companies in the 

areas of software development and systems engineering / management.  I have been interested in 

attending graduate school for some time as another way to freshen my CS knowledge and skills.  

More recently, I decided to undertake the OMS program and specifically this class in an effort to 

contribute back to CS and the field of education technology. 

My experience with education technology has predominantly been as a recipient in online 

classes such as Coursera, online training courses such as Codecademy, and employer education 

programs such as online information security and ethics training at the Federal Reserve.  As a 

father of a special needs child with DiGeorge Syndrome and learning disabilities, my focus on 

education technology has become much more personal based on his struggles with elementary 

education to date.  My son enjoys using technology and despite not being able to read, has taught 

himself how to use some technology devices and applications such as games through repeated 

trial and error.  This gives me hope that given the right motivation, his interest in technology 

could also assist in his education. 

This education technology class will provide the catalyst for me to make a meaningful 

contribution on behalf of my son and other special needs students with learning disabilities in 

grades K-12.  I have considered three different education problem areas in this segment that 

might benefit from an applied technology solution. 

Initially, I researched some of my son’s learning disabilities as a starting point.  

Mathematical learning disabilities are common in children with DiGeorge Syndrome, possibly 

due to their poor visuospatial attention.  Designing and building a unique learning app for math 

that would help address this accessibility shortcoming area such as using integrated visual and 

audio cues is my first idea for a class project.  Since this initial idea, I have witnessed that my 

son's motivation to learn in the classroom is very much driven by rewards and incentives such as 

getting to choose a prize from the class treasure box, getting to chew gum in class, receiving iPad 

time for playing educational games, etc.  My second idea borrows from this more general area of 

learning through reinforcement. 

When needed, teachers adopt and use various rewards and incentives to influence 

students’ behaviors in the classroom and learning outcomes.  This system of rewards and 

incentives can be quite large for special education classes and changes for each student over 

time, sometimes daily or even hourly.  My second idea is to design/build an application that 

assists teachers with tracking and managing rewards for students.  Inclusion of integration points 

with devices and other learning technologies might increase interactivity and the level of 

engagement with the student, such as the concept of an interactive treasure box in the classroom.  

My third and final idea improves on this second idea. 

After recently speaking with my sister-in-law who is a local elementary school teacher, 

she mentioned that trying to implement different rewards in a large classroom is in itself a 

problem.  She indicated that frequently she cannot multitask in a timely enough manner and the 

implementation of rewards can quickly break down across multiple students.  Students have to 

wait or are sometimes missed and not given reward ‘points’ as a result.  These students will 

likely complain about the missing reward points and require additional behavior situations to be 

addressed.  My third idea is to design/build a solution to assist with this more basic problem of 

the teacher to student ratio and implementation of rewards.  The solution would be in the form of 

intelligent teaching assistant (ITA) technology that can respond to the teacher’s requests in the 

classroom via voice recognition.  The ITA could be further developed and enhanced for other 

teacher-specific tasking for the classroom and might share similarities with Apple’s Siri or 

Amazon’s Echo. 

I am now most interested and very excited in this last idea for an ITA, which could be 

very beneficial to assist with the problem of growing classroom sizes and large teacher to student 

ratios.  Including the second idea, albeit a more simplified rewards management system, as a 

prototype for ITA tasking would help with meeting my personal goal for contributing to special 

education.  Adopting turnkey open source technology for the ITA would assist with the heavy 

lifting of the solution and make the ITA a real possibility for me in this class. 

 

