CS6460: Educational Technology (Spring 2016) 

Milestone 3 

Petr Bela 

 

 

 

 

Context 
 
 
The purpose of the project, working title “MOOC Mate”, is to build a hub for managing online
learning, particularly MOOCs. It would enable self­learners to keep track of their progress
 
across MOOC providers, take notes and search in lectures. 
 
The project is hosted at ​https://moocmate.herokuapp.com​. 
 

 
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Progress 
 
In the past two weeks I have worked on the Chrome extension. The extension now enables the
 
 
user to log in with a MOOC Mate account, take notes and search for relevant videos. In the
background, it tracks which videos the user has visited, and how much overall time they have
 
spent. I am still trying to work out some issues, and planning to publish it in a few days. 
 

 
 
 

 
 

 
 

 
 

 
 

 
 

   

 
 

 
 

 
 

 
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Next Steps 
 
Features planned for the next milestone: 

● Wrap up the Chrome extension 
● Build course dashboard 

