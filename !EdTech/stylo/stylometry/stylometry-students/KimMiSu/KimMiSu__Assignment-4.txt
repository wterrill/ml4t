Assignment	  4:	  Proposing	  Your	  Work	  
	  
Background	  

	  Recommender	  systems	  are	  initially	  developed	  to	  support	  users	  in	  their	  decision-­‐making	  in	  daily	  
life	  situation	  in	  terms	  of	  assisting	  the	  users	  who	  do	  not	  have	  sufficient	  experience	  or	  knowledge.	  
When	  I	  go	  watch	  a	  movie	  or	  a	  drama	  on	  Amazon.com	  or	  Netflix,	  the	  recommender	  shows	  me	  some	  
contents	  that	  I	  might	  be	  interested	  based	  on	  what	  I	  have	  watched	  on.	  I	  would	  like	  to	  apply	  this	  
concept	  to	  the	  educational	  setting.	  	  
	  When	  I	  first	  started	  the	  OMSCS	  program,	  I	  had	  no	  idea	  what	  class	  to	  take	  for	  the	  first	  semester.	  I	  
believe	  that	  many	  students	  spend	  a	  lot	  of	  time	  to	  knock	  around	  the	  foundational	  courses	  to	  gain	  
the	  information	  about	  the	  courses.	  After	  the	  first	  semester,	  did	  the	  situation	  become	  better?	  No,	  at	  
least	  in	  my	  case.	  I	  posted	  some	  questions	  on	  google+	  community	  about	  the	  courses	  I	  have	  taken,	  
what	  course	  is	  recommended	  for	  the	  next	  step,	  and	  how	  many	  hours	  I	  am	  available	  to	  study	  per	  
week	  but	  I	  was	  not	  able	  to	  get	  enough	  information	  from	  the	  community.	  In	  order	  to	  achieve	  the	  
degree	  successfully	  on	  time,	  it	  would	  be	  great	  to	  have	  some	  kind	  of	  semantic	  recommender	  system	  
that	  pre-­‐select	  courses	  based	  on	  the	  background	  and	  the	  specialization	  of	  the	  individual.	   
	  I	  am	  still	  scaling	  the	  project	  scope	  to	  narrow	  down	  so	  that	  within	  the	  time	  frame	  the	  project	  can	  be	  
addressed.	  	  I	  have	  another	  teammate	  working	  on	  this	  project	  together,	  so	  throughout	  the	  
discussion	  and	  research	  over	  the	  following	  weeks,	  we	  will	  generate	  the	  official	  project	  plan	  by	  the	  
time	  it	  is	  due.	  
	  
	  
1. Define	  the	  user	  groups	  and	  contents	  	  
:	  Whether	  the	  user	  group	  is	  limited	  to	  the	  OMSCS	  program	  students	  
:	  The	  contents	  will	  be	  displayed	  is	  limited	  to	  the	  OMSCS	  courses	  or	  including	  some	  sub-­‐
contents	  such	  as	  the	  basic	  programming	  or	  calculus	  class	  on	  Udacity	  
	  2. Obtain	  data	  	  
:	  Ensure	  all	  the	  OMSCS	  courses	  list	  and	  its	  information	  are	  gained	  	  
	  3. Research	  the	  recommendation	  algorithms	  
:	  Review	  existing	  recommendation	  algorithms	  that	  filter	  the	  contents	  to	  show	  such	  as	  the	  
Restricted	  Boltzman	  machines	  (RBM)	  and	  Form	  of	  matrix	  factorization	  algorithms	  
:	  Develop	  a	  filter	  algorithm	  that	  fits	  in	  the	  given	  situation	  	  
	  4. Build	  web-­‐based	  application	  to	  deploy	  the	  algorithm	  and	  test	  
:	  Technical	  detail	  will	  be	  presented	  in	  the	  official	  project	  plan	  

Project	  Description	  

	  

	  
	  
	  

References	  
	  

[1] ANDREAS	  GEYER-­‐SCHULZ,	  MICHAEL	  HAHSLER	  and	  MAXIMILLIAN	  JAHN.	  2001.	  	  
Educational	  and	  Scientific	  Recommender	  Systems:	  Designing	  the	  Information	  Channels	  of	  
the	  Virtual	  University.	  Int.	  J.	  Engng	  Ed.	  Vol.	  17,	  No.	  2,	  pp.	  153-­‐163.	  
	  
[2] Olga	  C.	  Santos,	  Jesus	  G.	  Boticario.	  2011.	  Requirements	  for	  Semantic	  Educational	  
Recommender	  Systems	  in	  Formal	  E-­‐Learning	  Scenarios.	  Algorithms	  2011,	  4,	  131-­‐154.	  	  

  	  	  

