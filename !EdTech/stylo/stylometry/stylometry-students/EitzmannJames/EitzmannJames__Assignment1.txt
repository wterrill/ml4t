Assignment 1 
James Eitzmann 
 
My name is James Eitzmann. I have a Bachelor of Computer Engineering degree from Georgia 
Tech. I have been a software engineer/architect for the past 18 years. I have considerable 
experience building web applications and platforms using cloud technology. I am involved in the 
OMSCS program in order to obtain my Master of Computer Science degree, learn new things, 
and keep my skills up to date. I’m currently in my fourth semester and am pursuing the 
Interactive Intelligence specialization. 
 
What I’d like to get out of this class is knowledge of the landscape of educational technology 
and challenges. I’d like to be able to contribute in some meaningful way to help further 
education technology in order to facilitate learning. My primary experience with educational 
technology to this point has been with the OMSCS program and Udacity. My overall experience 
with the technology has been positive although there is definitely room for improvement. From 
my experience some of the areas that could be improved are Piazza, T­square, and Udacity. 
Piazza as a tool suffers from email notification problems and poor search. T­square could use 
some general user interface improvements to have a more modern look and feel and be much 
more user­friendly. Udacity could use some improvements on programming exercises where the 
feedback given is often non­existent or has a steep learning curve. For example, for python 
exercises, it’s often necessary to have debug output. However, the output from print statements 
aren’t presented during submission tests. You can print to standard error to get the output from 
these test but often any printing to standard error is treated as a failure that prevent submission. 
 
I am especially interested in Artificial Intelligence and would any projects that would use A.I. to 
enhance education through technology. I was particularly intrigued by the discussion in the 
article ​AI Applications in Education​ that discusses using A.I. to model student learning and 
comparing that process to the learning process of an expert. Finding the differences help to 
develop smart software tutors. 

