CS	  6460	  –	  Assignment	  4	  

	  Will	  Johnson	  
tjohnson306	  
2/7/16	  
	  	  I	  am	  going	  to	  throw	  a	  curve	  ball	  at	  this	  time,	  and	  my	  apologies	  for	  that.	  	  After	  
spending	  several	  weeks	  looking	  at	  my	  old	  line	  of	  inquiry,	  I	  have	  come	  to	  the	  
conclusions	  that	  it	  is	  not	  as	  useful	  a	  topic	  of	  research	  as	  I	  had	  initially	  considered,	  
plus	  the	  idea	  of	  doing	  a	  survey	  of	  TA	  satisfaction	  felt	  light	  on	  the	  “tech”	  side	  of	  
“EdTech.”	  
	  Meanwhile,	  there	  is	  a	  problem	  which	  has	  serious	  ramifications	  for	  TAs	  in	  the	  OMS	  
program	  which	  I	  think	  could	  be	  greatly	  aided	  through	  the	  use	  of	  a	  consolidated	  suite	  
of	  software	  tools,	  and	  so	  that’s	  where	  I	  propose	  to	  take	  my	  project.	  
	  Ask	  any	  TA	  (or	  at	  least,	  head	  TA)	  in	  the	  OMS	  program,	  and	  they	  will	  tell	  you	  that	  
dealing	  with	  some	  of	  the	  bureaucratic	  minutia	  of	  the	  job	  can	  take	  a	  huge	  amount	  of	  
time.	  	  For	  classes	  that	  create	  GitHub	  repositories	  for	  students	  use	  on	  projects,	  there	  
is	  no	  standardized	  set	  of	  tools	  for	  building	  those	  repositories,	  and	  the	  tools	  that	  do	  
exist	  are	  temperamental	  and	  require	  much	  handholding.	  	  Similarly,	  there	  does	  not	  
seem	  to	  be	  a	  standardized	  tool	  set	  for	  dealing	  with	  Piazza	  groups	  or	  for	  dealing	  with	  
bulk	  uploading	  of	  assignment	  grades	  to	  T-­‐Square.	  
	  So	  I	  propose	  to	  write	  such	  a	  standardized	  suite	  of	  tools.	  	  When	  I’m	  finished,	  this	  
suite	  will	  contains	  tools	  to:	  
	  
•  Create	  GitHub	  repositories	  for	  individual	  students,	  with	  re-­‐try	  logic	  such	  that	  
run	  #n	  outputs	  data	  that	  can	  be	  used	  in	  run	  #n+1.	  	  This	  is	  particularly	  useful	  
when	  a	  student	  has	  not	  yet	  activated	  his	  or	  her	  GitHub	  account.	  	  The	  existing	  
set	  of	  tools	  I	  have	  require	  you	  to	  manually	  figure	  out	  which	  students	  
accounts	  are	  active	  and	  only	  create	  an	  input	  file	  for	  those,	  and	  then	  keep	  
manually	  looking	  to	  see	  when	  other	  accounts	  are	  activated	  and	  create	  them.	  
•  Create	  GitHub	  repositories	  for	  assigned	  teams	  (whether	  class	  assigned	  or	  
student	  assigned).	  	  This	  is	  an	  even	  bigger	  area	  of	  problem	  for	  the	  existing	  
“tools”	  that	  I’m	  using,	  you	  have	  to	  manually	  trim	  the	  list	  of	  groups	  of	  any	  
student	  whose	  GitHub	  account	  is	  not	  yet	  activated,	  and	  then	  manually	  add	  
them	  to	  the	  groups	  as	  they	  come	  on	  line.	  	  This	  could	  much	  more	  easily	  be	  
handled	  by	  the	  program	  recognizing	  accounts	  which	  couldn’t	  be	  added	  and	  
writing	  out	  re-­‐try	  information	  that	  could	  automatically	  be	  re-­‐run.	  
•  The	  tools	  for	  creating	  Piazza	  groups	  for	  team	  communication	  currently	  
require	  iMacro	  for	  Firefox,	  and	  are	  prone	  to	  misfiring	  if	  the	  Firefox	  or	  the	  

web	  site	  are	  slow	  in	  responding.	  	  I	  propose	  to	  investigate	  whether	  there	  is	  a	  
Java	  API	  for	  Piazza	  that	  may	  make	  creation	  of	  these	  groups	  more	  easily	  done.	  
•  Piazza	  groups	  also	  have	  a	  problem	  in	  that	  students	  often	  sign	  up	  for	  Piazza	  
with	  an	  e-­‐mail	  account	  other	  than	  their	  standard	  one,	  and	  if	  I	  can	  figure	  out	  
how	  to	  recognize	  the	  active	  account	  that	  goes	  with	  a	  student,	  a	  lot	  of	  manual	  
clean-­‐up	  on	  groups	  (as	  students	  complain	  that	  they	  cannot	  see	  their	  team’s	  
group)	  would	  be	  a	  lot	  easier.	  
•  Finally,	  the	  tools	  that	  I	  have	  access	  to	  for	  bulk	  uploading	  of	  grades	  to	  T-­‐
Square	  require	  multiple	  steps,	  and	  I	  would	  like	  to	  investigate	  whether	  those	  
could	  be	  collapsed	  into	  a	  single	  operation	  instead	  of	  multiple	  individual	  and	  
time-­‐consuming	  steps.	  	  Note	  that	  because	  T-­‐Square’s	  replacement	  is	  actively	  
being	  sought,	  this	  aspect	  of	  the	  utility	  suite	  may	  turn	  out	  not	  to	  be	  of	  much	  
use,	  and	  so	  it	  may	  be	  omitted	  from	  the	  final	  suite.	  
	  I	  propose	  to	  approach	  the	  TAs	  (particularly	  head	  TAs)	  of	  all	  classes	  currently	  in	  the	  
OMS	  program	  and	  see	  if	  they	  have	  any	  suggestions	  for	  tools	  or	  features	  that	  would	  
be	  useful	  to	  add	  to	  this	  suite,	  and	  then	  deliver	  the	  suite	  of	  tools	  as	  my	  final	  project.	  

