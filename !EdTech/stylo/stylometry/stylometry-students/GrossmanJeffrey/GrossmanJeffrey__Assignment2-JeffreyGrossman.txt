Assignment 2: Exploring Your Area 

Educational Technology CS6460 

Jeffrey Grossman 

I spent the last week reading about some of the topics that I was interested in as well as looking 

 
through projects from last semester’s Educational Technology students. For my first assignment I 
proposed two ideas that I was interested in working on. The first Idea was to build some sort of OMSCS 
robo-advising tool that would allow students to plan out their degree and see if it met the requirements. 
I believe there is great need for this in order to minimize the repetitive questions that the G+, Reddit 
and advisors are faced with daily. When I looked through the previous class’s projects, I found several 
that tackled this problem and did a good job at it. I feel that this is a tool that does not have to be 
incredibly advanced, just provide a good user interface to allow students to answer their own questions.  

The other project that I was considering due to my previous OMSCS experience was a way to 

 
improve group formation for online classes. I noticed one project that worked on this in the past 
semester, however it dealt more with the interface and less with the actual algorithmic group 
formation. Of course, an interface is eventually necessary in group formation, however I believe the first 
step would be to find a smarter way to group up students with varying skill sets, similar interests, and 
ones that would be likely to work well together. I focused my reading last week on seeing what research 
had been done on group formation algorithms and found quite a bit of content.  

The most common theme that I found amongst the articles that I read about student grouping 

 
had to do with whether homogeneous groupings were better than heterogeneous groupings and which 
type of grouping encourages learning. In this case, homogeneous groupings would refer to groupings of 
students with similar skill levels whereas heterogeneous groupings would mean grouping stronger and 
weaker students together. There appear to be several different opinions on this manner. I also think this 
is a very important distinction when it comes to OMSCS projects. In my health informatics class, we had 
2 people in our group that knew only the very basics of programming and 3 that had more experience. 
The 2 that knew only the very basics had very minor roles in the project (documentation only) while the 
other 3 did the vast majority of the work. I highly doubt the 2 less experienced people gained much out 
of the project, which would make me think that a homogeneous group would be better because 
everyone could participate. However, a homogeneous group with a weak skill set building a web app for 
that class would have been quite a struggle. I fear that the debate between the two types of group 
might be custom to the type of project that is being worked on.  

The most relevant paper that I found had to do specifically with e-learning environments (“A 

 
Group Formation Tool for e-Learning Environments” by Christos E. Christodoulopoulos and Kyparisia A. 
Papanikolaou). This paper discusses a tool that they built to aid in group formation based on a few 
fundamental skills and applying a fuzzy clustering algorithm to group students optimally. Additionally 
they tried different approaches for homogeneous and heterogeneous groupings. After discussing the 
tool that they created, they mention two areas for continued development that I found particularly 
interesting. One of them is a way to weight attributes and the other is a way to combine heterogeneous 

and homogeneous group formation. For the former, I can think of many situations of how this would be 
extremely useful, such as weighting a varying skillset higher than a similar time-zone. I think it would be 
much more important to have a group that has balanced abilities on front end, back end, 
documentation, etc. as opposed to having a group that is all in one time-zone. For the other suggestion 
of combining homogeneous and heterogeneous, I believe this is relevant to OMSCS projects where you 
might want a heterogeneous mix of skills in terms of the class topic (ex. Health informatics) whereas you 
might want a homogeneous mix of skills in terms of the technologies that will be used (front end/back 
end development). In this scenario the weaker members of the group on the class topic could learn from 
the stronger members, but all would be able to contribute evenly to whatever project they undertake 
from a technical perspective. Maybe this would hurt more than it would help as a decent web 
application would not be able to be developed for weaker skill sets.  

I was unable to find any commercial tools for group formation (or non-commercial, publically 

 
available). Maybe this is because of the custom nature of group formation and how it needs to be 
applied different to each class project. In terms of my actual project, I’m still debating whether to 
undertake a research or tool based project. I have a decent amount of Windows application experience 
in .Net, but very little front end/back end experience. I do find the grouping problem very interesting (in 
particular weighting of factors based on student preference or instructor preference) and would like to 
continue down the path of pursuing a project related to group formation for this course. 

 

Sources: 

Christodoulopoulos, C.E, Papanikolaou, K.A: A Group Formation Tool in an E-Learning Context. In: 19th 
IEEE International Conference on Tools with Artificial Intelligence(ICTAI 2007), October 2007, pp. 117-
123 (2007) 

