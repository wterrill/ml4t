How does one measure the effectiveness of a deck of flash cards? What are the factors that increase 
or decrease effectiveness and what is the relative impact of these factors?  How will you leverage 
collaboration to incrementally increase the effectiveness of cards, and why will this be effective? 

When measuring the effectiveness of a deck, I have two primary concerns:  

  How easy is it to recall a particular flash card? 

  How easy is it to leverage knowledge memorized through flash cards? 

The total number of cards in the deck is explicitly not a consideration. Once a student committed a card 
to long term memory, it has a very small footprint on daily review. Yet, number of cards in short term 
memory is a bottle neck. Cards in short term memory have a significant footprint. The standard practice 
is to limit the maximum number of cards scheduled per day. 

The first concern is ease of recall of a card. Supermemo calls cards that are inherently difficult to 
remember as leeches. Leeches can dominate review time of a poorly formulated deck. 

We can compute a metric calculating ease of recall for each card. The metric is a consideration on the 
learning and forgetting of a card. The consideration could be number of days, number of successful 
recalls, etc. For example, students forgetting a card in less than three recalls 80% of the time is one such 
metric. 

Histogram

y
c
n
e
u
q
e
r
F

5

1

24

2

43

3

22

4

3

5

2

6

0

MORE

Successful Recall Streak

 

The second concern is on leveraging knowledge memorized through spaced repetition. One can say that 
this is the whole point of using spaced repetition. Yet students may fail to recall knowledge at the time 
of use. The brain drops all knowledge that was not recalled during review. For example, consider the 
following cards: 

Q: What is the name of the measure for which the extent of a study can be generalized? 

A: External validity 

Q: What is the name of the measure by which a causal conclusion is warranted by the study? 

A: Internal validity 

These cards will help the student name a given concept. However, they may not be able to recall the 
concept given the name. i.e., they may not be able to answer to the question "What is internal validity?" 

The standard practice is to emphasize the importance of redundancy. Students will be able to 
reconstruct an item even after forgetting the said item in a redundant deck. Redundancy minimizes the 
effects of forgetting when using a learned skill. I will explain the exact details of redundancy later in the 
essay. 

Making a redundant deck is a herculean effort, even for expert practitioners. I believe that it would be 
better to "grow" the redundancy of the deck over time. Sadly, there is no mechanical way of 
determining the redundancy of the deck. Discovered happens organically. i.e., A student may forget 
some fact when leveraging the knowledge learned. From this, the student can add new cards to bolster 
redundancy. 

We now move onto the factors which determine effectiveness of a deck. First, let us focus on 
effectiveness of a single card. A card should be: 

 

 

Simple: The card should have a simple answer 

Specific: The card should have only one answer 

  Consistent: The answer should not differ or contradict another card 

  Comprehensible: The card should be understandable 

  Univocal: The card should be unambiguous and have only one meaning 

The community calls this the minimum information principle. There are also a host of other factors that 
make it easier to remember a single card: 

  Narrowing by example: The question also provides an example to make recall easier 

  Metaphoric approach & Vivid approach: The answer capitalizes on preexisting knowledge or 

emotion 

  Graphic approach: The answer makes use of visual processing capability. Visual processing 

capability is much greater than verbal processing capability in humans. 

Another factor is simplicity and intuitiveness of the beginning items. This is called the basics-to-
details approach. Having a simple initial foundation greatly increases the comprehensiveness of 
material. For example, 

Q: What is production? 

A1: Any activity that creates value 

A2: A series of activities by which resource inputs are transformed through a recipe and technological 
process into outputs of goods and services. 

A1 is much more preferable than A2 if production is a basic concept of the material being learnt. This is 
because A1 is much more simpler and intuitive. A2 also violates minimum information principle. 

A common factor that greatly increases the chance of a card becoming a leech is enumerations. Answers 
should never be a set if at all possible. Answers should also not be lists if possible. If a list is necessary, 

use cloze deletion. A cloze deletion removes a single piece of knowledge and lets remain the other 
pieces. The student has to recall the missing piece. 

The final factor is redundancy. The function of redundancy is not to make items easy to remember. It is 
to ensure students can derive knowledge forgotten from knowledge remembered. This will improve 
real world performance as students can rederive forgotten knowledge. 

There is no computable metric for redundancy. Students have apply knowledge learned to discover 
where to bolster redundancy. Yet, there are three methods for preemptively adding redundant cards 
without real world application. 

  Active and passive recall: Construct items to have definition of a name as both a question and 

an answer. 

  Provide derivation steps: Remember both the result and the derivation of the result. 

  Provide optional reasoning context: Add accessory material to the answer of a card. This could 

include explanations or reasoning clues. It is not necessary to remember accessory material 
during review. Mark accessory material of a card as such. 

We finally come to collaboration. Collaboration can decrease the per student effort required to create 
the cards. Collaboration can also increase the overall quality of the deck. 

To achieve these aims, nearly every edit should increases coverage or increases quality. A pitfall for 
coverage is that students may not necessarily agree on what to include and what not to include. To 
control this, we will have administrators who rule on these issues. 

I expect there should be little friction on edits aiming to improve quality. System provided statistics 
which will point to cards that need improvement. Redundancy will improve at a much faster pace as 
there will be more students. More students mean more application of learned knowledge. Thus, 
students will know what to add faster. 

