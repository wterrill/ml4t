Milestone 2

Project overview
This project takes a unique, I believe, approach to the peer-to-peer tutoring idea. By 
restricting the tutor and student space strictly to university campuses the added beneﬁt of 
domain speciﬁcity is added. That is, tutors are much more contextually away of the materials 
which the students are facing. Furthermore, the added restriction of forcing the tutors to be able 
to offer tutoring services only for classes which they, themselves, have already taken offers the 
student a higher level of conﬁdence in the tutoring which they expect to receive from a tutor.
In this milestone
1. Updating user proﬁle
2. Authentication
3. Payments and transcript manager
4. Backend overview
For the product demo, see: https://www.youtube.com/watch?v=scA_4WNU_h4

