Assignment 1: Introducing Yourself

1

My name is Eugen Istoc; I am a software engineer professional. Having nearly reached 
the end of my OMCS degree, I’m ecstatic to have the opportunity to take this class at this point. 
I have had the opportunity to be part of different forms of classes during my undergraduate and 
graduate career — most of my time, however, has been spent as the recipient of educational 
technology. I have, furthermore, had the privilege of contributing to classes as a teaching 
assistant in small classes as well as large. The approach to educational technology signiﬁcantly 
changes when dealing with large classes versus smaller ones. The problems that I address 
below are those that I have noticed and experienced as a teaching assistant.
As a teaching assistant, the primary goal is typically to grade assignments and offer help 
to students . A reality, however, is that with a class of over 300 students, it becomes very difﬁcult 
to be as effective. The reason for this is partly attributed to the type  of material which that class 
covers, and the fact that there typically is longer turn-around-time with a smaller number of 
TAs . I would like to explore ways in which to use technologies that increase the efﬁciency of 
TAs. This would involve studying techniques which leverage the data available to the TA from all 
the student posts and contextualizing it. This may involve, perhaps, AI techniques, but other 
ﬁelds like big data may also be explored.
the type between when a question is asked to when an answer is provided. This time can 
become very crucial as deadlines approach, but it can also when an assignment is very 
complex and many problems atomically arise. Blocking assignments  like these, can be very 
costly for the student, especially when a TA is not readily available. The naive solution to this 
problem is to simply increase the number of TAs — it his however a workaround. In an online 
setting with many students, there should be an easy mechanism for students to interact with 
each other in a more personal sense. That is, I would like to research ways in which students 
can help each other on their assignments based on how far each student has gotten in the 
assignment. A form of tutoring system amongst the students — similar to what Piazza attempts 
to do, but with a more practical aspect to it, geared speciﬁcally to students helping each other 
on various assignments.

The other problem that TA typically run into as the turn-around-time for answers. That is, 

3

2

4

 Via ofﬁce hours and Piazza
1
 Some classes yield a high degree of variance between results requiring student support on a 
2
case by case basis.
 Teaching Assistants
3
 Times when the student is stuck on an assignment, and needs help in order to continue.
4

