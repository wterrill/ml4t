Mason Gallo 

CS 6460 ­ Assignment 2 

 

My area of focus is research that will find a “common denominator” for education. Another way I 

like to think about this area is finding ways to bring better equality and efficiency to education 

through technology. I hope to use machine learning to answer questions that may help students 

to achieve better performance, teachers to create more efficient lesson plans, or administrators 

to select students that have the highest likelihood of completing their education. For the purpose 

of focus and access to data, I will aim to do analysis specifically in the context of computer 

science education. My immediate goal would be to utilize data for my research from the OMSCS 

program at Georgia Tech. Since the amount of research covering online programs and 

computer science education outcomes is rather sparse, I will also look to educational research 

from any subject for inspiration as long as it fits my area of focus. 

 

Student retention is perhaps the most widely researched area in all of higher education. 

According to Murtaugh et al (​http://link.springer.com/article/10.1023/A:1018755201899#page­1​), 
several factors were found to significantly impact retention: age, GPA, race/ethnicity, location, 

involvement in community. Research focuses on developing strategies for administrators in 

order to maximize retention. With a better understanding of retention, we can focus on acquiring 

students with the highest likelihood of remaining and increase the likelihood of retention of those 

already enrolled. Tinto 

(​http://academics.uky.edu/uge/ias/provosts%20retention%20group%20information/tintoretention
­whatnext.pdf​) focuses specifically on low­income students. While this is an area of interest, the 
data required is likely not possible to obtain. Another consideration is how specific to qualify 

“retention”: retention in a specific class? Retention in an entire program? OMSCS faces 

retention problems; however, the specificity is different given the different nature of the OMSCS 

program: most students work full­time and will likely take longer to graduate. I suspect that 

different factors drive retention in online programs. As online programs rise in popularity, it will 

become important to better understand these factors. 

 

Driving student success is a passion of mine because I believe that every student deserves an 

equal opportunity. Research in this area affects and benefits two parties: student and teacher. 

DeTure (​http://www.tandfonline.com/doi/abs/10.1207/s15389286ajde1801_3​) found that 
cognitive scores were poor predictors of success, defining success as GPA. Simpson 

(​http://www.94669.mrsite.com/USERIMAGES/Open%20Learning%20predic%20art.pdf​) 
mentions that previous experience, previous education, socio­economic status, gender, and 

age, among other factors, are strong predictors of success in an online distance­based 

program. As mentioned above with retention, I think it’s also important to consider the factors 

driving success at the class level along with at the program level. 

 

In conclusion, I have narrowed my area of focus to retention and success. My research 

questions center around this focus: What factors drive retention in an online program? What 

factors drive success in an online program? What factors drive retention in an online class? 

What factors drive success in an online class? Next week I will focus on narrowing down these 

research questions. 

