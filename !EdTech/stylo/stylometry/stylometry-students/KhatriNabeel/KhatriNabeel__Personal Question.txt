Personal Question 
nkhatri6 

Question 
Consider online math systems in the specific area of student engagement and retention rate.  What is the 
current published community knowledge in this area including long term knowledge retention and the 
forgetting curve?  How could you assess the student's skill level in order to increase or decrease the level 
of difficulty of the math items? 

 

According to the U.S. Department of Education, few rigorous research studies purport the effectiveness 
of online learning in the K-12 domain1.  This is due in part to the limited number of research studies that 
have  investigated  the  use  of  online  learning  programs  to  augment  classroom  instruction  in  K-12 
environments.  The limited number of studies that have been published validate the need for K-12 online 
learning  programs  as  they  demonstrate  the  positive  outcomes  from  student  interaction  with  these 
programs.  The results of some (older) research studies suggest that online elements such as quizzes and 
video games do not influence learning by students1.  This is contrary to other findings that suggest online 
instruction (either all or in part) is beneficial to student learning.  As a result, it could be suggested that 
different  types  of  online  learning  environments  could  have  differing  outcomes.    Similarly,  the  U.S. 
Department of Education reported that “the effectiveness of online learning approaches appears quite 
broad across different content and learner types”1. 

One  relatively  recent  research  study2,  the  results  of  which  were  published  in  2014,  reviewed  the 
effectiveness of a specific online learning resource over the course of two years, across 20 schools.  The 
findings  from  this  study  were  separated  into  different  areas  including  student  perceptions,  teacher 
perceptions, and student outcomes.  Student outcomes included two specific areas of improvement.  The 
first  was  improvement  in  test  scores,  which  were  a  direct  result  of  increased  retention.    The  second 
outcome  measured  self-reported  perceptions  with  respect  to  math  anxiety,  math  self-concept,  and 
academic confidence2. 

The improvement in test scores was a statistically significant finding.  Test scores prior to the introduction 
of the online learning tool were compared against scores nine months later, during which time students 
utilized the online learning tool to augment their in class instruction.  Instead of using traditional paper 
worksheets (in class), the time was spent practicing math problems online.  It is important to note that 
the  online  learning  tool  was  not  used  to  provide  homework  assignments  to  students,  but  rather  to 
supplement in class instruction by interacting with students using videos and problem sets.  In fact, more 
than 70% of the time spent with the online learning tool was used to work on problem sets; the remaining 
time was spent viewing videos.  Teachers did not expect students to access the online learning tool from 
home.  

Student engagement was measured through a variety of interactions including classroom observations, 
focus groups (with students), and teacher interviews.  The majority of students surveyed reported that 
they enjoyed the time spent using the online learning tool.  Interestingly, the number of students that 
enjoyed this time increased as the grade  level of students decreased.  Teachers interviewed validated 
these findings.  The high level of engagement was attributed to a number of observations.  In particular, 
students enjoying utilizing laptops and tablet devices, which suggests the medium through which students 
were engaged had an impact.  Similarly, feedback and hints provided by the learning tool allowed students 
to progress without getting stuck for long periods.  This likely  resulted in higher success rates thereby 
increasing student confidence.  In addition, the interaction with the online learning tool allowed students 

Personal Question 
nkhatri6 

to experience a level of independence that is not common to typical classroom instruction.  Lastly, the 
experience  provided  by  the  online  learning  tool  also  had  an  impact  on  the  level  of  engagement.    As 
students  would  complete  problem  sets,  they  would  earn  points  and  awards;  experiences  that  are 
commonly found in game like environments. 

Retention was measured using a specific methodology.  To start, students were presented with videos 
relevant to the current topic area.  During this time they were also provided with practice questions to 
help  establish  the  concepts  that  the  videos  were  demonstrating.    After  all  of  the  relevant  practice 
questions  were  completed,  the  students  were  given  a  set  of  challenge  questions  to  test  mastery.  
However, the questions were not given until a gap of at least 16 hours had passed.  Each set of challenge 
questions consisted of eight problems in the topic area.  In addition, a student was required to successfully 
complete three separate challenge problems sets, where each problem set was separated by a minimum 
of 16 hours.  If the student successfully completed all three challenge sets (with the aforementioned gaps), 
they were considered to have mastered the topic.  It should be noted that future challenges also included 
problems related to previous topics that were already mastered, thereby testing long term knowledge 
retention. 

One  method  by  which  student  skill  levels  can  be  assessed  is  by  tracking  problem  difficultly  with  the 
percentage of problems answered correctly (or incorrectly).  A threshold can be used wherein a certain 
number of problems answered correctly at the current level could trigger problems to be released from 
the next level.  What the threshold should be, however, is open to debate.  Some may feel that one or 
two problems may not accurately determine competency at the current skill level, whereas waiting for a 
large number of correct problems may delay advancement to the next level.  Still, a slowed approach is 
better than inaccurately advancing a student to the next level.  Likewise, a threshold would be required 
to  determine  if  and  when  problems  must  be  decreased  in  difficulty.    Much  like  increasing  difficulty, 
incorrectly answering one or two problems may not provide sufficient evidence to decrease the skill level, 
but it is likely that three or four problems may.   

It  is  worth  noting  that  the  well-known  Kumon  math  centers  that  specialize  in  augmenting  math  skills 
outside of school utilize a very specific approach for learning and advancement.  Kumon started with one 
location in Japan circa 1956; their math centers can now be found across the world.  Kumon teachers 
provide students with worksheets at specific concept levels, such as addition, subtraction, multiplication, 
etc.   Kumon levels  begin with basic counting and number sequencing and continue  to probability and 
statistics  (post  calculus).    Teachers  provide  basic  one  to  one  instruction  for  new  topics  and  concepts.  
Students are then given worksheets that they must complete at the math center.  Once completed, they 
are reviewed by the teacher to gauge the student’s competency in the new topic.  If necessary, additional 
tutoring  is  given.    Once  the  student  exhibits  sufficient  progress,  he  or  she  is  given  enough  worksheet 
packets to take home for practice.  The term enough generally constitutes one packet per day, on average, 
where one packet contains ten worksheets.  Self-study is encouraged; parents are asked not to intervene 
at home.  Students cannot advance to the next level until they achieve a perfect score on a full worksheet 
packet  that must  also  be completed in a certain amount  of time  at the math center.  Therefore,  both 
speed and accuracy are taken into account. 

There are a number of similarities between the aforementioned online learning tool and the approach 
utilized  by  Kumon.    For  starters,  both  systems  are  meant  to  augment  existing  school  or  classroom 
instruction.  Similarly, both require sufficient practice in the current topic area and both gauge mastery 
by requiring the student to complete problems sets with high, if not perfect, accuracy.  However, Kumon 

Personal Question 
nkhatri6 

takes into account  speed in addition to  accuracy.  It is likely that the online  learning tool may engage 
students more effectively then the worksheets packets used by Kumon. 

Although adjusting skill level is important to ensure that the student is adequately challenged, it is not an 
expected feature of the  proposed math (homework) portal.  The goal of the math portal is to replace 
paper worksheets commonly used by teachers in elementary schools to assign homework.  Since these 
homework  worksheets  contain  a  set  of  problems  based  on  a  particular  concept  (i.e.  multiplication, 
division, fractions, etc.), the online math portal will mimic this behavior by providing a web page of similar 
math problems.  Today, there are numerous problems on a (paper) worksheet, but the type (concept) of 
problems and their level of difficulty does not change.  Therefore, the type and difficulty of problems that 
will be presented through the online math portal will not shift based on individual responses.  The aim is 
to provide a teacher with an alternate method of assigning math homework, and to provide students with 
an alternate way of completing their math homework.  Features such as automated grading, performance 
tracking, and reporting are added benefits that cannot be easily realized with paper worksheets. 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

References 
1.  Patrick, S., Powell, A.  (2009). A Summary of Research on the Effectiveness of K-12 Online Learning.  

Vienna, VA:  International Association for K-12 Online Learning. 

2.  Murphy, R., Gallagher, L., Krumm, A ., Mislevy, J., & Hafter, A.  (2014). Research on the Use of 

Khan Academy in Schools. Menlo Park, CA: SRI Education. 

