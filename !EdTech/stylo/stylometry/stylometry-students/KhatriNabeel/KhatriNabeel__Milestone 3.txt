Intermediate Milestone 3 
Nabeel Khatri – CS 6460 
 
 
Milestone 3 provides a prototype of the Math Homework Online web application.  Not every feature that 
is  expected  in  the  final  deliverable  is  currently  functional  in  the  prototype,  however  many  of  the 
capabilities are.  Similarly, certain aspects of the user interface will undergo additional development and, 
as such, may not provide the appropriate level of feedback that would be expected. 

The prototype application can be accessed using the following URL:  http://mymh.azurewebsites.net 

There  are  three  points  of  entry  into  the  application,  namely  students,  teachers,  and  parents.    This 
deliverable will provide some details with respect to these three areas and what to expect when reviewing 
the prototype application. 

In order to speed up development, user credentials are not currently being validated for any login events.  
As a result, login screens such as the one presented below can be bypassed by clicking the Submit button.  
The application provides tiered access based on whether the user is a student, teacher, or parent.  Since 
credentials are not currently validated, there is a hardcoded session ID for each type of user.  Therefore, 
clicking the Submit button in the Teacher Login screen below will take the user directly to the teacher 
portal. 

 

 

 
 
The  teacher  portal,  as  it  is  called,  provides  three  separate  capabilities  to  the  teacher.    The  first  is  to 
assign/review homework.  Currently this section can be used to assign homework to students.  The review 
capability is still under development but is expected to provide teachers with a list of students showing 
complete/incomplete  status  of  the  selected  homework  assignment.    For  those  students  that  have 
completed their assignment,  their resulting score/grade  will  also  be  displayed.   The  second capability, 
creating user accounts, is largely functional.  However, user input validation is non-existent and will likely 
be added prior to the final deliverable.  In addition, the user (teacher) receives no feedback that a user 
account  has  been  created (even  though  it  has).    This  functionality can  be  validated  by  using  the  third 
capability, which is viewing accounts.  Viewing accounts provides a list of students, parents, and their 10-
digit phone number. 

The parent portal provides parents access to their child’s homework performance.  It should be noted that 
both parents and students will share the same account, which requires the use of a 10-digit phone number 

as  the  password.    The  different  entry  point  will  allow  parents  to  access  the  site  to  view  their  child’s 
homework performance.  Currently, the prototype application allows parents to view their child’s scores.   

The  student  portal  is  used  primarily  by  students  to  complete  any  outstanding  math  homework 
assignments.  Upon login, the application will determine if the student has an assignment that needs to 
be completed and, if so, presents the student with a screen similar to the following: 

 

 
 
The student would then proceed by clicking the Start button.  Currently, one sample problem screen is 
presented  to  demonstrate  the  user  experience.    The  bulk  of  the  remaining  development  effort  in the 
student  portal  involves  adding  problems  to  the  database  and  ensuring  that  the  application  properly 
displays the assignment problems and evaluates both correct and incorrect answers. 
 

 

