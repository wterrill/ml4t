Intermediate Milestone 3  
Spyridon Ganas 
 

The All-Payer Claims Database (APCD) is a data warehouse containing medical records for 

Project Summary 
 
 
approximately six million Massachusetts residents.  The state government created this resource to allow 
“health care providers, health plans, researchers and others to address a wide variety of issues, 
including price variation, population health and quality measurement. “i 

This project will create an online course titled “An Introduction to the APCD for Health Care 

 
Researchers”. This training will teach researchers about the available data and the process of requesting 
APCD data.  Key goals of this training program are: 

  Provide an overview of the data available in the APCD. 
  Teach terminology related to the data in the APCD. 
  Serve as “marketing” for the APCD, helping researcher become more aware of the data and 

resources that are available.  
 

Milestone 3 Tasks and Deliverables 
 

 

Intermediate Milestone 3: April 3, 2016 

o  Create videos that cover basic medical research terminology 

  What is a Member Month? 
  What is a provider? 
  What is the difference between fully insures and self-insured? 
  What are the different types of insurance (ME003)ii 
  What is the difference between primary and secondary insurance? 
  What are the different types of coverage (ME029) 
  What are the different insurance lines of business? 
  What are the different types of Medicare (ME081) 
  What are deductibles, co-pays, coinsurance, etc. 
  What is the difference between a claim and a claim line? 
  What do all the different dollar amounts on a claim mean? 

o  Deliverable: An updated website containing at least 20 videos. 

 

 

Results 
 

 

This phase of the project has been completed successfully.   

o  The “work-in-progress” website can be viewed at http://intro-to-apcd.github.io/.   
o  Videos created as part of this milestone can be seen on the website’s “Medical Claims 

Terminology” tab. 

  The “What is a provider” topic got merged into the provider overview video 
  The capitated claims topic got merged into the “How are Claims Paid” video 
  Videos about different lines of business, fully/self insured, etc. all got merge into 

a single video about commercial insurance policies. 

o 

I spent a substantial amount of time making the website “mobile-friendly”. 

 

 

JavaScript now makes the menu shrink to a button when the page is viewed on 
a mobile device. 
I used CSS to change the YouTube videos so they are smaller on mobile devices. 

 

Concluding Remarks 
 

 

Overall I am happy with the way this project is going.   

It looks like my final website will contain 20 videos, instead of the 30 I originally planned.  This is 

 
due to my decision to merge some related topics, and exclude a couple topics that I decided are not 
relevant to researchers. 

Some of the feedback I got was about viewing the website on mobile phones.  I spent a 

 
substantial amount of time trying to figure out how to make the website mobile-friendly, and I’m very 
happy with the results. 

 

 

                                                           
i http://www.chiamass.gov/ma-apcd/ 
 
ii Values like (ME003) or (MC023) refer to specific columns in the APCD.  For example, ME003 is the 3rd field in the 
Member Eligibility table, while MC023 is the 23rd field in the Medical Claims table.  A full list of these values is 
available in the APCD submission guidelines:  http://www.chiamass.gov/apcd-data-submission-guides 
 

