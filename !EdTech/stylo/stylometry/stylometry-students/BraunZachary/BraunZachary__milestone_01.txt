Milestone 1

Zachary E. Braun

Milestone 1 - Introdcution

As discusses in my project proposal, one of the key features of my project is having the
companies describe the language learning market and their place in it in their own words.
To do this, I will be sending a questionnaire to a set of language learning companies. Below
are those questions as well as a (cid:12)rst pass at a solicitation letter.

Questions

The following is the list of questions that I have created to send to the targeted companies.
I may make some changes to these before I send them and may cut a few of them in order
to increase the likelihood that they will be answered.

Questions about the speci(cid:12)c company

Q1: How would you describe your product?

Q2: What are the goals of your product?

Q3: Who do you feel are your target demographics?

Q4: What are the standout features of your product and how do you feel they diﬀerentiate
you from your competition?

Q5: How has your product evolved over time? What has been the biggest change?

Questions about the general market

Q6: Why is language learning important in general and how do you feel electronic language
learning products meet this need?

Q7: What are some of the advantages and disadvantages of electronic language learning
compared to classroom methods?

Q8: Do users of electronic language learning products tend to stick with just one product
or do they use many?

Q9: Do your users tend to supplement electronic learning with other types of learning (e.g.
classrooms, social networking based on language, etc.) and do you (cid:12)nd that those who do
perform better?

Page 1

Milestone 1

Zachary E. Braun

Questions about the future

Q10: How do you feel electronic language learning can improve itself?

Q11: Do you foresee a future in which electronic language learning products could replace
the classroom entirely?

Closing questions

Q12: What are some exciting changes you can share with me about the future of your
product?

Q13: Do you have any demographic information about your customers that you can share
with me?

Q14: Do you have any data on success rates for your product that you could share? How
long do users typically take to gain a satisfactory level of mastery?

Q15: Are there any questions that I forgot to ask or anything else you would like to share
about electronic language learning?

Solicitation Letter

Note: I am attempting to keep this letter short and to the point in hopes that it will grab
the attention of the targeted companies. I will need to add a section about how the results
will be used based on what is required by the IRB.

Dear XYZ;

I am a graduate student in the computer science department at Georgia Institute
of Technology and I am conducting research into educational technology in the
(cid:12)eld of language learning. XYZ is a leader in electronic language education and I
was hoping that I could get a representative from XYZ to answer some questions
for me about your product, language learning, and the language learning market.

Thank you,

Page 2

