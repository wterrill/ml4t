   Assignment -3: Exploring your Problem 

1  RECAP   

With the growing revolution of online education, academic honesty is yet another side of the 
same coin. How far we can achieve this, is something to be thought of if we claim the online 
degrees are credible. “Online education vs Plagiarism” and how it extends to age groups, 
communities and cultures can be sensitive. Irrespective of cultures and communities, 
considering every individual in the program as intelligent capable students, my intension is to 
dig deep into why such a tendency would arise. Given the fact that there are clear policies and 
standards already set, cases of plagiarism still arise every semester.  

Most existing theories just suggest that students are either lazy, pressed for time due to last 
minute submissions, pressure from family to get good grades which count towards 
scholarships, or even because students don’t understand or internalize the meaning of 
plagiarism. 

2  EXISTING PROBLEMS 

Last week I was attending an ACM conference where one of the key speakers was Prof. Jennifer 
Widom who is a Fletcher Jones Professor of Computer Science and Electrical Engineering at 
Stanford University. She launched one of the first MOOCs course online titled “Introduction to 
Databases” in the late 2011. The course had more than 100,000 students enrolled. Prof. Widom 
described how she found many cases of plagiarism where one student scored a 100/100 in less 
than 90 sec. Furthermore she said that the student had two accounts created, of which one was 
used to read the questions and the other account was used to submit off the answers after 
getting enough time to research and google over. The question raised by interviewers today is - 
“How can we rely on your course certificate on databases?”, “Is there a credibility behind it?”. 
There is absolutely no proof as to how the scores are achieved before the recruiter. 

  Paid Examinations - To counter this she said there are certifications or courses that are paid 

which ensures only serious individuals enroll. This also enhanced higher course completion 
rate. Examinations are conducted at test-centers thereby ensuring lesser chances of 
cheating. Taking exams at centers makes things inconvenient for students defeating the 
purpose of convenience. 

  Students can cheat - The fact that students can cheat if they want to, still exists. Proctor 

Track was laid out to monitor the students live during the progress of tests. However it’s not 
very difficult to beat the system. Google chats, hipchat and other collaborative networks 
also provide easy access for students to discuss over the answers to their projects and 
assignments. 

 

 

It’s on Google search engine - Projects, Assignments and other class works may be 
uploaded to GitHub or other sites which can be viewed by future students taking the same 
class making it easier for them to copy off lines of code. 
Ignorance - Many acts of plagiarism arise from the fact of either ignorance or not 
understanding the clear policies of academic honesty (though the statements are stressed 
every semester) 

  Varying Definitions - The meaning of the word plagiarism is interpreted differently by 

individuals. What seems “ok” to some may not be right in terms of law and the policy that 
has been enforced. Not all universities or colleges around the globe stress the importance 
of plagiarism. Teachers and Professors don’t enforce strict actions against such students 
either. Guidelines of the importance of citation, bibliography, how to research, how to 
rephrase are not laid out well enough. 

The question that lies here is, should we counter the act of plagiarism OR educate the students 
about plagiarism and its effects on their future. I would say both yet ensuring the cords are tied 
well. 

3  POSSIBLE SOLUTION 

Finding the root reason as to why plagiarism occurs stands ahead before proposing a feasible 
solution.  Since my project is revolves around research, I wish to survey out the possible 
reasons that students in the online education community think could be the cause. How they 
interpret plagiarism in their own words. What instances they would term as cases of plagiarism. 
What they think is ok and what’s not. Once these grounds have been laid out, we can be clear 
where we can trigger out our act.  

  Awareness - How can we better educate the students. One thing I would like to point out 
(after having taken 8 course in OMSCS), KBAI is the only class where I have seen Prof. Goel 
very strictly addressing the students over Piazza within the first week into the semester. He 
very clearly draws the line and explains the academic honesty clearly unlike posting links 
(which I doubt any student goes through). That gave the students the feel of how much 
serious and loyal the Professor is towards his course and his students.  

  Borders - Drawing the border line is important as students may stay behind in participating 

too much over forums fearing if they leak out information. So what is the maximum 
information that they can collaborate over, is the line that needs to be laid out as we need 
participation as well. 

  Consequences - Publicly addressing the students (names anonymous, just numbers) over 
Piazza with the number of cases of plagiarism that were handled and what happened to 
them is another point that Prof. Goel laid out. This instilled fear and cautiousness in the 
minds of hundreds of other students taking the course.  

  Tools - Ofcourse, there are many tools out there to capture cases of plagiarism comparing 

present submissions within the same class and across previous semesters. These tools have 
been useful and very handy as well.  

 

4  IMPACT ON COMMUNITY 

Preventing an act of plagiarism itself is a great contribution to the online community of both 
students and Professors. The self-confidence of many hardworking students are maintained 
and the credibility of the courses taken online increases a lot.  

The work and commitment towards these courses will be much higher as students know that 
they are each being rewarded well as they deserve for their hard work which further boosts the 
level of satisfaction in students. 

Being an online education, the community impact will be more like a butterfly effect (a small 
change causes a great impact). The impact is expected to be global where the standards are set 
equally everywhere on plagiarism for all students enrolled. 

5  SOURCES AND CITATIONS 

1.  Why students plagiarize - Link 1 
2.  Causes of Plagiarism - Link 2 
3.  Forms of Plagiarism - Link 3 
4.  Prof. Jennifer Widom - Wiki link 

 

 

