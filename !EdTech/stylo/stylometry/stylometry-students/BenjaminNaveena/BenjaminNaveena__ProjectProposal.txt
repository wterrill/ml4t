 
 

                                                                                                                                        Naveena Benjamin  

 

 nbenjamin@gatech.edu 

      

 

     Project Proposal 

1  ABSTRACT AND AIM 

 

Capstone Projects currently do not have all required features to enable project management for 

multiple areas of discipline which integrates materials from different sectors of engineering. Capstone 

Project Management tool focusses on building an integrated platform where students and 

professors/mentors/sponsors can collaborate and keep track of capstone projects typically in 

undergrad engineering settings. Professors can have easy help in forming teams or groups among 

students and monitoring the progress of their work. Bill of material, status of orders placed, managing 

inventory from multiple vendors and products that can be either optical, chemical, electrical, electronic 

or mechanical can be managed easily. 

This tool will be a senior design / capstone/ product management interface. 

2  MOTIVATION AND CONTEXT 

Lab\Product management portals mainly serve to manage inventory, materials ordered in the lab, the 

status of orders and so on. Most lab portals are customized to their area of discipline such as Quartzy 

for chemistry scientific research lab or PartsInPlace for electronic BOMs and inventory. The audience 

focused on are the research post grads. As of now there is no current platform where all components 

are managed collectively from multiple vendors and multiple sectors. Students at the undergrad 

level/industry don’t have a platform to organize materials to integrate into the complete workflow in 

the creation of a product. This paper will give a high level picture of the proposal of this project. 

(Darrell & Patricia, 2005)  [2] discusses that Industrial/ Academic partnerships are essential for 

technological development regardless of the discipline. The paper further talks about integrating 

teaming and project management skills into capstone design. There is an initial screening and 

feasibility of the projects and then teaming and grouping happens. The team members are responsible 

for the scheduled work and deliverables. The students are introduced to scheduling concepts and are 

 
 

                                                                                                                                        Naveena Benjamin  

 

 nbenjamin@gatech.edu 

required to use Microsoft Project to complete “scheduling” assignments. There are weekly meetings 

within the group as well as with the mentors. The product/ project are reviewed by the company 

contacts responsible for their projects.  

(Duston, Todd, Magleby, & Sorensen, Jan 1997) [3] states how teaching engineering design through 

senior project or capstone engineering course has increased in recent years. The capstone courses can 

be classified as those that include economic evaluation and those that include design for construction. 

Academic Professors/ TA’s/ Industrial Sponsors are the various stakeholders in the process. 

(Farr, Lee, Metro, & Sutton, April 2001) [5] discusses on the framework(SEDP) for approaching 

problems in a logical systematic process. The main emphasis is on problem definition, planning for 

action, and managing the project through completion. SEDP is uniquely suited for engineering 

managers who deal with large complex multidisciplinary problems that are not amenable by single 

functional engineers. 

3  PROBLEM DESCRIPTION 

There are few exiting problems present in the picture which is why this whole idea came up. 

  BOM Management - Currently it is difficult to manage multiple BOMs of components for a large 

product involving items from different sectors or verticals. Materials could either be machine parts, 

electronic, electrical, chemicals and so on. Managing orders and items via multiple vendors and 

tracking their delivery is quite tedious. 

  Workflow pipeline - The complete workflow of the product starting from the component 

acquisition online via orders to the finished product is non-existent in the academic environment 

and there is no integration available either. As of now, an individual has to approach various 

vendors separately and track deliverables through different courier services to get the desired 

item. Placing orders can be done only through their websites which are not integrated on a single 

platform making it cumbersome to search for their respective portals. 

 
 

                                                                                                                                        Naveena Benjamin  

 

 nbenjamin@gatech.edu 

  Academic Environment – Students pursuing various degrees and individuals with innovative ideas 

find it hard to get a product or deliverable from a lab on time. Having multiple plates of tasks 

makes it harder for them to process and a lot of time is spent as well.  

  Collaboration of Projects – Having the Professors manage multiple projects and multiple teams 

simultaneously and giving approvals to orders placed by students across multiple vendors makes 

their task tiresome. Tracking of many projects and its progress at a minute level is difficult too.  

4  EXISTING SOLUTIONS 

The already existing inventory management is mostly done over excels which can get cumbersome as 

more material management comes up. Below screenshot is taken from the site Bolt Blog authored by 

Ben Einstein. Pictures or thumbnails, links are not available and cannot be easily embedded into excel.  

(Einstein, 2015) [4] 

 

  The already existing almost similar software Quartzy (Overview tour - Quartzy Overview Tour) 

gives an idea of how the inventory is managed. But the only management done here is for a 

 
 

                                                                                                                                        Naveena Benjamin  

 

 nbenjamin@gatech.edu 

chemistry lab material procurement and does not meet the requirements of multiple vendors 

multiple orders management from various engineering disciplines. Orders have to eventually be 

made on the respective web portals manually and are not redirected. (Quartzy, n.d.) [7] 

  Group work or Collaboration is very effective in learning and how to design such a group is based 

on few factors: Creating Interdependence and Individual Accountability.  While some instructors 

don’t mind if students divide up tasks and work separately, others expect a higher degree of 

collaboration. If collaboration is your goal, structure the project so that students are dependent on 

one another. Limiting resources to compel students to share critical information and materials. 

Assigning Roles within the group that will help facilitate collaboration.  

Providing structure and guidance to help students plan the project and Incorporating Process 

Assessments are also involved in this process. Individual contribution Management is also 

important in this kind of collaboration. (Teaching Excellence and Educational Innovation, n.d.) [8] 

To facilitate all of the above, the design of group collaboration will involve group formation with 

assessment, monitoring, progress tracking and close mentoring.  

 

(Bruhn & Camp, 2004) [1] states in order to make the experience more realistic the students were 

asked to create teams to create a “mock” consulting company to work with. Group members with 

team leads were formed and bi-weekly meetings were held with sponsors and faculty members, 

and had deliverables too.  

5  TOOL DESIGN 

This section describes the how the tool will be designed and how the layout would look like. Wireframe 

screenshots have been attached to give a clear understanding of the user interface. 

1.  Login / Sign Up – The home page of the tool will have login / signup feature which enables users to 

login with already registered username and password which otherwise they will need to create. 

                                                                                                                                        Naveena Benjamin  

 

 nbenjamin@gatech.edu 

 
 

 

2.  Dashboard – The user logs in and can view all the projects currently under development and those 

that are completed. The completion rates are indicated alongside every project. 

 

 

3.  Project Overview – Clicking on a project can help user view the project in detail. Details include the 

team members involved, the roles they were assigned, the status of the project with start and end 

dates. 

  

 

                                                                                                                                        Naveena Benjamin  

 

 nbenjamin@gatech.edu 

 
 

 

4.  Project Stage – The menu bar shows the Project Stages divided into three main stages-1, 2 and 3. 

Each stage has been given a checklist and the team lead can check off as the stages are completed. 

The completion of the individual stages determine the overall completion rate. 

 

 

 

 

5.  Team Formation – Team Tab shows teams members in a group and the roles assigned to them. An 

Edit Tab facilitates adding / removing members from a group. 

 

 

                                                                                                                                        Naveena Benjamin  

 

 nbenjamin@gatech.edu 

  

 

 
 

 

6.  Project Details – Plan Tab - Clicking on the project details tab takes to the menu bar having four 

sections which cater to the project management. First tab is the PLAN which enables the user to 

provide links to the wiki , project plan and business plan of the project. 

 

7.  Project Details – Schedule tab provides the details of the items assigned to team members with 

due dates. This tab  

 

                                                                                                                                        Naveena Benjamin  

 

 nbenjamin@gatech.edu 

 

 
 

 

 

8.  Project Details – BOM tab provides the details of Materials to be acquired, orders to be placed and 

so on. The “New Order” tab enables entering new material information and the “Orders” tab 

enables to view all the orders before placing them for approval. 

                                                                                                                                        Naveena Benjamin  

 

 nbenjamin@gatech.edu 

 

 

 
 

 

 

 

9.  Orders Page – Clicking on Order Details button takes to the orders page where we can view the 

current status of the order placement, order approval and the order delivery. 

                                                                                                                                        Naveena Benjamin  

 

 nbenjamin@gatech.edu 

 
 

 

10. Project details – Building / Prototype Tab gives the overall picture of the current status of the 

product. Pictures, weblinks can be added to view the progress of the project. 

 

 

 

 
 

                                                                                                                                        Naveena Benjamin  

 

 nbenjamin@gatech.edu 

6  TECHNICAL DESCRIPTION 

The below tables shows the description of the various tools that will be used for the development and 

building of the project. 

Item 

Wireframe 

Tools 

Microsoft Axure 

Wireframe Hosting 

Axure Cloud 

Languages 

 

 

Project Language 

Backend 

Frontend 

Charts 

Hosting 

 

 

 

Gantt Chart 

Heroku.com 

Other support 

GitHub, Virtualenv, Open 

sourced modules 

Python – Flask (2.7 and above) 

MySQL / PostgreSQL / Mongo 

Bootstrap 

 

 

 

7  TASK AND SCHEDULES 

Date 

Item 

Description 

Feb 22 – Feb 28 

Getting started 

Prepare the resources, platform 

to begin work. Complete Flask 

tutorials and setup database. 

Prepare the wireframe to 

publish to Axure Share cloud. 

Feb 29 – Mar 06 (IM -1) 

Login and Dashboard 

Set up the login and the 

dashboard for the tool. 

Dependencies will be covered 

as modules are completed 

individually. 

                                                                                                                                        Naveena Benjamin  

 

 nbenjamin@gatech.edu 

Mar 07 – Mar 13 

Project Overview Page with 

The initial tab on project stages 

Project Stages 

with all three stages will be 

completed. 

Mar 14 – Mar 20 (IM -2) 

Team Tab with Project Details – 

Plan Tab will be completed 

Plan Tab 

along with the Team tab 

feature which can add/modify 

team members. 

Mar 21 – Mar 27 

Schedule Tab  

Gantt chart displaying the 

various tasks with task assigned 

to, start/due dates. 

Mar 28 – Apr 03 (IM - 3) 

BOM Tab 

BOM tab will handle order 

details and new order 

population. 

Apr 04 – Apr 10 

BOM – Order details 

Order status, approval will be 

displayed. 

Apr 11 – Apr 17 (IM - 4) 

Building Tab 

Gallery/ Carousel with links to 

final prototype will be setup. 

Apr 18 – Apr 24 

Testing and Hosting 

App will be hosted on Heroku 

for final testing and delivery. 

Apr 25 – May 1 

Final Project Presentation 

Prepare an overall demo / 

walkthrough and the 

presentation of the final project  

 
 

 

8  INTERMEDIATE MILESTONES 

There will be four intermediate milestones as laid out below: 

Intermediate Milestone 1 – 06th March: 

 
 

                                                                                                                                        Naveena Benjamin  

 

 nbenjamin@gatech.edu 

This milestone will include the Login / Sign up to the web tool. The Dashboard of the user which 

displays the project overview will also be displayed. Interactive demo displaying the feature will be 

submitted. 

Intermediate Milestone 2 – 20th March: 

Project Overview with Project stages which covers the different aspects in a particular project, Team 

formation handling and the Plan Tab from the Project details will be submitted. Interactive demo with 

presentation will be submitted. 

Intermediate Milestone 3 – 03rd April: 

Schedule Tab which helps user to assign tasks and give dates along with Gnatt chart will be shown. 

BOM tab handling both new orders and submitted orders will be displayed. Presentation/ Demo will be 

submitted. 

Intermediate Milestone 4 – 17th April: 

BOM Order Details displaying the status of the orders along with the Building Tab will be displayed. 

Presentation will be submitted.  

9  IMPACT OF THIS SOFTWARE IN EDUCATION 

The area of education where the capstone portal covers will be research, undergrad/grad engineering 

labs, innovative product labs, post-grad labs. The field of research which at present focuses on 

engineering can be made to include other fields of discipline as well. This software helps to work more 

efficiently as a team, making use of resources wisely and ability to interact closely with fellow peers 

and mentors. 

  Tracking the progress of work - Basically Professors and students have to be on the same page 

during the course of the project at all stages starting from the very beginning. Scope of project can 

either be reduced to meet milestones or scale up to achieve new features based on resources. 

  Mentoring - Closer level of attention and mentoring is possible as the groups/teams are segregated 

and sponsors/stakeholders can have a close watch at how the tasks are split. Level of interaction is 

 
 

                                                                                                                                        Naveena Benjamin  

 

 nbenjamin@gatech.edu 

brought down further. Multiple group management is not easy and requires much co-ordination 

and discipline which this portal can assure.  

  Managing inventory can divert the focus of the student from the project. Helping students to 

concentrate on bigger tasks and utilize more time on those than BOM procurement is crucial.  

Keeping track of all the projects that you are involved with can be challenging. New laboratory-

management software, including Web-based tools, can facilitate documentation, data sharing, 

planning, controlling and synchronization. “It is not very complicated to set up,” says Krishnan. 

“You can even use a spreadsheet or an electronic calendar to create a pretty good management 

system. If it becomes a bigger team, you may have to turn to commercial, off-the-shelf software. It 

is important to facilitate the process and not spend more time organizing the organization tools.” 

(Filipp, 2009) [6] 

 

  

The above diagram illustrates the relation and impact of the student with mentor, team and the final 

product. The mentor will have access to multiple project monitoring simultaneously. 

 

 
 

                                                                                                                                        Naveena Benjamin  

 

 nbenjamin@gatech.edu 

10 CONCLUSION 

(Bruhn & Camp, 2004) [1] notes that students gain real professional skills that use cutting edge 

technologies and that confer knowledge about industry standards; faculty gains insight to teaching 

strategies; corporate folks save money; reverie well-researched solution to  business problems and 

above all have the opportunity to train the universities brightest minds.  

11 WORKS CITED 

[1] Bruhn, R. E., & Camp, J. (2004). Capstone course creates useful business products and corporate-ready 

students. NY, USA: ACM SIGCSE Bulletin, Volume 36 Issue 2, June 2004. 

[2]Darrell, G. J., & Patricia, B. M. (2005). Capstone Design Projects With Industry : Emphasizing Teaming and 

Management Tools. American Society for Engineering Education Annual Conference & Exposition. 

[3]Duston, A. J., Todd, R. H., Magleby, S. P., & Sorensen, C. D. (Jan 1997). A Review of Literature on Teaching 

Engineering Design Through Project Oriented Capstone Courses. Journal of Engineering Education. 

[4]Einstein, B. (2015, Aug 25). Product designer and lover of hardware, Founder + partner at @BoltVC. Retrieved 

from Bolt Blog: https://blog.bolt.io/will-your-hardware-startup-make-money-

677a8e6c665b#.iomzwt3sb 

[5]Farr, J. V., Lee, M. A., Metro, R. A., & Sutton, J. P. (April 2001). Using a Systematic Engineering Design Process 

to Conduct Undergraduate Engineering Management Capstone Projects. Journal of Engineering 

Education. 

[6]Filipp, F. V. (2009, Dec). Effective Laboratory Management. Retrieved from asbmb.org: 

http://www.asbmb.org/asbmbtoday/asbmbtoday_article.aspx?id=4760 

[7]Quartzy. (n.d.). Retrieved from https://www.youtube.com/watch?time_continue=20&v=x4o-3EyI8ZY 

[8]Teaching Excellence and Educational Innovation. (n.d.). cmu.edu. Retrieved from Eberly Centre: 

https://www.cmu.edu/teaching/designteach/design/instructionalstrategies/groupprojects/design.html 

 

