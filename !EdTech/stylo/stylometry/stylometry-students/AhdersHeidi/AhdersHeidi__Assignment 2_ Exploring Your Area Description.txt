 
 

 

Ahders 1 

 
Heidi Ahders 
Assignment 2 
Dr. Joyner 
January 23, 2016 
 

Assignment 2: Exploring Your Area 

For this week, we were to think about the areas of focus that we would like to devote our time in this 

class. I found this activity very difficult. The reason for this was that I am interested in so many area of 
educational technology. I first went through many of the videos in the library materials on educational 
technology to get a sense of what is out there and what each one means and maybe where I can make my ideas 
fit in. Then I started thinking about the technologies that I would like to help with. Most of the ideas are 
creating tools to help my daughter with her education and that could help other youth in the community. I 
thought if it would help her, it would probably help other people. So my ideas were: creating a 4-H record book 
or portfolio app, counseling aid to help high school students with planning courses at a local community 
college, or 4-H Quiz bowl learning app. These ideas fall with in the technologies areas of online learning, game-
based learning, and maybe a Learning Management System.  

Currently a 4-H record book system exists, but while using it, I feel that it can be improved and better 

managed for the youth members. Currently the system is only online and as youth are tech savvy, they have not 
moved it over to the mobile devises. By moving it to the mobile devise, they can track what they have learned 
while they are at an event and upload an image as it is happening. This mobile app does not necessarily have to 
be just for 4-H, we can expand it to other areas.  

I found out that the community college that my daughter is currently attending as a high school student 

does have an application for educational planning, but before you can use it, you have to see a counselor to set it 
up or add. You are not able to explore different degrees or multiple degrees until it is set up, so for a high school 
student that wants to find out how long it will take them to graduate or transfer they have to see some one at the 
college. This can often takes months to get an appointment. I have been planning my daughter’s classes for the 
past 3 years and have done an adequate job. The part that I have had trouble with was that I did not know when 
classes were being offered in what semester. That you can only obtain if you have access to the course database. 
I will have to investigate the mechanisms/api’s to connect with the college’s database or even if I can. If I am 
not able to do this, I will have to set up an initial database with information that I have gleaned from experience 

 
 
with planning her classes so far. I think this project can be beneficial for many students, not just high school 
students.  

Ahders 2 

Angela, my mentor, also suggested maybe tackling the gender divide in science/tech. This has always 
interested me as well. I feel that the lack of role models for young girls in the field is part of the issue. While 
doing a bit of research in this topic, I found Gender and Science and Technology (GASAT) Association, which 
was first established in the 90’s. The objectives of the group are:  

• 

• 
• 

• 
• 

• 

to encourage research into all aspects of gender differentiation in science and technology 
education and employment; 
to foster gender equity in science and technology, in education and in the workplace; 
to facilitate the entry of women into employment in the fields of science and technology and 
their progress within such employment; 
to foster socially responsible and gender-inclusive science and technology; 
to provide a forum for the dissemination and discussion of research findings and experiences of 
those working in the field; and 
to provide a support network for those working towards the objectives outlined above. 

I do not think we have come far in encouraging more women in the field of technology, or making it conducive 
for women in the field. Georgia Institute of Technology has a total undergraduate enrollment of 14,682, with a 
gender distribution of 66.2 percent male students and 33.8 percent female students. I know that I read 
somewhere about the division in this class but can’t find it in Piazza. I want to say there are only 38 women in 
the class and the rest of the classmates are male. This is not surprising in this field where it is a male dominated 
field. Maybe these questions can be looked at:  

•  What can we do to encourage more females into the field? 

•  Why is technology geared more towards males than females?  
•  How can we change this bias? 
•  How can we make schools for technology more attractive to female students? 

 
These questions are just a few that should to be addressed to the public as a whole. I will be looking more 
closely into my project/research in the coming weeks to narrow it down. 
 

 

 

