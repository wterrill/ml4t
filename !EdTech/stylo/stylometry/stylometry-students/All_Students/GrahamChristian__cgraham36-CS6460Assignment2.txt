Chris Graham 
cgraham36 
Assignment 2 

Area of Focus: Automated Online Assignment Assessment 
 
The OMSCS program uses several different mature/stable platforms to facilitate learning and 
collaboration for students and instructors, such as T­Square (​Sakai​) for assignment submission 
and grading, ​Piazza​ for student discussions, and ​Udacity​ for lecture videos and quizzes. Many 
OMS courses involve graded programming assignments, and in theory these assignments could 
be submitted and graded on the Udacity platform. In practice, many OMS assignments are 
submitted and graded through T­Square. Written assignments are read and graded by 
manually, and programming assignments are also manually graded offline with grading scripts 
written by the course instructors. Given that one of the goals of the OMS program is to scale to 
handle large numbers of students with relatively few instructors, fully automating some or all of 
the manual grading process without sacrificing the quality of assignments or accuracy of grades 
would free up a significant amount of the workload for the instructors. 
 

Existing Systems 
 
With thousands or even millions of students and few human instructors, massive open online 
course (MOOC) systems such as ​Udacity​, ​Coursera​, ​edX​, and ​Khan Academy​ all heavily rely on 
automated assessment for grading student work. The simplest assessments are multiple choice 
questions that have static correct answers, such as the Udacity quizzes used in the OMS 
program. In some cases, the system allows students to retry after entering the wrong answer, 
which encourages the student to either learn from their mistake or simply continue guessing 
until the correct answer is found through trial and error. In other cases the format is a longer 
exam­style assessment with the student only seeing feedback after fully submitting all answers. 
 
Large MOOC systems are also able to automatically run and assess student submitted code by 
checking the code’s output against predetermined test cases to give immediate feedback to the 
student. For example, the edX platform includes an API for ​external grader systems​, which allow 
instructors to automatically receive student code, run the input in the correct environment, and 
return the results back to the student. One ​common criticism​ of this approach is that grading a 
programming assignment based solely on the output can miss important details that would be 
recognized by a human reviewing the code. The code might be flawed in that it arrives at the 
correct answer using the wrong method, or may be written in an inefficient or convoluted way. 
Or the code might be nearly perfect and fail all tests due to a minor error. 
 
Many MOOC courses also include free form written responses or essays. These responses are 
inherently more difficult to automatically assess, because students can express many different 
correct answers that machine AI is not sophisticated enough to actually understand and 
evaluate. One possible solution used by some MOOC platforms is purely peer grading systems, 
where students are randomly assigned other students as peer reviewers for assignments that 

Chris Graham 
cgraham36 
Assignment 2 

could not be graded automatically. Coursera for example heavily uses ​peer review for grading​, 
and many have pointed to ​issues with the approach​ such as that reviewers often give feedback 
that is flippant (only a few words), irrelevant (focusing on grammar or spelling), or incorrect. With 
Coursera’s approach the reviewers themselves are not evaluated so there is little direct 
incentive for students to put significant effort in reviewing and there is no indication of how 
reliable any given reviewer is at providing quality feedback. 
 
Another approach to free form responses is to apply automated machine grading to essays. 
Usually this is done by using a machine learner to build a model grading function based on 
sample essays and grades determined by the instructors. A beta version of ​grading AI​ exists for 
edX, and has had ​some success​ in approximating human grading. The use of automated essay 
grading is ​controversial​ in general though, and issues such as the difficulty of providing students 
with detailed feedback and the inability of current automated grading systems to correctly 
recognize ​generated gibberish​ are major obstacles that prevent automated systems from fully 
replacing human graders. 

