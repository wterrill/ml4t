Background 
Overview  
There are many online tools that exist to help with internalizing educational material.  Some of 
these leading tools are explored and critiqued below by their ability to promote active learning 
via retrieval practice across the practical considerations set out in a popular text on the subject: 
Making it Stick (Brown, Roediger III, & McDaniel, 2014). 
 
Retrieval Practice  
Retrieval practice is a learning strategy that is based on research that suggests repeated, 
delayed, and varied reinforcement exercises strengthen internalization of learning as measured 
by a student’s ability to recall and apply the material at different times and within different 
contexts (Carpenter, Cepeda, Rohrer, & Kang, 2012). 
 
The strategy is contrasted with massed practice – the approach of consuming educational 
material with little or no time between reinforcement activities.  In studies comparing the two 
approaches, retrieval practice consistently has produced better learning results when compare 
with learning techniques such as rainbow writing  (Jones, et al., 2015) and concept mapping 
(Karpicke & Blunt, 2011); so powerful is the approach at committing material to long term 
memory, that differences in how each gender employs retrieval patterns may correlate with 
gender differences in achievement in primary and middle schools (Royer, Tronskya, Chana, & 
Jackson, 1999). 
 
Active Learning  
Active learning is an educational model that champions the belief that an educator achieves 
better learning outcomes when students are charged with increased responsibility for learning.  
More succinctly, a student must do more than just listen, watch, and take notes in class to learn 
(Felder & Brent, 2009).  
 
Active learning has shown to increase performance in STEM subjects (Freeman, et al., 2014) as 
well as the humanities. (McCarthy & Anderson, 2000) and may manifest practically through 
applying a negotiative, reflective, and critical lens to a certain body of knowledge (Barnes, 
1989).  That is, a student that asks questions and receives feedback (negotiative), is able to look 
back and restate the material in their own words (reflective), and can find application or 
highlights gaps in their understanding when considering wider contexts (critical) will likely 
achieve a better learning outcome. 

Retrieval Practice Tools 
Evaluation Criteria 
Retrieval practice may be promoted through online tools via a set of practical dimensions that 
promote active retrieval: active learning and involvement with the material, distributed and 
varied practice, and interleaving contexts (Brown, Roediger III, & McDaniel, 2014).  An 
evaluation of several online retrieval practice tools (including Anki, Cerego, Memrise, 

Mnemosyne, SuperMemo) yields a large set of similarities across these two dimensions, with 
similar roots. 
 
History of Tools 
When it comes to reviewing material, as early 1939, researches have hypothesized that the 
introducing intervals of time between content review is an effective reinforcement learning 
technique.  After several studies corroborated that spaced repetition is effective, Sebastian 
Leitner created the Leitner System, a learning system based on flashcards (Spaced Repetition, 
2016) in which the student reviews material that she has more difficulty with more often than 
with material she’s mastered. When the student gets an answer correct, she “promotes” the 
card to a box that she practices from less often.  When she gets it incorrect, she “demotes” the 
card to a box that she practices from more often. 
 
SuperMemo is a similar spaced repetition algorithm – however, instead of buckets, the student 
is shown a card more or less often depending on the difficulty rating they gave the card when 
answering it. All the tools reviewed implemented a variation of either the SuperMemo 
algorithm or the Leitner System.   
 
State of Tools Today 
Commonalities and Differences 
The tools ranged from a simple spaced repetition flash card framework (Anki, SuperMemo, 
Mnemosyne) to full-fledged learning environments (Cerego, Memrise), however all employed 
very similar distributed practice approaches, usually based on the SuperMemo algorithm.   
 
Cerego and Memrise provide curated flash card sets, which could possibly facilitate the creation 
of interleaving concepts.  Cerego and Memrise also incorporated features that promote 
reflection by the student as well as feedback by educators. 
 
Shortcomings and Areas to Explore 
Introducing some friction in this process may help reinforce the routes to memory even more – 
desirable difficulties in internalizing the material leads to effortful learning which strengthens 
the active learning paradigm (Brown, Roediger III, & McDaniel, 2014).  
 
All the tools explored require a student to either create or find a curated set of flash cards 
covering their desired learning topics and nearly all1 tried to eliminate as much friction as 
possible in getting to the review.  Indeed, all of the apps featured communities where a student 
can obtain ready-made cards so that they can start testing right away.  These features reduce 
the amount of participation a student has in creating a testing plan, deviating from a principle 
of active learning. 
 
                                                     
1 Cerego is the only one that attempts to teach the user the association ahead of testing and, whether by design or 
not, Cerego has a steep learning curve and requires significant time investment from the user, even if they used a 
community sourced set of cards. 

Likewise, the variety of questions and their context is left entirely to the student or community 
creating the cards.  As such, there is no guarantee that there will be a variety of cards 
addressing the same concept nor is there any guarantee that context will be broadened. 
 
The state of online tools today has distributed practice covered, however, there is little in the 
way of coupling this practice with how and when the material is learned – a student utilizing 
these tools may never write the questions herself and does not receive guidance on how to 
create a plan that will maximize her learning outcome by ensuring the testing is varied and 
contains interleaving context. 
 
In the next assignment, I’ll be exploring the possibility of filling these gaps by wiring existing 
distributed practice tools earlier on in the learning process as well as to provide framework of 
feedback and reflection to provide a holistic retrieval practice approach to internalizing a set of 
new concepts. 
 
 
  

Works Cited 
Barnes, D. (1989). Active Learning. Leeds: Leeds University TVEI Support Projec. 
Brown, P., Roediger III, H. L., & McDaniel, M. A. (2014). Make it stick : the science of successful 

learning. Cambridge, Massachusetts: The Belknap Press of Harvard University Press. 

Carpenter, S. K., Cepeda, N. J., Rohrer, D., & Kang, S. H. (2012, 8 4). Using Spacing to Enhance 

Diverse Forms of Learning:. Educational Psychology Review, pp. 369-378. 

Felder, R. M., & Brent, R. (2009). Active Learning: An Introduction. Raleigh: North Carolina State 

University. 

Freeman, S. F., Eddy, S. L., McDonough, M., Smith, M. K., Okoroafora, N., Jordta, H., & Pat, M. 
(2014, June 10). Active learning increases student performance in science, engineering, 
and mathematics. (1. S. Scott Freemana, Ed.) Proceedings of the National Academy of 
Sciences of the United States of America, 8410-8415. 

Jones, A. C., Wardlow, L., Pan, S. C., Zepeda, C., Heyman, G. D., Dunlosky, J., & Rickard, T. C. 
(2015, July 25). Beyond the Rainbow: Retrieval Practice Leads to Better Spelling than 
does Rainbow Writing. Educational Psychology Review, 1-16. 

Karpicke, J. D., & Blunt, J. R. (2011, February 11). Retrieval Practice Produces More Learning 

than Elaborative Studying with Concept Mapping. Science, 772-775. 

McCarthy, P. J., & Anderson, L. (2000, June). Active Learning Techniques Versus Traditional 

Teaching Styles: Two Experiments from History and Political Science. Innovative Higher 
Education, 279-294. 

Royer, J. M., Tronskya, L. N., Chana, Y., & Jackson, S. J. (1999, July 1). Math-Fact Retrieval as the 

Cognitive Mechanism Underlying Gender Differences in Math Test Performance. 
Contemporary Educational Psychology, 181-266. 

Spaced Repetition. (2016). Retrieved from Wikipedia: 

https://en.wikipedia.org/wiki/Spaced_repetition 

 

