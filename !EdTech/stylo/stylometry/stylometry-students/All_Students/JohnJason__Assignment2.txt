Assignment 2 - Exploring Your Area 
Jason John 
 
The real power in game­based learning is revealed only if those games are authentic to the 
material being covered. If not authentic, the learner will lose the vigor and motivation to pursue 
further challenges. Harness the power of a well designed games to achieve specific learning 
goals and the result is a workforce of highly motivated learners who avidly engage with and 
practice applying problem­solving skills. I like how knowledgeguru​1​ stated; “creating a learning 
game is a dual process ­ you need to design gameplay that is fun and balanced, and you need 
to design a solution that achieves specified learning objectives’.  
 
One of the outstanding benefit of game­based learning is that it can simulate real world 
scenarios. It can impart the element of engagement and curiosity. Without that, every learning 
process becomes rote memorization. Furthermore, it pulls the learned into an environment that 
look and feel familiar, and relevant. It can constantly/instantaneously reward/warn the learner 
about the action that they take and there by keep the learner fully engaged.  
 
In the past, game­based learning was only accessible to high profile end­users, like military, 
aviation, law enforcement etc. Today, however, with the lowering cost of technology, lot more 
smaller players, like educational institution, business etc, have started embracing this learning 
technique. Here’s some interesting facts about the effectives of game­based learning​2​: 

 
 
Some of the key players that I came across in this field are ‘Institute of Play’, ‘Education Arcade 
­MIT’ & ‘GLS’. While GLS primarily focuses on adult, the former two put more emphasis on kids. 
Subsequently, the games that they produce targets differents aspects of human psychology. For 

 

e.g, ‘Fair Play’ from GLS focuses on how to navigate through challenges as an African 
American. 
 
Another interesting, yet important, element that I learned during my initial exploration is that 
Gamification is different from Game­based learning. According to Justin Eames​3​, Gamification is 
the concept of giving incentives in a competitive setting, while in game­based learning the idea 
is to impart educational concept/theory through games. This, however, doesn’t mean that 
gamification can’t exist within the context of game­based learning. It probably can, because 
providing incentives for the right action can infact act as an impetus in keeping the learners 
motivated. Here are some interesting K­12​3 ​games that I came across: 

● Banished ­ a city­building simulator 
● Bridge Constructor ­ great for teaching physics of forces 
● Portal II ­ Brain­warping puzzles 

 
As part of my research, I would like to dig deep into the relationship between metacognition and 
game­based learning. Clearly, introspection is the key element in evaluating one's thought 
process in solving problems or learning new concepts. As Dirk Ifenthaler​4​ puts it, tracking 
metacognitive characteristics while playing a game will help the educator understand specific 
traits/behavior of the learner. Another sub part that I would like to focus on is the impact that the 
timing of feedback (during the game­based learning) has on the metacognition of the learner. 
Santhos A Mathan​5 ​highlights that there are two schools of thought; some researcher argue that 
immediate feedback minimize unproductive, while other argue that immediate feedback impede 
metacognitive activities. This is something that I would like to also explore further as part of my 
research. 
 
 
References 
 
(1) ​http://www.newmedia.org/game­based­learning­­what­it­is­why­it­works­and­where­its­going.html 
(2) ​http://www.theknowledgeguru.com/game­based­learning­infographic/ 
(3) ​https://www.edsurge.com/news/2014­05­28­what­game­based­learning­can­do­for­student­achievement  
(4) 
http://deniz.eseryel.com/uploads/9/0/7/5/9075695/ifenthalereseryelge_assessment_in_game_based_learnin
g.pdf  
(5) ​http://pact.cs.cmu.edu/koedinger/pubs/mathan%20koedinger%20ed%20psy%2005.pdf  

