bandwidth efficient 

online learning

Milestone #1, CS-6460, Spring 2016

James Jackson

Georgia Tech

initial demo

Original: https://www.udacity.com/course/viewer#!/c-
cs313/l-48730215/m-48714289 

23.4MB video

Animatron re-creation: http://jamesjackson.github.io 

557KB animation (170KB library, 349KB audio, 38KB 
visual)

hand-drawn lectures

●

●

●

ex. Udacity “Intro to 
Theoretical Computer 
Science”
original data is 
unstructured
typically involve 
animation of strokes

presentation-based lectures

●
●

●

ex. Udacity KBAI
original data is 
structured
typically no animation of 
strokes

convert or re-create lectures ?

convert

○
○
○

complex process (potential difficulties), resulting content is less optimal
less time required
new conversion tools w/computer vision required 

re-create

visual content only, audio content can be retained
cleaner process, resulting content is more optimal

○
○
○ more time required
○

new flexible authoring tools required

convert hand-drawn lectures ?

extract raster image from video via ffmpeg

○

http://jamesjackson.github.io/snapshot.png

convert to vector image (SVG) via Super Vectorizer

○
○

http://jamesjackson.github.io/snapshot.svg
looks good, but no SVG “strokes”, no way to animate strokes

convert to vector image (SVG) using centerline tracing ex. autotrace

○
○
○

http://jamesjackson.github.io/snapshot_traced.svg
SVG “strokes” can be animated, but serious degradation in quality
difficult to calculate animation timing (image variation from frame to frame)

convert presentation-based lectures ?

save PowerPoint slides as PDF

convert to vector image (SVG) via pdf2svg

○

http://jamesjackson.github.io/ppt.svg

determine slide timing (CV problem - TBD)

determine hand pointer position/timing (CV problem - TBD)

re-create hand-drawn lectures

key authoring requirements:

○

○
○

record strokes and timing as animated vector 
graphics
sync audio
hand pointer overlay animation

no mainstream authoring solution

MIT Pentimento prototype (right)

○

https://github.com/aehsu/pentimento

re-create presentation-based lectures

key authoring requirements:

○
○
○
○

author slides using vector graphics
import PowerPoint slides
sync audio
hand pointer overlay animation

Animatron comes close:

○
○
○
○

http://www.animatron.com 
no PowerPoint import
hand pointer overlay animation is a tedious process
audio must be authored separately and imported

next steps...

determine slide timing (converting presentation-based lectures)

determine hand pointer position/timing (converting presentation-based lectures)

look at options for talking heads ex. avatars

 

