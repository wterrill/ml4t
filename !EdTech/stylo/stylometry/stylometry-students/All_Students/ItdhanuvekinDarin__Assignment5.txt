CS6460 Topic: Intelligent Tutoring Systems in the area of understanding student motivation and emotion 
while interacting with the AI 

What is the current published community knowledge? 

It is known that having a tutor can often help a student by providing one-to-one attention and 
can help the students overcome specific obstacles that they are personally struggling with.  An 
intelligent tutoring system would thus be computer software that is designed to act as a human 
tutor, being able to feed students questions and also being able to respond to the student 
accurately. However it is not just the ability to understand what the student is asking verbally 
that is essential but the ability of the AI to understand the emotions portrayed by the student 
and understanding student motivation that is key.  Human tutors have to adjust the way they 
interact with their students in order to motivate and keep the student interested.  The AI will 
have to be able to simulate a human tutor. 

Thus, the characteristic that separates the intelligent system from other computers that 
provide instruction is that it is able to interpret complex student responses and continue to 
learn.i  The system creates a personalized profile for each student which displays the level of 
mastery and can adjust its tutoring behavior based on the student’s profile. It is crucial that the 
AI can recognize when a student response is incorrect and why so that it can react accordingly. 

In general, the AI system will feed the student exercises and record detailed responses from the 
student (i.e. diagrams, equations, any work leading to the answer). The system will then go 
through each answer and mark areas that are off track.  It can then evaluate what the student 
is struggling with and continue to feed the student questions that relate to the same concept 
and can even provide the student hints when prompted. 

What current and past software applications have attempted this and how successful were 
they? 

There have been several attempts to create an intelligent tutor system, one being the Cognitive 
Tutor from developed in Carnegie Mellon for math and science.  Others include the Andes 
Physics Tutor from Arizona State University, and the Writing Pal for writing strategies.  While 
there was no specific information how successful each of these systems were, I imagine there 
were indeed varying levels of success with these AI tutors.  This is because in one study, it was 
“…found that students who explained their steps during problem-solving practice with a 
Cognitive Tutor learned with greater understanding compared to students who did not explain 
steps“ii Being told to write out answers as descriptive as possible instead of simply listening to a 
tutor explain the answer already is a benefit to the student’s learning process.  This helps them 
to avoid shallow learning and instead guide them to towards full understanding.  Each time a 
student is also stuck and does not know the next step, the AI tutor can provide a hint while still 
allowing the student to continue producing the answer themselves. 

A current intelligent tutor system that is being developed is one called GuruTutor for biology 
which attempts to model the strategies and dialogue of an expert human tutor.iii  It has been 
found to be highly successful, matching the success rate of students who were tutored by an 
actual human being.  The GuruTutor takes into consideration dialogue management, 
coordinated speech, gesture, and pointing, and natural language understanding.  There is an 
actual virtual human looking tutor that speaks to the student.  The lesson plan is also modeled 
after national standards for biology so that the same concepts that would normally be taught in 
a classroom is also implemented by the tutor.  There is a lot of interaction involved between 
the student and tutor which is essential to the student learning the material.  The student 
converses with the tutor via summary, concept map, scaffolding and cloze. 

Another tutor system which is a bit different is called ASSiSTments.iv The screen looks like a 
standardized exam being presented. It provides questions to students on the computer screen 
(i.e. multiple choice or open ended questions) and will provide immediate feedback to the 
student to indicate a right or wrong answer.  It will answer provide tips or hints to help guide 
the student to the correct solution.  To me, it has less depth than GuruTutor and simply does its 
job, “assisting” an actual human teacher. Without the human teacher, I doubt students would 
have as much of a success rate as with GuruTutor.  There is no auditory stimulation where it can 
talk to the student or intake a student’s verbal response.  It simply does not resemble a human 
tutor and I feel lacks the ability to form a connection with a student. 

One thing I would like to keep in mind with all the past and current systems out there is that 
there was no mention of interpreting student emotion and motivation outside their written or 
verbal responses.  The AI systems are analyzing and interpreting the student’s answers but not 
their body language.  It would be interesting for me to read about a system that is visually 
scanning a student’s body language or facial expressions, which would further provide more 
insight as to whether the student is understanding the material or struggling.  I think that could 
result in a higher success rate for students and truly test the boundaries of an intelligent tutor 
system. 

                                                           
i https://net.educause.edu/ir/library/pdf/ELI7098.pdf 
ii http://csjarchive.cogsci.rpi.edu/2002v26/i02/p0147p0179/00000078.pdf 
iii http://www.gurututor.org/ 
iv https://www.assistments.org/ 

