 
 
 

Name: Farid Khoshnevissan 
Class: CS 6460 
Date: 2/14/2016 

The current state of mobile devices in the classroom is that many software ideas have been 

EBooks are one of the most applicable digital media platforms that can be applied to education, 

 
 
 
Q: Compare and contrast the current mobile software applications and describe what is 
limiting their success and adoption 
 
 
implemented to either support current education methods, create new ways of teaching material, or 
replace existing technologies or tools. Some of the main ideas include eBooks, homework applications, 
polling applications, apps that lockdown devices, digital note sharing, and teacher-specific apps related 
to managing the classrooms. All of these ideas are good ideas, the main limitation is that they either 
aren’t tailored to an educational setting, or they are but the execution of the idea needs improvement. 
These limitations are one of the main reasons why such mobile applications have yet to be adopted over 
a decent percentage of classrooms. Multiple applications have tried different variations of the same 
idea; this paper will compare and contrast some of the common approaches and why they have yet to 
be successful or adopted into regular education. 
 
and their ease of use makes them perfect for all age levels and content. EBooks take what used to 
require heavy books and makes them available anywhere, on almost any mobile device. There are a few 
key app developers in the market, but eBooks generally fall into two categories: EPUB and Kindle book. 
The main difference between Kindle books and EPUB books is that Kindle books use a proprietary 
format and are designed to only work on Kindle devices and Kindle applications. Both EPUB and Kindle 
books have digital media protection capabilities, although it is optional for EPUB. Both work on most 
mobile devices, but where EPUB can be used with any app, Kindle books can only be used with the 
Kindle app. Both are good at what they do, but they’ve had challenges from becoming successfully 
adopted for education purposes in the classroom. One main limitation is content distribution. If using a 
student’s own personal device, most eBook apps aren’t designed to receive content from a central 
source editable by a school. For any content not in the public domain the school would have to 
negotiate licenses, which becomes even more of a challenge when negotiating licenses for personal 
devices, as content protection from theft needs to be considered and accounted for. Another limitation 
is that most eBooks don’t have a set universal display option; they don’t have a way for instructors to 
jump to or point out a specific line, or give definite areas to read. Each device and display option displays 
text differently, adjusting where ‘what page’ would be; PDFs do not have this issue on mobile devices, 
but they don’t have a way of adjusting text size, causing people to have to zoom in on the page and 
move the page around on the screen. 
 
education. Polling applications allow instructors to create quiz questions and ask students questions in 
the middle of class, creating engagement with the material. Some of these applications include REEF 
Polling by i>Clicker, Socrative Student, and Answer Pad. All three named applications provide the same 
functionality of polling students and asking quiz questions, either multiple choice, true/false, and short 
answer. REEF Polling differs from the other two by requiring a subscription for each student, or paying 
for expensive handheld polling equipment. The interface for REEF Polling was also not well received 
compared to the other two, receiving a user score of 3 compare to Socrative’s 3.3 and Answer Pad’s 3.6. 
Answer Pad provides more unique question types than the other two. Socrative unlike the other two 
does not require student account registration. While these types of apps have had success in the 
classroom, the main limitations to adoption have been cost (in REEF Polling’s case), unable to track and 

Polling applications are a few of the applications that have seen some success in classrooms and 

Lockdown applications are the answer for teachers and professors who want to make sure when 

record student’s responses in the case of a quiz setting (in Socrative’s case), and lastly marketing (in 
Answer Pad’s case).  
 
students are focusing on the task at hand and not browsing the web. Lockdown applications prevent 
users from accessing programs or settings while in a locked state, only allowing allowed programs like 
the calculator application or Wikipedia to be accessible. While lockdown software exists for computers, 
there aren’t any comparable mobile applications, especially for personal devices. iOS treats applications 
as walled gardens from each other; only system processes are allowed to access applications, a 
lockdown app would not have access to control other applications from being able to be opened or not. 
Android does allow applications to control other applications, but only on rooted devices; 
manufacturers lock systems from root to protect users from invasive apps. There are a lot of challenges 
in using lockdown applications for education. One challenge is to root un-rooted devices, which is not 
common to the normal instructor. Another challenge is most people would be apprehensive of installing 
a lockdown app on their personal device, especially if they won’t have control to the app. The third is 
only targeting Android and Windows leaves out a large portion of students who would have iOS devices. 
While this would be a requested feature that, if solvable, could be adopted by teachers, the challenges 
are the main reason limiting adoption of such an app. 
Note sharing applications are those designed for students to collaborate, share, and write down 
 
notes, similar to Google Docs where multiple people can work on and edit documents at the same time. 
In the classroom, these applications would be used for students to take down notes, and if they wanted 
to share something with the class they could share it, making it accessible right then and there for 
others to see. The main apps in this area are Evernote, Google Keep, and OneNote. All three provide 
similar functionality: syncing notes between compatible devices, providing access on both web, desktop, 
and mobile, and in the case of OneNote and Google Keep, easy integration into their respective 
company’s office software product line. None of the apps are tailored or have settings that would better 
fit an education environment, such as share with one person (the instructor) before sharing it with 
everyone else. Also each person using those apps would have to have everyone in their contacts or be a 
part of the group, and there could be multiple groups if used for more than one class. These apps have 
had success in the professional world, but not in a classroom setting. 
 
into the classroom, either by teachers choosing to include them or students using them for their own 
learning purposes. None have reached the point of being considered essential to learning, such as 
Google, Wikipedia, or graphing calculators. To fully get these ideas past the limitations and become 
ubiquitous with learning, they need to be really easy to use, adaptable to most subject material, and 
instructors must instantly understand what the purpose of the app is and how it can help them, as 
instructors lead busy lives and if they can’t see the value quickly enough, many will pass. Only if apps can 
manage these will they lead to widespread adoption.  

All of these ideas are good ideas. All of these ideas have had some success in becoming adopted 

