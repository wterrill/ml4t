 

 
 

 

 
 

 

 

 
 

 
 

 
 

 

 
 

 
 

 

 
 

 

   

 

 

 

 

 
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
 

 

 

 
 

 

 

 

 

 

 

 

 

 

 
 

 

 

 

 

 

 

 

 
 

 

 

 

 

 

 

 

 
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
 

 
 

 

 

 

 

 

 

 

 

 
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
 

 

 

 

 

 

 

 

 

 

 

 

 

 
 
 

 
 
 

   
 

   
 

   
 

 
   
 

I consider myself a hackpreneur. I like to solve problems and help people through technology. I
 
   
co­founded two startups. The first one focused on connecting university students with companies to
 
 
 
help them work on meaningful real­world projects (we shut down). The second one is a system for
 
finding optimal travel route and selling train and bus tickets (still running). Currently I work at Chute
(YC W12), where we help big brands discover relevant social content and bring them closer to their
 
 
customers. 
 
I love learning but I at the same time I believe education should explore our creativity, not just mere
 
 
knowledge. As Ken Robinson says in his TED talk, “We’re educating our kids out of creativity.” Much
 
 
of the problem is the lack of engaging applications and games that could be used in classes instead
 
of text books. In my view of the future, education on all levels will be performed through interacting
with a personified set of teaching applications, with teachers acting mostly as mentors. AltSchool is
 
 
 
one of the first examples going in that direction. 
 
 
I love building stuff. I have a few ideas to work on in this class, and my hope is that with the help of
 
 
my peers and mentors, I’ll be able to find one that can be really useful. Education technology is also
one of the potential spaces I’m considering for my next startup, and through my own work, as well as
 
other students’, I hope to better understand the market of educational technology and identify
 
opportunities. 
 
 
 
I’m a huge fan of Khan Academy, and have watched many of the videos. I’ve also played around
love how they’ve integrated
with their courses although they’re primarily targeted at K­12.
 
 
gamification into the material. Every kid loves to compete, and giving them explicit goals turns
 
 
 
learning into a game. It also allows the teachers (and parents) to assess them easily, while informing
them what lessons the kid struggles with and thus might require additional explanation. I’ve also
 
taken several courses through Coursera, Udacity, edX and, obviously, the OMSCS program. If I
   
should point out one aspect that always makes the content more memorable and understandable, is
 
 
the use of well­designed quizzes throughout the lessons. 
 
 
As for ideas I have for this class, an educational iPad app for learning a language or math seems
 
 
like something doable within the given timeframe. A simple tool to build such apps is also an
interesting option.
 
I’ve also been thinking about possible applications of VR to teach biology,
 
chemistry, and similar subjects although that would probably require a team. 

 
   

 
 
 

 
 
 

 
 
 

 
 
 

 

 
 

 

 
 
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
 

 

 

 

 

 

 

 

 
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
 

 

 

 

 

 

 

 

 
 

 

 

 

 

 

 

 

 

 

 

 

 

 
 

 

 

 

 
 

 
 

 
 

 

 

 

 

 

 

 
 

 
 

 

 

 

 

 

 

 

 

 
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
 

 

   

 

 
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
 

 

 

 

I
 

 

 

 

 

   

 

 
 

 

 
 

 

 

 

 

 

