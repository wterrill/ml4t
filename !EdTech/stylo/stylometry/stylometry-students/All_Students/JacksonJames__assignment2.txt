James Jackson (jjackson308@gatech.edu)  
CS6460 (Educational Technology), Spring 2016 
Georgia Tech 
Assignment #2 
 
 
 
Delivering online education in the face of highly constrained Internet access  
 
 
A discussion thread on Piazza entitled “Underserved Communities and Educational Technology” 
provides the motivation for this particular area of research. Initial Piazza discussions, with 
colleagues Matthew Beasley and Jace van Auken, suggest that there are a variety of avenues to 
explore within the broader realm, and potential for group collaboration towards a common goal. 
 
According to a report “Offline and falling behind: Barriers to Internet adoption” [1], by McKinsey & 
Company, there are 4.4 billion individuals offline that are at risk of being left behind. Initiatives like 
Google’s Project Loon [2], or Facebook’s Internet.org [3], aim to use innovative techniques, such as 
balloons and drones, to deliver Internet access to these individuals. However, we anticipate that 
wide-scale deployment will take a long time, and that any access provided will initially be highly 
constrained.  
 
Here, we specifically seek to identify the technical challenges associated with delivering online 
education in the face of highly constrained Internet access, and to create recommendations, or 
reference architectures, for online education providers. In this context, highly constrained may 
include anything from generally available, but low-speed access, to generally unavailable access. 
While underserved communities certainly exist in the developed world, we take the approach of 
addressing the worst-case scenario of the developing world, with the hope that the results can 
similarly be applied elsewhere. 
 
In examining existing Massive Online Open Courses (MOOCs), we find that the majority of content 
is delivered in the form of videos. These videos are typically quite large, and require a good 
broadband connection for reliable viewing. However, further examination indicates that the visual 
content is primarily composed of slides with simple text and drawings, and at times, talking heads 
or moving hands. We theorize that this content could be transferred to a client in a highly efficient 
manner, and re-constituted into an experience that closely resembles the current video formats. We 
seek to ensure that any transfer of content and intelligent client-side re-constitution relies only on 
mainstream Internet technologies. 
 
While optimizing MOOC content may provide significant benefits for low-speed Internet access, it 
does not address the issue of unavailable access. To address this scenario, we seek to assess 
options for taking the online educational web offline. As an example, Learning Equality [4] has 
created an offline version of Khan Academy [5], known as KA Lite, that has been downloaded in 
more than 160 countries, and used by hundreds of organizations. KA Lite runs a web server to host 
Khan Academy locally, and the administrator can download selected videos as needed, and when 
an Internet connection is available. 
 
Some key requirements of the offline educational web, include making application logic locally 
available, ensuring that larger content can be selectively downloaded (less significant with a highly 
optimized format), and allowing synchronization and updates when connections become available. 

In defining a model, we once again seek to leverage mainstream Internet technologies, such as 
HTML5 offline applications. 
 
We ultimately envision a world where online education providers will design or adapt their web 
applications and content in a manner that facilitates non-traditional access scenarios, and provides 
new opportunities to the otherwise forgotten 4.4 billion individuals without Internet access. 
 
 
[1] Offline and falling behind: Barriers to Internet adoption, 
http://www.mckinsey.com/~/media/McKinsey/dotcom/client_service/High%20Tech/PDFs/Offline_a
nd_falling_behind_Barriers_to_Internet_adoption.ashx  
[2] Project Loon, http://www.google.com/loon/  
[3] Internet.org, https://info.internet.org/en/  
[4] Learning Equality, https://learningequality.org/ 
[5] Khan Academy, https://www.khanacademy.org/  
 
 
 
 

