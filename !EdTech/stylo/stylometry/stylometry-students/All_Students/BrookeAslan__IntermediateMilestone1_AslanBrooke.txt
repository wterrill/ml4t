Intermediate Milestone 1: Low Fidelity Prototype 

Aslan Brooke, Education Technology, Spring 2016 

 

Low Fidelity Prototype Presentation  
Please visit the link below to watch the presentation. Google Chrome is the preferred browser. The presentation is a 

shared MP4 file through Google Drive. Everyone has been given read permission that has the link. 

https://drive.google.com/file/d/0B92K6mb93sJzekVXTUxIZTFYN0k/view?usp=sharing 

 

 

Presentation Slides 

 

 

 

Script of Presentation 
 

Introduction 

Intelligent Tutoring Systems (or ITS) are positioned in the education landscape to achieve the same improvements that 
one-on-one human tutoring accomplish in students but without the human and monetary costs associated with one-on-
one tutoring. However, the ability to create ITS are limited by the speed of codifying the knowledge domain models they 
depend on. My project is to develop a proof of concept tool to automatically create the knowledge domain model for 
teaching the skill of summarization. 

Application 

This presentation will be a low fidelity version of the final application. I will cover the architecture first to build context of 
what the application consists of. Then I will proceed to the main use case which is the automatic generation of the 
knowledge domain model. The automatic generation algorithm will be explored in detail to build an understanding of how 
it works.  Then we will look at a mock-up of the user interface (or UI) and finally the flow of using the application by a 
student user. 

Architecture 

The architecture is fairly simple and divided into 4 main parts: The command line interface (or CLI), the web application, 
the application programming interface (or API), and the database.  The CLI will be for loading text and administrative 
commands. The CLI will communicate directly with the API. The web application will interface with the user and the API. 
The web application will be composed of Web 2.0 technologies but will have a simple interface for this prototype project. 
The API interacts with the CLI and the Web application and is responsible for communicating with the database, running 
the business logic of the ITS application, processing the unstructured information management architecture pipeline, and 
ultimately building the knowledge domain model. The last component is the database which maintains the persistence 
storage of the knowledge domain model. 

Use Case: Generate Knowledge Domain Model 

In this use case the user is loading a text file, whether it be a book of the bible, or AWS documentation, or any text, the 
process is the same. The user will load the file through the CLI, which will post the file to the API. The API will 
acknowledge the job and return an accounting number. Meanwhile in the background the API will process the text through 
the unstructured information management architecture (or UIMA) pipeline, which I will describe in detail along with the 
additional processing necessary before ultimately saving the results to the database and marking the job complete. The 
CLI in the mean time can be used to get job status, details, and completion information. 

UIMA 

The unstructured information management architecture (or UIMA) facilitates the analysis of unstructured content including 
text, audio, and video. As an architecture it organizes the interaction of component parts. The component parts have their 
interaction standardized by the architecture, however each component in the pipeline has freedom in itself to perform 
analysis and annotation of the content in anyway it needs to. 

Input Text File 

Inputting the text file may be the simplest of procedures we review in UIMA. In fact the only remotely difficult parts are the 
CLI being able to send the text and the API being able to receive the text. These processes are common in web 
applications and will be supported by the Web 2.0 technology Java Spring Boot. The text sample we are looking at is from 
a short story “Aloha Oe” written in the “The House of Pride” book. http://london.sonoma.edu/writings/HousePride/ 

This sample will be used in the rest of the presentation. 

Pronominal Resolution 

Following the input of the text file, the process of Pronominal Resolution will take place. In this step the process is to 
identify all the subjects (nouns) for each corresponding reference (pronouns). The pronouns being replaced with their 
corresponding nouns is shown in red in the sample text.  

Part of Speech Tagging 

After the pronominal resolution, the next step is to do part of speech tagging (or POS tagging). In this task identification is 
made of the nouns, verbs, adjectives, pronouns, adverbs, etc. The POS tagging is done using grammar rules of language 
and machine learning models for choosing between multiple possible classifications for a word. 

Lemmatization 

After part of speech tagging the next step is to do lemmatization. This process removes the inflectional ending of words in 
the text, leaving just the base or dictionary form of the words in the text, also known as lemmas. 

Parsing 

After the lemmatization the next step is to do parsing. The part of speech tagging done previously enables the parsing.  

Semantic Role Labeling -> Frames 

Next is the semantic role labeling process which identifies the arguments and modifiers associated with each verb in each 
sentence? The verb, arguments and modifiers are used to compose frames. Each frame has a single action verb and 
associated arguments and modifiers as fillers in semantic role slots. 

Synset Identification 

The last process within the UIMA pipeline is to do synset identification. This process identifies all the synonyms for the 
verbs and the nouns within the frames.  The source of the synonyms is WordNet, a freely available standard corpus from 
Princeton University. 

Entailment Scoring -> Graph 

The frames that come out of the UIMA pipeline are then used in the creation of a graph with links between all frames with 
scoring applied to the links based on the two frames they connect.  The scoring is done based on the similarity of the 
verbs and nouns in the connected frames, including the usage of the synsets in the similarity comparison. 

Segmentation (Unsupervised Cluster) 

Once there is a fully scored and connected graph the segmentation process commences. First the links with a 0 score are 
all deleted. Then the remainder of the links are used in an unsupervised clustering algorithm to create segments of related 
frames. 

Centroid Frame Extraction 

Each segment then has one or more representative frames identified based on its position relative to the center location of 
the segment it is within. The representative sample will be at least one or more frames. 

Parameterized Permutations 

At this point in order to create a large variety of alternative summaries the before mentioned processes are iterated over 
with a variety of parameters. Those processes include: entailment scoring, segmentation, and centroid frame extraction.  

Tutoring Path Creation 

The optimum summary data is used as the root from which all other summaries from the previous process are either 
directly connected or indirectly connected through a tree of summaries expanding from the root. The edges in the 
resulting graph are identified by the parameterization used to create the associated summaries so that guidance can be 
created based on it to explain how to navigate to the optimum summary. 

Store in Database 

The entire graph of generated summaries along with their connections is stored into the database. At this point we have 
completed the generation of the knowledge domain model.  

User Interface 

We now change our view of the application to that of the student user being tutored. The user interface for this project will 
be very basic. At the top will be the window for the intelligent tutoring system to communicate to the student. The center 
window will be for the student to enter his or her summary. A button to the right of the window in the center will allow the 
student to submit the summary he or she writes. Finally the window at the bottom has the source text being summarized. 

Use Case: Tutoring the User 

This is the use case for the usage of the user interface. First the student user will visit the web application over the 
internet. This will load the user interface into their browser. The student user will be shown an initial greeting by the ITS 
and a text for summarization. The student will then write their summary and submit it. There will be an initial rejection for 
non-sense summaries, however if the summary seems realistic then it will be processed by the same pipeline used for 
creating the summaries previously, however this time the output will not be parameterized or saved, rather it will be used 
to search for a similar summary in the pre-computed knowledge domain model. The summary most similar to the input will 
have its associated guidance extracted and returned to the student user web interface for the process to continue until the 
student writes a summary that matches the optimum summary in the knowledge domain model. 

Thanks 

Thank you for taking the time to review my presentation. I look forward to your feedback. 

