Assignment 4 - Proposing Your Work 

Ashwin Hamal 

 
The purpose of this paper is to explore a better communication tool that will help students and 
project members efficiently initiate, collaborate and complete a project. 
 
In the previous papers we talked about different 4 different phases of communication during any 
project: Informational, Idea, Planning, and Execution. We also discussed 2 types of information 
during communication: Persistent, and Transient. Persistent being the picture on everybody's 
mind, and transient being the small dynamic information we pass when we communicate. And the 
problems we face with current communication platforms - Discovery, Localization, Dependency 
on Internet, 'Heat', and Persistent location. In this paper, we will discuss a design that could 
possibly address these issues. 
 
We will solve the issue by creating a web application that would require a modern web browser, 
and the internet. In this design, there are two main pages: 
 

● The Map Page 

The purpose of this page is to allow users to discover channels(chat rooms) or other uses, 
there will be a map, similar to google maps, users can type in their address, filter for 
specific channels, navigate the map. They are also allowed to create a private or a public 
group in this page, which would be as simple as dropping a pin, and naming it. Users can 
also toggle to a list view, if they don't prefer a map. 
 
The map/list solution is a popular solution for local discovery​1​. And is easily achievable by 
embedding a third party solutions, like Google Maps ​2​, or Openstreetmap for the display, 
and open source GeoSpatial database ​3​ for storing and retrieving channel/user 
information. 
 
 Once they click a channel, they are taken to the channel page. 
 

● The Channel Page 

The purpose of the channel page is to allow users to communicate dynamically. The 
channel page will have a permanent url that'd always remain the same for a particular 
channel. This way users can share the URLs. 
 
The channel page will be divided into two screens. The persistent screen and the transient 
screen. 

a. The persistent screen​'s purpose is to carry persistent information. A diagram, a 

document, shared code, a todo list, or anything that's a shared information among 
users. Since it might not be practical to build a complete application from ground 
up, for our current purpose we could embed in a third party application such as 

Google doc, Google draw,​draw.io​, or ​codeshare.io​. Ideally, the admin will be able to 
lock this screen, and allow only certain users to make modification. There can be 
multiple apps embedded in multiple tabs on a channel.  
 

b. The transient screen​'s purpose is to allow users to communicate on a personal, 

small, curiosity, question or discussion level. It could also be described as a screen 
that allows users to help each other. Primarily this screen will look like a most chat 
rooms, where we additionally embed in features like video or audio chat. We add 
could add in a simple way to track down what questions were being asked, who 
were the questions directed to and whether they were answered or not. The chat 
texts would not be stored for a long term so that the interaction casual and relaxed, 
so people can freely ask, answer or suggest. 

 
Localization and WebRTC 
Since most of the world still lacks a reliable internet for interactive apps​4​, in order to be applicable 
all around the world, our solution should have little to no dependency on the internet. We can use 
two approaches to solve this problem. 

● The centralized server will only act like a directory of the computers. All interaction 

happen among computers via WebRTC ​5​, which is a cutting edge peer-to-peer technology 
that's available on modern browsers. This way the communication to the server will come 
down to as less as a couple of kilobytes. And the direct peer-to-peer connection means 
much less latency among local apps. 

● In cases where there is no internet connection available, we could make the server 

lightweight, and allow users to deploy servers on their local machine, that'd allow them 
access to same apps and features without the internet. 

 
Promoting good behavior 
A friendly interaction is necessary to have any successful project. And to promote it we could take 
several approaches 

● Make the look and feel user friendly and relaxed. 
● Giving channel admins privileges of banning users from their channel. 
● Implement some kind of a point system, similar to Credit system by lending companies, 
● Having guidelines, reminding users time and over of what is a good behavior, and why is it 
● Allowing channels to be personalized, allowing themes, so the users feel familiar, and at 

that's effective yet forgiving.​6 

so necessary. 

ease. 

 
 
References 

1. Examples of List/Map services: Zillow ​http://www.zillow.com/homes/New-York-NY_rb/​ , 
2. Google Maps for Developers: ​https://developers.google.com/maps/  

AirBnB ​https://www.airbnb.com/s/Philadelphia--PA  

3. Geospatial Database: MongoDB 

https://docs.mongodb.org/manual/applications/geospatial-indexes/​ and PostgreSQL 
http://postgis.net/  

4. World Development Report 2016. Page 6. Digital Divides 

http://www-wds.worldbank.org/external/default/WDSContentServer/WDSP/IB/2016/0
1/13/090224b08405ea05/2_0/Rendered/PDF/World0developm0000digital0dividends.p
df  

5. WebRTC FAQs: ​https://webrtc.org/faq/ 
6. FICO Score ​http://www.investopedia.com/terms/f/ficoscore.asp  

 

