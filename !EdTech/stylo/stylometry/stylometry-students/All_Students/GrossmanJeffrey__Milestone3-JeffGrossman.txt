Jeffrey Grossman 

CS6460 Educational Technology 

Milestone #3 

From Proposal:  

Milestone 3 (Due 4/3/2016): Progress Report #2 – Provide information about the results of the data 
analysis that has been conducted. Also, provide information about the different group formation 
techniques that will be experimented with. (EDIT: I have decided to include a draft of my group 
formation section instead of just an overview of the group formation techniques) 

Background (for peer reviewers): Just to bring you up to speed so the following makes sense, my 
project is a research project about the group formation process for team based projects in the OMSCS 
curriculum. I took a survey of current and past OMSCS students to determine which attributes of 
students should be considered when forming the groups and how they should be distributed. I received 
91 results and am now going to close the survey so I can begin the data analysis process. For this 
milestone I will discuss the results of the analysis I performed on the data and the group formation 
techniques that I will be employing. 

Thus far I have completed drafts of the following parts of my research paper: 

Introduction  

1.  Literature Review 
2. 
3.  Data 
4.  Results  
5.  Group Formation Techniques 

This is in line with my proposal. Remaining pieces are: 

6.  Conclusion/Discussion 
7.  Abstract 

Data Analysis Results Overview: 

To analyze the data that was obtained through the survey for this project, I looked at each attribute and 
took the average and standard deviation of the results for the importance of that attribute and the 
desired distribution of people with that attribute. I will outline the main conclusions and interesting 
tidbits that I drew from the analysis: 

1.  All valuable attributes should be homogeneously distributed 

I found it weird that there was not a single “important” attribute that on average was voted to 
be distributed with varying values within a group. The only one that slightly went this way was 
gender, but that had very little importance so the distribution is irrelevant. The main takeaway 

from this conclusion is that people want to work with people that have a similar skillset and 
experience. The attribute I would have expected to tend towards varying values would have 
been writing skills, as it is always valuable to have one or two people with really good writing 
skills within a group. However, almost 50% of people voted for “Same Value”, and only 31% 
voted for “Varying Values.”   

2.  The most important skill to have in a group is writing skills, with the least amount of variance 

In my OMSCS experience, a lot of group projects have been writing intensive and represent a 
large portion of the work, thus this result is not very surprising. However it does indicate to me 
that a lot of the OMSCS projects might want to rethink how writing intensive they are. Around 
86% of people ranked this a 4 or greater (out of 5) on the importance scale.  

3. 

It is most important that hours per week be homogeneously distributed 

Although participants indicated that all of the attributes mentioned, or at least the important 
ones, should be homogeneously distributed, the one that was the most extreme was the “hours 
per week” attribute. 69% indicated that people should have the same value for this attribute 
and 19% indicated that people should have varying values within the same group. This was also 
reflected in several write-in comments and the final question about the most and least 
successful groups.  

4.  Gender is the least important attribute when considering group formation 

Not a very surprising result, especially when most communication is done through text. I’ve had 
projects where I didn’t even know the gender of my teammates.  

5.  Gender was the only attribute that was more heterogeneous than homogeneous 

I discarded statistics that had an average importance of less than 2.5, so the distribution was not 
too important for the gender attribute.  

6.  These values are transformed into weights to evaluate groupings 

As I will discuss in the group formation section, I used the average importance of each attribute 
as a weighting to determine how much emphasis should put on a student’s value for that 
attribute. 

7.  Two attributes most prevalent in a successful OMSCS group project were experience with a 

specific language and hours per week spent on the project 

These two attributes far surpassed any other responses to this question getting 69% and 73% of 
the votes respectively. It is interesting that on average most people said writing skills were the 
most important attribute, but in their experience it didn’t play the biggest role in their most 
successful project. It still had a significant impact (46% of respondents indicated writing skills). 

8.  Attribute most prevalent in an unsuccessful OMSCS group project were hours spent per week 

Number of hours spent to week received 59% of the votes in this question, with the second 
replace response receiving only 27% (experience with a specific programming language). Note 
that for this question and the previous question, multiple answers were allowed. To me, this is 
the most significant data point from the entire survey. However, it is difficult to apply this metric 
to group formation as asking students how many hours they plan to dedicate to a project would 
likely result in some untrue responses, especially if they knew they would be matched up with 
people with similar answers. It would be the equivalent of asking people if they want to work 
with dedicated, hard workers or people that just wanted to scrape by. Maybe some creative 
wording would elicit more useful responses from students in a survey to form a group for a new 
project.  

9.  81.1% Think group projects should remain in OMSCS classes 

This question was meant more as a sanity check as to whether this research was necessary. 
Clearly people find group projects valuable and they should remain in the curriculum. However, 
several write-in comments suggested that every group project should have the option to be 
done individually (similarly to EdTech). This is a good idea, but obviously creates complications 
in terms of scaling workload. Do individuals need to complete the same project or a smaller 
sized one? Having professors/TA create multiple instances of a project depending on the group 
size sounds a bit realistic, but maybe there is a workaround.  

10. 91 responses to the survey 

91 responses seem like a reasonable amount of data to perform the analysis I wanted to. I 
probably could have pressed a bit further on the distribution end and posted the survey to each 
class’s Piazza board, but I felt like that would have felt like spam. I’m satisfied with the amount 
of responses I got.  

11. 81.3% had an A average in OMSCS 

This seems a bit high, but with no baseline for the distribution of grades amongst all OMSCS 
students, I have no way to tell. I was able to verify that the A students did not answer the 
attribute questions any different than the other students in this survey. The averages for 
importance and distribution were pretty similar across different grades.  

12. Group projects beneficial average= 3.41 / 5 

On average, group projects are clearly well received. I don’t think anyone is going to doubt that 
improvements can be made, but there are no drastic problems. I believe better group formation 
is one of the lower hanging fruits that can help to improve acceptance and results of group 
projects immediately.  

 

Draft of Results Section: 

From the data that was collected via survey, several conclusions can be drawn. This data was 
analyzed to determine any patterns in what students believe are the most important attributes to 
consider when forming groups and how those attributes should be distributed. For each attribute that 
was asked about, the average and standard deviation of the importance and distribution metrics were 
calculated. Attributes with under an average of 2.5 on the importance scale were discarded which left 7 
attributes that were deemed to be important by students. The 4 attributes that fell below this midpoint 
of importance were number of OMSCS classes taken, age, nationality, and gender. The 7 that were 
above the threshold were writing skills, experience with an applicable programming language, hours 
dedicated per week, time zone, prior group project experience, experience with online communication 
tools, and GPA in OMSCS. The threshold of 2.5 was used because 1 was labeled “Not Important” and 5 
was labeled “Very Important” therefore the midpoint of 2.5 would be where students were exactly 
mixed between not important and very important. The term variance will be used to describe how 
consistent student responses were, and will refer to the standard deviation of the response values.  

The attribute that was voted as the most important was writing skills. This attribute also had the 

least amount of variance in responses. It received an average importance score of 4.13 out of 5. Many 
OMSCS group projects have a deliverable that consists of a written paper, and thus it makes sense that a 
group must consist of a certain level of writing skills. The least important attribute was gender, which 
had a score of 1.56. It is probable that this attribute is less important in distance based projects than it is 
in physical classroom projects, but also probable that it is not very important in either situation. The 
other three least important attributes (age, nationality, and OMSCS classes taken) were common to one 
another as they did not involve a skill, rather were an easily measurable characteristic of an individual.  

An interesting result from the distribution questions was that for all of the attributes that were 

deemed important from the aforementioned threshold, the average distribution response leaned 
towards a homogeneous distribution. The only attribute that averaged a more heterogeneous 
distribution was gender, which was the least important attribute to consider. The attributes with the 
highest homogeneous distribution were hours per week dedicated to the project and time zone. From 
both the quantitative and qualitative (written response) section of this survey, it was clear that students 
found it very important that members in each group had a similar level of dedication to the project. In 
one of the final questions, students responded that the attribute that played the biggest role in their 
least successful project’s lack of success were the number of hours per week dedicated to the project. 
58.5% of students selected this option, implying that it is incredibly important to have students equally 
dedicated to a project to avoid failure and frustration.  

The opposite question was also posed, asking students for the attributes that played the biggest role 

in their most successful OMSCS project, to which the top scoring answers were experience with a 
specific programming language and hours per week dedicated to a project. Seeing the same response 
(hours per week) appear as a major contributor in a project’s success and failure, places even more 
emphasis on the importance of using this as a balancing factor amongst groups. Experience with a 

specific programming language is something that should also be strongly considered as many OMSCS 
projects provide groups with a choice of languages, and most require heavy programming skills.  

The results from this survey, specifically the attribute importance and distribution responses, will be 
converted into weighting criteria in order to develop a group formation technique algorithm that can be 
utilized by professors. Each class will obviously stress different attributes, but the general process for a 
constraint based algorithm will be proposed in the following section. 

91 students responded to this survey and 81.8% of them believed that group projects should remain 

in OMSCS classes. Additionally when asked whether these group projects were beneficial, the average 
score was 3.41 out of 5. These results indicate that projects of this nature are valuable pieces of the 
OMSCS curriculum; however several qualitative responses suggested that students be given the option 
to complete the projects independently if they choose.  

Most of the demographic information collected from this survey seems to represent a well 

distributed response pool. The only distribution which seemed suspect was that 81.3% of students 
indicated that they have an average grade of an A so far in OMSCS. No information was available to 
determine whether this was the actual distribution of students outside of the sample size of the survey, 
however, statistical tests from the responses of the survey indicate that there were no significant 
differences in the attribute responses from students with higher or lower grades.  

 

 

Group Formation Techniques Draft: 

Application of the results that were drawn from this survey will be relatively specific to the 

 
actual project at hand. Determining which attributes are important in a particular project might differ 
greatly from the overall broad results of this survey. However, regardless of what attributes are deemed 
valuable in creating groups, the missing link between this survey data and the actual group project are 
the techniques or methods used to apply this data in the formation of the student groups. There are 
massive amounts of different ways to divide students into groups using anything from advanced artificial 
intelligence and constraint based algorithms to random sampling. This could easily be a topic for a 
completely separate research project. For the sake of completeness, three relatively simple group 
formation methods were simulated on artificial student data in order to demonstrate the application of 
this research. These are far from optimal techniques, but are far better than some of the methods that 
are currently being used.  

 
Before testing out any formation techniques, a mechanism for scoring groups was developed. In 
order to compare different techniques, there needs to be a quantitative mechanism to see whether one 
set of groups is better distributed than another set of groups. Only attributes that had an average 
importance over 2.5 (7 of them) were considered for this analysis. All of these were voted to be 
distributed homogeneously. A fake set of students were generated with values between 1 and 10 for 
each of the 7 attributes. These were distributed normally around a mean of 5 and a standard deviation 
of 2. Once each student was assigned into a group, each attribute of each group was scored by taking 
the standard deviation of the attribute’s values within the group. This value was summed across each 
attribute to create one score per group, and then summed across the set of groups to create one score 
for the entire class distribution.   

Depending on the size of the class and the computing power available, brute force might be a 

 
reasonable option. Iterating through every possible combination of groups within a class and taking the 
one that gives the lowest score could be a realistic approach. However, as class sizes increase, this 
method quickly becomes unrealistic. For example, choosing all possible combinations of 5 person groups 
from a class of only 50 students would yield over 2 million combinations, so it is clear to see how this 
approach could get unreasonable with OMSCS students of over 200. A very simple and similar strategy 
would just be to randomly sample sets of groups from a given class with a desired size and test how 
good of a score can be achieved. Depending on how much time and computational power is available, 
this could get a decent score. 

 
The next technique that was attempted was to randomly sample sets of groups and then pull 
out the best individual group that was found in all of the sampling. Then repeat the same process with 
the remaining students (minus the group that had already been formed). Continue this process until 
there are no students remaining and each extracted group will make up the final set of groups. The clear 
negative of this approach is that the remaining students to choose from might have no low scoring 
subgroups to choose from since more “useful” students had been extracted. This negative observation 
led to the final group formation technique that was employed. 

For the final technique, instead of choosing the lowest scoring group from all of the samples, the 

 
lowest scoring set of groups was selected, and then the lowest scoring group from that set was 
extracted. This method would avoid the issue of leaving high scoring groupings in the remaining student 
pool, while still extracting the best scoring group from each sampling round. Although these three 
methods have a high degree of randomness and could easily perform differently on any dataset, the 
results employing all three methods on the same dataset were pretty consistent. The worst performing 
method was obviously the first, which was to randomly sample entire sets of groups. Surprisingly the 
best method was to extract the best group from any set of groups and set that aside (the second 
strategy discussed), rather than extract the best group from the best set of groups (the third strategy 
discussed). Both of these performed significantly better than complete random sampling. Again, there 
are clearly more advanced algorithms that can be employed on actual student data in order to find more 
optimal groupings, but the amount of improvement that could be experienced by using the simple 
methods mentioned above could drastically increase the success of group projects in OMSCS.  

 

