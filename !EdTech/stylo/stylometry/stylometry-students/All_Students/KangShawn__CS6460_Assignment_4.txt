Shawn Kang 
skang313@gatech.edu 
February 7, 2016 

Assignment 4 

Proposing Your Work: Intelligent Tutoring Systems 

     Intelligent tutoring systems (ITS) are computer based systems that focus on 
providing real time and customized instruction and/or feedback to students more 
commonly without the assistance of a human instructor. ITS have been used in both 
formal education and in the professional setting for many years, first originating in the 
artificial intelligence (AI) movement of the late 1950s and early 1960s. 

Research shows that ITS are growing in popularity since they are able to increase 

performance in students, leverage cognitive development, and significantly decrease 
the time it takes to acquire knowledge and become proficient in a specific area. Modern 
day ITS essentially try to replicate the role of a teacher or a teaching assistant and 
usually entail problem generation and intelligent automatic feedback generation with a 
high recall value. However, ITS do have their pros and cons.  
The benefits of ITS are:  

• Are available at any time of the day 
• Provide real time data to instructors and developers looking to fine tune various       
   teaching methods 
• Reduce dependence on humans 
• Help students understand the subject better (allow student to explain what they   
   know and then the system can cater responses accordingly)   
• Allow instructors to create individualized programs for each student    
• Students tend to produce higher test scores than in traditional systems 
• Provide immediate feedback, refine the program, real time hints, and  
   support for mastering the topic 

CS6460

!1

The potential downsides of ITS are:   

• It is hard to accurately evaluate the effectiveness of a program  
• Real time feedback and hints could potentially fail to prompt a deeper learning in  
   some students 
• Systems can possibly fail to ask questions to students that might explain a  
   student’s action  
• ITS may eliminate administrative staff 
• Evaluating ITS can be difficult, expensive, and time consuming 
• Human tutors may be more suitable to provide appropriate dialogue and  
   feedback to students 
• Human tutors may be better able to adapt to the different emotional states of a  
   student 

     For the purpose of this assignment, I am going to focus on ways to improve ITS-
student interactions or to explore the various artificial intelligence (AI) techniques used 
to recreate how a problem is solved. Most current technology is limited to presenting a 
problem to a student and then testing their recall by way of multiple choice questions. 
As previously mentioned in the potential downsides of ITS, they are not proficient in 
giving intelligent, individualized coaching; performance assessment; and the appropriate 
feedback to students which would allow for a deeper understanding of the online course 
material. In addition, I will examine ITS that are able to evaluate various metacognitive 
indicators or attributes (self-explanation, gaming the system, self-monitoring, and help-
seeking) and then share this valuable information with other learning tools.  
     From the perspective of a student, one way to improve the ITS-student interactions 
is to better gauge a student’s progress when taking online courses. While using Udacity, 
I personally thought it would be beneficial to have access to additional learning 
resources embedded in the tutorial videos. In order for a student to have a more 
engaging learning experience, I propose the following enhancements:  

• Include links embedded in quizzes that will lead a student to additional earning 

resources (lecture notes and/or Wikipedia) for additional clarification. If a student 
answers a question and gets it wrong, then a link will appear that gives an option 
for the student to click on it and determine why they got that particular question 

CS6460

!2

wrong. A link could also appear if a student gets the question right so that they 
have the option to learn more on the topic or to get additional clarification. 
Receiving immediate feedback and being able to review the material in real time 
has the potential to help improve learning and lessen a student’s weakness in a 
course. By learning why one gets a question right or wrong can help a student 
retain the information better.  

• Improve how a student’s progress is gauged in the course. I suggest gauging the 
progress of all students enrolled in a course, such as in quizzes in Udacity being 
ranked in terms of the level of difficulty. A couple of features that I think would be 
beneficial are: listing the percentage of students who took a particular quiz during 
that semester, listing the percentage of students that got a particular question 
right on a specific quiz, and determining the level of difficulty of a quiz based on 
the aforementioned information. For example, if students in the current semester 
got a specific number of answers correct on a quiz, then the level of difficulty of 
that quiz can be gauged and displayed in the form of a percentage when the quiz 
is listed. This will help students better monitor themselves and their progress as 
compared to other students in the course. Real time progress of a student’s 
progress can be very helpful in the learning process. This can also assist 
professors as well since it can help them design better quizzes that are more 
representative of the course material. This sort of feature will shorten the trial and 
error learning process for both students and professors and can help design a 
better course overall.  

     Research shows that metacognition improves effective learning and that students 
show significant gain (same level of proficiency as compared to traditional instruction in 
one-third the time). Several instructional programs focused on improving metacognition 
have been shown to be successful. In addition, there has recently been a surge in ITS 
with metacognitive support. In contrast to a comprehensive program of metacognitive 
instruction, these systems have been most successfully implemented using computer-
based tutoring of specific metacognitive skills. I will continue to explore the effects of 
these forms of metacognitive support throughout the course.  

CS6460

!3

     There is a close relationship between intelligent tutoring and cognitive learning 
theories and design. ITS is an ever changing arena and there is currently ongoing 
research to improve the effectiveness of ITS. ITS serve to resolve the problem of 
students being too dependent on instructors for a quality education. By providing access 
to high quality education to all students, ITS can help change the education system for 
the benefit of all. 

References 

Intelligent Tutoring System. (n.d.). Retrieved February 5, 2016, from https://

en.wikipedia.org/wiki/Intelligent_tutoring_system 

Intelligent Tutoring Systems. (n.d.). Retrieved February 5, 2016, from http://

www.cse.msu.edu/rgroups/cse101/ITS/its.htm  

Intelligent Tutoring Systems: Can They Work for You. (n.d.). Retrieved February 5, 
2016, from http://www.opencolleges.edu.au/informed/trends/intelligent-tutoring-systems/ 

New Potentials for Data-Driven Intelligent Tutoring System Development and 

Optimization. (n.d.). Retrieved February 5, 2016, from http://www.columbia.edu/
~rsb2162/New potentials for ITS-source.pdf 

Chi, M., & VanLehn, K. (2010). Meta-Cognitive Strategy Instruction in Intelligent 

Tutoring Systems: How, When, and Why. Retrieved February 5, 2016, from http://
www.ifets.info/journals/13_1/4.pdf 

CS6460

!4

