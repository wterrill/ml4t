Background	
Problem	Hypothesis	
At	scale,	a	combined,	open	active	learning	and	retrieval	practice	framework	may	serve	any	
student,	at	any	level,	learning	any	new	concept,	within	structured	learning	environments.	These	
students	could	be	part	of	a	traditional	classroom,	a	MOOC	(such	Coursera),	or	a	distance	
education	program	(such	as	the	OMSCS).	
	
These	students	would	achieve	better	learning	outcomes,	as	defined	by	better	verbal	recall,	
inference,	and	problem	solving	for	immediate	and	spaced	retrieval,	by	using	an	active	learning	
framework	internalize	material	from	educational	content	resources	coupled	with	retrieval	
practice	to	commit	learning	to	long	term	memory.	
	
Solution	
The	proposed	solution	will	be	an	online	app	that	contains	an	open	interface	to	ingests	content	
from	free	online	resources	into	a	defined,	structured	learning	plan	that	facilitates	reflection	and	
feedback	and	fosters	retrieval	practice.	
	
The	app	will	integrate	with	popular	video	and	text	repositories	(YouTube,	Vimeo,	Bookboon)	to	
import	content	into	expert-created	learning	plans,	bundles	of	content	representing	related	
concepts.	These	learning	plans	can	leverage	existing	institutional	curricula	(such	as	the	lessons	
and	mini	courses	in	an	OMSCS	Udacity	course),	open	curricula	(such	as	Curriki),	and	user-
generated	curricula.	
	
The	app	will	foster	active	learning	by	asking	the	student	to	rephrase	and	summarize	material	
they	covered	in	a	specific	plan	as	well	as	provide	a	mechanism	to	solicit	feedback	on	their	
summation	from	their	peers.	
	
The	app	will	then	foster	retrieval	practice	by	asking	a	student	to	create	a	set	of	flash	cards	
which	they	share	publically	for	the	same	lesson	plan,	with	a	mechanism	to	export	to	existing	
spaced	repetition	programs	
	
Scope	of	Minimal	Viable	Product	(MVP)	
Constrained	by	100	hours	and	starting	without	any	customers,	the	MVP	will	be	based	on	
OMSCS	curriculum.	These	courses	will	be	identified	as	part	of	the	first	interim	deliverable	based	
on	the	following	criteria:	

•  Potential	users	interest	in	the	course	
•  Availability	of	video	content	on	YouTube	

	
This	approach	reduces	complexity	of	implementation	to	familiar	courses,	allowing	me	to	act	as	
an	expert	curator	(essentially	just	copying	the	existing	course	structure)	and	helps	to	start	to	
prove	out	assumptions.	Using	the	OMSCS	curriculum	as	a	test	board	also	allows	for	the	

flexibility	of	recruiting	target	users	in	a	friendly	environment	–	students	comfortable	with	online	
learning	and	peer	feedback	in	the	6460	forum.	
	
Previous	Documentation	
Project	proposal		
https://gtvault-
my.sharepoint.com/personal/tabonil3_gatech_edu/_layouts/15/guestaccess.aspx?guestaccesst
oken=zRgO2ECGoN4gtTq8Ce%2fFA4B%2brbau3QajAibTCuC%2bX0o%3d&docid=055b679d6756
941c486d2f10a1b5d9b32	
	
Initial	Mocks:	
https://drive.google.com/file/d/0B-IBIYMb4KHITmFzbU42S3ZRc2s/view?usp=sharing	
	

Intermediate	Milestone	2	Deliverables	
		
Identify	Beta	Testers	
A	post	on	Piazza	followed	by	private	email	exchanges	has	yielded	the	following	list	of	beta	
users:	

•  mlynch42@gatech.edu		
•  Piazza	user	“Mel”	
•  ddurruty@gmail.com	

	

	
Identify	MVP	Courses	and	Data	Structures		
Based	on	feedback	from	the	peers	above	as	well	as	my	own	enrollment	in	the	course,	I’ve	
decided	to	start	off	with	Machine	Learning	for	Trading.		
	
Exploring	the	Udacity	API,	it	became	clear	that	there	would	be	no	easy	way	to	ingest	the	
content	in	the	desired	structure.		While	manually	creating	the	structure	is	possible,	I	would	
have	to	manually	visit	300	pages,	copying	element	information	from	each.		As	such,	I	created	a	
scraper	to	parse	the	course	structure	and	retrieve	the	YouTube	videos.		The	scraper	can	be	re-
used	in	the	future	for	other	Udacity	courses.	
	
Scraping	Algorithm		
Note:	that	the	following	description	presupposes	some	background	in	web	development.		
Please	feel	free	to	reach	out	if	you	don’t	have	this	background,	but	would	like	to	understand	
the	flows	better.	
	
Starting	with	the	“introduction”	page,	the	scraper	identifies	links	to	each	lesson	by	“clicking”	on	
the	appropriate	links	to	each,	yielding	the	following	links:	
	

https://www.udacity.com/course/viewer#!/c-ud501/l-4315658577/	
https://www.udacity.com/course/viewer#!/c-ud501/l-4005908569/	
https://www.udacity.com/course/viewer#!/c-ud501/l-3975568860/	
https://www.udacity.com/course/viewer#!/c-ud501/l-4134798720/	
https://www.udacity.com/course/viewer#!/c-ud501/l-4156938722/	
https://www.udacity.com/course/viewer#!/c-ud501/l-4156938722/	
https://www.udacity.com/course/viewer#!/c-ud501/l-3909458794/	
https://www.udacity.com/course/viewer#!/c-ud501/l-4179049354/	
https://www.udacity.com/course/viewer#!/c-ud501/l-4351588706/	
https://www.udacity.com/course/viewer#!/c-ud501/l-4340498935/		
https://www.udacity.com/course/viewer#!/c-ud501/l-4383750632/	
https://www.udacity.com/course/viewer#!/c-ud501/l-4371378234/		
https://www.udacity.com/course/viewer#!/c-ud501/l-4389588610/		
https://www.udacity.com/course/viewer#!/c-ud501/l-4432279076/		
https://www.udacity.com/course/viewer#!/c-ud501/l-4439568998/		
https://www.udacity.com/course/viewer#!/c-ud501/l-4441149454/		
https://www.udacity.com/course/viewer#!/c-ud501/l-4442578629/		
https://www.udacity.com/course/viewer#!/c-ud501/l-4461768666//		
https://www.udacity.com/course/viewer#!/c-ud501/l-4631870412/	
https://www.udacity.com/course/viewer#!/c-ud501/l-4681618542/		
https://www.udacity.com/course/viewer#!/c-ud501/l-4684695874/		
https://www.udacity.com/course/viewer#!/c-ud501/l-4838329499/		
https://www.udacity.com/course/viewer#!/c-ud501/l-4802891095/		
https://www.udacity.com/course/viewer#!/c-ud501/l-4802710867/		
https://www.udacity.com/course/viewer#!/c-ud501/l-4930572236/	
https://www.udacity.com/course/viewer#!/c-ud501/l-5247432317/		
https://www.udacity.com/course/viewer#!/c-ud501/l-5326212698/		
	
For	each	lesson,	server-side	DOM	manipulation	yields	the	desired	structure	based	on	the	
following	approach	(“$”	is	an	operator	for	a	DOM	manipulation	library	similar	to	JQuery):	

•  To	get	lesson	title	$('button.btn.btn-default.dropdown-toggle.ng-binding').text()	
•  To	get	all	node	ids	$('li.subway-nav-station')		
•  To	get	all	node	titles	$('li.subway-nav-station	span	span')		

	
On	each	node		

•  To	get	video	URL	$('iframe.ng-scope').attr.src		

	
While	the	approach	seams	simple,	Udacity	uses	Angular	JS,	a	front-end	framework	that	
heavily	relies	on	client	side	DOM	manipulation.		This	means	that	Udacity	web	pages	
must	be	rendered	fully	by	the	client	before	any	usable	content	is	available.		To	
accomplish	this	goal,	the	server	must	utilize	a	headless	browser	to	render	each	page	

completely	in	memory	before	scraping	the	DOM.		This	feat	was	accomplished	by	using	
PhantomJS	–	a	headless	webkit	implementation	–	behind	a	node	js	server.		The	server	
utilizes	asynchronous	calls	to	retrieve	and	render	each	lesson,	and	each	node	on	each	
lesson,	utilizing	deferred	promises	for	the	control	flow.		The	memory	footprint	during	
the	scraping	is	over	8GB;	however,	scraping	should	only	happen	infrequently	in	the	
future.	
	
The	resultant	JSON	data	structure	looks	like	the	following:	
	
{	
				"_id":	{	
								"$oid":	"56ef0459ddd3d0f92d7b36ec"	
				},	
				"title":	"Machine	Learning	for	Trading",	
				"lessons":	[	
								{	
												"nodes":	[	
																{	
																				"video":	
"https://www.youtube.com/embed/s5xKxliBMTo?autoplay=1&color=red&controls=1&rel=0&s
howinfo=0&fs=1&theme=light&wmode=opaque&html5=1&enablejsapi=1&origin=https%3A%2
F%2Fwww.udacity.com",	
																				"content":	null,	
																				"title":	"Three	parts	to	the	course",	
																				"url":	"https://www.udacity.com/course/viewer#!/c-ud501/l-4315658577/m-
4272730277",	
																				"key":	"4272730277"	
																},	
																{	
																				"video":	
"https://www.youtube.com/embed/iATS6Qao9qM?autoplay=1&color=red&controls=1&rel=0&
showinfo=0&fs=1&theme=light&wmode=opaque&html5=1&enablejsapi=1&origin=https%3A%
2F%2Fwww.udacity.com",	
																				"content":	null,	
																				"title":	"Three	parts	to	the	course",	
																				"url":	"https://www.udacity.com/course/viewer#!/c-ud501/l-4315658577/m-
4272730277",	
																				"key":	"4272730277"	
																}	
												],	
												"title":	"\n\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t00-00	Introduction\n\t\t\t\t\t\t\t\t",	
												"lesonId":	"l-4315658577",	
												"course":	"c-ud501",	

												"url":	"https://www.udacity.com/course/viewer#!/c-ud501/l-4315658577/"	
								}	
				],	
				"__v":	0	
}	
	
We	now	have	a	JSON	object	that	contains	all	the	information	required	to	recreate	the	Udacity	
course	structure	locally.	

