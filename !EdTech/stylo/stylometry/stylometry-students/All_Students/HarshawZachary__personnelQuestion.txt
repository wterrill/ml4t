Harshaw, Zachary GTID#903049739  

Personal Question: 

You mentioned Obama's computer science initiative. Hypothetically, if you were hired as a technical 
consultant for a K-12 school district and given $100,000 to spend, how would you spend the money to 
bring computer science education to that school district? Base your decisions on research you find 
supporting a particular approach. 

Given a tight budget of 100,000 dollars there are still many ways that computer science 

 
education can successfully be brought to a school district.  There are three major aspects that I believe 
would need focused on to successfully bring computer science education to a given school district.  First 
a focus must be given to the faculty that would be responsible for teaching the courses.  Second, the 
courses available to students must not be too narrow in topic and must aim to include a diverse group of 
students.  Finally advancements, projects, and research in education technology should be used to their 
fullest to limit costs and assist in delivering content to the students. 

First and foremost one of the most important tasks to handle is to ensure that the school’s 

 
faculty is capable of teaching computer science topics.  It shouldn’t be a surprise that computer science 
teachers are often teaching out of subject and lacking a professional learning community [1].  So what is 
the best way to ensure that teachers within the school are competent enough in computer science and 
have the necessary support of the educational community?  It doesn’t make sense financially to go out 
and hire a teacher willing to specifically teach computer science topics, nor would it be easy thing to 
find.  The best alternative option is to train and support the existing teachers in becoming competent 
computer science teachers.  A common practice of many other fields is to educate teacher and 
sometimes students through a summer program or workshop.  Math and a large majority of science 
teachers may already have background knowledge of computing topics from previous education, so 
these individuals would be the targets for participation in these workshops.  Expecting most teacher to 
take the time out of their busy lives were they may have previous obligations just to attend training 
workshops is something that may not go over too well with some.  So it would be best to motivate 
teachers to attend the workshops with a supplied stipend or small bonus.  Encouragement to join 
groups and programs specifically aimed collaboration and constructionism between computer science 
teachers would also be a key for maintaining materials and resources needed to teach computer science 
course.  

 
Once a stable staff capable of teaching computer science topics is available, the next move is 
motivating students to become interested in the topic.  Motivating “all” types of students to become 
interested in computer science is a common problem not exclusive to the K-12 learning environment.  
It’s commonly known across the world that computer science is typically a male dominated topic.  A 
recent poll of students taking the Advanced Placement Computer Science at Los Angeles high school 
found that, Latino participation comprised only 8 percent of the students and only 1 percent of black 
students in the school district.  For reference, at this school the approximate population makeup was 41 
percent Latino and 8 percent black.  Even more alarming female participation in the exam only came in 

at 18 percent [2].  As can be seen from these numbers the diversity in computer science needs to be 
assisted.  That’s why some of the 100,000 dollars allotted to the district should be utilized to engage 
both female and minority ethnicities within the school district.   

Programs such as “Exploring Computer Science” have found success in achieving interest in 

 
computer science among groups of diverse student.  Unlike traditional courses that may explore only as 
single concept like programming in a certain language, “Exploring Computer Science” tried to present a 
wide variety of interesting, useful, and practical topics from the field of computer science.  The 6 
different topics that are covered in the “Exploring Computer Science” curriculum are as follows Human-
Computer Interaction, Problem Solving, Web Design, Introduction to Programming, Robotics, and 
Computing Applications [3].  A course exploring more of an overview of just a few of the many topics 
present computer science field may have more of chance of achieving student interest in the field.  Due 
to the success of a curriculum like that found in “Exploring Computer Science” program, I would propose 
designing a similar class for the school district as well as maintaining a few more centralized topics 
classes on programming and some other major computing topics.  Once a general computer science 
class constructed and available to the students, changes to the high school curriculum could be made 
make taking at least one single computer science course a requirement for graduation.  This would 
ensure that students at least have some sort of exposure to computer science if they intend to continue 
their education. 

In addition I believe it would be a good decision to also invest a little bit of money into a way of 
 
demonstrating the potential career opportunity that computer science can offer.  Students reaching the 
end of their high school career, who are thinking on continuing their educations at a university, should 
be educated on the job outlooks for computer science student.  In fact, it’s probably best if the job 
outlooks for all types of college majors be explored and not just computer science.  I’d reach out to the 
community to pull in potential candidates to talk to theses students about their careers and experiences 
in their fields.  Most professionals would be willing assist their community and its youth at no cost, so 
this a little bit of bonus.  

The majority of my focus so far has been at a high school level view.  If exposure to computer 

 
science thinking and topics is presented to students at a younger age, then reluctance and disinterest in 
the topic may begin to fade.  Utilizing some educational like games and applications will be one of the 
best routes for attracting younger students (middle school age range) to the beginning concepts of 
computer science.  Existing applications such as “Scratch” have been successfully utilized to teach 
computer science concepts to middle school aged children.  The drag and drop interfaces offers 
exposure to a variety of programming concepts and components such as: expressions, conditions, 
statements, and variables [4].  Utilizing “Scratch” in a school setting would however require a structured 
lesson plan and high-quality learning materials to assist teachers.  In an experiment pairing “Scratch” 
with a detailed lesson plan it was found that “findings showed that meaningful learning process of many 
(but not all) CS concepts occurred as a result of the courses in Scratch with the students performing at 
relatively high cognitive levels” [4].  By utilizing readily available software such as “Scratch”, education in 
computer science can be used to begin the general interest in computer science that will hopefully be 
carried over to some student’s high school careers.  Experiments have also shown that game like 

approaches to learning have increased test scores when compared to applications utilizing a non-game 
like approach [6].  So I do believe that choosing the software that the younger students are using is also 
a key.  The software should be fun, informational and maintain the interest of students. 

As described a large amount of the money allotted would be spent on training and materials for 

 
the teachers.  This fact is unavoidable in most cases and necessary as the teachers will be the ones 
shaping the students interests and thoughts.  Licensing of software and hardware purchases needed in 
teaching computer science topics will also be a major cost as well.  By choosing available free software 
and utilizing artifacts from educational technology the cost  should be able to be mitigated slightly.  
Either way it will be a challenging task to budget 100,000 dollars accordingly to construct competent 
computer science curriculum in any school, but with careful planning and utilization of available 
resources, I believe it is doable. 

 

Bibliography 

[1] Margolis J, Goode J, Bernier D. The Need for Computer Science. Educational Leadership [serial 
online]. February 2011;68(5):68-72. Available from: Professional Development Collection, Ipswich, MA. 
Accessed February 10, 2016. 

[2] Margolis, Jane, Joanna Goode, and David Bernier. 2011. "The Need for Computer Science." 
Educational Leadership 68, no. 5: 68-72. Professional Development Collection, EBSCOhost (accessed 
February 11, 2016). 

[3] Goode, J. and Margolis, J. 2011. Exploring computer science: A case study of school reform. ACM 
Trans. Comput. Educ. 11, 2, Article 12 (July 2011), 16 pages. DOI = 10.1145/1993069.1993076 
http://doi.acm.org/10.1145/1993069.1993076 

[4] Orni Meerbaum-Salant, Michal Armoni & Mordechai (Moti) Ben-Ari (2013)Learning computer science 
concepts with Scratch, Computer Science Education, 23:3, 239-264, 

DOI:10.1080/08993408.2013.832022 

[5] Franklin D. A Practical Guide to Gender Diversity for Computer Science Faculty. Synthesis Lectures on 
Professionalism and Career Advancement for Scientists and Engineers 2013 1:2, 1-81  

[6] Holmboe, C. and McIver, L and Carlisele, G. 2009. Digital Game-Based Learning in high school 
Computer Science education: Impact on educational effectiveness and student motivation. 
http://130.216.33.163/courses/compsci747s2c/lectures/paul/GameBasedLearning_CSEducation.pdf 

[7] DiSalvo, B. and Bruckman, A. 2009. Questioning video games’ influence on CS interest. 

[8] Mahmoud, Q.H. ; Dobosiewicz, W. ; Swayne, D. Computer, Feb. 2004. , Vol.37(2), pp.106-108 [Peer 
Reviewed Journal]. Making computer programming fun and accessible 
http://ieeexplore.ieee.org.prx.library.gatech.edu/xpls/icp.jsp?arnumber=1266305  

 
[9] Su, A. and Yang, S. and Hwang, W.2013. and Huang, C. and Tern, M. , Investigating the role of 
computer-supported annotation in problem-solving-based teaching: An empirical study of a Scratch 
programming pedagogy. 
http://onlinelibrary.wiley.com.prx.library.gatech.edu/doi/10.1111/bjet.12058/full 

 

 

 

 

 

 

 

 

 

