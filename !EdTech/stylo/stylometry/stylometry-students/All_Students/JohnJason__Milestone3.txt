Intermediate Milestone 3 

Jason John 

As part of the third milestone, my goal was to introduce several post­test scenarios which are 

follow ups of the pre­test scenarios. The pre­test scenarios were administered during milestone 

2.   

Pre­test Analysis 

The results from pre­test scenarios were tabulated and analyzed. I’ve to say, I was impressed at 

discovering some interesting trend line within the data. 

Overall team 1 outperformed team 2 in all aspects of motivational elements. Certainly this could 

be attributed to many factors. Hopefully the regression analysis, which I’ll perform at the end of 

 

the project, will reveal the dependency between the motivational elements. The goal of pre­test 

scenarios were to just allow the users to exhibit their normal behaviour/reaction while playing 

the game. Post­test scenarios will guide the users so that they maintain each of these 

motivational elements at their peak so at to foster full engagement. Here’s a look at the how 

each of the team performed w.r.t each of the motivational elements as the pre­test scenarios 

progressed from 1 to 5. 

Something worth noting, among others, from the above graph is that team 2 seem to have 

elevated their interest during scenario 4. During that time their competence level also increased. 

Clearly there seems to be a strong correlation between competence and interest. This is an 

important point that I’ll factor in as the post­test scenarios are created. 

 

Post­test Scenarios 

Now with pre­test behind us, it’s time to move onto post­test scenarios. Here, instead of driving 

users on their own, post­test scenarios will drive them so that through each of the scenarios, the 

user’s  

 

Motivational elements affecting Engagement 

 

motivational elements are driven to their peak. As before, each of the groups are given five 

scenarios (listed below) to work through as a team. The difference, however, is that these 

scenarios are a byproduct of things discovered during the pre­test analysis.  At the end of each 

task/step within the scenario, each team member is advised to fill out a motivational inventory 

scale. These scales will allow me to track their motivational states with respect to each of the 

elements mentioned above. Here’s the motivational inventory scale that each team member has 

to fill out at the end of each step. Each value in the scale has an associated numerical value. 

 

Interest 

1.

I’m enjoying the challenge: [strongly disagree, disagree , neutral, agree, strongly 

agree] 

2.

I was curious about the outcome of this step as I was working on it.: [strongly 

disagree, disagree , neutral, agree, strongly agree] 

Self­Efficacy 

1.

I think I found the best solution: [strongly disagree, disagree , neutral, agree, strongly 

agree] 

2.

I believe that my suggested choice of action was better than my partner.: [strongly 

disagree, disagree , neutral, agree, strongly agree] 

Autonomy 

1. This challenge is encouraging me to think creatively: [strongly disagree, disagree , 

neutral, agree, strongly agree] 

2.

I contributed effectively to make decision as a team: [strongly disagree, disagree , 

neutral, agree, strongly agree] 

Competence 

1.

I’m confident that my team made a good decision:[strongly disagree, disagree , 

neutral, agree, strongly agree] 

2.

I and my team member will be able to tackle the next challenge/scenario 

easily:[strongly disagree, disagree , neutral, agree, strongly agree] 

 
 

As mentioned before, I’ve constructed five different scenarios targeted at empirically measuring 

each of the above mentioned motivational elements. Each of these scenarios have several 

tasks and since it too many, I’m not going to list them. However, I’m going to outline the 

elements that each of the scenarios are intended to quantitatively assess. 

Scenario 1: In this scenario the steps are constructed to promote self­efficacy, competence 

and interest. This, I hope, will be reflected in the inventory scale that they fill out. 

 

Scenario 2: As they progress to the next scenario, I want to make sure their self­efficacy, 

competence and interest elevates, while autonomy starts to build up. 

Scenario 3: This scenario is geared towards understanding team members self­efficacy and 

competence level. I’m hoping by the end of this scenario the team will have elevated in their 

self­efficacy and competence. 

 

Scenario 4: In this scenario (as we push towards the end) I want to drive the team into 

increasing their interest, while maintaining or increasing their levels in self­efficacy, autonomy 

and competence.  

 

Scenario 5: Finally, towards the end of the game, the aim is to have the team exit with 

relatively higher interest and competence. 

 

 

The above scenarios will be administered in sequence and the results will be tabulated and 

analyzed. 

 

 

References 

Ryan, R., & Deci, E. (2000). Self­determination theory and the facilitation of intrinsic motivation, 

social development, and wellbeing. American Psychologist, 55(1), 68­78.  

 

Ryan, R., & Powelson, C. (1991). Autonomy and relatedness as fundamental to motivation and 

education. The Journal of Experimental Education, 60(1), 49­66.  

 

Bandura, A. (1993). Perceived self­efficacy in cognitive development and functioning. 

Educational Psychologist, 28(2), 117 ­ 148. doi: 10.1207/s15326985ep2802_3 

