CS	6460	–	Spring	2016		

	

Vinod	Dinakaran	

a	CS	6460	Project	proposal	

grok.code.guide	
(NNN	words	without	references)	

by	Vinod	Dinakaran	

Table	of	Contents	

1	
INTRODUCTION	..............................................................................................................................	2	
2	 THE	PROBLEM	.................................................................................................................................	2	
2.1	 ORIGINAL	PROBLEM	STATEMENT	...............................................................................................................	2	
2.2	 REFINED	PROBLEM	AREAS	...........................................................................................................................	2	
2.3	 A	MODEL	FOR	CODE	COMPREHENSION	AND	ITS	TOOLING	......................................................................	3	
2.4	 NATURAL	APPROACHES	TO	SOLVING	THESE	PROBLEMS	.........................................................................	4	
3	 EXISTING	SOLUTIONS	...................................................................................................................	4	
4	 PROPOSED	SOLUTION	...................................................................................................................	5	
5	 DESIGN	OF	THE	PROPOSED	SOLUTIONS	.................................................................................	6	
5.1	 USER	STORIES	................................................................................................................................................	6	
5.2	 HIGH	LEVEL	DESIGN	.....................................................................................................................................	6	
5.2.1	 Platform	Assumptions	.........................................................................................................................	6	
5.2.2	 Tool	Design	...............................................................................................................................................	6	
5.3	 BRANDING	.......................................................................................................................................................	7	
5.4	 COMMUNICATION	WITH	COMMUNITY	........................................................................................................	7	
5.5	
INTEGRATIONS	...............................................................................................................................................	7	
IMPLEMENTATION	PLAN	.............................................................................................................	8	
6	
6.1	 PROJECT	TEAM	...............................................................................................................................................	8	
6.2	 DETAILED	PROJECT	PLAN	............................................................................................................................	8	
7	 REFERENCES	....................................................................................................................................	9	
8	 APPENDIX	A:	PROPOSAL	ELEMENT	TRACEABILITY	MATRIX	........................................	10	

	

	

1 

Introduction	

2  The	Problem	
2.1  Original	Problem	statement	

Vinod	Dinakaran	
CS	6460	–	Spring	2016		
This	document	is	Vinod	Dinakaran’s	submission	for	project	proposal	as	part	of	
CS6460	–	Spring	2016.		It	outlines	code	comprehension	as	a	problem	area,	reviews	
existing	solutions	to	specific	problems	in	it	and	proposes	some	novel,	if	evolutionary	
solutions	to	them.	As	required	by	the	assignment,	it	also	provides	a	design	for	the	
solutions	and	an	implementation	plan.	
At	the	beginning	of	the	course,	my	problem	statement	read	thus:	
	“The	problem	I’d	like	to	explore	is	Code	Comprehension:	how	do	developers	learn	
and	understand	code?	How	does	the	newest	member	on	a	development	team	learn	
the	architecture,	design	and	idiosyncrasies	of	the	systems	that	the	team	owns?	How	
can	a	tool	–	specifically	the	editor	–	be	used	to	aid	this	process?”	
During	the	literature	survey	that	I	did	as	part	of	the	early	assignments	in	the	course,	
I	found	that	there	are	some	theoretical	models	proposed	for	code	comprehension	as	
summarized	by	Storey	(2001)	and	many	of	tools	built	both	by	academia	and	
industry	–	a	partial	summary	of	Gomolka	and	Humm	did	(2013).	There	were	still	
problems	in	the	space,	however,	as	found	by	Maalej,	Tiarks,	Roehm	and	Koschke	
(2014).	Their	findings	that	are	relevant	to	tool	enhancements	could	be	distilled	into	
the	following:	
	1.  Gap	between	research	and	practice:	Developers	don’t	use	(or	even	know	
about)	advances	in	code	comprehension	research.	
2.  Context	Awareness	in	Code	comprehension:	Understanding	code	needs	a	
record	of	the	state	at	the	time	of	building	the	code,	not	at	the	time	of	reading	it.	
Developers	are	Ok	to	share	this	detail	anonymously.	
3.  User	Behavior	and	Usage	Management:	Developers	sometimes	pretend	to	be	
users	to	understand	a	system	and	sometimes	avoid	deep	code	comprehension.	
4.  Knowledge	sharing	dilemmas:	Developers	need	to	know	who	built	the	existing	
code.	
	These	therefore,	are	the	specific	areas	that	my	project	will	attempt	to	address.	Of	
these,	#2	requires	particular	mention	because	it	is	the	one	finding	that’s	related	to	
the	time	when	code	is	created	versus	when	its	read	and	understood	afterwards.	This	
and	a	related	question	by	my	mentor	led	me	to	expand	my	research	to	include	tools	
and	techniques	that	enable	the	creation	of	understandable	code	along	with	those	
that	enable	their	comprehension	once	created.	

2.2  Refined	Problem	areas	

	

Vinod	Dinakaran	
CS	6460	–	Spring	2016		
Code	comprehension	occurs	at	two	key	junctures	in	the	software	lifecycle,	named	
“Grok”	in	the	diagram	below,	following	the	developer	parlance	for	“understand”	(see	
the	Hacker’s	Dictionary,	n.d).	

2.3  A	model	for	code	comprehension	and	its	tooling	

the	

understands	

	
synthesizes	
requirements,	
developer	
•  A	
an	
architecture/design	(grok)	and	creates	the	software	(code,	verb).	The	only	of	
part	this	intense	process	that’s	available	for	future	inspection	is	the	code	
itself.	
o  There	is	lots	of	tooling	for	the	code	process.	
o  There	is	very	little	tooling	for	the	grok	process	or	to	retain	its	artifacts	
(i.e.,	the	understanding	gained).	Instead,	engineering	best	practices	
and	documentation	(off-codebase	and	inline)	are	relied	upon.		
•  Subsequently,	a	change	needs	to	be	made	to	the	existing	code	–	to	fix	a	bug	or	
to	add	a	feature.		At	this	point,	another	(or	sometimes	the	same)	developer	
reads	the	code	and	understands	it	(grok)	enough	to	make	the	appropriate	set	
of	changes	(code).	
o  A	lot	of	the	code	comprehension	and	visualization	tooling	is	aimed	at	
this	juncture,	attempting	to	reverse	the	initial	process	to	the	extent	
possible.	
•  The	gap	in	understanding	between	the	two	developers	where	there	is	
opportunity	for	the	first	to	guide	the	second	is	depicted	as	the	black	dashed	
arrow.	

Problem	

Proposed	Approach	

2.4  Natural	approaches	to	solving	these	problems	

Vinod	Dinakaran	
	
CS	6460	–	Spring	2016		
Given	the	problem	areas	mentioned	above,	the	natural	next	step	is	to	propose	some	
general	approaches	that	might	work,	like	so:	
	
Gap	between	
Use	this	fact	as	a	guiding	principle	in	coming	up	with	
research	and	
tools.	If	it’s	entirely	a	new	solution,	it	may	not	work.	
practice:	
Build	a	set	of	tools	that	meet	the	intent,	but	are	small	
and	can	be	adopted	easily	
Context	Awareness	
Record	developer	activity	and	make	it	available	for	
and	Code	
self-analysis,	summarization	and	sharing	–	with	
comprehension	
respect	to	the	developer’s	privacy	and	comfort	level.	
User	Behavior	and	
Usage	Management	 Enable	automation	of	this	step,	potentially	recording	
trial	runs	for	replay	later.	Provide	ability	to	zoom	to	the	
appropriate	code	easily.	
Make	the	authors	of	code	easily	accessible	on	a	per-
Knowledge	sharing	
dilemmas	
component	basis.	
	Clearly,	none	of	these	suggestions	are	particularly	new	and	have	found	expression	in	
multiple	tools	in	some	shape	or	form,	which	will	be	explored	in	the	next	major	
section.	
•  Tools	that	make	adoption	easy:	Plugins	for	IDEs.	
•  Tools	that	record	developer	activity:	Selenium.	
•  Tools	that	track	trial	runs	of	the	application:	Light	Table,	Elm	Language.	
•  Tools	that	provide	information	about	authors:	git-blame,	Atom’s	git	blame	
gutter	display.	

3  Existing	Solutions	

	

Problem	

Tool	#	

Proposed	Solutions	
Description	

4  Proposed	Solution	

Vinod	Dinakaran	
CS	6460	–	Spring	2016		
My	proposal	to	improve	the	state	of	response	to	these	problems	is	a	set	of	tools	that	
I	call	“Grok.Code.Guide”,	guided	by	the	approach	to	address	the	adoption	problem.		
For	each	of	the	other	problems,	at	least	one	tool	is	suggested,	sometimes	two.	Each	
tool	is	further	broken	up	into	versions	that	are	Fallback,	Comfortable	or	Preferred;	
which	represent	positions	of	implementation	completion	that	would	progressively	
improve	the	degree	to	which	the	problem	is	addressed.	
	
Fallback	
Create	individual	plugins	
Gap	between	
research	and	
Comfortable	
Add	tools	as	a	package	of	plugins	for	
practice	
easy	adoption	
Preferred	
Add	tool	as	core	feature	of	IDE	
Context	
Same	as	above	but	less	portable	or	
Fallback	
Awareness	and	
IDE-specific	tracker	
code	
Preferred	
Create	system-wide,	IDE-agnostic,	
comprehension	
portable	tracker	
Create	a	journal	to	capture	thoughts	
Comfortable	
and	pre-commit	decisions	
Comfortable	
Create	a	basic	input	record/replay	
User	Behavior	
tool	
Create	a	full-featured	input	
Preferred	
record/replay	tool	
Preferred	
Store	a	reason	for	the	change	at	time	
Usage	
of	commit	
management	
Knowledge	
Display	author	list	for	each	file	
Fallback	
sharing	
Preferred	
Visualize	the	codebase	as	a	treemap	
dilemmas	
colored	by	author	
	The	next	section	dives	deep	into	each	tool	in	terms	of	user	stories,	a	high	level	
design	and	some	implementation	considerations.	The	different	versions	are	not	
depicted	as	separate	user	stories,	but	each	one	is	discussed	in	the	implementation	
sections.	

All	
1	
2	
3	
4	
5	
6	

Type	of	
Solution	

CS	6460	–	Spring	2016		

	

Vinod	Dinakaran	

5  Design	of	the	proposed	solutions	
5.1  User	Stories	

Story#	
1	
2	
3	
4	
5	
6	

As	a..	
Developer	
Developer	
Developer	
Developer	
Developer	
Developer	

..I	would	like	to..	
See	an	automatically	
tracked	record	of	my	
coding-related	actions		
Store	a	journal	of	my	
planned	coding	thoughts	
and	actions	
Have	recorded	
application	runs	
Store	a	reason	for	the	
code	change	at	commit	
See	a	list	of	authors	for	a	
file	
See	the	codebase	as	a	
treemap	colored	by	
author	

..so	that..	
Review	them	and/or	
share	them	
Remind	myself,	use	for	
documentation	and/or	
share	them.	
Speed	up	exploration	of	
the	system.	
Others	can	read	and	
understand	my	change	
I	can	communicate	with	
them	to	understand	the	
code	
I	can	communicate	with	
them	to	understand	the	
code	

	

5.2  High	Level	Design	
5.2.1  Platform	Assumptions	
IDE’s	used:	

1. Github’s	Atom	Editor	
2. Intellij’s	IDEA	
3. Eclipse.	
The	reasons	for	this	sequence	of	choices	are:	
•  Atom	is	easy	to	hack	and	add	plugins	for,	making	it	the	ideal	choice	for	the	
“fallback”	and	“comfortable”	level	of	implementation	
Intellij	and	Eclipse	already	have	a	lot	of	plugins	that	address	some	of	the	
problems.	
•  Git:	Other	version	control	systems	may	be	taken	up	in	the	future,	but	this	
choice	covers	a	lot	of	the	developer	community	both	in	open	source	and	
private	industry.	
•  Tracker:	The	tracker	will	be	implemented	as	a	native	OS	application,	
runnable	in	user	space	so	that	there	are	no	security	hurdles	at	installation.	
Further,	the	application	will	store	data	in	an	open,	single	line	format	making	

Version	Control	System	used:	

5.2.2  Tool	Design	

• 

	

CS	6460	–	Spring	2016		

Vinod	Dinakaran	
it	amenable	for	processing	with	command-line	utilities	(that	developers	
prefer).	
Journal:	The	journal	will	be	implemented	as	a	plugin	to	the	IDE.	It	will	allow	
the	developer	to	press	a	keyboard	shortcut	to	invoke	a	UI	that	will	allow	
input	of	a	journal	entry.	The	previous	entries	will	be	visible	for	read	and	edit	
at	the	same	time.	A	timestamp	will	be	automatically	added	by	the	plugin.	
•  Usage	 Record/Replay:	This	tool	will	be	implemented	as	a	native	OS	
application	(one	per	major	OS	or	portable)	with	the	basic	ability	to	track	
keyboard	and	mouse	input.	Data	collected	will	be	sent	to	the	Tracker.	The	
preferred	version	will	enhance	the	basic	one	with	the	abilty	to	track	context	
in	terms	of	the	application	that	was	in	focus	at	the	time	of	the	input.	
•  “Why”	at	commit:	This	tool	will	be	implemented	as	a	simple	IDE	plugin.	The	
commit	template	will	be	enhanced	to	include	language	requesting	the	
developer	to	add	a	reason	for	the	code	change.	
•  List	 of	 authors:	This	tool	will	be	implemented	as	an	IDE	sidebar	view	that	
shows	all	the	authors	who	have	ever	changed	the	file	in	current	focus.	The	
data	for	this	will	be	got	from	the	VCS.	
•  Codebase	as	Treemap:	This	tool	will	be	implemented	as	a	plugin	that	shows	
an	alternative	to	the	left	navigation	panel	of	most	Editors.	It	will	instead	
show	a	treemap	of	the	codebase	with	authors	highlighted	for	easy	reconing.	
The	name	for	the	set	of	tools	to	be	built	will	be	clear	and	understandable	without	
additional	documentation.		
	
Once	a	working	version	is	built,	I	will	publish	to	the	interest	groups	of	the	
appropriate	IDEs	and	to	developer	news	sites	such	as	Hacker	news	and	
r/programming.	
	
See	Platform	assumptions.	

5.4  Communication	with	community	

• 

5.3  Branding	

5.5 

Integrations	

CS	6460	–	Spring	2016		

	

Vinod	Dinakaran	

Implementation	Plan	

6 
6.1  Project	Team	

6.2  Detailed	Project	Plan	

This	is	an	individual	project,	so	I,	Vinod	Dinakaran,	will	be	the	sole	team	member.	
Milestone/Task	
Project	Proposal	
Research	building	plugins	in	Atom	and	
Intellij	
Weekly	status	check-in	#1	
Build	Why	at	commit,	start	tracker	
Weekly	status	check-in	#2	
Intermediate	milestone	#1	
Build	tracker	
Weekly	status	check-in	#3	
Build	Journal	
Weekly	status	check-in	#4	
Intermediate	milestone	#2	
Build	List	of	authors	
Weekly	status	check-in	#5	
Build	basic	recorder	
Weekly	status	check-in	#6	
Intermediate	milestone	#3	
Start	codebase	as	treemap	
Weekly	status	check-in	#7	
Finish	codebase	as	treemap	
Weekly	status	check-in	#8	
Intermediate	milestone	#4	
Weekly	status	check-in	#9	
Final	Project	
Project	Presentation	
Project	Paper	
	

Due	Date	 Notes	
2/21	
	
2/28	
	
	
2/28	
3/06	
	
3/06	
	
	
3/06	
3/13	
	
3/13	
	
3/20	
	
	
3/20	
3/20	
	
3/27	
	
	
3/27	
4/03	
	
4/03	
	
	
4/03	
4/10	
	
4/10	
	
4/17	
	
	
4/17	
4/17	
	
4/24	
	
	
5/01	
5/01	
	
5/01	
	

	

7  References	

Vinod	Dinakaran	
CS	6460	–	Spring	2016		
•  Walid	Maalej,	Rebecca	Tiarks,	Tobias	Roehm,	Rainer	Koschke.	(Aug	2014).	On	
the	comprehension	of	Program	Comprehension.	ACM	Transactions	on	
Software	Engineering	and	Methodology,	Vol.	23,	No.	4,	Article	31.Retrieved	
from:	https://mobis.informatik.uni-hamburg.de/wp-
content/uploads/2014/06/TOSEM-Maalej-Comprehension-PrePrint2.pdf	
•  Storey,	M.(2005).	Theories,	methods	and	tools	in	program	comprehension:	
past,	present	and	future.	13th	International	Workshop	on	Program	
Comprehension,	2005.	IWPC	2005	Proceedings.	Pages:	181	-	191,	DOI:	
10.1109/WPC.2005.38	
•  Robert	A	Heinlein.	“Grok”.	Retrieved	from:	
http://www.hackersdictionary.com/html/entry/grok.html	
•  Andreas	Gomolka,	Bernhard	Humm	(2013).	Structure	Editors	–	old	hat	or	
new	future?	Retrieved	from:	https://www.fbi.h-
da.de/fileadmin/personal/b.humm/Publikationen/Gomolka_Humm_-
_Structure_Editors__Springer_ENASE_.pdf	

	

8  Appendix	A:	Proposal	element	traceability	matrix	

Vinod	Dinakaran	
CS	6460	–	Spring	2016		
The	assignment	description	expects	the	proposal	to	have	the	following	elements.	
Mapped	here	are	the	sections	where	each	can	be	found.	
	
	
	
	
	
	
	
	
NA	
	
	
	
	
	
	
	

Members	in	team	
Task	List	including	
	
Trailer	
Progress	Report	
	
	
Final	Paper	
Calendar	with	weekly	milestones	from	Feb	22	to	May	1	
Description	of	intermediate	milestones	
Division	of	Responsibilities	
Tool	Proposals	
A	description	of	the	problem	to	be	solved	
A	description	of	existing	solutions	for	that	problem,	specifically	
to	contextualize	why	your	solution	is	needed.	
A	description	of	the	design	of	the	tool	you	will	create.	
A	technical	description	of	the	tools,	languages,	and	other	
resources	that	will	be	used.	
A	description	of	the	integrations	or	external	resources	that	will	
need	to	be	obtained.	
Fallback	plans	in	case	portions	of	these	details	cannot	be	
completed.	
Extra:	Connection	and	contribution	to	community	

All	Proposals	

Element	

Section	

				

