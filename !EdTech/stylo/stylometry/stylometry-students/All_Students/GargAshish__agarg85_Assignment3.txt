Assignment 3 

Problems in Patient Education 

Ashish Garg (agarg85) 
 
In  the  previous  assignments  (1  &  2)  I  introduced  myself  and  to  an  area  that  I  am  interested 
exploring as part of my research – Healthcare Patient Education, which is application of Education 
Technology in the real life challenges. 

 

Recap: 

From  the  earlier  assignment  we  explored  how  one  area  of  Education  Technology  –  Simulation 
Technology  is  providing  skills  necessary  for  the  healthcare  professions  to  perform  in  this  high 
stake industry. They do not have worry about harming patient’s while honing their skills. 

While,  there  are  plenty  of  focus  and  advances  in  training  the  healthcare  professionals,  which  I 
believe should be, but we have not invested enough of our efforts, time and money in patient’s 
education. We have seen the benefits it can have on the overall healthcare delivery and patient’s 
health at high level in assignment 2. 

Patient Education problems today: 

In assignment 2 we saw  that there are numerous resources for patients to  gain  their knowledge 
about their health issues.  

We explore the problem areas further in this assignment by digging deeper into the areas that are 
going to be my basis for research project. My research will take pragmatic approach to resolve the 
each of the problem areas listed below and propose a comprehensive solution.  

A.  Problem of lack of education among patients: 

A  major  problem  is  that  the  patient’s  lack  knowledge  of  the  medical  conditions,  terms  and 
measures. 

This is true due to factors as mentioned below: 

1.  Low general literacy levels among patients.  
2.  Low health literacy among patients.  

My research would explore the areas where education technology can help and ways where it 
can bridge patient literacy gaps with the information they need to enhance their overall health.  

B.  Problem  of  knowledge  centralization  and  lack  of  information  technology  /  education 

technology knowledge among sources: 

The  best  source  of  patient  education  is  the  healthcare  professional’s  community.  However, 
they lack the means to deliver their knowledge in form of meaningful patient education to their 
patients. 

My  research  would  explore  the  possibilities  of  creating  the  tools  and  processes  where 
knowledgeable  professionals  are  able  to  impart  knowledge  without  learning  IT/Education 
Technology. 

C.  Problem of justifying the investment into patient education for the healthcare professionals: 

 

1       

Assignment 3 

Problems in Patient Education 

Ashish Garg (agarg85) 
 
Many of the healthcare professionals feel they are and  will not be rewarded suitably if they 
invest their time, money and energy in creating a patient specific knowledge base. 

 

I will try to explore the mechanism and ways to reward healthcare professionals in a way it is 
beneficial to both patients and their care providers. 

D.  Problem of information overload: 

As we saw in my earlier assignment submission, there are many sources and means of patient 
education today.  
The information each source provides is phenomenal. Which means information is abundant 
creating an issue that a patient may not get the right information concerning their disease at the 
time they want the most. It also creates a problem of under or over educating patients leading 
to more issues in their treatment. It becomes the responsibility of the receiver to search for the 
right information they need and filter.  
I would explore the ways in which patient education is specific and delivered directly to the 
patients based on the parameters. 
 

E.  Problem with cost justification: 

Who needs to spend on patient education? Should it be? 
1.  Doctor’s office. 
2.  Patients. 
3.  Governments. 
4.  Private companies. 

I will explore these areas as cost is the backbone of the patient education systems. Benefits will 
outweigh the cost of the creating a patient specific education systems. 

Another area that is a problem is access to the technology among patients. However, there are 
means  to  provide  them  accesses  like  public  libraries,  house  rental  offices,  senior  living 
communities and doctor’s offices.  

F.  Problems with patient demographic: 

Patient’s age, location, social background play a major role in delivering meaningful patient 
education. 

Most of the healthcare needs are by senior citizens who are  typically not technology savvy. 
That  poses  a  real  challenge  in  delivering  patient  education.  I  will  explore  how  a  patient 
education system be built that interests such patients. 

G.  Stringent HIPAA rules and regulations: 

While  protecting  patient’s  confidential  information  should  be  the  priority  in  any  healthcare 
delivery, it should not be a limiting factor in providing disease/condition specific education to 
the patients. We can always mask PHI data and use secured networks to provide education. 

H.  Lack of interactive and intelligent education systems: 

 

2       

Assignment 3 

Problems in Patient Education 

 

Ashish Garg (agarg85) 
 
As we have seen earlier in assignment 2, today most of the patient education systems are just 
one way traffic. From knowledge source to patients. Patients do not feel connected with such 
systems where interactivity is limited.  
 

Educational Technologies: 

In assignment 2 I mentioned the list of technologies that can be used in solving the problems of 
patient education. 

Now as we have defined the problem statement, I think using a combination of Knowledge based 
Simulation Technology with some Intelligent Tutoring aspects that provides continuous feedback 
mechanism would be a good choice. 

 

 

3       

