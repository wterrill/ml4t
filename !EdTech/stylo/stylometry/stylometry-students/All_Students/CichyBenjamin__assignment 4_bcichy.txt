Benjamin	Cichy	
Assignment	#4	
Sunday,	February	7,	2016	
	
Note:		
My	project	involves	the	A/B,	split	and	and	Multivariate	testing	and	analysis	of	the	LEGO	
Wedo[1].	I	am	checking	learning	rates,	stoppages,	and	other	metrics	for	a	simple	robotics	
design.	The	robotic	projects	are	programmed	using	modified	version	of	Scratch[2]	from	MIT.	
Scratch	allow	for	simple	visual	visual	programming	of	the	robotics	kit,	with	primitive	if-then	
blocks.	This	allows	for	children	a	quick	and	easy	exposure	to	programming.		

Summary	
For	this	week	I	am	exploring	the	basics	of	my	project,	my	target	audience,	and	a	testing	plan.	I	
am	addressing	the	topics	of	what	experiments	I	might	do,	and	the	testing	metrics	I	hope	to	use.		
Last	week	I	touched	upon	the	framework	for	testing	and	the	some	of	the	skills	I’d	like	to	test	on	
K-6	children.	I	have	not	narrowed	the	age	range	yet	(K-6ish),	until	I	receive	my	kit	and	can	see	
some	basic	projects,	and	beta	test	their	scope.		

Learning		
Differences	in	learning	
One	of	the	goals	of	the	project	is	measure	gender	differences,	and	possibly	socio-economic	
differences,	if	possible.	In	assignment	#3	I	touched	upon	this	topic,	and	I	plan	to	test	children	in	
two	districts	with	different	economic	conditions[3].	Permission	will	need	to	be	obtained	for	
personal	information	on	family	economic	conditions.	Splitting	the	two	conditions	with	a	team-
mate	would	also	speed	the	process	along	for	data	points		
	
Gender	Differences	
In	many	articles	referring	to	LEGO	being	gender	biased,	many	coming	recently	with	the	
introduction	of	LEGO	Friends[4].	LEGO	had	defended	this	decision,	from	their	previously	more	
gender-neutral	sets	(despite	many	having	clearly	male	figure).	Further	proof	comes	form	
outliers	of	girls	bending	the	gender	roles[5].		
Now	testing	the	bias	leads	to	having	to	target	a	different	set	of	age	ranges.	At	some	(un-
determined	point,	need	to	cite)	girls	discover	“girl”	toys,	and	abandon	robotics[6].	Covering	
grade	2-3	and	then	grade	6	or	7	might	make	this	difference	more	obvious,	in	the	experimental	
portion	of	this	project.		
		
Speed	of	acquisition	
Besides	gender	differences	(if	any)	I’d	like	to	cover	whether	some	students	are	pre-disposed,	
aka,	can	be	binned	by	innate	skill.	Is	there	a	difference,	or	is	it	nature/nurture?	The	next	section	
covers	some	more	of	that.		
Knowledge	separation/Skills	separation	
	

Questions	I	am	pondering,	but	might	not	be	able	to	answer	in	this	paper	are:	Are	the	K-3	
children	capable	enough	or	ready	enough	for	robotics.	Will	they	still	be	able	to	complete	the	
projects	easily?	Perhaps	testing	with	ascending	difficulty,	but	A/B	tested	with	no	ascension	
would	play	this	scenario	out.		
	
The	socio-economic	aspect	also	covers	this	area	in	overlap.	Are	PhD	parents	producing	skilled	
children?	This	is	separate	from	rich	parents	that	buy	tutoring.		
	

Project	
	
I	am	now	considering	in	reaching	out	to	other	educators	in	the	K-6	learning	space[7].Tufts	has	a	
very	active	educational	group	that	covers	LEGO.	
	
Existing	designs	I	have	found	for	projects	are	for	the	original	Wedo	kit.	They	have	to	be	adapted	
to	the	new,	slightly	more	complex	system.		
	

Figure	1	–	Drumming	monkey[8]	

	

This	particular	project,	is	not	age	separated	well,	and	would	need	to	be	tested	against	an	age	
range	with	the	above-mentioned	metrics.		

Testing	
I	am	pushing	off	this	area	still,	till	I	have	a	better	grasp	and	list	of	areas	I	am	covering.	I	need	to	
narrow	the	breadth	here	--	unless	I	manage	to	acquire	a	partner,	or	just	push	it	off	to	another	
paper.		
	
So,	lots	of	todos:		I’ll	probably	have	to	post	this	for	feedback:	

•  1-2	projects	age	targeted.		
•  1	Gender	biased	project.	
•  1	Complex	projects,	with	economic	bias.			

	
Still	need	a	statistical	framework	for	significance	such	as	T	and	ANOVA	tests,	to	measure	
significance	after	data	acquisition.	Additional	sample	points	might	need	to	be	acquired	during	
peer	review.		
[1]	

“LEGO.com	Primary	School	-	WeDo	2.0.”	[Online].	Available:	

https://education.lego.com/en-gb/lesi/elementary/wedo-2.	[Accessed:	08-Feb-2016].	
“LEGO®	WeDoTM	Construction	Set	-	Scratch	Wiki.”	[Online].	Available:	
http://wiki.scratch.mit.edu/wiki/LEGO%C2%AE_WeDo%E2%84%A2_Construction_Set.	
[Accessed:	08-Feb-2016].	
T.	Karp,	“Work	in	progress:	Serving	rural	communities:	A	K-8	LEGO	robotics	case	study,”	
Proc.	-	Front.	Educ.	Conf.	FIE,	2012.	
“Lego	Sales	Soar	Thanks	To	Popular	Series	Designed	For	Girls.”	[Online].	Available:	
http://www.huffingtonpost.com/2013/09/05/lego-sales-girls-series_n_3872147.html.	
[Accessed:	08-Feb-2016].	
“‘Badass	Lego	Girls’	Made	By	A	Badass	3-Year-Old	(PHOTOS).”	[Online].	Available:	
http://www.huffingtonpost.com/2013/09/24/badass-lego-girls_n_3975818.html.	
[Accessed:	08-Feb-2016].	
“Lego	Has	a	Gender	Problem	|	Architect	Magazine	|	Design.”	[Online].	Available:	
http://www.architectmagazine.com/design/lego-has-a-gender-problem_o.	[Accessed:	
08-Feb-2016].	
“DevTech	Research	Group	at	Tufts	University.”	[Online].	Available:	
http://ase.tufts.edu/devtech/publications/.	[Accessed:	08-Feb-2016].	
“wedobots:	LEGO®	WeDo	designs	for	the	busy	teacher:	WeDo	Included	Designs.”	
[Online].	Available:	http://www.wedobots.com/2012/12/wedo-included-designs.html.	
[Accessed:	08-Feb-2016].	

[2]	

[3]	

[4]	

[5]	

[6]	

[7]	

[8]	

	

