Harshaw, Zachary GTID#903049739  

Milestone 2 

Project Overview: 
Construction of a drag and drop coder framework that can be utilized to teach the upper k-12 audience 
about the major problem solving and fundamentals of programming.  The project looks to remove some 
of the common pitfalls that can accompany first programming such as syntax mistakes, object typing, 
code structure, and so on. 

Milestone 2 excerpt from Proposal: 
The second milestone will focus on ensuring that the application can generate runnable JavaScript 
code.  Assets dropped in the draggable area should be able to be converted to code.  At this point as 
well the draggable/droppable functionality should be stable. 

 

For this milestone I figured the best way to show that the application is proceeding along as planned 
would be to show a video of the functionality thus far.  Please take into account a large amount of 
styling is still needed and some bugs in need of fixing. 

To demo the software I’m going to propose a simple programming task and complete it using the 
application. 

 

Create a program that counts down from 100 to 0 which will display “NUMBER is even” when the 
number is even and display “NUMBER is odd” when the number is odd. 

The link to the demo is listed below (NOTE: video is silent and about 4 minutes long): 

https://youtu.be/md_DP_T45oo 

As can be seen by the video the application is functional and will just need various enhancements and a 
some work making it look nice. 

