Shawn Kang 
skang313@gatech.edu 
February 14, 2016 

Assignment 5 

Personal Question: Intelligent Tutoring Systems 

     Intelligent tutoring systems (ITS) are computer based systems that focus on 
providing real time and customized instruction and/or feedback to students more 
commonly without the assistance of a human instructor. ITS have been used in both 
formal education and in the professional setting for many years, first originating in the 
artificial intelligence (AI) movement of the late 1950s and early 1960s.  
     AI pioneers of that time postulated that computers could potentially think just like 
humans. The main impedance to this idea was the creation of bigger and faster 
computers. However by the 1960s, researchers had already created a number of 
generative Computer Assisted Instructional (CAI) systems. CAI produced sets of 
problems that were designed to enhance a student’s performance in skill-based 
domains, such as arithmetic and vocabulary recall. They were essentially automated 
flashcard systems. CAI presented a student with a problem, it received and recorded a 
student’s response, and then calculated a student’s performance. However CAI did not 
address how humans actually learn.  
     During the late 1960s and early 1970s, researchers began to design systems that 
adapted the presentation of new material based on a student’s progress. The students 
who used these systems improved on relevant skills and factual recall. These 
researchers postulated that students first need to learn the basic skills in order to better 
prepare for higher synthetic skills. By 1982, Sleeman and Brown first coined the term 
ITS. Researchers began to focus on problems of natural language, student models, and 
deduction.  

CS6460

!1

     At the end of the 20th century, the Byzantium project, which involved six universities, 
developed Intelligent Tutoring Tools (ITTs).  ITTs were essentially basic tutoring system 
builders. An Intelligent Tutoring Applet (ITA) for various subjects would be created and 
instructors would build up a large database of knowledge. After an ITS was created, 
instructors would copy it and modify it for future use. At the time, these systems were 
considered to be proficient. However current day ITS actually duplicate the role of an 
instructor. They involve creating a problem and then giving automated intelligent 
feedback. The architectures currently dominating the field are the AutoTutor, Cognitive 
Tutor, and the Generalized Intelligent Framework for Tutoring. 

Research shows that ITS are growing in popularity since they are able to increase 

performance in students, leverage cognitive development, and significantly decrease 
the time it takes to acquire knowledge and become proficient in a specific area. As 
previously mentioned, modern day ITS essentially try to replicate the role of a teacher or 
a teaching assistant and usually entail problem generation and intelligent automatic 
feedback generation with a high recall value. However, ITS do have their pros and cons.  
The benefits of ITS are:  

• Are available at any time of the day 
• Provide real time data to instructors and developers looking to fine tune various       
   teaching methods 
• Reduce dependence on humans 
• Help students understand the subject better (allow student to explain what they   
   know and then the system can cater responses accordingly)   
• Allow instructors to create individualized programs for each student    
• Students tend to produce higher test scores than in traditional systems 
• Provide immediate feedback, refine the program, real time hints, and  
   support for mastering the topic 
The potential downsides of ITS are:   

• It is hard to accurately evaluate the effectiveness of a program  
• Real time feedback and hints could potentially fail to prompt a deeper learning in  
   some students 
• Systems can possibly fail to ask questions to students that might explain a  

CS6460

!2

   student’s action  
• ITS may eliminate administrative staff 
• Evaluating ITS can be difficult, expensive, and time consuming 
• Human tutors may be more suitable to provide appropriate dialogue and  
   feedback to students 
• Human tutors may be better able to adapt to the different emotional states of a  
   student 

In addition, ITS has four basic components: 

1. Domain model (aka cognitive model or expert knowledge model): Based on the            

ACT-R theory, it attempts to take into account all prospective steps necessary   
to solve a problem. It consists of the concepts, rules, and problem solving  
strategies of the domain to be learned. This model can act as a source of  
expert knowledge and as a standard for evaluating a student's performance  
and/or detecting errors. 

2. Student model: Considered to be an overlay of the domain model, it is usually   
considered as a core component of an ITS. It focuses on a student's cognitive  
and affective states as well as their progress as the learning process advances.  
As a student attempts to solve a problem and goes through the solving  
process, the system goes through a process called model tracing. Whenever a  
student steers away from the domain model, then the system identifies that an  
error has occurred. 

3. Tutoring model: This model accepts information from both the domain and  

student models and makes decisions concerning tutoring strategies and  
actions. At any time, a student can request assistance on how to proceed. The  
system can also determine when a student has deviated from the production  
rules of the model and provides real time feedback. This can potentially result  
in a quicker turnaround for the student in mastering the presented material.   
4. User interface model: It combines three types of information necessary for  

completing a dialogue. These are knowledge about patterns of interpretation (in  
order to understand a speaker) and action (in order to generate utterances)  
within dialogues; domain knowledge necessary for needed for communicating  

CS6460

!3

material; and the knowledge necessary for communicating intent. 

     In 1987, Anderson et al. composed a list of eight principles of ITS design and 
development. 
Principle 0: An intelligent tutor system should enable the student to work to the 
successful conclusion of problem solving: 

• Represent student competence as a production set 
• Communicate the goal structure underlying the problem solving 
• Provide instruction in the problem solving context 
• Promote an abstract understanding of the problem-solving knowledge 
• Minimize working memory load 
• Provide immediate feedback on errors 
• Adjust the grain size of instruction with learning 
• Facilitate successive approximations to the target skill 

      There are also two classes of effective tutors: cognitive tutors and constraint-based 
tutors. They depend upon knowledge representations and production rules or 
constraints, which require extensive programming, expertise, and research to develop. 
However data driven methods allow the quick development of new intelligent tutoring 
systems. In addition, there exists various viewpoints of different communities or 
research methodologies in terms of ITS-student interactions. These include various data 
driven symbolic or statistical machine learning approaches for automated or semi-
automated development of key components and functionalities of ITS and are as 
follows:  

1. SimStudent - A theory of student learning based on a software tool that promotes 

the development of cognitive models. Its main purpose is to promote non-AI-
programmers to program by tutoring in order to create the central cognitive 
model component of an ITS. This methodology involves creating a graphical user 
interface in which students will use to solve problems. The user enters a task, 
such as solving an equation, thus promoting SimStudent to solve the task. In the 
beginning there will be no relevant scenarios in order to correlate against, so 
SimStudent prompts the user to illustrate a step. This illustration initiates a 
candidate production rule for successfully completing this step. Subsequently, 

CS6460

!4

future steps will use previously created production rules to issue the next 
prospective steps. In this case, the user will give yes or no answers denoting the 
accurateness of the step involved. If the user denotes a no answer, then 
SimStudent will relearn the production rule for that step and will try again until it 
receives positive feedback from the user. This involves numerous AI and 
matching learning techniques in order to learn a rule-based production system. 
This methodology has been successfully used to learn cognitive models in 
several domains involving several cognitive models in algebra equation solving, 
stoichiometry, multi-column addition and subtraction, tic-tac-toe, and fraction 
addition. 

2. Hint Factory - A method of automatically generating specific hints by using 

previously stored student data. This method is designed to be as specific as 
possible, real time, directed towards a student’s problem-solving goal, and to 
provide the appropriate assistance at the right time. It uses a student’s attempt 
data in order to automatically evaluate a student’s actions and to provide 
appropriate hints for a particular problem. Hint Factory involves giving direct, 
data-driven feedback and allows students to choose from a large list of actions to 
perform, many of which are potentially correct. This method builds a graph of 
potential states and actions or scenarios and represents all previous student 
approaches to a certain problem. The state is what the student can see on the 
screen and the actions are what the student actually does. This state-action 
graph is converted to a Markov decision process (MDP), which is ruled its state 
set S, action set A, transition probabilities T, and a reward function R. The reward 
function gives a small negative reward for all non-solution states thereby to 
promote reaching the solution as quickly as possible. The MDP is used to create 
hints with the Hint Factory and finds the best policy or path related to the 
problem. This essentially involves calculating a value (expected discounted sum 
of rewards completed by following an optimal policy from state s), which is then 
calculated using value iteration. The Hint Factory uses these values to determine 
the next optimal state from which to generate a hint. When a student clicks the 
hint button, the hint provider searches for a current state in the MDP and 

CS6460

!5

determines whether a successor state exists. If a successor state exists, then the 
state with the highest value is used to issue a hint sequence. Consequently if the 
student makes an error and requests another hint, then the next hint presented is 
the hint in the current sequence. When a student gives the correct answer, then 
the hint sequence is reset.  

3. Detector approach (Science inquiry tutor example) - Machine learning has also 

been used to develop the inner loop evaluate functionality of an ITS.  In this 
approach, a detector or classifier is trained using human labels of desired and 
undesired student actions within the context of an open ended simulation or 
performance environment. Research from Pedro et al. 2010 illustrates this 
approach. In this approach, people hand labeled student data by using text-
based replays of portions of student interaction logs. These labels illustrate 
whether the correct strategy was in the replayed segment (designing controlled 
experiments, testing the stated hypothesis, and haphazard inquiry). If the hand 
labels were deemed inter rater reliable across several coders, then they could be 
used as training labels for automated detectors of a student’s skill by using 
standard classification algorithms. These detectors can be implemented into 
automatic interventions, which evaluate a student’s actions, determines incorrect 
strategies, and gives students advice on how to conduct more effective 
experimentation. 

     Research also shows that metacognition improves effective learning and that 
students show significant gain (same level of proficiency as compared to traditional 
instruction in one-third the time). Several instructional programs focused on improving 
metacognition have been shown to be successful. In addition, there has recently been a 
surge in ITS with metacognitive support. In contrast to a comprehensive program of 
metacognitive instruction, these systems have been most successfully implemented 
using computer-based tutoring of specific metacognitive skills.  
     Metacognitive feedback is feedback that is triggered by a student’s learning behavior 
(avoiding necessary help) rather than by the accuracy of their responses at the domain 
level. Metacognitive feedback promotes metacognitive content or it correlates 
information about the desired learning behavior (suggesting the student to ask for a 

CS6460

!6

hint), rather than actual domain knowledge. In order to provide metacognitive feedback, 
the ITS must have the ability to detect metacognitive errors in real time and without 
disrupting the learning process. An example of this is the Geometry Cognitive Tutor (see 
Fig. 1). 

 

Fig. 1. The help-seeking model. This flowchart describes the main decision points of the 
complete help-seeking model. The full model is implemented using 80 if-then rules.  

  
In this methodology, a student who lacks knowledge to solve a particular step should 
seek assistance in order to solve a problem (instrumental help-seeking). On the other 
hand, seeking a final hint in the series is not desirable when a student’s knowledge level 
and help-seeking pattern indicate that the hint will be used only to find the final hint and 
skip the intermediate hint levels. If a student does have enough knowledge to solve a 
particular step in a problem, then that student should do so without the assistance of a 
hint. The help-seeking model allows for beneficial metacognitive actions in all problem 
solving scenarios when in a cognitive tutor environment.  

CS6460

!7

 
     The help-seeking model is also able to predict help-seeking errors using 80 if-then 
statements or rules. Since these rules are inclusive, the actions of a student in the 
cognitive tutor are labeled as desired or undesired help-seeking behavior. This can be 
achieved by comparing the student’s actions against the actions predicted by the help-
seeking model. In this way, a student’s moment by moment help-seeking behavior can 
be assessed in real time. As expected, less desirable forms of help-seeking behavior 
went hand in hand with poor learning at the domain level.   

There is a close relationship between intelligent tutoring and cognitive learning 

theories and design. ITS is an ever changing arena and there is currently ongoing 
research to improve the effectiveness of ITS. ITS serve to resolve the problem of 
students being too dependent on instructors for a quality education. By providing access 
to high quality education to all students, ITS can help change the education system for 
the benefit of all. 

References 

Intelligent Tutoring System. (n.d.). Retrieved February 12, 2016, from https://

en.wikipedia.org/wiki/Intelligent_tutoring_system 

Intelligent Tutoring Systems. (n.d.). Retrieved February 12, 2016, from http://

www.cse.msu.edu/rgroups/cse101/ITS/its.htm  

Intelligent Tutoring Systems: Can They Work for You. (n.d.). Retrieved February 

12, 2016, from http://www.opencolleges.edu.au/informed/trends/intelligent-tutoring-
systems/ 

New Potentials for Data-Driven Intelligent Tutoring System Development and 

Optimization. (n.d.). Retrieved February 12, 2016, from http://www.columbia.edu/
~rsb2162/New potentials for ITS-source.pdf 

Koedinger, Kenneth R., Emma Brunskill, Ryan Baker, Elizabeth A. McLaughlin, and 

John Stamper. "New Potentials for Data-Driven Intelligent Tutoring System 
Development and Optimization." 2014. Web. 12 Feb. 2016. 

Gutstein, Eric. “Learning from Students to Improve an Intelligent Tutoring System." 

Web. 12 Feb. 2016. 

CS6460

!8

Roll, Ido, Vincent Aleven, Bruce M. McLaren, and Kenneth R. Koedinger. 
"Improving Students’ Help-Seeking Skills Using Metacognitive Feedback in an 
Intelligent Tutoring System." 2011. Web. 12 Feb. 2016. 

CS6460

!9

