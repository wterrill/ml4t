CS 6460 – Progress Report 

Intermediate Milestone #1 

 
Will Johnson 
tjohnson306 
3/6/16 
 

OMSCS Head TA Tool Suite 
 
Project Background 
 
As specified in the Project Proposal, this project is to build a Tool Suite of tools for 
Head TAs in the OMS program to help automate tasks relating to GitHub repository 
creation and maintenance (both for individual students and for teams) as well as for 
T-Square grade maintenance. 
 
 
Achievements in the Reporting Period 
 
Since approval of the project last week, the following activities have occurred: 
 

  Creation of a GitHub repository for the project.  This is currently a private 

repository, but I’m happy to grant read access to anyone who would like to 
follow the development and perhaps comment on possible improvements.  
Just e-mail me at tjohnson306@gatech.edu and give me your GT GitHub 
username. 

  Created the project in Eclipse. 
  Wrote the code which parses the JSON configuration file into the Context file 

for the project. 

  Began writing the “Executor” functions which will execute the various tasks 

 

against GitHub. 
Initial GitHub access is based upon some rudimentary legacy code written by 
a former Head TA for OMSCS 6300, but due to some of the problems listed 
below, refactoring has begun to use the JGit set of tools rather than low level 
web calls. 
 

 
 

 

Problems Identified 
 
During the course of the week, it became obvious that there’s no simple way to do 
certain things like Cloning repositories or adding files to repositories using the web-
url interface that previous code has used.  This has necessitated moving towards a 
tool called JGit, which supports full Git functionality from within a JAVA application.  
This refactoring has only comparatively recently begun, so at this point that’s a 
major unknown in terms of the project. 
 
The team also learned that programmatic access to T-Square does not appear to be 
easily accomplished through Java.  As a result, we will definitely be going to the 
scaled back version of the T-Square tool suite as touched on in the project proposal, 
and may add a suite of tools for automatically checking out student submissions 
from T-Square in preparation for grading.  Scripts for this already exist and work 
quite well, which is why they were omitted from the original proposal, but 
consolidating them into a single Java suite of tools would be a good idea, and so if 
the team decides that the project needs some additional work, this may be added. 
 
 
Next Up 
 
This week, we will be continuing to work on the use of JGit as well as finalizing the 
beta release of the GitHub tool suite. 
 
We will also be writing the Beta test plan for the project and will begin work on the 
user documentation for this portion of the tool suite. 
 
 
Conclusion 
 
With minor bumps in the road, the project is generally proceeding according to plan.  
There are no reasons to expect any delays or significant alterations from the initial 
project proposal. 

