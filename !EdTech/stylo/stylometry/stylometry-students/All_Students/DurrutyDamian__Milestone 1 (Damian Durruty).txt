Milestone 1 

Progress Report 

Damian Durruty 

CS64640 – Educational Technology 

 

Thus far I’ve been doing significant research into all of the technologies that I will be using for this 
project, including: Bluemix, Watson APIs, AlchemyAPI, Javascript, Node Package Manager, CloudFoundry 
CLI, Node.js, HTML, CSS, and GitHub. Because I have zero web development experience and only a 
modest amount of programming expertise, this undertaking is both ambitious and overwhelming. I am 
trying to learn the least I need to develop the MVP (minimum viable product), and I’m still confident 
that I will be able to master the rudimentary technical skills I need to execute the core features of my 
project. 

In addition to the intensive research, I have also developed a rough prototype of my application using 
some helpful Watson starter code that IBM provides. My goal is to use that starter code to modify 
mostly the visual elements while keeping the underlying server code intact. The goal is to leverage the 
resources available to me without starting from scratch and “reinventing the wheel,” so to speak. I will 
still need to work on the entire stack, not just the UI/UX aspect, but given my minimal expertise I will 
need as much of a head start as possible. So far this barebones prototype has the most fundamental 
functionality: it takes student data and generates a personality profile. I will need to see if I can integrate 
both Personality Insights and Tone Analyzer APIs without having to pay, but I am happy that I was at 
least able to get *something* up and running this quickly. There’s plenty left to accomplish, but it’s a 
promising start. 

I am have already invested time and effort learning the stack that I will be working with, but I am curious 
for those with web development or software development what has helped and what I should avoid? 
Are there common traps or pitfalls I should look out for as a newcomer? Are there are methodologies or 
paradigms I should employ? What workflow works best for a project of this scale? Any general advice? 
To recap, I am building a Watson powered web-based app that will take student data (assignments, e-
mails, Piazza posts, etc.) and produce a personality profile as well as analyze sentiment. This will allow 
instructors to get an overview of how the class is doing (frustrated, energized, apathetic, etc.) and how 
each student should be approached. 

