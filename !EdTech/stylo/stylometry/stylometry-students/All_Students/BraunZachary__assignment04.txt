In my previous essays I wrote about my interest in language learning technology. This
(cid:3)
is in part due to my own use of language learning packages
, but also because I desire to
understand how technology can bring about the revolution in education that each of these
packages promise. I found that despite all promising to leverage technology to teach you a
language, each of the major players employ diﬀerent technological techniques to teach, be
convenient to use, and to keep the attention of their students. In fact, these techniques are
the major selling feature for each package and are how they diﬀerentiate themselves from
each other in a relatively crowded market. Through my research, I came to the conclusion
that the jury is still out on how eﬀective some of these methods are (see Gami(cid:12)cation of
Education [5] as an example), but I came to the conclusion that questions of eﬃcacy might
not be the most interesting way to look at language learning technology.

When you examine how diﬀerent each of the available language learning packages are, it
becomes clear that each attempts to grab on to a portion of traditional language learning
education and \disrupt" it in some way. \Disruptive technology" has certainly become a
buzzword these days and carries with it all sorts of positive and negative connotations, but,
as (cid:12)rst described in The Innovator’s Dilemma, it is the tendency towards experimentation
without concern for proven results which distinguishes disruptive technology from \sustain-
ing" (or more established) technologies. When successful, disruptive technologies can topple
the status quo [3]. when not successful, the low cost and speed at which these technolo-
gies can be employed means that failure doesn’t produce the same sorts of anxieties as it
does for entrenched technologies. In other words, these language learning technologies (cid:12)nd
themselves in a situation with huge bene(cid:12)ts to success, but relatively few negatives to failure.
This pattern of disruption can be seen in other recent applications of technology in
existing markets. Famously, ride sharing companies such as Uber or Lyft perceived the
world of for-hire transportation diﬀerently than the established, well entrenched players and
told a story about how technology coupled with marketing could bring about a revolution in
transportation. For relatively little cost to themselves, ride sharing companies were able to
experiment with apps that tapped into, and helped shape what is now known as the sharing
economy.

If we accept the idea that the drive and willingness to experiment with new technologies
in existing markets is what distinguishes disruptive and sustaining technologies, then it is
not the eﬃcacy of language learning technologies that make them unique.
Instead, their
most important aspect is how well each can (cid:12)nd and exploit a niche in their wider market.
Put another way, it is the story each tells about language learning and how well they perform
their role inside that story that needs to be understood to understand the language learning
\revolution". Yet, unlike in the world of transportation technology, little eﬀort has been
y
made to understand the narratives in language learning technology

.

Thus, this is what I propose for my project. I will deeply examine the narratives of a
collection of the major language learning technologies and attempt to distill these narratives.

(cid:3)
y

Duolingo, Busuu, and Rosetta Stone
This almost certainly because there is far less money and excitement around educational technologies
in general. Everyone can call a car and immediately see the bene(cid:12)ts of ride sharing, it takes desire and time
to learn a language.

1

I will supplement standard research materials with interviews from as many of these compa-
nies as possible to see, in their own words, how they describe their market and techniques.
From these narratives, I will examine how the technological techniques (e.g. social media,
multi media, gami(cid:12)cation, etc.) they employ are performative of the narratives they tell
about language learning.

I do not intend to produce a paper which will answer the question of which technology is
best or how likely any is to succeed, instead, I hope to produce a research paper that might
serve as a guide to understanding the language learning narrative in much the same way that
Gami(cid:12)cation of Education attempted to produce a guide to the game techniques employed
(cid:3)
in education technology
. It is my belief that by understanding sectors of the educational
technology, we can began to piece together an understanding of the patterns and ideals of
the whole.

(cid:3)

Though with less focus on categorization.

2

Annotated Bibliography

[1] Abilene Christian University

2016. Mobile-learning research.
http://www.acu.edu/technology/mobilelearning/Research/.
Note: This page contains the abstracts of a large number of research papers
surrounding the topic of mobile learning. Their subject matter ranges from questions
about eﬃcacy to questions about motivation.

[2] Busuu

2016. About busuu. https://www.busuu.com/about.
Note: Busuu’s about page lists what it feels are the primary selling points of the
software. It highlights features such as being \designed by teachers", allowing students
to \learn from native speakers", and its ability to be used \on both mobile and the web"
to \(cid:12)t your busy schedule".

[3] Christensen, C. M.

1997. The Innovator’s Dilemma: When New Technologies Cause Great Firms to Fail.
Harvard Business School Press.
Note: Though more business oriented, this book coined the now commonplace term
\disruptive technology" and talked about how such technologies can cause widespread
changes in an industry.

[4] Duolingo

2016. Duolingo: Learn languages for free. https://www.duolingo.com.
Note: Duolingo’s about section primarily touts its use of gami(cid:12)cation and, to a lesser
extent, its mobile apps.

[5] Fiona Fui-Hoon Nah, Qing Zeng, V. R. T. A. P. A. and B. Eschenbrenner

2014. Gami(cid:12)cation of education: A review of literature. In HCI in Business, Pp. 401 {
409.
Note: This is a literature review of methods and outcomes for gami(cid:12)cation in
education. The paper (cid:12)nds that the literature on the subject is still only in its early
stages and that the results from early research into the (cid:12)eld have been mixed.

[6] Rosetta Stone

2016. About rosetta stone. https://www.duolingo.com.
Note: Rosetta Stone and its competitors seem like an interesting historical bridge
between the modern language learning software packages and the classroom
environment. These packages have the highly structured rubrics of the classroom, but
with some of the convenience and (cid:13)exibility of educational technologies.

[7] Wikipedia

2016. Language immersion. https://en.wikipedia.org/wiki/Language_immersion.

3

Note: Though I am loath to cite Wikipedia, many of the resources the article cites seem
to be useful. Unfortunately, most of these scholarly articles are locked behind journal
subscriptions and I cannot, yet, access them. For now, I’ll cite the Wikipedia page and
try to unearth more rigorous articles as I go.

4

