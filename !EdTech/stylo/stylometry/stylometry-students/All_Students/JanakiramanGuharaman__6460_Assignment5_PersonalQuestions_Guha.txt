CS 6460- Education Technology 

Individual Assignment #5 (Personal Question): by Guharaman Janakiraman 

Abstract 
 
This paper explores three personal questions:  

1.  How can technology best be used to teach soft skills?  
2.  Are personal productivity techniques (such as pomodoro) effective in this regard?  
3.  How can courses best be developed / altered to take advantage of these 

techniques? 

Technology for Soft Skills  
Answering the question “What exactly are soft skills?” is not straight forward, because 
the perception of what is a soft skill differs from context to context. A subject may be 
considered a soft skill in one particular area, and may be considered a hard skill in 
another. Even Wikipedia provides a very general and a broad brush definition- “Soft 
skills refer to the cluster of personality traits, social graces, and facility with language, 
personal habits, friendliness, and optimism that mark people to varying degrees. Soft 
skills complement hard skills, which are the technical requirements of a job.”  
 
Examples include: Communication skills, Critical and structured thinking, Problem 
solving skills, Creativity, Teamwork capability, Negotiating skills, Self-management,  
Time management, Conflict management, Cultural awareness, Common knowledge, 
Responsibility, Etiquette and good manners, Courtesy, Self-esteem, Sociability, Integrity 
/ Honesty, Empathy, Work ethic, Project management, and Business management 
 
These are generally behavioral as opposed to demonstrable. The focus is on “how one 
goes about it” as opposed to “what they accomplish”, and generally involves the human 
element.  
 
In order to learn soft skills, two methods are quite popular 

1.  One way is enrolling for formal training, e.g. taking evening classes on 

communication or presentation skills, cultural awareness, etc.  

2.  The other way of acquiring soft skills is self-training. Changing of behavior or 

personal traits often requires long-term practice and therefore self-training might 
be more useful regarding the improvement of this category of soft skills.  

3.  During the last decade another method of self-training has become increasingly 

popular. Electronic Learning, usually abbreviated to e-learning. The article “Yes, 
web based training can teach soft skills” (Horton, 2007) emphasises the 
practicality of this approach. 

 
Quoting my personal experience, I took the course "Psychology of Leadership" by Prof. 
Tal Ben Shahar available on Youtube. While the concepts can be understood and 
imparted effectively using the techniques above, it could take a life time of practice. I 
have taken the concepts from the course and have been practicing them. But it is easier 
said than done in terms of mastery. As real-life situations come up as a litmus test to act 
and demonstrate the soft skills, it is not as easy as learning a hard skill.  

 

CS 6460- Education Technology 

Individual Assignment #5 (Personal Question): by Guharaman Janakiraman 

 
A first step in improving soft skills of students is to raise their awareness about the 
importance of soft skills and the consequences of shortcomings in this regard. 
Technology can play a significant role in this regard.  
 
Technology can be leveraged to impart such concepts. Significant improvements have 
been achieved in the way e-learning courses have evolved, including story boarding, 
interactions, etc. Technology has become a great enabler in connecting the right talent 
even for something considered to be a softer skill.  

Personal Productivity Techniques in teaching Soft Skills  
Soft skills are also based on a set of basic principles. Pomodoro can be effective in 
teaching the concepts or core principles, as there is a time box to learn them. While 
efficiency is one aspect of teaching soft skills. Effectiveness of how much the learner has 
learned, is a whole different challenge. Effectiveness of learning soft skills, cannot be 
time bound. Technology can help bridge the gap in achieving effectiveness, such as 
online forums or online interaction tools can help with enhancing communication, as an 
example.  

Potential Modifications to Courses  
Courses can be modified to have a standard time box. As per the reference 7, Hemrooke 
and Geri Gay, laptops (generalized to online in 2013) offering an environment for multi-
tasking can take away from the intended learning experience. This can be addressed by 
modifying the courses to fit a predictable time-box (or a Pomodoro). While this is 
limiting the intended flexibility offered by courses, this would be easier for students to 
adapt to over a longer term leading to habit formation.  
 
For instance, two courses that I take may have a totally different treatment of the lecture 
and module time. One may have short lectures of 5-10 minutes videos, whereas some 
other MOOC course may have over 30-45 minutes lectures. It becomes impossible for 
students to get to a predictable pattern that they can follow.  
 
If courses can be tailored to fit to a fixed length, it helps students plan their schedule and 
accomplish better success 

Sources 
Sources include the following:  

1.  Schulz, Bernd. "The importance of soft skills: Education beyond academic knowledge." 

(2008). Available at: 
http://ir.polytechnic.edu.na/bitstream/handle/10628/39/The%20Importance%20of%20
Soft%20%20Skills-Education%20beyond%20academic%20knowledge.pdf?sequence=1 

2.  Chase, Mackie, et al. "Intercultural challenges in networked learning: Hard technologies 

meet soft skills." First Monday 7.8 (2002). Available at: 
http://firstmonday.org/ojs/index.php/fm/article/view/975/896 

 

CS 6460- Education Technology 

Individual Assignment #5 (Personal Question): by Guharaman Janakiraman 

3.  Adams, J. & Morgan, G. (2007). “Second Generation” E-Learning: Characteristics and 
Design Principles for Supporting Management Soft-Skills Development. International 
Journal on E-Learning, 6(2), 157-185. Chesapeake, VA: Association for the Advancement 
of Computing in Education (AACE). Retrieved February 15, 2016 from 
http://www.editlib.org/p/19865 

4.  Lee, William W., and Diana L. Owens. Multimedia-based instructional design: computer-

based training, web-based training, distance broadcas:t training, performance-based 
solutions. John Wiley & Sons, 2004.  Available at: 
http://samples.sainsburysebooks.co.uk/9780787973445_sample_386377.pdf 

5.  Govindasamy, Thavamalar. "Successful implementation of e-learning: Pedagogical 

considerations." The internet and higher education 4.3 (2001): 287-299. Available at: 
http://www.qou.edu/arabic/researchProgram/eLearningResearchs/successfulImplemen
tation.pdf 

6.  Carbone, Thomas A., and Sampson Gholston. "Project manager skill development: A 

survey of programs and practitioners." Engineering Management Journal 16.3 (2004): 10-
16. Available at: http://www.tomcarbone.com/papers/Carbone-EMJSept04-PM.pdf 

7.  Hembrooke, Helene, and Geri Gay. "The laptop and the lecture: The effects of 

multitasking in learning environments." Journal of computing in higher education 15.1 
(2003): 46-64. Available at: http://www.ugr.es/~victorhs/recinfo/docs/10.1.1.9.9018.pdf 
 

 

