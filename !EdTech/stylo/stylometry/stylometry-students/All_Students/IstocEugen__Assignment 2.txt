Assignment 2: Exploring Your Area

2

3

4

1

In the ﬁrst assignment, I proposed a few area of research which predominately focused 
on the interaction between the TAs and the students, as well as the interaction between the 
students themselves. In exploring this area further, I have come to the realization that in another 
sense, the problem can be also approach from the side of the lecture presentation itself. That is, 
the number of students needing help in a particular class is somewhat inﬂuenced by the clarity 
(or lack of) at which the material is presented in the lecture videos . I have, hence, decided to 
narrow my area of research to this method of delivery. 
It is difﬁcult , when producing lecture videos, to determine the best way to explain a 
given concepts. For example, being too broad (or general) compromises on the important 
particulars, whereas being to speciﬁc compromises on the overall concept as well as adding 
more length to lectures . Thus, a middle ground is many times chosen — present a concept in 
way where enough details are presented to achieve the main scope of the topic at hand. This 
way, most of the students are accommodated . Its important to note, however, that although this 
can be considered a well deﬁned goal, it fails to really fully accommodate students of all levels. 
As such, my proposal and area of research is to explore a mechanism to deliver online video 
lectures which can accommodate students in a much more dynamic range of understanding.
I propose the concept of dynamic time-interval content delivery for video lectures. There 
are two components to this which work harmoniously to achieve the desired purpose. First, the 
way that the lecture is prepared from a content and sentence structure is important. Each 
sentence should generally be atomic, and have little to no reference to previous sentences. The 
second component involves the interval playback of the lecture video using the html5 video 
spec . That is, the video player plays the video at a predeﬁned interval — going from one atomic 
sentence to the next. These components are further explained below.
high level, the tutorial would looks like so:

Consider a simple tutorial on creating a gmail account and sending an email. From a 

5

This high level representation however, makes several assumptions about the user viewing the 
tutorial. It assumes that the user already knows how to create an account, as well as how to 
compose an email — it assumes familiarly with the gmail email system. 

 A in the case of OMSCS or other online classes which are driven by the video delivery of its 
1
content.
 http://www.jite.informingscience.org/documents/Vol11/JITEv11IIPp227-250Brecht1091.pdf
2
 So that particular can be captured.
3
 Students can vary in their level of understanding — accommodating as many can generally be 
4
considered a well made lecture.
 http://dev.w3.org/html5/spec-author-view/video.html
5

The goal, at this point is to decompose this generic tutorial and introduce explanatory steps like 
so:

Notice that at each level, more information is introduced to add clarity to the instruction. 
Therefore, the complete training video produce by the lecturer, would actually be on the order of 
all of the levels. The level  at which the training video would play, however, is dependent on the 
user. The user decides at which level the choose to view the tutorial based on their own 
determination of whether they understand the instruction or not. Viewing the lecture at a higher 
level would result in a longer tutorial, and viewing it at a lower level would result in a shorter 
lecture. This mechanism, I believe, can accommodate students of various levels of 
understanding.

6

6

 The level is synonymous to the playback interval in this case

