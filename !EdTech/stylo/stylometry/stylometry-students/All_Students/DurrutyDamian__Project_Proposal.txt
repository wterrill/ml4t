 

 

 

 

 

Project Proposal 

CS6460 - Educational Technology 

Damian Durruty 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

The Problem 

2 

Managing a large class can pose unique challenges that instructors in smaller classes may never 
encounter. In a small- to moderately-sized class, there are usually enough interactions between 
the instructor and the students for everyone to get a sense of the dynamics, personalities, 
preferences and sensibilities of the participants involved. Knowing these subtle yet crucial 
subtleties can make the difference between a sterile, lifeless class and a compelling, 
personalized learning environment. The problem is especially exacerbated in the case of 
current online classrooms: there are few visual or audial cues to distinguish a frustrated student 
from an enthusiastic one, and textual content can often be difficult to interpret, forcing both 
parties to “read between the lines.” This in turn frequently results in inaccurate judgements or, 
in some cases, hidden grievances. 

 

Moreover, in a large class there are so many submissions that it can be difficult for any 
instructor or TA to get a sense of the student’s unique learning style. It is widely known that 
every person processes the same information differently. Some learners prefer social settings, 
some are autodidacts, while others have a preference for visual, audial, tactile, or kinesthetic 
pedagogical content. One student may learn best by taking notes while listening to a lecture, 
while another may process the same information most effectively if she paraphrases it while 
pacing back and forth. It is very difficult to personalize instruction if there are so many students 
that it becomes impossible to target feedback to anyone except the hypothetical, non-existent 
“average” student.  

 

Existing Solutions 

There are many solutions in the EdTech space that aim to the address the problem of regulating 
large classes. Platforms such as T-Square and Piazza, which manage submissions and class 
discussions, arguably fall into this category. Other tools such as DyKnow [1] and Alma [2] 
employ data-driven behavioral models to capture disparate student interactions (“360 degree 
view”) and predict learning outcomes. Some educators have even administered personality 
diagnostics based on the Myers-Briggs Type Indicator to evaluate the personality types that 
succeed in different courses [3]. However, in spite of thorough research, I have not 
encountered any comprehensive technical solution that specifically address the niche problem 
discussed above. 

 

Other solutions have attempted to replace human instructors entirely through the use of AI. 
Cognitive Tutor, developed by Carnegie Mellon University, is an Intelligent Tutor that can be 
programmed for specific domains or problems, while the Andes Physics Tutor by Arizona State 
University targets beginning Physics students in particular. Although these AI tutors can 

 

3 

supplement human instruction, they are by no means a replacement. Intelligent Tutors are 
rigid, limited in scope, and usually not capable of learning or radically adapting to new 
circumstances. Needless to say, this makes them ill-suited for addressing a student’s changing 
emotional needs and learning preferences [4][5]. 

 

Proposed Solution and Design 

I will use IBM’s Watson—a cognitive AI system specialized for Natural Language Processing 
(NLP), to build a web-based application capable of analyzing a student’s output (e-mails, forum 
posts, assignments) and determining the student’s personality and (current) sentiment. These 
analytics will be outputted in the form of various easy to interpret visualizations such as pie 
charts, bar graphs and pictographs. All of the inputted student data and outputted analytics will 
be stored in a database and accessible via a master view that will allow instructors to easily 
acquire a “bird’s-eye” perspective of a class. 

This tool will serve three primary purposes: 

 

 

 

It will allow instructors to get an overall feel for how the overall class is progressing. If 
the overall class sentiment is declining (for instance, if students are becoming relatively 
more frustrated or angry with time) then the teacher knows that something is clearly 
going awry. 
It will give instructors the ability to identify which students are struggling the most and 
consequently which students require the greatest amount of attention. Frustrated, 
disappointed, angry or sad students will likely require more assistance than enthusiastic, 
self-motivated learners. 
It will permit educators to identify the type of personalized help each student needs. A 
shy, introverted and insecure student will require a completely different pedagogical 
approach than an energetic and confident learner. 

Again, while these critical insights are easy to determine in small classrooms, they are very 
difficult to assess and keep track of in larger settings or in online classrooms. 

 

Contributors 

This will remain a solo project for the time being, albeit I am open to the possibility of a team 
effort if my project overlaps with or complements another willing student’s project, or if the 
scope of my project expands. 

 

 

 

Technology 

4 

The underlying analytics will be performed by Watson, which uses sophisticated natural 
language processing techniques, parallelization, machine learning, and evidence-based retrieval 
algorithms to generate insights based on textual content. IBM offers a series of distinct Watson 
APIs as cloud services. These services range anywhere from “Concept Expansion” to “Language 
Translation” to “Relationship Extraction.” For the purposes of this project, I will only be 
employing the “Personality Insights” and “Tone Analyzer” APIs. The latter is still in beta and 
may not perform up to the standard; only experimentation will tell whether it can be reliably 
used to generate accurate results [6]. 

The Watson APIs can be accessed via IBM’s Bluemix cloud platform. Bluemix not only allows for 
different runtime environments such as Node.js, Python, Java and, PHP, also but offers free 
trials of all of its various services pending certain conditions. Bluemix also boasts GitHub and 
CloudFoundry integration so that code management is as seamless as possible.  

I will be building my Watson application using a combination of Node.js, HTML, CSS, and 
MongoDB while using GitHub to keep track of my code progression. Fortunately, IBM offers 
various “template apps” for each one of its Watson services that can be used as a starting point. 
In addition, there is abundant documentation and a burgeoning community of Watson 
aficionados eager to share their expertise. IBM is also pushing its Watson Ecosystem, a 
community of Watson-powered businesses that serve as living proof of the amazing things can 
be accomplished with its cognitive platform. 

 

Integration and Fallback 

I would ideally like to integrate my proposed tool with T-square and Piazza, since those are the 
two main platforms that facilitate student contributions and submissions. E-mail integration 
would provide the most trenchant insights, but privacy concerns might obstruct this avenue 
(although I might implement this purely as a proof-of-concept feature). If for technical or 
bureaucratic reasons these integrations are not possible, I can always scale back my application 
so that it only accepts uploaded documents or manually inputted blocks of text, albeit this 
would make the process less streamlined and more cumbersome. At the very least, I would like 
the application to be able to integrate with Piazza in some capacity since that is the platform 
that would most accurately gauge overall class sentiment. Dr. Joyner did mention that Piazza 
can export JSON objects--if that’s the case then Piazza integration would not pose a 
tremendous challenge.  

 

 

 

 

 

Task List 

5 

The following tasks must be completed for the project to function in its current proposed form: 

Implement analytics visualizations and formatting 

  Set up and configure Bluemix account 
  Set up and configure a GitHub repository 
  Branch off Personality Insights Template app 
  Thoroughly research selected technologies (Node.js, MongoDB, Watson API, HTML/CSS) 
  Test if Personality Insights can be used simultaneously with Tone Analyzer in same app 
  Create backend database that will store student input 
  Create document and text submission interface and integrate with database 
 
  Create master view summarizing overall sentiment and list of personalities 
  Piazza integration 
  E-mail integration (if technically feasible within allotted time) 
  T-square integration (if technically feasible within allotted time) 
  Test, debug, refine, repeat (involve other student input if possible) 
  Prepare Functional prototype 
  Write final paper 
  Produce trailer 
  Generate progress reports (x2) 

 

Milestones 

Two milestones will consist of simple written progress reports that discuss the progress I’ve 
been able to accomplish thus far. 

A third milestone will consist of a functional prototype that fellow peers can experiment with. 
The goal of this milestone is to receive quality feedback and help detect bugs that I might have 
missed. I anticipate that my proposed tool will be in full swing by this point with most of the 
important functionality already implemented. 

The fourth and final milestone will consist of a polished video trailer showcasing my work. I will 
use Camtasia to record a screengrab of the revised prototype accompanied by an elevator pitch 
or brief narrative explaining the usefulness of my “revolutionary” tool. This last milestone will 
require extensive video editing and by far the greatest amount of time and effort. 

 

 

 

 

Schedule 

6 

Week 
of 
2/22 

2/29 

3/7 
3/14 

3/21 

3/28 
4/4 

4/11 
4/18 

4/25 

 

Tasks 

Revise proposal with feedback from mentor. Research Node.js, Watson APIs, 
HTML/CSS, Bluemix, Github, MongoDB. Verify that Personality Insights and Tone 
Analyzer can be used in tandem free of charge. 
Branch off Personality Insights Template App. Create and customize basic interface 
for text and document submissions. Thoroughly test Tone Analyzer to ensure that it 
works well despite being in beta.  First progress report. 
Create and customize analytics output visualizations. Polish submission interface. 
Create MongoDB database, link with submission interface. Polish analytics output. 
Second progress report. 
Create master view, link with database to display overall class sentiment and 
personality types. 
Test and debug. Polish master view. Prepare functional prototype. 
Attempt Piazza integration. Continue testing and debugging using feedback from 
other students. 
Polish Piazza integration. Prepare a polished trailer. 
Attempt T-square and/or e-mail integration (if time permits). If not on track, use this 
week to catch up and complete basic functionality or to perform further testing and 
debugging. 
Finalize presentation, paper and project. 

References 

[1] DyKnow. (n.d.). Retrieved February 21, 2016, from http://www.dyknow.com/ 

[2] Meet Alma: The world's first truly integrated SIS LMS. (n.d.). Retrieved February 22, 2016, 
from http://www.getalma.com/ 

[3] Crews, T. B., Sheth, S. N., & Horne, T. M. (2014, February 24). Understanding the Learning 
Personalities of Successful Online Students. Retrieved February 21, 2016, from 
http://er.educause.edu/articles/2014/2/understanding-the-learning-personalities-of-
successful-online-students 

[4] Woolf, B. P. (n.d.). Intelligent Tutors: Past, Present and Future. Retrieved February 21, 2016, 
from http://www.adlnet.gov/wp-
content/uploads/2011/08/woolf_intelligent_systems_part1_iFest2011.pdf 

[5] EDUCAUSE. (2013). 7 Things You Should Know About Intelligent Tutoring Systems. Retrieved 
February 21, 2016, from https://net.educause.edu/ir/library/pdf/ELI7098.pdf 

 

7 

[6] Watson Services: Take your first step into the cognitive era with our variety of smart 
services. (n.d.). Retrieved February 21, 2016, from 
https://www.ibm.com/smarterplanet/us/en/ibmwatson/developercloud/services-catalog.html 

 

