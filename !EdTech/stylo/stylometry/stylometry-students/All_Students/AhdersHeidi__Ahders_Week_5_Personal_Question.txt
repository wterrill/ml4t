Ahders 1 

Heidi Ahders 
GT ID: hahders3 
Mentor Question Assignment 
Dr. Joyner 
February 12, 2016 

  
  

Mentor Question 

 
Question: 
 
Because of its mechanism (administering a survey to current OMSCS students), your project will likely focus in 
on questions of gender balance in OMSCS specifically.   
 
How does the prevalence of women in computer science and related fields differ between online and 
"traditional" programs? What unique factors exist in each setting?  (E.g., are there differences in retention by 
gender?  Does the applicant pool differ in other key demographic dimensions, such as country of origin or age, 
that may tend to lead to differences in gender representation as well?) 
 
Answer: 
 

The question that I was given was to find out if the prevalence of women in computer science and 
related fields differ between online and "traditional" programs. I did find a graph of online students versus 
traditional students at Georgia Tech, which I have included. I thought it was really interesting in the 
demographics.  

Ahders 2 

 

I thought it was really interesting to see the differences in the online versus the on­campus demographics, which 
was taken in 2013​[5]​. Because Georgia Institute of Technology wants to make sure their new program succeeds, 
they have this data, but I found that finding it at other schools was a challenge. Schools do not publish just 
computer science data for the schools with demographics and then separate it out by traditional and online. I 
know that the school that I teach at has hybrid classes and they still don’t have the data readily available. The 
statistics that I found, which I had previously stated in an earlier paper was that OMSCS program has 13 percent 
women in the program. At Stanford, a traditional college setting there are just under 21 percent women in the 
undergraduate CS major​[6]​. Carnegie Mellon, again a traditional college setting there were 40 percent women in 
the incoming first­year computer science class in the fall of 2000. This charts shows a steady growth in their 
program to attract and admit women to their program.​[1] 

Ahders 3 

 

Harvey Mudd in California is another traditional school that has increased their percentage of women in 
computer science to 40 percent​[7]​. I think that Georgia Institute of Technology and specifically the OMSCS 
program has a perfect opportunity to improve their program to attract more women into it. OMSCS program 
should look at what these other schools are doing and build on their success and get to 40 percent women in the 
program. 
 

When looking at the unique factors of each setting I think back to when I decided to go back to school. I 

took a Introduction to Computer Programming class at the local community college. It happened to be a Java 
programming class. Now I had already been teaching myself to program for years, you could say on the job 
training. What I noticed in the class was that there were twelve of us and I was the only female student. The 
setting here is that we are all together programming. I have to say none of the gentleman in the class gave me a 
bad time or the instructor. Everyone was supportive and I knew quite a bit more than some of the other students. 
The instructor said I could have probably taught the course. That made me feel good. We did the programming 
together in class with a partner or could ask questions. I feel that in the online setting when we are given a 
project and told we need to do it on our own that we are on our own. I am truly feeling this in my other class 
where I am a bit lost. I wish we had partners to work out the programming part. So, I think the difference for me 
is that I don’t feel as supported in the online situation as I did in the traditional class.  
 

Gender retention in the computer field is a big deal. It starts early on in elementary school and goes 

through to the PhD program. In the article, “Improving the Persistence of First­Year Undergraduate Women in 
Computer Science,” by Rita Manco Powell from the University of Pennsylvania[3], it looks at how to retain 
women and finds they were losing women, they were leaving. The introductory class had 25 percent women, 
but the second class in the series only had 19 percent women. They were asking why they were leaving. When 
they were asking why they were leaving, they found many of the students lacked prior knowledge of 
programming and the male counterparts had been programming for a while. This inequity of skills has been 
found to drive them out. The study also found the male students in the class were not letting the female students 
participate in the study groups and they were too timid to ask for help. By telling the female students to work on 
the projects by themselves this  made them feel isolated.  
 

The National Center for Educational Statistics keeps track of all the students that have graduated and 
this is where many of the statistics are found, but only for the nation as a whole and with the institutions of 
15,000 students or more and it isn’t broken down any further into degree programs. It would have been nice to 
find the data here since you can download all of the data to a spreadsheet. As to the question of demographics, 
this has been shifting in the computer science field. According to the NCWIT Scorecard, the number of women 
receiving a bachelor's degree in computer science declined from 2000(10,263 women) to 2012 (8,917 women) 
and the racial/ethnic composition shifted. ​[2]​ Looking at the chart in the article: 

Ahders 4 

 
But more women are since 2000 are getting a master’s degree as seen in this chart based on ethnicity: 

 

Ahders 5 

 

 

and the number of women achieving a doctoral degree increased as well:

 
 

Ahders 6 

References 

[1] Blum, Lenore. "Women in Computer Science: The Carnegie Mellon Experience1." ​Women in Computer 

Science: The Carnegie Mellon Experience 1​ (201):Carnegie Mellon, 2001. Web. 11 Feb. 2016.  

[2] "NCWIT Score Card: A Report on the Status of Women in Information Technology." ​National Center for 

Women & Information Technology (NCWIT)​ (n.d.): n. pag. ​NCWIT​. National Center for Women & 
Information Technology. Web. 11 Feb. 2016.  

[3]Powell, Rita Manco. "Improving the Persistence of First­year Undergraduate Women in Computer Science." 

Proceedings of the 39th SIGCSE Technical Symposium on Computer Science Education ­ SIGCSE '08 
(2008). Web. 11 Feb. 2016. 

[4]Crenshaw, Tanya L., Erin Wolf Chambers, and Heather Metcalf. "A Case Study of Retention Practices at the 
University of Illinois at Urbana­Champaign."​Proceedings of the 39th SIGCSE Technical Symposium on 
Computer Science Education ­ SIGCSE '08​ (2008). Web. 11 Feb. 2016. 

[5]"Georgia Tech Admits First Cohort Ahead of Online Master's Degree Program Launch." ​Georgia Tech 

Admits First Cohort Ahead of Online Master's Degree Program Launch​. N.p., n.d. Web. 12 Feb. 2016. 
<​https://www.insidehighered.com/news/2013/12/13/georgia­tech­admits­first­cohort­ahead­online­mast
ers­degree­program­launch​>. 

[6]Gallagher, Billy. "No Women In CS? Well, Not For Long." ​TechCrunch​. N.p., 27 Dec. 2012. Web. 12 Feb. 

2016. <​http://techcrunch.com/2012/12/27/stanford­bridging­gender­gap/​>. 

[7]Alvarado, Christine, Zachary Dodds, and Ran Libeskind­Hadas. "Increasing Women's Participation in 

Computing at Harvey Mudd College." ​ACM Inroads​ 3.4 (2012): 55. Web. 

 

