Introduction 
 
 
As you may have figured out by now my name is Kimanii Daniel. I was born and 
have lived most of my life in Grenada ­ spent a couple years in Dominica and St. 
Lucia. From the time i’ve known myself I have loved programming, something about 
solving problems. So I got my first degree in IT because I felt it would give my career 
a boost by making me more marketable. While at the university I fell in love with 
education and learning new things. I decided to continue my education to my 
Masters and was pointed to this program at Georgia Tech which has catapulted my 
knowledge and rekindle my love for technology and education. 
 
I took this class because I’m very interested in giving back to the body of education. I 
believe that knowledge is useless if you cannot share it or make the world a better 
place because of it. My hope is to create something of value to the education body 
and put my mark in history. Yes, tall order you may say but anything is possible if 
you put your mind to it. 
 
I have three possible problems I would like to solve that I believe will bring value to 
education.  
 
Problem 1 ­ Collaborative note taking using digitized pen, I've tried onenote 
but it's not the best for collaboration. I believe this can be a valuable tool for 
learning. 
 
Problem 2 ­ Online A.I. proctoring, using concepts from computational 
photography and ML to determine suspicious behavior while taking an online 
exam. I believe Proctortrack has this already (​http://www.proctortrack.com/​). 
So this not something that would be new to the community but maybe I can 
make it better. 
 
Problem 3 ­ Identity verification from written assignments. I saw this on the list 
of assignments and fell in love with the idea, so I did some quick research and 
realise that there is research in this area but I haven’t found a product that 
offers this service as yet so it might be a great opportunity. 
 
So essentially I have to choose one of these and work with it, and i’m leaning to #3. 
 

