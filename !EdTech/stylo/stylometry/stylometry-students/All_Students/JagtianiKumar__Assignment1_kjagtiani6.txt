My name is Kumar Jagtiani, I'm a Data Scientist from AT&T Labs in NJ.  I’ve been with them 
since the days of Bell Labs nearly 25 years now.  Although the historic and legendary Bell 
Labs taught me a lot, AT&T Labs for the past 20 years has challenged me intellectually and 
spiritually to achieve more through exploring, learning, sharing and teaching.  Because of 
the kindred spirit of the Labs, I find myself constantly evolving by educating myself.  I hold 
a BS in Computer Science, MBA in Technology Management and a sprinkle of certificates in 
Telecommunications.  I love to research and hold a few patents.  I’m always on the learn 
and really looking forward to this 6th class, bringing together 2 of my favorite subjects, 
Technology and Education.    

  

I feel the landscape of education is rapidly evolving with the introduction of MOOC and other 
technologies.  Coming from a strong research background, I’m hoping to expand my 
interest in learning how Virtual Reality would enhance the experience of education in the 
coming years. I’m not a strong programmer, but hope to do relevant research in the area of 
Virtual Reality with the guidance of my mentor, lectures in this class and material covered 
throughout the course to learn, research and design the future of education.  Although 
MOOC is far better than any online education platform in existence today, it still presents a 
visual void between Professor, TA and Student.   

 

The problem I’d like to address is one of researching and presenting a classroom experience 
to students in Virtual classrooms through the use of Virtual Reality technology.  With the re-
emergence of VR, wearables are taking on worlds of their own again.  I’d love to understand 
how this class can help me pursue a path of research in the field of Virtual Reality in 
Education Technology with more theoretical concepts and some proof of concept with little 
or no programming.  I’ve always been curious to learn, research, and hopefully present 
material to an extremely smart audience of self-motivated, deterministic and a focused 
generation of students that can justify positive reasoning for incorporating Virtual Reality 
within their learning.  

 

Distance learning does not have to make you feel as though you’re learning in a vacuum 
and isolated from other students.  If there was a way to place every student enrolled in a 
class within the realm of a classroom it would make learning so much more richer, debates 
more livelier, Q & A more interactive, and test taking in platforms such as Proctortrack 
possibly less stressful and more manageable than having a camera stare at you for the 
duration of the test. 

 

I’m really excited to be in this class, and am especially looking forward to working with the 
mentors and the famed Professor Joyner to guide me in brainstorming, leveraging library 
material, pairing up with like-minded researchers in tailoring my research interest in fusing 
Virtual Reality within the educational ecosystem.  

 

I’m hoping the guidance I receive in this class will allow me to achieve my goal of 
brainstorming steps needed to bring the virtual world within the educational community 
even closer by leveraging the ideas behind MOOC in Virtual Reality. 

