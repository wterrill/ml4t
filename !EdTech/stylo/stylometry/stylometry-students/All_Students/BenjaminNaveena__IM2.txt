 
 

                                                                                                                                        Naveena Benjamin  

 

 nbenjamin@gatech.edu 

         Intermediate Milestone - 2 

1  ABSTRACT AND AIM 

 

Capstone Projects currently do not have all required features to enable project management for 

multiple areas of discipline which integrates materials from different sectors of engineering. Capstone 

Project Management tool focusses on building an integrated platform where students and 

professors/mentors/sponsors can collaborate and keep track of capstone projects typically in 

undergrad engineering settings. Professors can have easy help in forming teams or groups among 

students and monitoring the progress of their work. Bill of material, status of orders placed, managing 

inventory from multiple vendors and products that can be either optical, chemical, electrical, electronic 

or mechanical can be managed easily. 

This tool will be a senior design / capstone/ product management interface. 

2  VIDEO TRAILER 

A video trailer is uploaded at this link - IM2 - video trailer 

Since the web tool is under development please excuse errors or hiccups you notice. 

The wireframe created on Axure is published at this link - Wireframe Link (Password: axure12345) 

3  WALK THROUGH 

1.  Add a Project – They logged in valid user can now create a new project by providing in the 

name of the project, the start date and the estimated due date. The newly created project will 

be added to the dashboard. 

2.  Project Overview – Clicking on a particular project can take the user to the project overview. 

This panel displays details such as the completion rate, the start date, end date, members in the 

team, their roles and so on. 

 
 

                                                                                                                                        Naveena Benjamin  

 

 nbenjamin@gatech.edu 

3.  Project Settings (Stages Tab) – The Stage Tab under project settings are used to monitor the 

progress of the project. Checking off the items can change the completion status and rate of 

the project.  

4.  Project Settings (Team Tab) – The Team Tab helps to view the team members, add more, edit 

their details or even remove them. The roles are determined based on what the school/ 

industry protocol has decided to be included for such projects. 

5.  Project Details (Plan Tab) – The Plan tab helps to provide details such as the links/URLs to 

project plan, business plan, wiki. The intention of not providing an uploading feature was to 

provide security and discretion to other folks not included in this project. 

 

4  TECHNICAL DESCRIPTION 

The below tables shows the description of the various tools that will be used for the development and 

building of the project. 

Item 

Wireframe 

Tools 

Axure 

Wireframe Hosting 

Axure Cloud 

Project Language 

Backend 

Frontend 

Charts 

Hosting 

 

 

 

Gantt Chart 

Heroku.com/pythonanywhere.com 

Other support 

GitHub, Virtualenv, Open sourced 

 

 

modules 

 

Languages 

 

 

Python – Django – 1.9 

MySQL / PostgreSQL / 

Mongo/SQLite 

Bootstrap 

 

 

 

 

 
 

                                                                                                                                        Naveena Benjamin  

 

 nbenjamin@gatech.edu 

5  SCHEDULED TARGET FOR MILESTONE - 2 

Date 

Item 

Description 

Mar 07 – Mar 13 

Project Overview Page 

The initial tab on project stages 

Project Settings –Stages Tab 

with all three stages will be 

completed. 

Mar 14 – Mar 20 (IM -2) 

Project Settings - Team Tab  

Plan Tab will be completed along 

Project Details – Plan Tab 

with the Team tab feature which 

can add/modify team members. 

 

 

Intermediate Milestone 2 – 20th March: 

  Project Overview Page with Project stages which covers the different aspects in a particular 

project 

  Team formation handling  

  Plan Tab from the Project details will be submitted.  

Video trailer displaying/describing the feature will be submitted. 

 

6  CHALLENGES AND CONCLUSION 

1.  The initial method of implementing the Project settings stages tab was as shown below –  

 
 

                                                                                                                                        Naveena Benjamin  

 

 nbenjamin@gatech.edu 

  

But during the implementation , the thought of having to shift between multiple tabs to fill in details 

made me go forward in having all stages under a single tab.  

2.  Similarly, the team tab had add, edit, delete of team members all in a single page as shown below. 

This seemed too cluttered and which is why I decided to implement a modal popup to make things 

look better and easier to the users. 

 

 
 

                                                                                                                                        Naveena Benjamin  

 

 nbenjamin@gatech.edu 

3.  During the process of implementing the plan tab, I was wondering if I should go for the uploading 

feature. But then the security of the doc links for each project may be different and so went onto 

providing just clickable links. 

There were few challenges but those I consider as part of the learning curve. It was interesting 

implementing these features. 

