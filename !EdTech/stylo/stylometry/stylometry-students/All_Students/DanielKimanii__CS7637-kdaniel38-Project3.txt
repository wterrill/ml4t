 
 
 

 
 
 

 
 

 
 
 
 
 
 
 

Kimanii Daniel 
kdaniel38@gatech.edu 

2014.09.13 

Build an AI Agent to solve 3x3 Raven matrix problems 

Project3 

 

 

kdaniel38

      1 

Raven’s matrix is one of the popular test of the human IQ, which is used to measure human 
intelligence. Raven’s tests consist of matrix of visual objects that are manipulated between 
squares with the last image missing in the last square, this is the one that needs to be 
determined from a set of multiple choice options. The key in most raven problems is to 
determine the transformations of the objects or groups of objects to determine what the last 
object should be.  
 

 

 
“3x3 Basic Problem 03” shows a 3x3 matrix, illustrating three columns, three rows of 
figures. If you look at image A, it represents a square, which image B represents as a 
circle and image C, a plus. Your first clue would be to determine what’s changing and 
what’s consistent between the rows. With that knowledge you would then infer the same on 
the mystery shape in row three. Looking at the rows you can infer that each row has the 
same type of shapes, in the same proportions except row three. Row three is missing the 
square to make it complete, which is answer #1. This forms the basis of the project ­ to 
build an artificial intelligence agent that can smartly apply methods of reasoning and logic 
to solve a set of Raven’s matrix tests, in particular 3x3 matrices. 

 
When building an AI agent to solve these tests, It is important to 
first determine how the input for the test would be passed to the 
agent. Fortunately the inputs will be done via textual representation, 
as shown in the diagram. All the visual tests have already been 
decomposed to a textual representation, this is parsed by the 
calling application and sent to the agent via objects that represent 
the problem set which is the entire set of figures and object 

kdaniel38

      2 

contained in the problem set, including the answer options. The agent will then take this 
textual representation and try to infer the answer to the problem. 
 
My agent employs the use of Generate and Test, Production rules, Logic, and working 
memory to solve these raven problems. Generate and test is the overarching technique 
used to infer the answers. There is a smart generator and a less smart tester. The smart 
generator does two major functions, determine shape transformations and also determine 
shape patterns in the previous figures to guess what the resulting figure should be. The 
tester uses this knowledge to score the answer options, then chooses the option with the 
highest score. But before the generator can do this it first decomposes the inputs into a 
simpler form for later processing. It uses a shape object to represent each shape in a 
figure and employs a list of this shape object to represent a figure, then puts each List of 
shapes into a Figures HashMap with the key being the name of the figure (A,B,1,2 etc..). 
The shape object has all the expected attributes, as size, fil, angle, left­of, right­of, etc as 
public members of the object. This makes for much easier processing in that shapes can 
now be compared to other shapes directly by just comparing one object to the next instead 
of looping through each attributes and comparing them explicitly; this speeds up 
processing tremendously.  
 

HashMap<”A”,List<Shape>> → List<shape> → shape 

A<shape> == B<shape> : Same_Shape 

 
Production rules are mostly used in the smart generator. These rules look at two objects at 
a time and use working memory to compare the object features (attributes). From these 
comparisons it makes conclusions and stores this in working memory. To do this the 
generator first takes a shape in one figure then tries to determine which shape in another 
figure it corresponds to, by looking at shape and size, if there is no correlated shape it 
assumes the shape was deleted and notes this in memory, however if there is a correlated 
shape it then compares their features. For example when comparing shape A in figure 1 to 
shape A in figure 2 it may look at the fill feature to determine if the fill changed, and note if it 
did in working memory. It strings these rules together to form a production system which 
updates working memory. Here’s an example production rule used in the Generator 
 

if A<shape>.fill != A<shape>.fill then Fill_Changed 

 
Logic is used both in the tester and the smart generator. These logic rules uses logic 
operators as equals, not equals, greater than etc to compare various features between 
shapes. This is used to determine expected angles, rotations, flipped shapes, changes in 
fills, movement of shapes (inside, above etc) and most importantly to determine the 

kdaniel38

      3 

expected shape, size and quantity. The last two proved to be a very important aspect of 
knowledge that was used to solve 60% of the problems. The quantity of expected objects in 
the answer was determine by looking at changes in shape count between rows and also 
columns of figures. So for example if figure1 had 2 shapes, 3 in figure2 and 4 in figure3, 
the generator would assume that there is a shape count change of 1 from left to right. With 
this knowledge it determines if the other rows and columns follow the same rule, if they do 
then it infers that the answer must be the quantity of shapes in figure H plus the difference it 
determined between the figures (in this case a difference of 1). It determines the expected 
shapes by looking at all the shapes between rows and columns. So in our “3x3 Basic 
Problem 03” above the agent would notice that row 1 has a plus, square and circle, row 2 
same shapes, so it infers that row 3 should follow the same pattern, so it then tries to 
determine which of these three shapes is missing from row 3, and it will discover that the 
square is missing. It uses the same logic columnwise which reinforces its confidence in its 
prior knowledge that the missing shape is a square. It then uses the same logic for fill and 
sizes to infer the size and fill of the mystery figure. With these three artifacts expected 
shape, expected shape count, and expected sizes it was able to solve all the problems that 
did not include angular transformations.  
 
Working memory proved to be an invaluable asset in solving these problems. Memory had 
to be structured and easy to interpolate with. This was achieved by creating strong objects 
to store information as shape and figure data along with transformations between the 
shapes in each figure. As mentioned above the inputs are decomposed into objects that 
are easier to process. Transformations between shapes are stored in a HashMap as to 
avoid duplicates and increase speed of retrieval for processing. Transformations were 
stored in a form that was easy to use and compare. Each transformation type was 
composed of an Long value using bit shifting (the idea was taken from Seth’s project1). So 
for example if a shape was shrunk, the transformation bit assigned would be 0100 (3 bit left 
shift), which represents a 4 in decimal, whereas moved above would be 01000000 (1 << 
7), 64 in decimal. These unique bit shifted values ensured each transformation was scored 
uniquely and comparison would be as simple as doing bitmasks comparison with the AND 
operator. A similar concept was used to compare shape sizes to determine growth. Each 
shape size was given a unique real number, very­small = 1, small = 3, large = 5 etc. Then if 
the agent wanted to compare A<shape>.size = large to A<shape>.size = small, it would 
change the shapes to its respective number then determine if one is higher than the other 
or the same. This is how the agent knows large is bigger than small and medium and very 
large is bigger than large etc. It employs this logic to determine shape size patterns for 
example if shape A is small in figure 1, medium in figure 2, and large in figure 3, by 
decomposing the size textual values to numbers it can infer that the shapes are jumping up 

kdaniel38

      4 

1 size between figures, therefore the final shape must be one size above the shape before 
it. 

 
The tester uses the knowledge gained 
from the generator then compares the 
figure before the mystery figure and all the 
answer options to infer which answer 
option is the most probable option. It first 
makes quick assumptions by looking only 
at the expected shapes, sizes and count 
that should be in the final figure to 
determine an answer then continues to 
look at transformations between the 
shapes in the figures if there’s an impasse 
on the answer. This makes the tester 
dynamic and quick. 
 
My implementation of this methodology 
has a few strengths and weaknesses. One 
of it’s strengths is the ability to process the 
shapes between the figures quickly and 
efficiently because of the decomposition of 
the inputs into a more structured format. 
Another strength is the ability to iteratively 
make assumptions, test these 
assumptions, then either pick an answer or 
make more assumptions. The modular 
design afforded by the method of generate 
and test where there is a distinct generate 
function and test function allows the 
solution to be extensible and very flexible. 
One weakness is that this solution is totally 

optimized for 3x3 raven problems so it intentionally skips the others (2x1 and 2x2). Also 
smarter logic could have been programmed into the tester, may be employing the use of 
case base reasoning because a few problems were very similar. 
 
The AI agent was designed to mirror human cognition closely using abstract concepts and 
structures available in the programming language of choice ­ Java. When trying to solve a 
3x3 matrix, we tend to compare the figures in two ways, figure to figure and rows and 

kdaniel38

      5 

columns. We may compare A to B then B to C and so forth but it is unlikely that we would 
compare A to B to C all at once, the agent mirrors this theory by comparing two figures at a 
time. We also look at groups of figures forming rows or columns but from the broader 
perspective of what’s consistent between them, this helps us formulate patterns which can 
then be used to determine the answer. The agent is fashioned this way also, it finds 
patterns horizontally (rows) then looks vertically to reinforce its assumptions. Humans also 
do a quick inspection of the problem then try to infer the answer from this quick analysis, 
we would check our quick assumption against the answer options and if there is a match 
we would most likely pick that one as the answer but if there isn’t a match or there’s a tie in 
the answer options (impasse), we tend to dig deeper to look for other patterns that will help 
us debunk the impasse. The AI agent also does this by first looking at its expected shape, 
size, count and determine if any of the answer options meet those. If there’s an impasse, 
meaning there are two exactly related option it looks deeper by scoring horizontal 
transformations between figures, then looks at the answer options again to break the 
impasse. If there is still an impasse it looks at the vertical transformations between columns 
(figures forming columns top­down left to right) to attain more artifacts which it compares 
against the answer options. So like a human it iteratively makes assumptions, test these 
assumptions then based on the results continue to make more assumptions up to a certain 
limit until there’s an answer or it cannot assume anymore. Employing the methods 
mentioned in the paper my agent was able to solve 18 of the basic 3x3 problems in under 
1 second. 
 
 

kdaniel38

      6 

