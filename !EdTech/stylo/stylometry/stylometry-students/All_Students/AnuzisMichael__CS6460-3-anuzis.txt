CS6460-3: Exploring your Problem 

Michael Anuzis (​o@gatech.edu​) 
 

Contents 

Problem Area 
Existing Systems 
Research Scope 
Appendix 

Problem Area 
 
I shifted my approach in the last week, keeping the underlying goal the same. For any new peer 
reviewers, my primary inspiration is the finding of Bloom’s 2 Sigma problem from the 1980s 
which showed learning outcomes improve an average of 1 standard deviation when shifting 
from lectures to mastery­based learning (not moving students forward until they demonstrate 
mastery at a given skill), and another standard deviation by providing individual tutoring. 
 

 

 

 

 
Much of today’s education is already taking strides towards mastery­based learning as we see 
with sites like the Khan Academy, which encourages students to continue practicing until 
they’ve achieved proficiency at a given skill. In prior weeks my emphasis was on improving the 
machine learning techniques used by sites like Khan Academy to infer what a student knew and 
to provide recommendations accordingly.  After diving into the current literature, I’ve concluded 
there appears to be a bigger potential for impact focusing more directly on providing individual 
tutoring at scale. (the 2nd sigma) 
 
Looking at today’s Interactive Tutoring Systems (ITS), one of the primary criticisms is the 
amount of time it takes to produce quality instructional content relative to the time learning. One 
study found that it takes an average of 300 hours production time to produce 1 hour of 

1

instructional content . Cognitive Tutor, developed by Carnegie Mellon, similarly takes an 
average of 200 hours production time per hour of instructional content . These benchmarks 
must be improved dramatically in order for ITS­based teaching systems to scale more broadly. 
 
A previous ITS used internally at my company required content authors to hand­code XML in 
order to specify the feedback and rules that would bring learners forward through an 
experience.  One non­technical team member was brought to tears trying to get it working. 

2

Existing Systems 
 
Past approaches to accelerate the creation of ITS content have tried using software like 
Macromedia Flash, or the bulk uploading of rules via spreadsheet . 3
 

[2004 example of ITS content authoring via Macromedia Flash.] 

 

[2006 example ‘mass production’ ITS content authoring via spreadsheet.] 

 

 

 

1 ​Murray, T. (1999). Authoring intelligent tutoring systems: An analysis of the state of the art. International Journal of Artificial 
Intelligence in Education (IJAIED), 10, 98–129. 
2 Anderson, J.R.; Corbett, A. T.; Koedinger, K. R.; Pelletier, R. (1995). "Cognitive tutors: Lessons learned". The Journal of the 
Learning Sciences 4: 167–207. ​http://www.tandfonline.com/doi/abs/10.1207/s15327809jls0402_2 
3 ​Aleven, V., Sewall, J., McLaren, B. M., & Koedinger, K. R. (2006). Rapid authoring of intelligent tutors for real­world and 
experimental use. In Kinshuk, R. Koper, P. Kommers, P. Kirschner, D. G. Sampson, & W. Didderen (Eds.), Proceedings of the 6th 
IEEE International Conference on Advanced Learning Technologies (ICALT 2006), (pp. 847­851) 
http://repository.cmu.edu/cgi/viewcontent.cgi?article=1148&context=hcii 

More recently, I worked on an open­source ITS at ​www.oppia.org​. Oppia’s goal is to become 
the Wikipedia of interactive tutoring content on any topic. Oppia’s authoring interface still has 
room for improvement, but requires no programming experience and can be edited 
collaboratively in a Wikipedia­like manner. 
 

[2015 example ITS content authoring in Oppia.] 

 

 

4

Research Scope 
 
No studies have been done comparing production time on Oppia to other ITS systems. While I 
would like to include tracking production time in my research, I would also like to address a 
broader scope and contribute something that makes a direct difference in STEM education. 
 
Another criticism of ITS is that human tutors outperform systems by providing higher 
responsiveness to student errors . While this may be true, the digital nature of ITS also provides 
the opportunity for increased entertainment, which can increase learner attention. 
 
For my research project, I propose the implementation of an exemplar ITS content teaching 
programming fundamentals. I’d like to do a comparative analysis of its effectiveness versus 
status quo options like Codecademy.com and human instruction. Learning evaluation should 
include both objective measurements of proficiency, along with subjective measurements of fun 
and ease of learning. 
 
The unique angle leveraging ITS that I believe will allow this project to raise the bar on status 
quo systems will be an immersive choose­your­own­adventure style of learning where learners 
are recruited as a computer scientist to assist in the colonization of Mars. The ITS will probe for 
their level of existing knowledge, if any, and will bring them into the story assigning them 
missions as appropriate for their level of knowledge. 
 
I’ve personally used many of the self­study introductions to Python available online and believe 
they can be done better. I’d love to create an exemplar content that provides learning for 
100k­1M learners, and invites others to expand on the learning in a Wikipedia style way. 

4 VanLehn, K. (2011). The relative effectiveness of human tutoring, intelligent tutuoring systems and other tutoring systems. 
Educational Psychologist 

