Individual Project Proposal 

Educational Technology - Spring 2016  

Richard Guilmain 

PROBLEM DESCRIPTION 

In the traditional education model, a single person who is an expert on a particular 
subject will stand in front of rows of students who are not experts, and that person will 
explain that subject to them. The pace of the lesson, inevitably, ends up being too slow 
for the students who grasp the material quickly, and too fast for the students who need 
extra explanation; so, half of the class ends up bored, and the other half gets left 
behind. 

Bloom (1984) has shown, in fact, that students who are tutored one-on-one by an 
expert capable of adapting to the student’s learning pace perform two ​standard 
deviations​ better than students who learn via the conventional lectured approach. 
Unfortunately, there aren’t enough human resources to give every student such an 
expert tutor. 

EXISTING SOLUTIONS 

The field of Intelligent Tutoring Systems has begun to address this problem by 
providing digital tutors that can adapt to students’ learning behaviors in real time. 
Separately, teaching self-regulated learning skills provides students with the tools 
necessary to tackle this problem on their own. Metacognitive tutoring systems attempt 
to combine the benefits of both of these strategies into a single comprehensive 
approach. 

Current research in metacognitive tutoring systems include Aleven and Koedinger 
(2002), who performed studies on the benefits of having students explain their 
reasoning while solving problems, Mathan and Koedinger (2005) who looked at ways 
to involve students in self-correction when they make errors, Baker et al. (2007) who 
experimented with ways to teach students to not “game” the instruction system, and 
Roll et al. (2007) who researched how to improve students’ help-seeking behaviors. 

In all of these studies, however, the metacognitive lessons were made secondary to 
those of some other primary subject, such as geometry or algebra. None of them have 
attempted to teach metacognition as a first-class citizen, and none of them touched 
upon any of the many other foundational concepts in the study of metacognition, such 
as growth versus fixed mindsets or any of the most common cognitive biases, such as 
confirmation bias, relying on anecdotal evidence, or believing that prior random 
events affect the outcomes of future random events. 

THE PROPOSAL 

I propose to build a metacognitive tutoring system with three objectives. The first is to 
provide students with an essential overview of basic metacognitive theory in a way 
that adapts to their real-time understanding. The second is to systematically debias 
students so that they can approach content in any domain with a more 
critically-minded perspective. The third is to create an extensible platform that lays the 
groundwork for teaching more advanced metacognitive and debiasing concepts in 
future work. 

The software architecture will consist of four primary components: a Domain Model, a 
Student Model, a Tutoring Model, and a User Interface. The Domain Model will encode 
the knowledge to be taught, the Student Model will track the student’s state over the 
course of the learning process, the Tutoring Model will merge knowledge from both 
the Domain Model and the Student Model to decide how best to proceed to teach the 
student, and the User Interface will be the learning environment in which the student 
interacts with the tutoring system. 

It will written in Java in the Eclipse IDE, and be authored using Carnegie Learning’s 
Cognitive Tutor Authoring Tool (CTAT) framework (​http://ctat.pact.cs.cmu.edu​) which 
represents the standard in the field. CTAT supports creating both “example-tracing 
tutors” which do not require any AI programming, and “cognitive tutors” which are 
based on the ACT-R cognitive learning theory and provide a more robust and 
extensible platform. The proposed tutor will be the latter. 

There are other Intelligent Tutoring System authoring tools available, including 
ASPIRE (​http://aspire.cosc.canterbury.ac.nz/ASPIRE.php​), Autotutor Lite 
(​http://www.skoonline.org​), The Extensible Problem-Specific Tutor 
(​http://xpst.vrac.iastate.edu​), and Oppia (​https://www.oppia.org​). If it is discovered that 
CTAT does not meet the needs of the project, these other authoring tools can be 

considered. 

 

 
DELIVERABLES OVERVIEW 

Throughout the development process, and starting on February 28th, I will submit nine 
weekly status reports. These reports will summarize the progress made each week, and 
the challenges faced. They will provide an opportunity for continual communication 
between myself and my mentor, and help us adjust expectations in a timely fashion as 
the project matures. 

Additionally, and starting on March 6th, I will submit four biweekly milestones. These 
will be used to demonstrate my work and elicit feedback from not only to my mentor, 
but also my fellow students. My mentor will use these milestones to ensure that the 
amount of work I do is appropriate. 

The first milestone will be a working tutor. It will be relatively scant in functionality, 
but it will demonstrate that I have a firm grasp on the tooling environment and that I 
will be able to deliver a final working system using the approach I have decided on. 
The second will be a combination of both the syllabus for the adaptive course content 
and the planned architectural layout of the tutor’s software. These will be the 
blueprints for the system moving forward. The third will be a working alpha version of 
the tutor. It will be testable by those it is submitted to, and will represent a technical 
baseline for the final project submission. The fourth will be a first draft of the final 
paper. Soliciting feedback on this early on will ensure that the final revision will be 
what is expected. 

Finally, on May 1st, I will submit my three final deliverables. These include the final 
executable version of the metacognitive tutoring software I have proposed to make, a 
video presentation demonstrating the software, and a paper describing my results. 

TASK CALENDAR 

● Week 1 -- Due by 02/28/2016 

○ Build a “Hello World” tutor using the CTAT framework 

○ Submit Weekly Status Check 1  

● Week 2 -- Due by 03/06/2016 

○ Build and submit a functional and non-trivial tutor using CTAT 

○ Submit Weekly Status Check 2 

 

● Week 3 -- Due by by 03/13/2016 

○ Determine the content of the course and write the adaptive syllabus 

○ Weekly Status Check 3  

● Week 4 -- Due by 03/20/2016 

○ Finalize and submit the course outline and architectural software layout 

○ Submit Weekly Status Check 4 

● Week 5 -- Due by by 03/27/2016 

○ Build skeleton and first-pass implementations of the tutor  

○ Submit Weekly Status Check 5  

● Week 6 -- Due by by 04/03/2016 

○ Build and submit a working alpha implementation of the tutor 

○ Submit Weekly Status Check 6  

● Week 7 -- Due by 04/10/2016 

○ Make incremental improvements to the tutor 

○ Outline final paper 

○ Submit Weekly Status Check 7 

● Week 8 -- Due by 04/17/2016 

○ Make incremental improvements to the tutor 

○ Write and submit a draft of the final paper 

○ Submit Weekly Status Check 8 

● Week 9 -- Due by 04/24/2016 

○ Make incremental improvements to the tutor 

○ Create a video demonstration of the tutor 

○ Submit Weekly Status Check 9 

● Week 10 -- Due by 05/01/2016 

○ Wrap up and submit the final executable version of the metacognitive tutor 

○ Edit and submit the video demonstration of the software 

○ Write and submit the final revision of the paper describing the results 

REFERENCES 

Aleven, V., & Koedinger, K. R. (2002). An effective metacognitive strategy: Learning by doing and explaining with a 
computer-based Cognitive Tutor. ​Cognitive Science​, 26, 147-79. 

Baker, R., et al.Corbett, A., Koedinger, K. R., Evenson, S., Roll, I., Wagner, A., Naim, M., Raspat, J., Baker, D., & Beck, J. 
(2006). Adapting to when students game an intelligent tutoring system. In M. Ikeda, K. D. Ashley, T.-W. Chan (Eds.) 
Proceedings of the 8th International Conference on Intelligent Tutoring Systems, 392-401. Berlin: Springer-Verlag 

Bloom, Benjamin (1984). The 2 Sigma Problem: The Search for Methods of Group Instruction as Effective as One-to-One 
Tutoring. ​Educational Researcher​, 13(6): 4-16. 

Koedinger, K. R., Aleven, V., Roll, I., & Baker, R. (2009). In vivo experiments on whether supporting metacognition in 
intelligent tutoring systems yields robust learning. Handbook of metacognition in education, 897-964. 

Mathan, S. A., & Koedinger, K. R. (2005). Fostering the intelligent novice: learning from errors with metacognitive 
tutoring. ​Educational Psychologist​, 40(4), 257-65. 

Roll, I., Aleven, V., McLaren, B. M., & Koedinger, K. R. (2007). Can help seeking be tutored? Searching for the secret 
sauce of metacognitive tutoring. International Conference on Artificial Intelligence in Education. 

