What	  makes	  academic	  advising	  difficult	  and	  what	  are	  the	  various	  ways	  that	  these	  have	  been	  
addressed	  in	  the	  past?	  What	  makes	  the	  OMSCS	  problem	  different	  (either	  easier	  or	  harder)	  and	  how	  
can	  you	  utilize	  available	  technologies,	  student	  interests	  and	  available	  textual	  and	  numeric	  

Traditional	  Academic	  Advising	  

A	  Course	  Recommender	  for	  OMSCS	  students	  

Assignment	  5:	  Personal	  Questions	  
	  
Questions	  

pedagogy,	  and	  a	  set	  of	  student	  learning	  outcomes.	  Academic	  advising	  synthesizes	  and	  contextualizes	  
students'	  educational	  experiences	  within	  the	  frameworks	  of	  their	  aspirations,	  abilities	  and	  lives	  to	  
extend	  learning	  beyond	  campus	  boundaries	  and	  timeframes."	  [1] 

	  
information	  to	  provide	  recommendations	  on	  course	  of	  study	  and	  next	  courses	  to	  take?	  
	  
	  The	  academic	  advising	  is	  defined	  as	  "a	  series	  of	  intentional	  interactions	  with	  a	  curriculum,	  a	  
	  The	  basic	  goal	  of	  the	  academic	  advising	  is	  the	  interaction	  between	  a	  student	  and	  advisor	  to	  
increase	  students’	  retention	  and	  graduate	  rates.	  In	  order	  to	  achieve	  the	  goal,	  a	  variety	  of	  
approaches	  have	  been	  developed.	  The	  developmental	  advising	  approach	  is	  helping	  a	  student	  to	  
have	  a	  long-­‐term	  goal	  through	  process	  oriented	  advising,	  but	  developing	  the	  relationship	  between	  
the	  advisor	  and	  student	  is	  crucial.	  The	  prescriptive	  advising	  model	  is	  focused	  on	  providing	  
students	  information	  to	  solve	  immediate	  problems.	  This	  can	  address	  the	  issue	  directly	  in	  short	  
term	  but	  does	  not	  consider	  students’	  academic	  goals	  in	  long-­‐term	  situation.	  Another	  major	  
approach	  is	  the	  intrusive	  advising.	  In	  this	  model,	  the	  advisor	  contacts	  the	  students	  to	  provide	  
advising	  throughout	  an	  academic	  period.	  In	  traditional	  academic	  advising	  has	  been	  provided	  in	  
face-­‐to-­‐face	  setting.	  And	  it	  brings	  more	  successful	  results	  particularly	  with	  topics	  like	  future	  
classes	  and	  careers.[2]	  
	  Online	  students	  experience	  feelings	  of	  isolation	  as	  well	  as	  lacking	  of	  information	  and	  resources	  
compare	  to	  the	  on-­‐campus	  students.	  Therefore	  advising	  online	  students	  is	  more	  challenging	  and	  
the	  advisers	  need	  to	  develop	  new	  advising	  strategies	  such	  as	  using	  a	  video	  call	  and	  proactively	  
contacting	  students	  by	  sending	  emails	  and	  phone	  calls.	  However,	  providing	  too	  much	  information	  
or	  sending	  too	  many	  emails	  can	  be	  ignored	  eventually.	  The	  communication	  between	  the	  advisor	  
and	  student	  should	  happen	  when	  it	  is	  necessary	  and	  relevant	  to	  students’	  needs.	  	  
	  
	  Georgia	  Tech	  OMSCS	  program	  launched	  in	  May	  2013	  with	  four	  specializations	  and	  now	  over	  2,000	  
students	  are	  enrolling	  the	  program.	  There	  are	  23	  courses	  on-­‐going	  and	  5	  new	  courses	  will	  be	  
started	  in	  Fall	  2016.	  Students	  can	  access	  the	  omscs	  website	  to	  gain	  the	  course	  information.	  
However,	  these	  course	  descriptions	  are	  very	  general	  and	  not	  sufficient	  to	  measure	  the	  difficulty	  of	  
the	  course	  and	  contents.	  Many	  students	  rely	  on	  the	  unofficial	  course	  survey	  or	  asking	  the	  students	  
who	  already	  took	  the	  course	  to	  gain	  the	  course	  information	  (including	  myself).	  But	  these	  survey	  
information	  and	  answers	  from	  other	  students	  are	  somewhat	  subjective	  because	  everybody	  has	  
different	  standards	  on	  skill	  and	  background	  knowledge.	  	  

	  Since	  omscs	  students	  have	  variety	  of	  reasons	  to	  study	  online,	  it	  seems	  impossible	  to	  handle	  all	  the	  
students’	  issues	  with	  a	  few	  advisers.	  Therefore	  there	  should	  be	  a	  course	  recommender	  system	  that	  
calculates	  the	  inputs	  from	  a	  student	  and	  recommend	  the	  next	  course	  to	  take.	  In	  order	  to	  develop	  
this	  course	  recommender,	  I	  can	  think	  of	  the	  machine	  learning	  approach	  that	  classifies	  the	  each	  
student’s	  case	  to	  the	  right	  place	  based	  on	  the	  features	  (input	  information).	  For	  example,	  I	  have	  40	  
hours	  to	  study	  each	  week,	  strong	  at	  math	  but	  weak	  at	  java	  language	  (this	  may	  convert	  to	  numeric	  
score	  from	  1	  to	  5),	  specialization	  on	  machine	  learning,	  and	  have	  taken	  SDP,	  etc.,	  then	  these	  inputs	  
are	  taken	  by	  the	  system	  to	  calculate	  based	  on	  the	  algorithms	  and	  recommend	  me	  a	  next	  course	  to	  
take.	  
[1] Matthew	  Morano,	  George	  Mason	  University	  (1999).	  “Challenges	  Encountered	  by	  New	  
Advisers:	  Honest	  Answers,	  Practical	  Solutions”.	  The	  Mentor	  1999,01,01.	  
[2] Johnson,	  E.J.;	  Morgan,	  B.L.	  (2005).	  "Advice	  on	  Advising:	  Improving	  a	  comprehensive	  
university's	  program".	  Teaching	  of	  Psychology	  32	  (1):	  15–18.	  
[3] Olga	  C.	  Santos,	  Jesus	  G.	  Boticario	  (2011).	  “Requirements	  for	  Semantic	  Educational	  
Recommender	  Systems	  in	  Formal	  E-­‐Learning	  Scenarios”.	  Algorithms	  2011,	  4,	  131-­‐154.	  	  
[4] 7	  Ways	  Advisors	  Can	  More	  Effectively	  Engage	  Online	  Students.	  
http://www.academicimpressions.com/news/7-­‐ways-­‐advisors-­‐can-­‐more-­‐effectively-­‐
engage-­‐online-­‐students	  
[5] Academic	  Advising.	  https://en.wikipedia.org/wiki/Academic_advising#cite_note-­‐13	  
	  

	  
	  
References	  
	  

	  

