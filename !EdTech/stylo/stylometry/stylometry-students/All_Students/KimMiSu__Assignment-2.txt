Big	  data	  in	  learning:	  	  

Assignment	  2:	  Exploring	  Your	  Area	  

General	  current	  problem	  in	  big	  data	  in	  learning:	  

	  In	  the	  last	  assignment,	  I	  described	  my	  idea	  problem	  that	  intelligence	  system	  recommends	  courses	  
based	  on	  each	  employee’s	  capability	  and	  interests	  along	  with	  their	  job	  functions	  and	  level	  in	  HR	  
side.	  In	  this	  assignment,	  I	  will	  generalize	  my	  idea	  from	  working	  place	  to	  any	  learning	  
environments	  such	  as	  K-­‐12,	  higher	  education,	  and	  professional	  development	  while	  narrowing	  
down	  the	  area	  of	  technology.	  
	  
	  As	  described	  above,	  I	  am	  interested	  in	  an	  intelligence	  system	  that	  give	  a	  customized	  direction	  or	  
feedback	  to	  learners	  and	  at	  the	  same	  time,	  the	  system	  let	  learning	  providers	  such	  as	  HR	  managers,	  
course	  developers,	  or	  teachers,	  measure	  the	  effectiveness	  of	  learning	  in	  both	  qualitative	  and	  
quantitative	  on	  individual	  or	  group	  learning.	  And	  also	  the	  learning	  providers	  can	  gain	  valuable	  
insight	  into	  how	  to	  make	  better	  learning	  programs.	  Providing	  a	  customized	  direction	  to	  learners	  
or	  measuring	  the	  effectiveness	  of	  learning	  requires	  to	  collect	  data	  from	  the	  learners	  and	  their	  
learning	  activities.	  Data	  can	  be	  obtained	  from	  the	  learning	  management	  system,	  feedbacks	  from	  
leaners,	  survey,	  and	  social	  media	  and	  converted	  into	  an	  actionable	  intelligence	  through	  big	  data	  
analytics.	  
	  
	  Big	  data	  especially	  for	  education	  is	  an	  emerging	  area	  and	  has	  a	  lot	  of	  challenges	  as	  well	  as	  
opportunities.	  I	  am	  going	  to	  point	  out	  a	  few	  major	  problems	  that	  big	  data	  in	  learning	  has	  
encountered.	  The	  fundamental	  problem	  is	  that	  the	  data	  from	  variety	  sources	  is	  not	  very	  organized	  
and	  indexed	  yet.	  So	  the	  data	  integration	  is	  the	  key	  factor	  to	  start	  big	  data	  analysis.	  On	  the	  other	  
hand,	  as	  a	  student	  I	  am	  already	  aware	  of	  the	  importance	  of	  the	  immediate	  feedback.	  To	  provide	  a	  
real-­‐time	  or	  almost	  real-­‐time	  feedback	  to	  learners	  or	  to	  track	  the	  learner’s	  behavior	  and	  learning	  
pattern,	  there	  should	  be	  a	  strong	  platform	  that	  transforms	  data	  into	  valuable	  insights.	  However,	  
developing	  this	  kind	  of	  tool	  or	  application	  is	  very	  complex	  and	  expensive	  due	  to	  the	  relationships	  
between	  big	  data	  technology,	  learning	  research,	  educational	  psychology,	  and	  software	  design.	  	  	  
	  
	  I	  am	  going	  to	  spend	  one	  more	  week	  to	  explore	  the	  big	  data	  in	  education	  area.	  There	  are	  two	  main	  
communities,	  the	  educational	  data	  mining	  and	  the	  educational	  analytics	  community.	  I	  will	  focus	  on	  
the	  detail	  about	  how	  to	  apply	  big	  data	  technology	  to	  measure	  the	  effectiveness	  of	  learning.	  	  
	  
	  Academic	  Research:	  	  
Madhavan,	  Krishna,	  Journal	  of	  engineering	  education	  /	  2016,	  Vol.105(1),	  p.6	  
	  Web-­‐source:	  	  

Plan	  for	  my	  research:	  

Cite	  specific	  sources:	  

Problems	  in	  Big	  Data	  Analytics	  in	  Learning.	  

“The	  4	  Hottest	  eLearning	  Trends	  For	  2016” http://elearningindustry.com/4-­‐hottest-­‐elearning-­‐
trends-­‐for-­‐2016 
	  “Big	  Data	  in	  eLearning:	  The	  Future	  of	  eLearning	  Industry”	  http://elearningindustry.com/big-­‐data-­‐
in-­‐elearning-­‐future-­‐of-­‐elearning-­‐industry	  
 “Big	  data	  and	  Education:	  What’s	  the	  big	  idea?”	  https://www.ucl.ac.uk/public-­‐policy/public-­‐policy-­‐
briefings/big_data_briefing_final.pdf	  
	  Open	  source	  for	  “Big	  data	  and	  Education”	  
http://www.columbia.edu/~rsb2162/bigdataeducation.html	  
	  
	  Blackboard	  (http://www.blackboard.com)	  
Civitas	  Learning	  (https://www.civitaslearning.com)	  
Knewton	  (https://www.knewton.com)	  
	  	  	  

Big	  data	  in	  Education:	  Major	  player	  

