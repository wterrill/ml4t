     Assignment -2: Exploring your Area 

1  FOCUS   

After going through a couple of topics, my interest settled down on two topics. One was on 
“Online education vs Plagiarism” and the other was “Catering online education to students with 
special needs and its success”. I decided on the latter and to do more research on it. How we 
can incorporate OMSCS (or any other MOOCs) to students with special needs. How much 
successful will such an education be for them, without having a special tutor to sign and how 
much information can they gather accurately. Coupling these factors how much will they be 
able to behave as much as any other normal student in a class.  

Reading an article on EdTech Friday: Using Technology with Special Needs Students (EdTech 
Friday Special Needs Students ) cites that “Children with certain kinds of disabilities, such as 
those on the autism spectrum, respond especially well to technology programs because the 
programs behave in consistent, predictable ways”  

For the scope of this project, I wish to focus on students who are mentally capable to learn, 
understand, think like any other healthy individual but have hearing difficulties. 

2  WHAT TOOLS, COMMUNITIES OR THEORIES EXISTS  

Going through many sites and research papers, I came across an interesting piece - “A 
Phenomenological Study of Online Learning for Deaf students in Post-secondary education” - by 
Patricia Michelle Wooten, Liberty University. She has written in depth of how much the deaf 
students can relate to online education, how good their writing and reading skills are compared 
to healthy students. She has also done some very interesting experiments to see how effective 
such a kind of education can be. 

There are many other papers that talk about delivering education to such students as these. 
Some tools such as providing closed captions are out in market today. Even though most of our 
OMSCS courses have captions and titles to it, most of the scripts show up wrong or are 
confusing. Some videos don’t have captions at all. The same applies to office hour videos where 
the videos are not captioned. This is difficult for students who have to decipher non-native 
speaking English accents.  

MOOCs and other online courses are currently built to cater to normal healthy students who 
can interact well with other students easily and respond quickly.  

Currently tools like Amara can help caption videos (Amara captioning). They can also help 
translate to languages and can be made globally accessible to various students.  

3  GENERAL CURRENT PROBLEMS 

a. Captioning Videos/Titles - After much googling, I came across this post - MOOCs for deaf 
students 

The deaf student here wants to take up a course at Udacity – “Intro to OOP” with her other 
deaf friends. They wish to try out how it would be like. But they say the captions/titles are 
either not available or are incorrect most of the times. She like many others wish that these 
online courses would be more deaf-friendly. 

b. Unfriendly / Unfair - Another article from The Baltimore Sun - Baltimore Sun states how the 
deaf person claims that MOOCs is unfriendly and unfair. More than unfair he claims it is illegal. 

c. Unavailability of individual tutors for deaf students – Volunteers who can help out with the 
signing language are not very readily available anytime. The cost of pay may be higher to hire 
them.  

d. Networking – In a traditional classroom these students (most not being able to speak) may 
feel unconfident and reserved.  

4  POSITIVE SIDE 

a.  Equality online - Making online courses available to them at the same cost the regular 
students pay(minus the special tutor hiring fee), and to make it easily accessible anytime 
anywhere to these students including self-pacing is very important point as far as MOOCs is 
concerned. 

b. Networking - Being able to network with regular students over forums such as google 
hangouts, piazza, hipchat, slack will enable them to feel part of this massive group of students 
without having to talk literally or face anyone in person. This can surly boost their level of 
confidence. 

5  MAJOR PLAYERS 

Udacity, Coursera, edX, and other online course (MOOCs) providing platforms are currently the 
top players. They are all equally competitive in their capacities and are all trying to deliver the 
courses with topmost quality. Colleges and universities who actually cater online courses are 
other players who can provide deaf-friendly environment.  

 

6  CONCLUSION 

I wish to research more around this idea and see how far we can get into making online courses 
equally available to many other students true and fair in every way. 

 

 

