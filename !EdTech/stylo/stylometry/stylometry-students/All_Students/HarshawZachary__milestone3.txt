Harshaw, Zachary GTID#903049739  

Milestone 3 

Project Overview: 
Construction of a drag and drop coder framework that can be utilized to teach the upper k-12 audience 
about the major problem solving and fundamentals of programming.  The project looks to remove some 
of the common pitfalls that can accompany first programming such as syntax mistakes, object typing, 
code structure, and so on. 

Excerpt from proposal: 
Milestone 3 

At this point the application should at least be a functional prototype.  The drag and drop interface 
should be fully functional with minimal changes needed.  The code generation should also be in a fully 
functional state as well, needing only little tweaks and bug fixes.  Utilizing the functional framework, 
work on example “micro-lessons” should be constructed for demoing the potential of the prototype. 

Milestone 3: 
In my previous Milestone 2 submission I submitted a controlled demo video showing off my application.  
The original video can be found at https://youtu.be/md_DP_T45oo.  I had originally planned to provide 
a demo of the application for Milestone 4 because I needed to find a location to host the web 
application.   My Milestone 3 was simply going to be another video further demoing the application 
however I chose to investigate hosting the application this weekend and found that amazon’s web 
services provide a real quick and easy way to host my web application (I honestly didn’t think it would 
be as simple as uploading WAR file, but amazon’s web services really surprised me with their flexibility 
and functionally). 

So, on this note I’ve decided to deviate from my original plan and instead provide a functional prototype 
for Milestone 3. The application can be found at the following URL:  
http://draganddropcoder.fw3pb7hd2b.us-west-2.elasticbeanstalk.com/main  

Usage of the application should be pretty self explanatory but taking a look at Milestone 2’s video may 
assist if you need some guidance. 

Please take into account that all bugs haven’t quite been worked out and there exists some styling 
issues that I will need to further work out in the last couple weeks.  Also try to avoid creating any infinite 
loops as I haven’t implemented a way to handle these elegantly yet, and since nearly all browsers run 
their JavaScript engine on a single thread it can lock browser up.  Further validation on the user 
constructed code is development so at the moment you should still be able to create bad code but in 

the future this will be prevented.  Also I’m working on increasing usability and guidance of the 
application, but definitely like to hear from possible improvements our ideas that could assist in this.  A 
few of the draggable assets have guidance popups if you double click them to demonstrate common 
usage and further explain functionality. 

 

 

