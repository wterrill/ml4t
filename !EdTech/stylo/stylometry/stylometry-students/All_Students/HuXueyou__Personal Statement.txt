Personal Statement 

I like to challenge myself and learn new things. During last 20 years, the world is changing 
dramatically and rapidly. There are two big events: 1) human genome project was completed in 
2000; 2) computer and internet are developing exponentially. The first event opens the door 
allowing us to explore ourselves and the second event builds the path for us to access the whole 
world. I have been trying to grasp these opportunities and keep growing. 

I came from a small village located in the northeast of China. At that time, my dream is to 
become a plant “doctor” and help the farmer defense the bugs and plant disease. Hence I entered 
an agricultural college and studied in plant protection. After that, I worked on insect for several 
years and discovered 6 new fly species in the best research institute in China.  

When I came to US in 2003, the molecular biology became very popular and I also got chance to 
earn my PhD in Biomedical Sciences in 2008. Since I used catfish and mouse as animal model in 
my study, I would say that I evolved from invertebrate insect, vertebrate fish, to mammal mouse 
and humans in 5 years. These experiences gave me a historic view of life evolution and enhance 
my understanding of many life phenomenons. 

After  graduated  from  Auburn  University,  I  spent  quite  a  few  years  in  biomedical  research  at 
Baylor College of Medicine and published more than 10 articles on descent journals. While the 
term “Data Science” has emerged recently, many scientists including me realize that “Big Data” 
has become the bottleneck in biomedical research. Then I decided to become a data scientist by 
learning more computer knowledge and skills. 

From my experience, you can see that I am a lifelong learner and I have been exploring several 
different domains. Meanwhile I had a lot of mentoring experience for undergraduate and 
graduate students. In another word, I know how to learn and know how to teach people learn. 
One of the key points in learning is to choose study material. Nowadays the information or 
knowledge is exploding exponentially. Choosing suitable study material becomes a must-facing 
issue for our learners.  

Fortunately we have existing internet technology to solve this problem. My plan for this course is 
to build a web feedback or evaluation platform for the learners. Inspired by many online 
shopping sites such as amazon.com, I think our learners also need some feedback from other 
learners when they choose courses or study material online. On such a platform, every learner 
can give his/her feedback or evaluations to any public course, which will help other learners 
make right decision during their material or program searching.  

My knowledge and skills in programming allows me to implement such a project. Since I have 
already completed 6 courses from GT and set up several websites using Java web technologies 
from the frontend to the backend, I could compete this project by myself. Considering that this is 

also a learning process for me and the other classmates, I would like to invite one or two more 
people join in this project, especially someone who is good at frontend design can make more 
contributions.  

By the end of this semester, I expect to install this web app online which will be completely 
functional. This platform should have some example of resources such as the courses from this 
program. And it should allow the learners to view, search, and give their feedback or evaluation 
to these online resources. Eventually this platform will serve as a tool that can help the learner to 
select study material based on the introduction and feedback or reviews by the other learners. 

 

