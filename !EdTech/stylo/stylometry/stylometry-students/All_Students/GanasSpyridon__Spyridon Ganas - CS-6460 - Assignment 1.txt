My name is Spyridon Ganas.  I’m a data warehouse analyst currently working for 
Massachusetts’s Center for Health Information and Analysis (CHIA).  CHIA’s is “the hub 
of information and analysis about the Massachusetts health care system”.   

CHIA’s primary tool is the state’s All-Payers Claims Database (APCD) 

http://www.chiamass.gov/ma-apcd/.  The APCD is a multi-terabyte collection of medical 
claims and other health insurance industry data.  This is an amazing resource is 
available to Health Care researchers, but there is currently no easy way for people to 
learn about this database and it’s many quirks. 

I decided to take the Educational Technology class so I could learn how to create 

an effective online class.  My goal is to create an “Introduction to the APCD for Health 
Care Researchers” course.   

This class will include videos from CHIA staff, a curated collection of documents 

that explain the APCD data, and a library documenting past research that was based on 
APCD data.   

I have a great deal of experience taking online classes.  I’ve completed to 

GaTech classes, and over 20 EdX, Coursera and Udacity classes.  I’ve taught myself 
Python, Java, HTML and JavaScript from online classes.  I’ve also used online classes 
to improve my project management and emotional intelligence skills. 

My hope is that this APCD class will be complete enough to allow graduate 

students with limited research experience to successfully use the APCD for their 
research.  So I would like to work with someone who has no health care experience (to 
insure that the APCD class we develop is useful to someone outside the industry).  I 
also feel this project would be more successful if I had a team that included someone 
with strong web development skills.   

Overall, I am very excited to build a tool that will accelerate the pace of medical 

research. 

 

