CS-6460: Educational Technology 

Personal Question 

 
 

 
 

 
 

 

 
 

Carlos Eduardo Corpeño 
 
ecorp@gatech.edu, ecorp@galileo.edu   
 
 
This document contains my answers to all parts of the personal question assigned to me. I’ve slightly rephrased parts 
of the question text in order to answer each part separately. 
Some of these answers come from a literature review, while the uncited claims come from the criteria I’ve gathered 
from my own personal experience as an educator over the last 15 years. 
 

 
      Feb-14-2016 
            GTid: 903049683 

Why is it important to mitigate plagiarism?  

Because plagiarism is detrimental to education, and thus to the institutions that grant academic degrees to their 
students. Also because plagiarism is a very real problem present at virtually every level of education all around the 
world [3][4][5]. 
 
The following questions and answers expose many characteristics and realities of plagiarism, but all agree on its 
detrimental nature. Since plagiarism contributes to something other than the very objectives of education, then it 
must be mitigated (if not eradicated) as long as the goals of education remain akin to learning and internalization 
of knowledge.  
 
A particularly shocking confession of plagiarism titled “The Shadow Scholar” [5] describes the experience of an 
individual who has made a living out of writing papers for anybody who’s willing to pay. This shadow scholar isn’t 
working alone nor without any competition. He works for a company that offers this service and achieves a stable 
revenue by doing so. In my opinion, this is an unfortunate reality that has to stop because of its lack of moral 
values, to name a reason.   
 
It is, of course, very important to have a clear definition and understanding of what constitutes plagiarism [1], 
since there is a subjective threshold that divides plagiarism and authentic, cited research [2].  
 
 
 

What is the frequency of plagiarism?  

Several sources show an alarming frequency of plagiarism in many levels of education, many forms of education 
and many parts of the world.  
 
In the US, a 1964 study showed that 75% of students from 99 colleges and universities admitted participating in 
academic dishonesty. Another 2003 article estimates a plagiarism rate of 30% in the US [3]. 
Several metrics have been reported in a study conducted at the United Kingdom on 1996, where students 
admitted to plagiarism in many forms. Some of these results include 46% of students allowing classmates to copy 
their work, 42% copying material without acknowledgments, and 16% writing someone else’s homework [3].     
 
Several findings on plagiarism have been published, such as a 1993 study conducted on High school students in the 
US, where approximately 80% admitted to cheating, including copying someone else’s homework. There have also 
been numerous cases of plagiarism in academic research, for example a study conducted on 4,000 researchers 
shows that one in three scientists plagiarize, 22% handle data ‘carelessly’ and 15% occasionally withhold 
unfavorable data [4]. 
 
After reviewing several documents on plagiarism frequency, I have found the presence of plagiarism to be an 
invariant among the problems that have caught the attention of educators. 
 

 

What are the consequences of plagiarism to learning outcomes of the students (both the ones who plagiarize 

and the ones who don't)? 

Students who plagiarize and get away with it are given false credit, which often results in them not being prepared 
for further courses. If they are caught, it may also delay their learning because of a potential negative impact in 
their grades which may lead to failing a course [7]. 
 
Students who don’t plagiarize may become discouraged to do legitimate work when informed of classmates 
getting away with plagiarism, so students who don’t plagiarize may be encouraged to plagiarize and get as much 
credit if they don’t get caught, the motivation being having to do less work for the same reward. 
 
 

What are the consequences of plagiarism to the reputations of the educational institutions? 

Eventually cheaters may fail in their professional duties due to all the cumulative plagiarism in their careers. This 
will in turn alert their employers of the poor quality of the education they received by the specific institutions they 
represent and show in their diplomas. Thus, plagiarism has a negative impact on the reputation of educational 
institutions in the long run. 
 
One particular case I’d like to cite on this particular subject is a local one [6]. In 2014, when campaigns for 
presidential elections were about to begin in Guatemala, presidential candidate Manuel Baldizón was accused of 
plagiarizing a very significant portion of a book supposedly authored by himself. This book, which was supposed to 
approach social, political and economic issues, was subject to several web searches performed by many 
Guatemalan citizens with internet access. The resulting verbatim sources included the well-known Spanish essay 
sharing site monografias.com. 
 
This case is relevant because after these accusations were made and the poorly written book was taken out of the 
market supposedly for a review, some new accusations on Baldizón were issued. This time he was accused of 
having plagiarized his doctoral Thesis in 2007. This obviously had negative consequences on the reputation of his 
Alma Mater, Universidad de San Carlos, which is Guatemala’s state university. The doctoral program Baldizón 
obtained his PhD from became discredited for having a terrible peer review system. 
 
 
 
References 

[1]  Pennycook, A. (1996). Borrowing others' words: Text, ownership, memory, and plagiarism. TESOL quarterly, 

30(2), 201-230.  

[2]  Maurer, H. A., Kappe, F., & Zaka, B. (2006). Plagiarism-A Survey. J. UCS, 12(8), 1050-1084. 
[3]  Hart,  M.,  &  Friesner,  T.  (2004).  Plagiarism  and  poor  academic  practice–a  threat  to  the  extension  of  e-

learning in higher education?. Electronic Journal on E-learning, 2(1), 89-96. 

[4]  Ercegovac, Z., & Richardson, J. V. (2004). Academic dishonesty, plagiarism included, in the digital age: A 

literature review. College & Research Libraries, 65(4), 301-318. 

[5]  Dante, E. (2010). The shadow scholar. The Chronicle of Higher Education, 12. Retrieved February 15, 2016, 

from http://chronicle.com/article/The-Shadow-Scholar/125329/ 

[6]  Guatemala opposition leader plagiarized his doctoral thesis as press. (n.d.). Retrieved February 15, 2016, 
from  http://www.kcba.com/2014/02/05/guatemala-opposition-leader-plagiarized-his-doctoral-thesis-as-
press/ 

[7]  Bugeja, M. (2004). Don't Let Students" Overlook" Internet Plagiarism. Education Digest: Essential Readings 

Condensed for Quick Review, 70(2), 37-43. 

 
 

