Marcus King (mking60) 
CS­6460 Spring 2016 
Assignment 4 
 
 

In my previous writing I narrowed into an intelligent tutoring system that focused on language. 
During my research I came across a relatively recent publication by Heilman & Eskanazi where 
they too were developing an intelligent tutoring system for language development.  Their 
approach was to focus on vocabulary, a manageable, but important building block for language 
learning [1].  Similarly, rather than trying to focus on the entire spectrum of language skills, I 
took will be focusing on an essential component of language learning.  That being spelling.   
 
In my research I discovered that there are, unsurprisingly, a lot of technology aids for teaching 
spelling.  However, the commonality between them is that they don’t take a student’s vocal 
response as input.  As we all know the entire population of students don’t all optimally learn with 
the same type of exercises.  Unfortunately every popular form of a spelling tool I could find 
required a non­verbal input response from students.  Typically the learning was in the form of 
the technology tool providing an audio version of the word being spoken and the student was 
expected to type in the correct spelling of the word.  What I’m proposing, and what makes my 
approach uniques, is that rather than requiring three different sensory inputs to use the tool I 
instead would only require the two typically used in language hearing, and speaking. The three 
sensory inputs for most tools being hearing for receiving the word that needs to be spelled, 
tactile for keying in the correct spelling, and visual for seeing where the expected word is to be 
spelled and for controlling the workflow of the tutoring system.  In my ideal system the student 
would be able to have full control over the tutoring system by just using voice commands, and 
would receive words to be spelled as spoken by the tutoring system.  The student would then be 
able to spell out the word letter by letter and receive positive or negative feedback depending on 
how correct their response was.  To me this seems rather obvious as this is how traditional 
spelling bee type competitions are held, yet most tools don’t follow the same format.  Of course 
this isn’t to say that my approach is any more important or valid, but from my observations there 
are two major forms of how language is represented; written and conversational.  As mentioned 
earlier most tools that I could find approach the written aspect, but few approach it as 
conversational. 
 
So what makes this an intelligent tutoring system?  My current thinking is that the student would 
be able to provide a target list of words that they would like to be trained on, or they could go on 
a skill building path where they would continually be challenged.  The first scenario is targeted 
at helping students’ prepare for an upcoming spelling exam and isn’t very interesting in it’s 
design. I’ll focus instead on the second scenario. The ITLS would present the student with a 
random word based on the word’s pre categorized difficulty and depending on whether or not 
the student spells the word correctly the ITLS system would start giving the student more and 
more difficult words.  Where things get interesting is when the student provides incorrect 
responses, the ITLS system would attempt to categorize why the student errored and would 

attempt to categorize that mistake.  The system would of course, correct the student, and then 
would present them with other words that contained the same characteristics that the student 
errored on.  For instance a student may have difficulty with silent letters similar to those present 
in words like descend and crescent.  The tutor would try and reinforce the student experiencing 
words similar to those until the student had shown a firm understanding and had corrected their 
spelling mistakes.  In addition a student may struggle with certain phonemes or certain tenses of 
root words and ideally the ITLS system would be able to discern and categorize those and 
expose the student to further instances of similar words.  
 
My current thinking is that I can leverage the Amazon Alexa language system which powers the 
Amazon Echo device.  Alexa has a public SDK that can be leveraged which takes care of doing 
the text to speech and speech to text components on your behalf, which quite frankly is much of 
the heavy lifting.  As an end result, if successful, I would be able to release this tool as a new 
skill set on any Alexa enabled devices making it accessible to a very wide audience immediately 
[2]. 
 
References: 
[1] ​http://www.cs.cmu.edu/~hypoform/its­workshop/papers/ITS06_illdefinedworkshop_HeilmanEskenazi.pdf  
[2] ​https://developer.amazon.com/public/solutions/alexa/alexa­skills­kit/getting­started­guide  

