CS	6460	–	Spring	2016		

	

Vinod	Dinakaran	

a	CS	6460	Project	proposal	

grok.code.guide	
(2388	words	without	references)	

by	Vinod	Dinakaran	

Table	of	Contents	

1	
INTRODUCTION	..............................................................................................................................	2	
2	 THE	PROBLEM	.................................................................................................................................	2	
2.1	 ORIGINAL	PROBLEM	STATEMENT	...............................................................................................................	2	
2.2	 REFINED	PROBLEM	AREAS	...........................................................................................................................	2	
2.3	 A	MODEL	FOR	CODE	COMPREHENSION	AND	ITS	TOOLING	......................................................................	3	
2.4	 NATURAL	APPROACHES	TO	SOLVING	THESE	PROBLEMS	.........................................................................	4	
3	 EXISTING	SOLUTIONS	...................................................................................................................	4	
4	 PROPOSED	SOLUTION	...................................................................................................................	6	
5	 DESIGN	OF	THE	PROPOSED	SOLUTIONS	.................................................................................	7	
5.1	 USER	STORIES	................................................................................................................................................	7	
5.2	 HIGH	LEVEL	DESIGN	.....................................................................................................................................	7	
5.2.1	 Platform	Assumptions	.........................................................................................................................	7	
5.2.2	 Tool	Design	...............................................................................................................................................	8	
5.3	 BRANDING	.......................................................................................................................................................	8	
5.4	 COMMUNICATION	WITH	COMMUNITY	........................................................................................................	8	
5.5	
INTEGRATIONS	...............................................................................................................................................	8	
IMPLEMENTATION	PLAN	.............................................................................................................	9	
6	
6.1	 PROJECT	TEAM	...............................................................................................................................................	9	
6.2	 DETAILED	PROJECT	PLAN	............................................................................................................................	9	
7	 REFERENCES	..................................................................................................................................	10	
APPENDIX	A:	PROPOSAL	ELEMENT	TRACEABILITY	MATRIX	...............................................	11	

	

	

1 

Introduction	

2  The	Problem	
2.1  Original	Problem	statement	

Vinod	Dinakaran	
CS	6460	–	Spring	2016		
This	document	is	Vinod	Dinakaran’s	submission	for	project	proposal	as	part	of	
CS6460	–	Spring	2016.		It	outlines	code	comprehension	as	a	problem	area,	reviews	
existing	solutions	to	specific	problems	in	it	and	proposes	some	novel,	if	evolutionary	
solutions	to	them.	As	required	by	the	assignment,	it	also	provides	a	design	for	the	
solutions	and	an	implementation	plan.	
At	the	beginning	of	the	course,	my	problem	statement	read	thus:	
	“The	problem	I’d	like	to	explore	is	Code	Comprehension:	how	do	developers	learn	
and	understand	code?	How	does	the	newest	member	on	a	development	team	learn	
the	architecture,	design	and	idiosyncrasies	of	the	systems	that	the	team	owns?	How	
can	a	tool	–	specifically	the	editor	–	be	used	to	aid	this	process?”	
During	the	literature	survey	that	I	did	as	part	of	the	early	assignments	in	the	course,	
I	found	that	there	are	some	theoretical	models	proposed	for	code	comprehension	as	
summarized	by	Storey	(2005)	and	many	of	tools	built	both	by	academia	and	
industry	–	a	partial	summary	of	which	Gomolka	and	Humm	did	(2013).	There	were	
still	problems	in	the	space,	however,	as	found	by	Maalej,	Tiarks,	Roehm	and	Koschke	
(2014).	Their	findings	that	are	relevant	to	tool	enhancements	could	be	distilled	into	
the	following:	
	1.  Gap	between	research	and	practice:	Developers	don’t	use	(or	even	know	
about)	advances	in	code	comprehension	research.	
2.  Context	Awareness	in	Code	comprehension:	Understanding	code	needs	a	
record	of	the	state	at	the	time	of	building	the	code,	not	at	the	time	of	reading	it.	
Developers	are	Ok	to	share	this	detail	anonymously.	
3.  User	Behavior	and	Usage	Management:	Developers	sometimes	pretend	to	be	
users	to	understand	a	system	and	sometimes	avoid	deep	code	comprehension.	
4.  Knowledge	sharing	dilemmas:	Developers	need	to	know	who	built	the	existing	
code.	
	These	therefore,	are	the	specific	areas	that	my	project	will	attempt	to	address.	Of	
these,	#2	requires	particular	mention	because	it	is	the	one	finding	that’s	related	to	
the	time	when	code	is	created	versus	when	its	read	and	understood	subsequently.	
This	and	a	related	question	by	my	mentor	led	me	to	expand	my	research	to	include	
tools	and	techniques	that	enable	the	creation	of	understandable	code	along	with	
those	that	enable	their	comprehension	once	created.	

2.2  Refined	Problem	areas	

	

Vinod	Dinakaran	
CS	6460	–	Spring	2016		
To	put	code	comprehension	and	its	tooling	in	perspective,	its	useful	to	have	a	mental	
model	of	the	steps.	Code	comprehension	occurs	at	two	key	junctures	in	the	software	
lifecycle.	This	process	is	dubbed	“Grok”	in	the	diagram	below,	following	the	
developer	parlance	for	“understand”	(see	the	Hacker’s	Dictionary,	n.d).	

2.3  A	model	for	code	comprehension	and	its	tooling	

	
•  A	 developer	 understands(groks)	
the	 requirements,	 synthesizes	 an	
architecture/design		and	creates	the	software	(codes	it).	The	only	of	part	this	
intense	process	that’s	available	for	future	inspection	is	the	code	itself.	
o  There	is	lots	of	tooling	for	the	code	process.	
o  There	is	very	little	tooling	for	the	grok	process	or	to	retain	its	artifacts	
(i.e.,	the	understanding	gained).	Instead,	engineering	best	practices	
and	documentation	(off-codebase	and	inline)	are	relied	upon.		
•  Subsequently,	a	change	needs	to	be	made	to	the	existing	code	–	to	fix	a	bug	or	
to	add	a	feature.		At	this	point,	another	(or	sometimes	the	same)	developer	
reads	the	code	and	understands	it	(re-groks	it,	so	to	speak)	enough	to	make	
the	appropriate	set	of	changes	(codes	furthers).	
o  A	lot	of	the	code	comprehension	and	visualization	tooling	is	aimed	at	
this	juncture,	attempting	to	reverse	the	initial	process	to	the	extent	
possible.	
•  The	gap	in	understanding	between	the	two	developers	where	there	is	
opportunity	for	the	first	to	help	(ie,	guide)	the	second	is	usually	relegated	to	
software	best	practices	such	as	good	coding	styles	and	documentation.	This	
is	depicted	as	a	gap	-	the	black	dashed	arrow	-	above.	

Problem	

Proposed	Approach	

2.4  Natural	approaches	to	solving	these	problems	

Vinod	Dinakaran	
	
CS	6460	–	Spring	2016		
Given	the	problem	areas	mentioned	above,	the	natural	next	step	is	to	propose	some	
general	approaches	that	might	work,	like	so:	
	
Gap	between	
Use	this	fact	as	a	guiding	principle	in	coming	up	with	
research	and	
tools.	If	it’s	entirely	a	new	solution,	it	may	not	work.	
practice:	
Build	a	set	of	tools	that	meet	the	intent,	but	are	small	
and	can	be	adopted	easily	
Context	Awareness	
Record	developer	activity	and	make	it	available	for	
and	Code	
self-analysis,	summarization	and	sharing	–	with	
comprehension	
respect	to	the	developer’s	privacy	and	comfort	level.	
User	Behavior	and	
Usage	Management	 Enable	automation	of	this	step,	potentially	recording	
trial	runs	for	replay	later.	Provide	ability	to	zoom	into	
the	appropriate	code	easily.	
Make	the	authors	of	code	easily	accessible	on	a	per-
Knowledge	sharing	
dilemmas	
component	basis.	
	Clearly,	none	of	these	suggestions	are	particularly	new	and	have	found	expression	in	
multiple	tools	in	some	shape	or	form,	which	will	be	explored	in	the	next	major	
section.	
•  Tools	 that	 make	 adoption	 easy:	Editors	are	generic	tools	by	nature	and	
therefore	usually	make	adoption	easy	by	sticking	to	standard	UI	affordances	
and	open	data	formats.	Advanced	ones	are	similar	to	IDEs	in	that	they	allow	
new	features	to	be	plugged	into	the	code	via	extensions	or	plugins.	The	
nuance	in	this	case,	therefore,	is	one	of	the	ease	of	adoption	of	plugins	
themselves.	If	the	plugin	marketplace	is	crowded,	developers	experience	the	
same	adoption	issues	at	the	plugin	level	as	they’d	experienced	at	the	tool	
level.	Some	IDEs	such	as	Intellij	circumvent	this	issue	by	making	some	
features	part	of	the	core	product	itself.	Another	option	for	captive	audiences	
such	as	.net	is	to	keep	innovating	on	the	single	IDE	in	the	environment,	
namely	Visual	Studio.			
•  Tools	that	record	developer	activity:	Minelli,	Mochi,	Lanza	and	Kobayashi	
(2015)	describe	two	such	tools:	the	DFLOW	plugin	for	the	Smalltalk	Pharo	
IDE	and	the	PLOG	plugin	for	Eclipse.		
•  Tools	 that	 track	 trial	 runs	 of	 the	 application:	Light	Table	by	Chris	
Granger(2012)	and	the	Elm	Language	by	Evan	Czaplicki	(2012)	are	two	
examples	of	tools	that	provide	rich	feedback	of	the	running	application.	
However,	they	are	language-specific	and	do	not	allow	recording	of	runs	for	
future	use.	
•  Tools	 that	 provide	 information	 about	 authors:	This	is	a	easily	satisfied	
ask	and	most	VCS	tools	and	editors	have	some	kind	of	support	for	display	of	

3  Existing	Solutions	

	

Vinod	Dinakaran	
CS	6460	–	Spring	2016		
author	information.	For	example,	git-blame	and	Atom’s	git	blame-based	
gutter	display	provide	this	information.	Gource	by	Cauldwell	(2014)	is	
another	tool	that	provides	a	video	narrative	of	authors	arriving,	influencing	
and	departing	from	a	codebase.	
	The	key	issues	that	make	these	tools	less-than-optimal,	therefore,	are:	
•  Exclusive,	 not	 inclusive	 solutions:	Some	tools	work	only	within	their	
domain	and	do	not	allow	for	the	multi-tool	usage	described	by	Maalej	et	al.	
•  Alternative,	 not	 mainstream:	Some	tools	like	the	DFLOW	plugin	are	built	
for	the	Smalltalk	IDE,	not	for	mainstream	languages	like	Java,	Javascript	and	
C#.	
•  Non-portable:	Most	solutions	are	not	intended	to	work	the	same	across	
multiple	Operating	Systems.	
•  Large,	not	small:	Most	tools	are	part	of	behemoth	soverign	applications	and	
therefore	make	adoption	difficult,	eg,	the	Eclipse	Journal	plugin	is	lost	in	the	
sea	of	other	eclipse	plugins.	
•  Do	not	guide:	Most	importantly,	most	existing	tools	do	not	address	the	gap	
between	the	initial	developer	and	the	subsequent	one.	The	ability	to	guide	
via	the	codebase	is	therefore	lost.	

	

Problem	

Tool	#	

Proposed	Solutions	
Description	

Type	of	
Solution	

4  Proposed	Solution	

Vinod	Dinakaran	
CS	6460	–	Spring	2016		
My	proposal	to	improve	the	state	of	response	to	these	problems	is	a	set	of	tools	that	
I	call	“Grok.Code.Guide”,	guided	by	the	approach	to	address	the	adoption	problem.		
For	each	of	the	other	problems,	at	least	one	tool	is	suggested,	sometimes	two.	Each	
tool	is	further	broken	up	into	versions	that	are	Fallback,	Comfortable	or	Preferred;	
which	represent	positions	of	implementation	completion	that	would	progressively	
improve	the	degree	to	which	the	problem	is	addressed.	
	
Fallback	
Create	individual	plugins	
Gap	between	
research	and	
Comfortable	
Add	tools	as	a	package	of	plugins	for	
practice	
easy	adoption	
Preferred	
Add	tool	as	core	feature	of	IDE	
Context	
Same	as	above	but	less	portable	or	
Fallback	
Awareness	and	
IDE-specific	tracker	
code	
Preferred	
Create	system-wide,	IDE-agnostic,	
comprehension	
portable	tracker	
Create	a	journal	to	capture	thoughts	
Comfortable	
and	pre-commit	decisions	
Comfortable	
Create	a	basic	input	record/replay	
User	Behavior	
tool	
Create	a	full-featured	input	
Preferred	
record/replay	tool	
Preferred	
Store	a	reason	for	the	change	at	time	
Usage	
of	commit	
management	
Knowledge	
Display	author	list	for	each	file	
Fallback	
sharing	
Preferred	
Visualize	the	codebase	as	a	treemap	
dilemmas	
colored	by	author	
	To	address	the	issues	with	existing	tools,	my	project	intends	to:	
• Implement	the	tools	as	OS-native	applications	where	applicable	and	breaking	
them	
into	 smaller	 client-server	 architectures	 where	 applicable.	 This	
increases	inclusion	and	portability	and	the	sizes	smaller.	
• Implement	the	tools	as	plugins	for	Atom.io,	Intellij	IDEA	and	Eclipse	–	in	that	
order.	This	addresses	the	mainstream	developer	tools.	
• Most	importantly,	it	introduces	the	ability	for	the	original	developer	to	pass	
information	onto	the	subsequent	ones	through	two	mechanisms	–	the	journal	
and	the	commit	message	–	improving	the	guidance	given.	To	aid	adoption,	I	
plan	to	do	this	in	a	code-adjacent	fashion.	
	The	next	section	dives	deep	into	each	tool	in	terms	of	user	stories,	a	high	level	
design	and	some	implementation	considerations.		

All	
1	
2	
3	
4	
5	
6	

CS	6460	–	Spring	2016		

	

Vinod	Dinakaran	

5  Design	of	the	proposed	solutions	
5.1  User	Stories	

Story#	
1	
2	
3	
4	
5	
6	

As	a..	
Developer	
Developer	
Developer	
Developer	
Developer	
Developer	

	

..I	would	like	to..	
See	an	automatically	
tracked	record	of	my	
coding-related	actions		
Store	a	journal	of	my	
planned	coding	thoughts	
and	actions	
Have	recorded	
application	runs	
Store	a	reason	for	the	
code	change	at	commit	
See	a	list	of	authors	for	a	
file	
See	the	codebase	as	a	
treemap	colored	by	
author	

..so	that..	
Review	them	and/or	
share	them	
Remind	myself,	use	for	
documentation	and/or	
share	them.	
Speed	up	exploration	of	
the	system.	
Others	can	read	and	
understand	my	change	
I	can	communicate	with	
them	to	understand	the	
code	
I	can	communicate	with	
them	to	understand	the	
code	

5.2  High	Level	Design	
5.2.1  Platform	Assumptions	
IDE’s	targeted:	

1. Github’s	Atom	Editor	
2. Intellij’s	IDEA	
3. Eclipse.	
The	reasons	for	this	sequence	of	choices	are:	
•  Atom	is	easy	to	hack	and	add	plugins	for,	making	it	the	ideal	choice	for	the	
“fallback”	and	“comfortable”	level	of	implementation	
Intellij	and	Eclipse	already	have	a	lot	of	plugins	that	address	some	of	the	
problems.	
•  Git:	Other	version	control	systems	may	be	taken	up	in	the	future,	but	this	
choice	covers	a	lot	of	the	developer	community.	
	
Languages	used:	
•  Go/Java	for	the	tracker	server	component	
•  Coffeescript	for	Atom	Plugins	
Java	for	Intellij	and	Eclipse	Plugins	
•  C/C++/Objective	C	for	the	Recorder	tool	

Version	Control	System	used:	

• 

• 

	

5.2.2  Tool	Design	

Vinod	Dinakaran	
CS	6460	–	Spring	2016		
•  Tracker:	The	tracker	will	be	implemented	as	a	client-server	application	pair.	
The	client	will	be	an	IDE	plugin	and	the	server	will	be	a	native	OS	application,	
runnable	in	user	space	so	that	there	are	no	security	hurdles	at	installation.	
Further,	the	server	application	will	store	data	in	an	open,	single	line	format	
making	it	amenable	for	processing	with	command-line	utilities	(that	
developers	prefer).	
Journal:	The	journal	will	be	implemented	as	a	plugin	to	the	IDE.	It	will	allow	
the	developer	to	press	a	keyboard	shortcut	to	invoke	a	UI	that	will	allow	
input	of	a	journal	entry.	The	previous	entries	will	be	visible	for	read	and	edit	
at	the	same	time.	A	timestamp	will	be	automatically	added	by	the	plugin.	The	
data	will	be	stored	“code-adjacent”,	i.e.,	in	a	branch	of	the	code	itself.	This	
makes	it	easy	to	connect	to	specific	versions	of	code	and	to	share	it	using	
facilities	provided	by	the	VCS.	
•  Usage	 Record/Replay:	This	tool	will	be	implemented	as	a	native	OS	
application	(one	per	major	OS	or	portable)	with	the	basic	ability	to	track	
keyboard	and	mouse	input.	The	IDE	will	invoke	the	tool	as	an	external	one	
and	the	data	collected	will	be	sent	to	the	Tracker’s	server	application.	The	
preferred	version	will	enhance	the	basic	one	with	the	ability	to	track	context	
in	terms	of	the	application	that	was	in	focus	at	the	time	of	the	input.	
•  “Why”	at	commit:	This	tool	will	be	implemented	as	a	simple	IDE	plugin.	The	
commit	template	will	be	enhanced	to	include	language	requesting	the	
developer	to	add	a	reason	for	the	code	change.	
•  List	 of	 authors:	This	tool	will	be	implemented	as	an	IDE	sidebar	view	that	
shows	all	the	authors	who	have	ever	changed	the	file	in	current	focus.	The	
data	for	this	will	be	got	from	the	VCS.	
•  Codebase	as	Treemap:	This	tool	will	be	implemented	as	a	plugin	that	shows	
an	alternative	to	the	left	navigation	panel	of	most	Editors.	It	will	instead	
show	a	treemap	of	the	codebase	with	authors	highlighted	for	easy	reckoning.	
The	name	for	the	set	of	tools	to	be	built	will	be	clear	and	understandable	without	
additional	documentation.	“Grok.Code.Guide”	was	chosen	as	it	fits	this	criteria.	
•  A	Github	organization	named	“grok-code-guide”	(Dinakaran,2016)	has	been	
created	to	house	all	the	source	code	for	the	tools	to	be	built.	
•  Once	a	working	version	is	built,	I	will	publish	to	the	interest	groups	of	the	
appropriate	IDEs	and	to	developer	news	sites	such	as	Hacker	news	and	
r/programming.	
	
See	Platform	assumptions.	

5.4  Communication	with	community	

• 

5.3  Branding	

5.5 

Integrations	

• 

	

6.2  Detailed	Project	Plan	

Implementation	Plan	

6 
6.1  Project	Team	

Vinod	Dinakaran	
CS	6460	–	Spring	2016		
This	is	an	individual	project,	so	I,	Vinod	Dinakaran,	will	be	the	sole	team	member.	
This	is	a	draft	plan	and	doesn’t	depict	the	following	fallback	mechanisms:	
•  The	“project”	consists	of	multiple	tools	that	can	be	released	independently.	
Based	on	the	speed	with	which	I’m	able	to	execute	on	their	implementation	I	
plan	to	build	as	many	as	I	can.	The	general	sequence	is:	Why	tool,	tracker,	
journal,	authors,	recorder	and	treemap.	
•  Each	tool	that	has	multiple	versions	will	be	built	in	“Fallback”	version	first,	
followed	by	“Comfortable”	and	“Preferred”.	
IDEs	will	be	addressed	in	the	order	laid	out	in	Sec	5.2.1	and	adjusted	
downwards	if	implementation	takes	more	time	than	expected.	
Intermediate		and	major	milestones	are	bolded.	
	Milestone/Task	
Due	Date	
2/21	
Project	Proposal	
Research	building	plugins	in	Atom	and	
2/28	
Intellij	
Weekly	status	check-in	#1	
2/28	
3/06	
Weekly	status	check-in	#2	
3/13	
Weekly	status	check-in	#3	
Weekly	status	check-in	#4	
3/20	
3/27	
Weekly	status	check-in	#5	
Weekly	status	check-in	#6	
4/03	
4/10	
Start	codebase	as	treemap	
Weekly	status	check-in	#7	
4/10	
Overflow	tasks	
4/17	
4/17	
Weekly	status	check-in	#8	
Overflow	tasks	
4/24	
Weekly	status	check-in	#9	
4/24	

Build	Why	at	commit,	start	tracker	

Finish	codebase	as	treemap	

3/06	

3/13	

3/20	

3/27	

4/03	

Build	tracker	

Build	Journal	

Build	List	of	authors	

Build	basic	recorder	

	

Final	Project	
Project	Presentation	
Project	Paper	

4/17	

5/01	
5/01	
5/01	

	

7  References	

Vinod	Dinakaran	
CS	6460	–	Spring	2016		
•  Walid	Maalej,	Rebecca	Tiarks,	Tobias	Roehm,	Rainer	Koschke.	(Aug	2014).	On	
the	comprehension	of	Program	Comprehension.	ACM	Transactions	on	
Software	Engineering	and	Methodology,	Vol.	23,	No.	4,	Article	31.Retrieved	
from:	https://mobis.informatik.uni-hamburg.de/wp-
content/uploads/2014/06/TOSEM-Maalej-Comprehension-PrePrint2.pdf	
•  Storey,	M.(2001).	Theories,	methods	and	tools	in	program	comprehension:	
past,	present	and	future.	13th	International	Workshop	on	Program	
Comprehension,	2005.	IWPC	2005	Proceedings.	Pages:	181	-	191,	DOI:	
10.1109/WPC.2005.38	
•  Roberto	Minelli,	Andrea	Mocci,	Michele	Lanza	and	Takashi	Kobayashi	(May	
2015).	I	know	what	you	did	last	summer.	An	investigation	of	how	developers	
spend	their	time.	Retrieved	from:	
http://www.inf.usi.ch/faculty/lanza/Downloads/Mine2015b.pdf		
•  Robert	A	Heinlein.	“Grok”.	Retrieved	from:	
http://www.hackersdictionary.com/html/entry/grok.html	
•  Andreas	Gomolka,	Bernhard	Humm	(2013).	Structure	Editors	–	old	hat	or	
new	future?	Retrieved	from:	https://www.fbi.h-
da.de/fileadmin/personal/b.humm/Publikationen/Gomolka_Humm_-
_Structure_Editors__Springer_ENASE_.pdf	
•  Chris	Granger(Apr	2012).	Light	Table	–	a	new	IDE	concept.	Retrieved	from:	
http://www.chris-granger.com/2012/04/12/light-table---a-new-ide-
concept/	
•  Evan	Czaplicki	(2012).	Elm	Language.	Retrieved	from:	http://elm-lang.org/.	
•  Andrew	Cauldwell	(2014).	Gource.	Retrieved	from	http://gource.io.	
•  Vinod	Dinakaran	(Feb	2016).	Grok-code-guide.	Retrieved	from:	
https://github.com/grok-code-guide	.	

	

Element	

Appendix	A:	Proposal	element	traceability	matrix	

Vinod	Dinakaran	
CS	6460	–	Spring	2016		
The	assignment	description	expects	the	proposal	to	have	the	following	elements.	
Mapped	here	are	the	sections	where	each	can	be	found.	
	
Sec	6.1	
Members	in	team	
Task	List	including	trailer,	progress	report,	final	paper	
Sec	6.2	
Calendar	with	weekly	milestones	from	Feb	22	to	May	1	
Sec	6.2	
Sec	6.2	
Description	of	intermediate	milestones	
Division	of	Responsibilities	
NA	
A	description	of	the	problem	to	be	solved	
Sec	2	
Sec	3	
A	description	of	existing	solutions	for	that	problem,	specifically	
to	contextualize	why	your	solution	is	needed.	
A	description	of	the	design	of	the	tool	you	will	create.	
Sec	5	
Sec	5.2.1	
A	technical	description	of	the	tools,	languages,	and	other	
resources	that	will	be	used.	
A	description	of	the	integrations	or	external	resources	that	will	
Sec	5.5	
need	to	be	obtained.	
Sec	6.2	
Fallback	plans	in	case	portions	of	these	details	cannot	be	
completed.	
Extra:	Connection	and	contribution	to	community	
Secs	5.3-4	

Section	

	

All	Proposals	

Tool	Proposals	

				

