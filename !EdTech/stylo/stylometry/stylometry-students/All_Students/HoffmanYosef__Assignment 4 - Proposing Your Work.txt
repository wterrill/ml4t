I have been researching the online tutoring community and looking at what the existing web 

 
sites have to offer. So far, I have only seen sites that have many tutors on staff available to help 
students. The main purpose of these sites is to match you up with a tutor who is knowledgeable in your 
area of need. This likely fills the need of a college students who needs someone to help him with his 
homework or an adult who has gone back to school to further her education. But what about students 
in K – 12? Some of the sites discuss how they do background checks on their tutors and ensure that 
safety is a priority of theirs, while other sites leave the responsibility on being safe to the student. 
Whether or not a student and parent feels safe using one of these tutoring sites is a completely different 
story to whether or not students and parents trust these tutors. Tutors need to build a rapport with 
their K-12 students as well as their students’ parents. When I tutored a high school boy in Computer 
Science, his parents had to make sure they could trust me before letting me tutor their son. Additionally, 
over time as I got to know the boy, and he got to know me, our sessions became more and more 
productive. Despite his ADD, I was able to help him do well in his class. 

I would like to build YeshivaTutors.com to facilitate tutoring relationships that build trust. The 

 
site will not just simply set up students with tutors, but it will contain a set of tools to help build the 
relationship. Tutors will write up a summary of students’ progress after each session, which will then be 
available to parents. Tutors will also be able to suggest work for students to do before the next session, 
and since the parents are being kept in the loop, they will help ensure that their child completes the 
work. Students will be able to communicate with their tutors outside of their scheduled sessions. Part of 
being a good tutor is helping out the student, even if he just has a quick question and does not need a 
full session. And the same goes in the other direction. If a tutor helps a student prepare for a test, the 
tutor should check in with the student after the test to see how it went. This helps build the relationship 
and shows the student that the tutor cares about their progress. All of this communication between 
students and tutors should be kept inside of the web app. This will be a feature like LinkedIn messaging 
where you get an email whenever you receive a message and can then reply to the message. A 
Facebook messaging-like feature would be even better where tutors and students could communicate in 
real time using an app. As you can tell, my ideas for this site are not 100% worked out, and that is what I 
hope to do for my project. I would like to design YeshivaTutors.com with these features and think of 
even more as I progress. I would really like to tap into the K-12 tutoring market and design a web app to 
help facilitate these kinds of tutoring relationships. 

