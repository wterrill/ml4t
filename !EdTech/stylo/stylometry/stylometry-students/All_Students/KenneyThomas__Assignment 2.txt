Thom Kenney 
24 January 2016 
 

 
 

Education Technology 
Assignment 2 

 
Learning in countries with comparatively lower investments in education are often 

countries with significant rates of impoverished people. Comparing the government expenditures 
of education spend as a percentage of GDP between 
the United States and the African country of Uganda 
for example (see right), it is clear that Uganda’s 
ratio of spending on education is well below the 
United States.1 Correlating that data to poverty, 38% 
of Uganda’s population is under the poverty line,2 
whereas the United States Census Bureau estimates 
the poverty level for the United States as 14.8%.3  
There is an assumed link between poverty 
and success in education, but that may only be a 
symptom of a larger problem. There is evidence to 
support parental involvement has a direct impact on student success. One potential option for 
countries like Uganda could be to implement social gamification to engage parents with the 
expectation that student achievement would increase. 

Socioeconomic status is often seen as an impediment to student achievement. Sui-Chu 
and Williams assert “a number of studies have suggested that parents of higher [socioeconomic 
status] are more involved in their children’s education,” and that “greater involvement [by 
parents]” plays a major role in “enhancing academic achievement.”4 There is likely significant 
additional research to support this theory. One could postulate that parents in developing 
countries have less time to spend with their children in support of their education, and that lack 
of engagement could lead to lower academic achievement. The challenge is how to more closely 
engage parents. 

																																																								

1 http://data.worldbank.org/indicator/SE.XPD.TOTL.GD.ZS/countries/US-UG?display=graph 

2 http://www.unicef.org/infobycountry/uganda_statistics.html 

3 https://www.census.gov/hhes/www/poverty/about/overview/ 

4 Sui-Chu, E. H., & Williams, J. D.. (1996). Effects of Parental Involvement on Eighth-Grade Achievement. 
Sociology of Education, 69(2), 126–141. http://doi.org/10.2307/2112802 

The United States has no lack of helicopter parents overly engaged in their children’s 
education (no citation needed, fact assumed); but, even in such a wealthy country, there are 
impoverished families that lack the social or financial means to be active enough to truly support 
their children. Social gamification can help as parents could have “proper tools and access to 
data” to help motivate and reward students. This motivation can help change behaviors to 
improve the learning outcomes as “social recognition and rewards motivate students to improve 
their skills.”5 

 Gamification is starting to take hold in the entrepreneurial (or teacherpreneurial6) world. 
Although there are solutions like iPass7 and Follett Aspen8 for education management, personal 
experience shows they fail to truly engage parents in the process. In addition, these solutions 
would likely not be successful in developing countries because of the lack of access to sufficient 
broadband connections to interact with the web based products. According to the UN News 
Centre, “90 per cent of those living in the poorest nations”9 do not have access to broadband 
internet connections. To bridge technology, gamification, and parent engagement, a completely 
different solution needs to be developed. 

Engagement for parents can take many forms, and gamification can use a variety of 

mediums to extend the social networks for students. One such method could be mobile devices 
rather than web applications as, on average, “84% [of the population] in emerging and 
developing nations [own] some type of cell phone.”10 Cell phone texting may be just as effective 
as web applications because parents could receive and provide feedback via text, a common 
mobile communication medium. Badges that indicate a student’s success on a test or project can 
be easily communicated to parents through texting, and parents forwarding that text to others in 
their social circle can broaden the positive social effect of that student’s success.  

																																																								

5 Simões, J., et al. (2012) A social gamification framework for a K-6 learning platform. Computers in Human 
Behavior, http:// dx.doi.org/10.1016/j.chb.2012.06.007  

6 http://www.teachingquality.org/teacherpreneurs 

7 http://www.imgsoftware.com/products/ipass 

8 http://www.follettlearning.com/webapp/wcs/stores/servlet/en/fssmarketingstore/curriculum-management-system 

9 http://www.un.org/apps/news/story.asp?NewsID=51924#.VqWNKTZfnNU 

10 http://www.pewglobal.org/2015/03/19/1-communications-technology-in-emerging-and-developing-nations/ 

 Mobile text-based engagement coupled with gamification has the potential to 

significantly improve education outcomes in developing countries. Developing a system that 
successfully engages parents, rewards students for achievement, and builds strong social 
networks of support has yet to be written, but has tremendous potential. Research for this term 
will seek to outline a plan for an application that can bring this all together at a low cost with 
relatively low barriers to adoption. 

 
  

