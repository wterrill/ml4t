Chris Graham 
cgraham36 
Assignment 3 

Large Scale Assessment for Programming Assignments 
 
Practical programming assignments are a common part of computer science education, and the 
grading of student programming work involves many different factors. Traditionally student work 
has been graded with a combination of automatic methods, such as instructor­created test 
cases that the student code is executed against, and manual methods, such as visual 
inspection of the code to verify that the student’s implementation is efficient and well­designed 
(Ala­Mutka 2005). With the advent of the massive open online course (MOOC), the ability of a 
human instructor to visually inspect a student’s work is not always a practical option. Many 
MOOC courses have implemented systems with purely automated test cases as the only 
assessment criteria, and have ​faced criticism​ for ignoring the aspects of grading that were 
traditionally done manually. Given the relatively short time that these very large courses have 
existed, an opportunity exists to improve how these assessments are structured and graded to 
best suit the unique constraints introduced by the MOOC format. 
 

Advanced Automation 
 
Many researchers have explored using more advanced techniques than simple test cases to 
fully automate various aspects of programming assignment assessment. Some approaches 
apply existing automated code evaluation techniques to student assignments. FrenchPress 
(Blau 2015) for example applies the sorts of rules­based static code analysis used in 
professional tools such as ​FindBugs​ to automatically provide feedback on coding style to novice 
programmers. Similarly, static code analysis can also be used to directly detect more subtle 
functional errors in student code, such as using the LAV static analysis tool to find possible 
buffer overflow conditions (Vujošević­Janičić et al. 2013). Other approaches use generated 
feedback (Singh & Solar­Lezama 2013) to help students debug issues with their code  in real 
time while working on the assignment, helping alleviate the need for human tutoring. Some 
methods such as QuickPACK (Brusilovsky & Sosnovsky 2005) combine both the generation of 
programming problems and the assessment of student answers to fully automate the process. 
Although progress is being made in these areas, assessing the quality of student code through 
purely automated methods is still a challenge. It remains difficult, for example, for automated 
grading systems to compare the relative strengths of different approaches to implementing a 
given project, and the expressiveness of programming languages ensures that many unique 
solutions are possible for any given assignment. 
 

Peer-Based Assessment 
 
Given that the MOOC structure involves many concurrent students, many MOOC­based 
courses use some form of peer­based assessment to augment fully automated assessment. 

Chris Graham 
cgraham36 
Assignment 3 

Generally these sorts of peer reviews are used for open ended assignments such as essays, 
but peer grading can also be used to assess aspects of programming assignments that cannot 
be effectively evaluated through automated methods. Although each individual student may not 
have the expertise to properly grade the code of other students, research has shown that in 
aggregate the student review scores are comparable to scores from trained instructors (Reily et 
al. 2009). Using tools such as the EduPCR system (Sitthiworachart & Joy 2008), peer review 
can be integrated into the process of completing the assignment so that students have an 
opportunity to revise their code after viewing feedback from peers, and also apply lessons 
learned from understanding different programming approaches used by peers. In the current 
state of MOOC courses, much of the interaction between peers takes place as unstructured 
communication in the course discussion forums. Students help each other debug issues and 
understand the material through creating and responding to discussion topics, but these sorts of 
activities could be more focused and integrated into the course itself. A combination of 
automated assessment methods and peer­based interaction could combine the positive aspects 
of both to better match the effectiveness of traditional semi­automated programming assignment 
grading. 
 

References 
 
Ala­Mutka, K. M. (2005). A survey of automated assessment approaches for programming assignments. 
Computer science education​, ​15​(2), 83­102. 
 
(2012). Automatic Grading of Code Submissions in MOOCs isn't ... Retrieved January 29, 2016, from 
http://gregorulm.com/automatic­grading­of­code­submissions­in­moocs­isnt­perfect­either/​. 
 
Blau, H. (2015). FrenchPress Gives Students Automated Feedback on Java Program Flaws. ​Proceedings of 
the 46th ACM Technical Symposium on Computer Science Education​. ACM. 
 
Vujošević­Janičić, M., Nikolić, M., Tošić, D., & Kuncak, V. (2013). Software verification and graph similarity 
for automated evaluation of students’ assignments. ​Information and Software Technology​, ​55​(6), 
1004­1016. 
 
Singh, R., Gulwani, S., & Solar­Lezama, A. (2013). Automated feedback generation for introductory 
programming assignments. ​ACM SIGPLAN Notices​. ACM. 
 
Brusilovsky, P., & Sosnovsky, S. (2005). Individualized exercises for self­assessment of programming 
knowledge: An evaluation of QuizPACK. ​Journal on Educational Resources in Computing (JERIC)​, ​5​(3), 6. 
 
Reily, K., Finnerty, P. L., & Terveen, L. (2009). Two peers are better than one: aggregating peer reviews for 
computing assignments is surprisingly accurate. ​Proceedings of the ACM 2009 international conference on 
Supporting group work​. ACM. 
 
Sitthiworachart, J., & Joy, M. (2008). Computer support of effective peer assessment in an undergraduate 
programming class. ​Journal of Computer Assisted Learning​, ​24​(3), 217­231. 

