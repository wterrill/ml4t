CS 6460- Education Technology 

Individual Assignment #3: by Guharaman Janakiraman 

Abstract 
 
Phenomena being explored for Education Technology is to mixing personal productivity 
techniques such as Pomodoro and existing Education Technologies to increase 
effectiveness in Students’ learning. Particular problems to be solved is to teach Agile 
Methodologies and concepts that are soft skill based to a group of learners. In addition to 
the productivity techniques, leveraging a knowledge base of common misconceptions in 
Agile will help accelerate the learning of students as well 

Problem Elaboration  
Challenge in teaching a methodology or process based course has a different challenge 
compared to something more quantitative like programming. Student engagement is 
easier in such scenario because of immediate demonstrable results. In case of soft skills 
training, it is easy for students to get tuned out. Also measuring the effectiveness of 
whether the students understood the course content or not will be a challenge of its own 
 
This research attempts at addressing the two problems listed above 

1.  Bringing in concepts from personal productivity techniques to the classroom to 

increase the learning effectiveness 

2.  Reinforce soft skill concepts by having a misconception database for concepts (as 

opposed to a right or wrong answer that is possible for hard skills) 

 
Diving deep into productivity technique, Selective sustained attention, also known as 
focused attention, is the level of attention that produces the consistent results on a task 
over time. While studies have a range of average human attention span established from 5 
minutes to about 20 minutes at a time. One thing is for sure that it is generally no longer 
than 20 minutes. Ability to renew attention can extend it for longer such as long films.  
 
In a class room setting, following techniques can be transferred from productivity and 
agile  
Blocking and Chunking- Avoiding distractions for spurts of time followed by focused 
breaks for distractions  
Power of Habit- Habit is based on Cue-Behavior-Reward cycle. If there is a cue, and 
behavior requires will power, then the general tendency is for humans to jump to reward 
without performing the required action. This is what procrastination is about. If the will 
power factor is eliminated and it is more of a routine, then it becomes an effective habit. 
 
Combining the two factors above, a model for an effective course structure could be to 
have specific chunks of periods of concentration, followed by breaks. Key thing is to 
have this intervals to be absolutely the same in order to avoid variability. Once this is 
done, it becomes a behavior that is in line with human nature.  
 

CS 6460- Education Technology 

Individual Assignment #3: by Guharaman Janakiraman 

As an example to demonstrate the same, the sample course is chunked into Pomodoro’s 
and breaks. Specific problem that is being taken up is to teach Agile Methodology to a 
diverse group of students.  
 

Pomodoro 1 
Short Break 

9:30 
9:55 

9:55 
10:00 

Intro to class 

0:25 
0:05  Team Formation and greetings 

Pomodoro 2 

10:00 

10:25 

0:25 

Short Break 

10:25 

10:30 

0:05 

Pomodoro 3 
Long break 

 

10:30 
10:55 

10:55 
11:15 

0:25 
0:20 

Agile and Waterfall 
- Process Rigidity Continuum 
- Agile triangle 
- Empirical Process 
Agile in your organization – 
Discussion  

Agile Mindset - 10 min. team work 
10 minutes team presentation 
5 minutes solution 
 Forced distraction for students 

 
Having a repeatable time box and specific cadence, would help the students concentrate 
and internalize concepts in a better manner. Similar to movies, this structure also aims at 
renewing the concentration by having a mix of activities as opposed to just lectures 
 
Second part of the problem is to accelerate learning by having the common 
misconceptions highlighted on very specific topics being taught. An example concept is 
shown below 

For selling Agile to executive management, commonly sold concept is that the 
Productivity would increase manifold using Agile. In Reality, taking on anything new 
with humans involved has to go through forming, storming, norming and performing. 
It is no surprise that productivity actually dips when Agile is adopted for the first time. 
Having students share their experience would be an effective way to teach this concept 

 

Conclusion 
Idea of this project is to first test drive some of these hypotheses in a class room setting, 
and take the learnings online to develop an online course module incorporating learning 
from class room as well as from industry best practices and academic research 

Sources 
Sources include the following:  

  Dr. Barbara Oakley, Dr.Terry Sejnowski - Learning How to Learn: Powerful mental tools 
to help you master tough subjects, Coursera https://www.coursera.org/learn/learning-
how-to-learn/home/welcome 

  Dr. Barbara Oakley, Harnessing your Zombies, https://www.coursera.org/learn/learning-

how-to-learn/lecture/YS6FO/harnessing-your-zombies-to-help-you 

CS 6460- Education Technology 

Individual Assignment #3: by Guharaman Janakiraman 

  Duhigg, Charles. The Power of Habit. NY: Random House, 2012. 

http://www.amazon.com/The-Power-Habit-What-Business-ebook/dp/B0055PGUYU 

  Wan, X., H. Nakatani, K. heno, T. Asamizuya, K. Cheng. and K. Tanaka. “The Neural Basis 
of Intuitive Best Next-Move Generation in Board Game Experts.” Science 331, no. 6015 
(Jan 212011): 341.6. http://science.sciencemag.org/content/331/6015/341.full 
(Requires free registration and login) 

  Using Pomodoro technique in class room 

o  http://www.edudemic.com/use-pomodoro-method/ 
o  http://www.slu.edu/blogs/cttl/2014/07/30/using-the-pomodoro-technique-to-

help-you-and-your-students-be-more-productive/ 

  Pomodoro Technique – A student’s point of view 

http://www.slu.edu/blogs/cttl/2014/07/30/using-the-pomodoro-technique-to-help-
you-and-your-students-be-more-productive/ 
Leslie Belknap, How to conquer short attention spans 
http://www.ethos3.com/2015/01/how-to-conquer-short-attention-spans/ 

 

