Assignment 3: Exploring Your Problem 

 

 

 

 

 

 

 

 

 

 

Tianjin Chen 

902646298 

tchen63@gatech.edu 

 

 

Introduction 

In the Intelligent Tutoring System(ITS) world of Education Technology, there are 

too many topics, issues and challenges to talk about. Before we dive into details, let’s 

take a step back and talk about what is ITS. ITS is an effective education tool to finish 

one­to­one tutor work without involving a human. It is computer software designed to 

simulate a human tutor’s behavior and guidance[1]. Since this is built to replace a 

human tutor, ITS should be able to fulfill most responsibilities from a human tutor. For 

example, explain concepts based on student’s understanding, provide feedback to 

student’s answers and evaluate the masteries of taught knowledge from a student, etc. 

Therefore, the focus should be on student. However, this is also the most challenging 

part of the design. 

What I have learned from my experience  

When I was a junior student back at Georgia Tech, I started being a teaching 

assistant for Modern Physics II. My responsibilities include: grading on last week’s 

quizzes, explaining any question about the quiz from students, leading all students in 

one classroom to complete a lab session exercises and answering any questions during 

that time. I am required not to give direct answers to any students. They have to 

complete all tasks as a team. Whenever a student or a group asked me a question, I 

always first try to “detect” if there is any issue about fundamental concepts from the 

question itself. Because there is no point answering a “false” question, a question based 

on a wrong concept. In this case, most of the questions can be solved automatically 

after clearing up the concepts. This is a very fundamental and important skill to check 

student’s masteries of knowledge at a very early stage. However, most of the ITS 

products on the market doesn’t have this feature. Some of them do not even give the 

students a chance to ask questions. What students can do is just to review previous 

lessons again and again. Sometimes they are just one step away. With a human tutor, 

once missing “step” is identified, leading student to “success” is very simple. But with an 

ITS software which only has limited and defined responses, it is very hard to tell what a 

student is misunderstanding. Sometimes, this situation could cause a student frustrated 

or bored during certain topics. Once the learning interest is discouraged, it is hard to 

gain it back in a short time. 

What we can do 

Based on current technology, we are nowhere near to replace a human tutor with 

a software program. However, we do have some systems that is “smart”. One of them is 

called Cognitive Tutor. A Cognitive Tutor is a particular kind of intelligent tutoring system 

that utilizes a cognitive model to provide feedback to students as they are working 

through problems. This feedback will immediately inform students of the correctness, or 

incorrectness, of their actions in the tutor interface; however, cognitive tutors also have 

the ability to provide context­sensitive hints and instruction to guide students towards 

reasonable next steps[2].  

The name of Cognitive Tutor  now usually refers to a particular type of intelligent 

tutoring system produced by Carnegie Learning for high school mathematics based on 

John Anderson's ACT­R theory of human cognition[2]. This system keep track of each 

student’s learning pace and masteries of knowledge to provide proper feedback. One of 

their features is: “Before working on problems, students can review the lesson, read, or 

look up the applicable key terms, and see the skills for that particular section.”[3] This 

aligns with what I mentioned earlier that students need to get the concept right at the 

beginning.  

What I can do 

There is only a small part of my tutoring experience reflected in this paper. My 

tutoring experience can help identify what is missing in the current cognitive tutor 

software. Cognitive Tutor from Carnegie Learning interests me the most and I am 

planning to focus on what they have found out so far to identify what and how I can help 

to contribute to Cognitive Tutor community. At the same time I am learning from 

Knowledge­Based Artificial Intelligence class so I should be able to provide a different 

view to ITS as well. 

 

Reference 

[1] ​7 Things You Should Know About Intelligent Tutoring Systems​, by Ken Koedinger of 
Carnegie Mellon University and Michael Tanner of the APLU 

[2] ​Cognitive tutor (​https://en.wikipedia.org/wiki/Cognitive_tutor​) 
[3] Cognitive Tutor Software 

(​https://www.carnegielearning.com/learning­solutions/software/cognitive­tutor/​) 
 

 

 

 

