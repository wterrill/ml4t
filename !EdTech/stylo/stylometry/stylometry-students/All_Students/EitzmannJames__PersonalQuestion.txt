Personal Question 
James Eitzmann 
 
Question: 
Now that you have some time to learn about LMS, this is your opportunity to share and 
show off what you have learned. You're about to embark on the challenge of evaluating 
some of the top LMS systems on behalf of Georgia Tech. So let's start with the 
fundamentals, why is LMS important?  How can LMS benefit the educators, students, 
and administration? When evaluating LMS, are there any expectations? For example, 
are there minimum features you expect every LMS to provide?  What would you 
consider to be core LMS features, what would you consider to be optional, that is, 
features that would not affect the quality of an LMS system? 
 
An LMS is comprehensive, integrated software that supports the development, delivery, 
assessment, and administration of courses in traditional face­to­face, blended, or online 
learning environments . An LMS is critical for e­Learning since there is no in­person 
interaction or distribution of learning content and resources. Simply put, an LMS is an 
important tool for instructors and students to administer and participate in learning and 
brings together learning features that would otherwise need to be managed using a 
disparate set of tools. Without an LMS or with an ineffective LMS, learning and teaching 
becomes a more arduous process as effort that could be spent learning or teaching is 
instead spent managing tools not specifically designed to mean the needs of learning. 
 
An LMS provides many benefits to several groups. The primary beneficiaries are 
educators and students as they are the heaviest users of the system. Another 
beneficiary of an LMS is the education administration that oversees education at the 
institution.  
 
Educators can take advantage of typical LMS features like document management for 
course materials, grading, course calendars, student rosters, announcements, and 

1

1 
http://er.educause.edu/articles/2014/4/selecting­a­learning­management­system­advice­from­an­academic­p
erspective 

2

discussion. Educators using an LMS are more efficient delivering courses and can 
benefit from adapting and reusing course materials over time . In addition, more 
powerful LMS software can be used by educators to view analytics of student data to 
identify students that may be struggling as well as course material that may need to be 
taught more effectively. 
 
Students benefit from an LMS because of the features provided to facilitate finding 
course materials, reviewing the course calendar, and course collaboration with 
educators and peers. 
 
Institution administrators benefit from an LMS through common student registration and 
reporting, security, and privacy of information. 
 
When an LMS is evaluated for educational use, it is important to note requirements from 
each stakeholder group (i.e., educators, students, administration, and IT). Each group 
has a minimum set of requirements that must be met for the LMS to be successful. 
 
Beyond the core feature set though, an LMS must also be graded on how flexible it is 
for different pedagogical methods of teaching. Alternatively, if an institution has a 
primary pedagogical method, the LMS should be graded on how well it serves that 
method . Another primary expectation of an LMS is that is makes learning and 
educating more efficient. 
 
For educators, the minimum expectations of an LMS are features for managing student 
rosters, managing course resources like documents, providing a simple and easy 
mechanism for editing course information (e.g., a wiki), assignment management and 
scoring, and provide a way to make announcements. 
 
For students, the minimum expectations are that the user interface is intuitive and easy 
to use, that the LMS provide easy access to course information including due dates 

3

2 https://www.mindflash.com/lms 
3 
http://er.educause.edu/articles/2014/4/selecting­a­learning­management­system­advice­from­an­academic­p
erspective 

(calendar functionality), and that the system be reliable when accepting work 
(assignments or tests).  
 
For the educational institution, the LMS must provide security, privacy, and affordability. 
 
For IT, the LMS must be reliable, perform well, and not be a maintenance burden. 
 
Beyond the minimal set of requirements for an LMS, an LMS implementation should 
also be evaluated for how close it comes to an ​optimal​ LMS for the institution. Optimal 
features are those that enhance learning, maximize learning and teaching efficiency, 
and provide methods for learning that would not otherwise be available. 
 
One optimal feature of an LMS is collaboration. An LMS should provide robust 
collaboration capability for instructor/student collaboration as well as peer student 
collaboration. Ideally, that collaboration could be wide ranging and include video 
conferencing, course content annotation, and peer review and feedback. 
 
Another optimal feature of an LMS is accessibility. The LMS should be accessible in 
one form or another across devices including mobile devices. This is especially true of 
collaboration functionality.  
 
Among other LMS features that are not critical to success are what I consider to be 
“nice­to­haves.” A Personal Learning Environment (PLE) is set of tools and resources 
outside of the institution that can be used by the student while learning . LMS 
integration and embrace of PLE is not well defined at this point nor is there any proven 
use case available in my opinion. 
 
Some LMS systems also provide functionality for course sharing and even a 
marketplace for course content. While this may be a way to monetize educational 
assets, it’s not a feature that enhances educating or learning. 

4

4 http://zorgacademie.ou.nl/documents/7088488/7088603/sclater.pdf 

