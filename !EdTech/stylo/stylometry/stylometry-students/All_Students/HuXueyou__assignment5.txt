Assignment 5: Personal Question 

Xueyou Hu 

You've laid out a plan to build an app for student MOOC selection, the 
centerpiece of which will be recommendations for MOOCs relevant to 
particular users.  One problem which plagues recommendation systems 

is variously called the "cold start problem" or "bootstrapping 

problem."  This refers to the difficulty of gleaning enough data to make 
meaningful inferences about item value and user choices in a system 

that the audience might not have much incentive to use without 
effective recommendations.  How do you plan to overcome this 
challenge?  What are popular techniques (from the literature) for 

doing so, and how do they apply to your project? 

 

 

 

 

 

 

 

 

1.  The Problem 
All  beginnings  are  hard.  “Cold  start”  is  a  common  problem  in  computer-based  information 
systems. It is saying that the system cannot draw any inferences for users or items in the very 
beginning because of insufficient information.[1] The “cold start” problem is most prevalent in 
recommendation system. 
Similarly, “bootstrapping” usually refers to a self-starting process.[2] For example, the computer 
loads basic software into memory after power-on. In artificial intelligence and machine learning, 
“bootstrapping”  is  a  technique  used  to  iteratively  improve  a  classifier’s  performance.  This 
required a seed AI; in another word, the system needs a push in the very beginning. 

2.  Literature Review 
Recommendation system or recommender system is a subclass of information filtering system, 
which  seeks  to  predict  the  rating  or  preference  that  a  user  would  give  to  an  item.[3]  
Recommendation systems have become very popular in recent years and applied in a variety of 
items  such  as  movies,  music,  books,  articles,  news,  etc.[3]  The  most  successful  applications 
includes Amazon, Netflix, and Yelp. 
Recommendation  system  can  produce  a  list  of  recommendations  with  two  approaches: 
collaborative filtering or content-based filtering. Collaborative filtering builds a model based on 
the user’s past behaviors (purchase, selection, or ratings) as well as similar decisions made by 
other users; then it can use this model to predict the user’s preference or selection. Content-based 

1 | P a g e  
 

filtering utilizes a series of discrete characteristics of an item in order to recommend additional 
items  with  similar  properties.  These  two  approaches  are  often  combined,  called  Hybrid 
Recommendation Systems.[3]  

2.1 Collaborative Filtering  
This approach needs to collect and analyze a large amount of information on users’ behaviors or 
preferences in order to predict what the users will choose based on their similarity to other users. 
Collaborative filtering supposes that people who agreed in the past will agree in the future. This 
method  is  a  user-model  based,  which  is  heavily  relied  on  the  data  of  users.  One  of  the  most 
famous  examples  of  collaborative  filtering  is  Amazon’s  recommendation  system,  which  can 
recommend  similar  products  to  the  customers  based  on  their  previous  purchases  or  searching 
results.  
One of the problems of collaborative filtering is “cold start”. The system usually requires a large 
amount of users’ data in order to make accurate recommendations. However this approach could 
fail if the system lack of user’s information especially in the very beginning. That is why we call 
it “Cold Start”. 

2.2 Content-Based Filtering 
This approach is based on a description of the item and a profile of the user’s preference. This 
method  recommends  items  that  are  similar  to  those  that  a  user  liked  in  the  past.  In  practice, 
various items are compared with items preciously rated by the user; and the system will choose 
the best-matching items to recommend.    
The system offers the recommendations  based on two data sources: 1) item feature matrix; 2) 
the user’s rating of the item. Basically, this recommendation system is a classification system, 
which classifies the item into two categories (user like or not) or more categories (score 1~5) 
based on the item’s features.  

2.3	Hybrid	Recommendation	Systems	
As we can see that both collaborative filtering and content-based filtering have some problems 
besides  their  advantages.  Hybrid  approach  that  combines  collaborative  filtering  and  content-
based filtering could be more effective in some cases. Besides the two methods we just discussed, 
some other approaches, such as knowledge-based and demographic techniques, have also been 
studied.  
A combination of multiple techniques can be used to solve the “cold start” problem. The system 
can rate the new items automatically according to the already rated similar items. The similarity 
between the items can be determined by content-based filtering. On the other hand, the user’s 
profile can also automatically set up by getting information from other user’ activities such as 
searching records.  

2 | P a g e  
 

The good example of hybrid recommendation system is Netflix. It recommends by comparing 
the watching and searching habit of the users, which belongs to collaborative filtering. It also 
offers movies that share common features with the ones that the user liked before, which uses 
content-based filtering.  

3.  My Solutions To The Cold Start Problem 
The key of the “cold start” problem is that the system lacks of data in the very beginning. As we 
discussed, both collaborative filtering and content-based filtering need user’s data: either user’s 
profile or the user’s rating to the item. The common solution to the cold start problem is to apply 
the hybrid approach of combining collaborative filtering and content-based filtering. Some tricks 
that I can think of include: 

1)  List as many courses as I can, especially those already have been popular. For example, I 
can list the courses from OMSCS program. The good thing is that many of them have 
already been rated and reviewed. These information will definitely give a good start to 
my system. 

2)  The  system  can  assign  a  rating  to  the  new  items  automatically  based  on  the  ratings 
assigned to other similar courses. The similarity between courses is a clustering problem 
and it can be determined based on the courses’ features (content-based characteristics). 
The features could be subject, hardness, ratings, etc. 

3)  As to the user’s profile, the system can ask the user to register an user account. Thus, it 
can set up an basic user profile. Another effective way to collect user preference is to 
store the user’s browsing history. The searching preference is an important source of user 
data, which can help to improve the cold start problem. 

4)  The system can attract users to write reviews. The review is similar to the rating, which is 
the bridge between the user and the course. Many techniques, such ML (machine learning) 
and NLP (nature language processing), can be used here to process the users’ reviews. Of 
course, the user’s review is an effective way to collect both the course data and the user 
profile data. 

In  summary,  “cold  start”  problem  or  “bootstrapping”  problem  are  common  problems  in 
recommendation systems due to lack of user’s profile or user’s rating data in the very beginning 
of the system. The solution to the “cold start” problem is to adopt hybrid approaches. Multiple 
techniques  combined  will  help  to  boot  the  recommendation  system  and  get  through  the  “cold 
start” stage.  
 
References 
1.  Cold Start. https://en.wikipedia.org/wiki/Cold_start 
2.  Bootstrapping. https://en.wikipedia.org/wiki/Bootstrapping#Computing 

3 | P a g e  
 

3.  Recommender system. https://en.wikipedia.org/wiki/Recommender_system 
4.  Andrew I. Schein, Alexandrin Popescul, Lyle H. Ungar, David M. Pennock (2002). 

 

Proceedings of the 25th Annual International ACM SIGIR Conference on Research and 
Development in Information Retrieval (SIGIR 2002). Methods and Metrics for Cold-Start 
Recommendations. New York City, New York: ACM. pp. 253–260. 

5.  Xiam (2007). "Vendor attempts to crack ‘cold start’ problem in content recommendations". 

Mobile Media (PDF) (United Kingdom: Informa Telecoms & Media): 18. 

6.  Beel, J.; Langer, S.; Genzmehr, M.; Gipp, B. (2013). "Research Paper Recommender System 

Evaluation: A Quantitative Literature Survey" (PDF). Proceedings of the Workshop on 
Reproducibility and Replication in Recommender Systems Evaluation (RepSys) at the ACM 
Recommender System Conference (RecSys). 

7.  N. Rubens, (2011). D. Kaplan, M. Sugiyama. Recommender Systems Handbook: Active 
Learning in Recommender Systems (eds. F. Ricci, P.B. Kantor, L. Rokach,B. Shapira). 
Springer. 

 

4 | P a g e  
 

