CS 6460- Education Technology 

Individual Assignment #2: by Guharaman Janakiraman 

Background 
The premise of my research is based on hypotheses that mixing personal productivity 
techniques such as Pomodoro and Agile concepts from methodologies such as SCRUM 
and Kanban along with the existing learning management systems and course authoring 
technologies makes for a more effective course ware.  
 

Diagrammatically, this project would 
allow for leveraging some of the existing 
LMS and Authoring tools, while 
innovating on the way course is structured 
as well as building in concepts to structure 
the course.  
 
Inspiration for this project comes from my 
personal experience of trying out better 
ways of teaching and studying. 
 
Specific areas to draw innovation from are 
listed below 

 

Online Tools and Teaching Systems Available for teaching 
This week’s research was based primarily on Learning Management Systems and 
Authoring Tools, as well as personal productivity and agile tools available.  

Learning Management Systems  
Even though I considered both commercial and open source systems, I have narrowed 
down the scope to primarily free open source systems only. Top tools based on this 
include:  

1.  Moodle: LAMP based Moodle seems to be the 800 pound gorilla in the LMS 

market, which is feature rich and third party plugins. This is also perceived to be 
more complex to learn and setup 

2.  Sakai: Java based tool integrates very well with Google ecosystem of docs, sheets 

and presentation. This is perceived to have a higher cost of ownership 
3.  Google classroom: This is not yet widely available (Restricted only to 

educational institutions). But has fantastic integration with Youtube and other 
Google products 

 
Other products such as Latitude Learning, Canvas, Blackboard, etc. also had their own 
strengths. But the three listed above were in my opinion providing the minimum viable 
features at least learning curve  
 

CS 6460- Education Technology 

Individual Assignment #2: by Guharaman Janakiraman 

Authoring Tools  
When it comes to course authoring tools, there were not many open source options. Top 3 
authoring tools were  

1.  Adobe Captivate and connect - Adobe Captivate stands out as the application with 

the richest features. 

2.  Articulate presenter and engage – These are mostly extensions to PowerPoint. As 

such, they provide a great entry point for PowerPoint users. 

3.  Camtasia  

 
Based on the features, articulate seems to be easy to learn. It also provides online hosting 
of content. Lectora and raptivity also emerged as potential authoring tools to be 
considered, but were priced higher than the ones listed above 

Productivity and Agile Tools  

  Pomodoro is an effective way to stay focused and get things done, by nibbling 

away small bits of work 

o  Given that the concept of Pomodoro is simpler, the tools are also not very 
complex. Tools such as tomato-timer.com provides an implementation of 
it 

  Agile methodologies bring in concept of time box instead of a scope box. It also 

brings in concepts such as having a set cadence and rhythm which allows the 
teams to stay focused and accomplish the focus. Even though there are many 
tools, it is beyond the scope of this exercise 

 

Conclusion  
The idea is to combine some of these available tools to prove or disprove the hypotheses 
that bringing in personal productivity techniques and agile methodologies into classroom 
will increase the teaching effectiveness  

