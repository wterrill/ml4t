Matthew Cohen

Ed Tech

Milestone 2

Milestone 2: Text Transcript Tool
Progress Report and Functional Demo

Project
My original project concept was to make a Chrome extension for use in the OMSCS that 
grabs the text transcript for each video and builds it into the page, creating an extra way 
of taking in the material and searching it, while also creating an alternative controller 
that allows navigating the video by text, instead of just guessing with the progress bar. 
After building the ﬁrst demo web page for Milestone 1 and seeing some other course 
content that had been created with YouTube, as well as realizing that there were some 
obstacles to accessing the Udacity YouTube player in the ways that I needed, I came up 
with an alternate plan possibility to create a presentation of a course that utilizes my 
Text Transcript Tool, using only its YouTube playlist ID. This would only require that the 
videos already have subtitles in at least one ofﬁcial language (auto-created doesn’t 
count as a language to YouTube, even though it’s in English), or that the creators have 
made at least some minimal effort at ﬁxing the transcript which gets automatically 
created by YouTube. The truth is that YouTube already has a great system for working 
with the transcripts which even has some community functionality built in, so there’s 
really nothing stopping anyone from ﬁxing the transcripts, at least when they plan to use 
them for educational purposes.
Main Functionality
As a controller for the video, clicking the text in the transcript brings you to that exact 
spot in the video lecture. In addition, the text highlights as it is spoken in the video, 
keeping track of where you are in the transcript and video, as well as “remembering” 
lines of text that you jumped from, leaving the highlighting on them, so that you can go 
back to where you just came from if wanted. You can jump around in the video by 
clicking the text, but you can also use the normal YouTube controls, including the 
progress bar, and the highlighting will continue. These highlights can be cleared if 
wanted with the “Clear Highlights” button. Separately, you can ﬁnd text to highlight in the 
transcript, which updates as you type, and can be cleared with the accompanying 
“Clear” button. As the video plays, the autoscrolling will highlight and scroll to the 
spoken text, but the auto scrolling is overridden by hovering over the transcript area, so 
that you’re never ﬁghting against the auto scrolling as you try to look through the video 
text. The auto highlighting, however, will continue while the video is playing. There are 
additional Play and Pause buttons for ease of use, as well as a button to hide and 
unhide the entire transcript.
For the purposes of this milestone, I have created a functional demo that allows you to 
load new videos based on their YouTube IDs. You can use any YouTube video, as long 
as it can be embedded, but for the transcript to work, the creators must have published 
the video in English. This really only requires going in to the auto transcription, changing 
1 letter and publishing it as English, but of course it’s best if creators ﬁx the transcript 

Matthew Cohen

Ed Tech

Milestone 2

entirely. I could easily extend this to other languages, but for now I’m using only english. 
If there is no transcript available, the transcript section never gets built, but the video will 
still load and play. In my Functional Demo section, I have a list of video IDs that all have 
transcripts that work, so try those. I suggest looking at some from the “Other Videos with 
Transcripts” section for longer examples, and speciﬁcally at the “CS50 Mark Zuckerberg 
Lecture”, for a long example with characters used in html that are within the transcript 
text.
Progress
I now have a fully functional demo of a single video page which implements the 
functionality I have described. I made my own responsive system targeted at desktop, 
allowing you to hopefully see the controls on all desktop screens, as well as shrinking 
up in a way which allows you to use it along side other things, maximizing space as the 
window width gets smaller. I need to change the media queries a little to target phones 
and tablets I guess, but it works very well in a shrunken width desktop screen. I’ve 
tested that it’s working with current versions of Chrome, Firefox, and Safari. 
Functional Demo
The demo starts off with the ﬁrst Ed Tech video, dynamically pulling the transcript from 
xml data and parsing it in order to build the transcript on the page. There are external 
play and pause buttons on the bottom for ease of use. You can look through the text in 
the transcript and click any piece of text to go directly to that spot in the video. In this 
way the text acts as an external controller for the video. 
The text in the transcript highlights as it is spoken in the video, also leaving highlighting 
on text that you’ve jumped from, so that you know where to go back to, if desired. You 
can clear this with the Clear Highlights button.
Separately, you can ﬁnd text by entering it into the Highlight Text box. This will highlight 
the matching text in the transcript, is updated as you type, and can be cleared with the 
Clear button. It doesn’t scroll to the found text because that would ﬁght the autoscrolling 
of the other highlighting system, but I’m considering whether it should have a button to 
accomplish this.
You can load other videos by entering the YouTube video ID in the input box on top of 
the screen and pressing the “Load Video Data” button or hitting Enter. In theory any 
video with a text transcript should work, but in practice certain videos have transcripts 
that don’t show up either because they lack a deﬁned language version or because they 
are formed slightly oddly. All of the OMSCS course videos that I tested worked, so it was 
really other YouTube videos with auto-transcription that suffered from this problem. I’ve 
included a list of working YouTube video IDs below the demo link for convenience, but 
feel free to try other videos that have the CC logo, indicating that they have a text 
transcript.

Matthew Cohen

Ed Tech

Milestone 2

Functional Demo: http://secretsong.net/text-transcript-tool/

Video IDs with Working Transcripts
wnCSVssDPv8
mkYEVWZ6tYI
vWNnOnzpxbo
6IVOnxiYhYs
50a0Ai2RXgU
M9Bvrtknm_4
SNJj8B0mGhU
KCcc6oqx6qk
H0d2OHt65OU
CzFXhfRU9L8
VK5b5ZQEI7M
Other Video IDs with Transcripts
7SWvDHvWXok
7H3ksmxwpWc
UPA3bwVVzGI
dk60sYrU2RU
BdHK_r9RXTc
xFFs9UgOAlE

Description
Readiness Questions
An Experiment in Educational Technology
Learning Goals
Learning Outcomes
Learning Assessments
What is Educational Technology?
The Class Project
The Structure of The Class
Mentorship
The Course Library
Let’s Get Started
Description
Unanswered Questions
Is Time Travel Possible
How Big is Inﬁnity
Experiments in Self Teaching
Reggie Watts
CS50 Mark Zuckerberg Lecture

Questions for Peers
Does anyone have a regular computer screen that the demo didn’t ﬁt on?

I tried to make it responsive for desktop, so that the controls would always ﬁt on the screen 
without needing to scroll down. The autoscroll of the transcript will pop the page back up if it 
doesn’t ﬁt and you scroll down the page.
Any obvious bugs that I missed?

It currently won’t work in Internet Explorer, so I already know about that one.

