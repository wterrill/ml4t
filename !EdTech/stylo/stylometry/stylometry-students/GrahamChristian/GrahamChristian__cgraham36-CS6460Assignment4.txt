Chris Graham 
cgraham36 
Assignment 4 

Programming Assignment Assessment in MOOCs 
 
In the previous assignments I explored how massive open online course (MOOC) systems scale 
assessment systems to large numbers of students, and what recent research has been done to 
improve assessment of programming assignments in particular. For the most part, MOOCs use 
a fully automated system to grade programming assignments based on the output of the code 
when run against pre­determined test cases. While this method has many benefits, such as 
immediate feedback to the student in the case of errors, automatic grading has faced ​criticism 
because it cannot detect code that produces correct output using an incorrect method, or code 
that does not adhere to proper coding standards. Traditionally in smaller in­person courses, 
these factors have been assessed manually by expert instructors visually inspecting the code 
(Ala­Mutka 2005), which is not feasible at the MOOC scale. 
 
Another important aspect of existing MOOC grading systems is that while the automated grader 
can return feedback on what tests failed with detailed error messages, novice students often 
have trouble understanding what the errors mean and how to fix them. In practice students often 
rely on help from other students via the course discussion forum. Much of the research into 
improving the existing systems for online programming assignments has been directed at 
automatically providing help to the student troubleshoot problems, such as suggesting ways to 
correct coding errors or giving hints about how the problem can be solved (Singh et al. 2013). 
 

Adding Peer‐Based Systems 
 
Many of the issues mentioned above result from the lack of human instructors to perform tasks 
that are difficult to fully automate, such as visually inspecting code and providing help to 
struggling students. In similar situations, such as assessment for ​written work​, MOOCs have 
leveraged the large student population to perform these sorts of tasks. Until fully automated 
systems catch up with human ability, peer­based methods could be used to bridge the gap. 
 
While any given student may not be confident in grading another student’s work, in aggregate 
peer grading systems tend to reach a relatively accurate consensus (Freeman & Parks 2010). 
Peer review of programming projects has been successfully used with in­person courses (Reily 
et al. 2009), so the same general idea should also find success in the MOOC format. After a 
student completes a given assignment and the code passes all automated functional tests, the 
code can be assigned to a random set of other students for review. Especially for larger projects 
this could be done in an iterative manner, where feedback from one part of the project is used to 
revise the code used in future parts of the project. Students who do not fully understand the 
problem could learn both from reading feedback given by other students about their own code, 
and from reading and understanding code written by the students whose work they are 
assigned to review. Since code review is a standard task in software development, existing 

Chris Graham 
cgraham36 
Assignment 4 

ideas and tools for facilitating the process can be used, such as in­line code annotation used in 
commercial tools like ​Gerrit​. 
 
Peer­based help for troubleshooting errors while working on programming assignments is 
already used in the form of course discussion forums, but this process is disorganized and 
unstructured. Students might not be able to find existing discussions about common errors, and 
the suggestions in any given thread might be incorrect or unhelpful. The discussion forums 
themselves tend to be ​unpopular and underused​. A better system would integrate this kind of 
activity into the course software itself. Instead of simply displaying an error message when an 
incorrect assignment is submitted, the system could display an official discussion thread specific 
to that error type. While error messages are not always exactly the same for different students 
who choose different names for variables and structure code differently, machine learning 
techniques could be used to identify likely duplicate errors or assign a general error categories 
such as “environment setup issue” or “syntax error”. Existing sites that deal with programming 
questions such as ​Stack Overflow​ could serve as a model for organizing the discussions in such 
a way that a consensus best answer emerges and is displayed prominently. 
 
For my project I plan to implement a prototype for an online grading system that combines the 
existing automatic grading for code correctness with the new peer­focused ideas discussed 
above. For the most part this project involves applying technologies and methods that are 
already successful in other domains, so the idea itself should be feasible. With that said given 
the time constraints of the course most of the duration of the project will most likely involve 
implementing the actual prototype system itself without much extra time to study the relative 
effectiveness of the system compared to existing solutions. Overall while these new peer­based 
methods may not fully match the quality of human instructors or tutors, they could represent a 
step above the current systems. 
 

References 
 
(2012). Automatic Grading of Code Submissions in MOOCs isn't ... Retrieved February 1, 2016, from 
http://gregorulm.com/automatic­grading­of­code­submissions­in­moocs­isnt­perfect­either/​. 
 
Ala­Mutka, K. M. (2005). A survey of automated assessment approaches for programming assignments. 
Computer science education​, ​15​(2), 83­102. 
 
Singh, R., Gulwani, S., & Solar­Lezama, A. (2013). Automated feedback generation for introductory 
programming assignments. ​ACM SIGPLAN Notices​. ACM. 
 
(2013). An instructor's thoughts on peer­review for data analysis in ... Retrieved February 1, 2016, from 
http://simplystatistics.org/2013/03/26/an­instructors­thoughts­on­peer­review­for­data­analysis­in­coursera/​. 
 
Freeman, S., & Parks, J. W. (2010). How accurate is peer grading?. ​CBE­Life Sciences Education​, ​9​(4), 
482­488. 

Chris Graham 
cgraham36 
Assignment 4 

 
Reily, K., Finnerty, P. L., & Terveen, L. (2009). Two peers are better than one: aggregating peer reviews for 
computing assignments is surprisingly accurate. ​Proceedings of the ACM 2009 international conference on 
Supporting group work​. ACM. 
 
(2013). MOOC Discussion Forums: barrier to engagement? ­e­Literate. Retrieved February 2, 2016, from 
http://mfeldstein.com/mooc­discussion­forums­barriers­engagement/​. 

