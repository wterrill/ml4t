Intermediate Milestone 1 

Due Date: March 7, 2016 

 

 

Exploring and Integrating IoT, AR & VR Technologies to  

Enhance Online Education 

By 

Kumar H. Jagtiani 

Kjagtiani6@gatech.edu 

Georgia Institute of Technology 

 

 

1.  Gather detailed knowledge about best in class technology for IoT, AR and VR: 

 

Internet of Things (IoT): 

The Convergence of Workplace services as presented to us by Cisco (Figure 1) is giving 

life to objects in our existing workspace.  Internet of Things tied to the IP backbone and 

integrated with Wi-Fi networks are enabling static objects to create controlled 

environments for virtual travelers to work, school or play.  These sensor aided objects 

are powerful enablers to bridge IP Telephony calls, turn on Cameras for Video 

Conferencing, facilitate appropriate lighting and temperature for studying, and even 

allocate appropriate bandwidth on your network to provide clear packet transmission 

for standard IP calls, conferences or deep Augmented or Virtual Reality.  The 

autonomous setup of the basic elements can provide a tremendous improvement in the 

overall classroom experience.  This pre-mounting triggers will alter the way we 

experience reality and provide an infrastructure that’s ready for the next generation of 

communication.  Taking you to virtual classrooms smoothly and without a glitch, 

keeping the network and wi-fi signals clear of traffic, and data throughput dedicated at 

max capacity.  IoT will be the gateway to a successful overall classroom experience that 

allows you to open the doors to your next out of this world experience of integrating 

Augmented Reality. IoT is here and now, sensors are enabling lighting, and Li-Fi, 

telecoms are increasing capacity, clouds are hosting virtual services with fantastic visual 

effects for our Virtual Reality classrooms, and data lakes are holding our data, studying 

and improving our experiences through science.  Here’s where all our Machine Learning, 

Artificial Intelligence and Informatics collide, bringing together an infrastructure of 

integrated knowledge and virtual experiences. This “Digital Ceiling” will create an 

adaptive environment based on personal preferences.[1] 

 

Figure 1. Cisco: The Expanding World of Smart Lighting 
 

 

Figure 1. Cisco: The Digital Ceiling 
 
 
Augmented Reality (AR): 

 

As IoT gains in popularity, so does the need to control and integrate these objects. Tools 

like RealityEditor, Microsoft Hololens and others are doing just that.  Reality Editor 

makes it easy to “blend the digital world into the physical or real world where no longer 

will our smartphones be a buffer between the two.  Reality Editor (Figure 2a) isn’t a 

solution so much as it is a tool to get us to the point where we can naturally and 

seamlessly interact with both the digital and physical worlds around us”[2].   Although 

tools like Reality Editor or Hololens look futuristic, they really work and are visions of 

our future.   These technologies use a grid like fingerprint to identify and overlay 

objects, pull up their corresponding webpage representing its functionality or tap into 

holographic share drives to present and collaborate capabilities for you interact in 

intersecting multi-dimensional plains.  These next generation programmable worlds 

present Software like Fluid Interface for Reality Editor and Unity and Hololens Emulator 

for Microsoft Hololens (Figure 2b).  It provide the fundamentals that provision 

applications to augment reality and enhance our User Experience for learning, 

understanding, decision making and collaboration [3] to “transform the ways we 

communicate, create, collaborate and explore” life in the multi-dimensional plain as 

stated by Microsoft. 

With the advent of imbedded sensors, wearable technology and advanced software 

architecture, augmented reality will open the doors to  Enhanced Communication and 

Computer-Human Engagement that leverages our senses towards a new media 

experience [4].  This enhanced experience integrated with our digital ceiling will engage 

our adaptive environment for classroom engagement.   

Let’s walk through a life experience scenario.  As evening falls and I return from work 

wearing my smartwatch, the Augmented Reality (AR) Editor app engages with the IoT 

objects in my Study room.  It informs the objects interfacing with my digital ceiling that I 

have an hour long Office Hour session at 7pm.  Tonight it’s an Office Hour session with 

the Professor.  This scenario could be one with a TA, a Q&A Session with a special 

interest guest, a Live Group Discussion, a Mentor Discussion, a Team meeting, or simply 

a commitment to participate in a Piazza session.  The sensors in the room would have 

received my vital data and would have started to prepare the room for heading to class.  

The lights would have turned to an engaging Study mood, the temperature would have 

adjusted to a perfect 71, the Wi-Fi cleared of all traffic, coffee pot brewing a fresh cup, 

and my laptop turned on and awaiting my authentication.  Once fingerprinted I would 

put on my Google Cardboard Virtual Reality (VR) glasses and step into the Classroom.   

Finally reiterating what Simmons and Hwa mention:  Augmented Reality has the 

potential to make students more engaged and motivated in discovery, learning and 

applying their studies to the real environment in new and innovative ways that have 

never been developed before [5]. Also See Table 1 below as explained by Simmons and 

Hwa: 

Table 1. Advantages of AR use in Education [5] 

 

 

 

 

Figure 2a. Reality Editor Interface to either Automatically or Manually engage classroom session 

 

 

 

Figure 2b. Microsoft Hololens potential in collaborating and teaching innovative and 
motivated students. 
 
AR Holographic images in real worlds will supplement VR classroom experience in virtual 

 

worlds.  A holistic approach in collaboration and teaching will consume and immerse 

innovative and motivated students and show them ways to create, interact and share in 

virtual spaces their research and learnings in team space or presentations. 

 

 

Virtual Reality (VR): 

We are on the verge of a VR explosion with many vendors touting their technologies. 

Google, Facebook (Oculus) [6], Apple Microsoft, GoPro, Sony, Samsung are the top 

players with many others knocking on the bleeding edge door or primed for acquisition. 

Telecomm Carriers like AT&T, Verizon and others have started expanding their networks 

to support Virtual Reality caliber gigabit throughput.    It is also predicted by many Wall 

Street analysts that VR will soon become the next mega technology trend.  This will alter 

both the hardware and software industry as they engage in creating virtual experiences 

in all areas of reality.  Sales for this industry is expected to reach nearly $60 billion in 10 

years.   

 

I’m excited to say I am now fully engaged in Virtual Reality.  I have a sturdier version of 

Google Cardboard (Figure 4a) made by Starlight (Figure 4b), compatible in every way 

with its software and experience.  It is better since it has a stronger frame and has a 

locking mechanism to hold any smartphone.  Google Cardboard has the potential to 

immerse you into an alternate reality as you engage your odyssey using it’s integrated 

apps.  It is a complete VR platform that uses low-cost viewers, with the reference design 

made of foldable cardboard or you can buy more durable, compatible versions made 

from plastic, or aluminum, or EVA foam.  Although it’s made of cheap material it 

works great. It’s fully immersive & engaging, lightweight & well-designed. It’s main 

purpose is to hold the phone in front of the lens at the right distance, and provide a 

button to interact with the screen.  There are several apps at the moment that provide 

the content.  Couple that I’ve tested are the Google Cardboard app that contains simple, 

exploratory app to others that are tied to Google Play showing Sir. Paul McCartney in 

concert.  Other apps I’ve tried include INVR, VRSE and Google Street View.  The one I 

found to be the best and one that gave me the best experience and extended thought 

process was VRSE. This app included various immersive situations that allowed me to 

imagine it being a Classroom situation.  “SNL 40” is an immersive live recording of 

Saturday Night Live, hosted by comedian Jerry Seinfeld (Figure 3).  The app places you in 

the audience of the show surrounded by many people, very similar to that of being in a 

lecture hall surrounded by other students.  Seinfeld calls on people in the audience, like 

Michael Douglas, John Goodman, & John Franco.  As they speak, in the VR 360 

Experience the camera pans and within this virtual environment it makes you turn 

around and look at them and attentively listen to their comments and that of Seinfeld 

on stage, which could best befit a situation where a set of students would intelligently 

interact with the professor in a lecture hall.   

 

As part of the investigative reporting, studies and surveys done by many I’d like to end 

by saying IoT, AR and VR present many opportunities in the coming years.    

See the following Youtube Video and imagine yourself in a 360 screen if you do not have 

Google Cardboard:  https://www.youtube.com/watch?v=6HS9h4xFRww 

Figure 3. Audience Q&A (360°) - SNL 40th Anniversary Special…  
 

 

       

Figure 4a.  Starlight VR - VR Headset for smartphones 

 

Figure 4b.  Starlight VR - VR Headset for smartphones 

 

 

 

Below is Gartner’s Hype Cycle for Emerging Technologies including AR and VR ready to 

Enlighten audiences in every industry, including Education: 

 

The Stage is Set: 

Now that I’ve defined the piece parts that would help formulate the experience, from 

Cisco and AT&T laying down the network and infrastructure technologies to integrate 

objects in your environment online, to Reality Editor and Microsoft Augmenting it and 

finally Google, Facebook and others taking it to the next level in 360 Virtual Reality.  

Let’s now look at what the various VR classrooms would look like or should be designed 

like and what technology should be leveraged to rapidly prototype a VR Classroom 

experience using Google Cardboard.   

What you need: 

  Google Cardboard Glasses  

  Google Experiences 

  Creating the VR Classroom Experience 

  VR Classroom API for Application Development toolkit and more 

  VR Experience Hardware Technology Design Lab & 360 Video mount cameras 

VR Classroom Design: 

  Lecture Hall – best reflected in the SNL 40 VR … here’s the same video w/o VR:  

  Professor & TA Office Hour Meetings – imagine this being an Office Hour the way it’s 

presented in first couple minutes in studio with the Professor or TA talking with slides 

and videos reflecting outdoor events. 

  Piazza Group Discussion Room – a way for you to imagine this link is if members 

with similar mindset & thought process were to get together and share their 

ideas, in this case music, from around the world where  Time, place, 

personalities have no barriers and sharing is learning from each other 

  Team Meeting Room – although not truly a team meeting, however if you imagine 

each singer presented in this video experience being your team member presenting and 

sharing their project material in 360 VR world 

 

 

Attempts in VR Education: 

 

iSchool 

 

iED – Immersive Education 

 

 

References 

March 7 

Intermediate Milestone 1  Gather detailed knowledge about best in class 

technology for IoT, AR and VR 
Turn in a Paper describing in detail what a Student will 
experience as he sets up to go to Class.  

 

 

 

1.  “The Expanding World of Smart Lighting”  by Tony Shakib VP, IoT Vertical Solutions 

Group Cisco, February 25, 2015 

2.  “L’evolved from Fluid Interfaces.” March 1, 2016, 

http://fluid.media.mit.edu/projects/levolved  

3.  “Publictions from Fluid Interfaces.” Web. http://fluid.media.mit.edu/publications 
4.  Barfield, Woodrow. Fundamentals of Wearable Computers and Augmented Reality, 

Second Edition, Boca Raton: CRC, 2016.  

5.  Choi, Dong Hwa, Amber Dailey-Hebert, and Judi Simmons Estes.Emerging Tools and 

Applications of Virtual Reality in Education: IGI Global, 2016. 

6.  “inside Mark Zuckerberg’s Big Bet That Facebook Can Make VR Social. “Wired.com. 

Conde Nast Digital, Web. 05. Mar. 2016. 

