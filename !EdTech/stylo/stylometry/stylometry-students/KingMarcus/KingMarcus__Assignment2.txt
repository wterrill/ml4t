Marcus King (mking60) 
CS­6460 Spring 2016 
Assignment 1 
 
 

I’ve been discussing the layout and goals for the Educational Technology course to my close 
family and friends, many of whom are educators themselves, to get some ideas of where they 
would see benefit.  Of course there were many potential areas of improvement that were 
brought about but the one that stood out was that of tailored learning for each student. There 
have been studies that have found students who receive one­on­one instruction perform two 
standard deviations better than students who receive traditional classroom instruction [1]. Many 
school districts are seeing budget cutbacks that are leading to increases in average classroom 
size.  As such many students are no longer receive the individualized attention that is necessary 
for understanding and excelling at the material presented.  This is even true for higher education 
as likely demonstrated by our own OMSCS program.  The fact that the average class size for 
online courses is so large, it impedes the professor’s ability even more to give students direct 
individual attention.  This compounded with students being remote from tutoring resources that 
universities have traditionally offered makes for, perhaps, a less than ideal learning scenario. As 
such I’m beginning to see an increasingly valuable use for ‘Intelligent Tutor’ systems.   
 
An intelligent tutoring system is computer software designed to simulate a human tutor’s 
behavior and guidance. It can assist students studying a variety of subjects by posing questions, 
parsing responses, and offering customized instruction and feedback [2].  There are a myriad of 
benefits realizable from these types of systems.  Firstly they allow for scalable individualized 
instruction that will no doubt increase students mastery of materials.  Second intelligent tutors 
would remove time restrictions by allowing the students to receive instruction at any time. Third 
intelligent tutors are cheap to scale across many students.  This allows for economically 
disadvantaged students to benefit from individualized learning and allows for better efficiency of 
funds by educational institutions. Fourth and perhaps most powerful, interconnected tutoring 
systems can share feedback with each other and can learn and improve themselves by finding 
what tutoring techniques are effective and which aren’t.  Over time the tutoring systems, through 
refinement, can develop empirically proven techniques that can translate into the physical world 
to help teachers and professors improve how they teach students. 
 
Obviously there are tremendous benefits possible.  Unfortunately, the general understanding is 
that these systems require a substantial investment to build and test and as such there aren’t 
too many mature practical offerings in this market.  Most are university research projects and I 
have yet to discover any commercial version.  Luckily there is a significant interest in the 
technology and there are several valuable projects in place to study and learn from.  One 
particular publication that I’ve already received valuable information from is “Authoring Intelligent 
Tutoring Systems”.  When describing general techniques and type of intelligent tutoring system 
it says about expert systems: 

Students using these systems usually solve problems and associated sub­problems 
within a goal space, and receive feedback when their behavior diverges from that of the 
expert model. Unlike most other systems described above, these systems have a 
relatively deep model of expertise, and thus the student, when stuck, can ask the tutor to 
perform the next step, or to complete the solution to the entire problem [3]. 

 
For me this seems like a good starting point for focusing on researching existing systems as 
having a deep set of rules for one particular domain would be the easiest to specialize in to 
become effective.  I hope to do further research on some of the publications provided by Dr. 
Joyner on this subject and further more hope to find source code of existing systems to help 
with concrete strategies on building these learning systems as my end goal is to have a product 
that can actually be used. 
 
 
References: 
[1] ​http://www.stottlerhenke.com/papers/astd_learning_circuits_2000_its_what_how.pdf 
[2] ​https://net.educause.edu/ir/library/pdf/ELI7098.pdf  
[3] ​https://telearn.archives­ouvertes.fr/hal­00197339/document  

