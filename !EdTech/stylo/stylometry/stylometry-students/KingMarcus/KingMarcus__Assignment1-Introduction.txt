Marcus King (mking60) 
CS­6460 Spring 2016 
Assignment 1 
 
 

Hello!  My name is Marcus King. I’m a software engineer by trade and by passion and have 
been working professionally for just shy of 10 years. I’m lucky enough to get paid for work I 
would happily do in my free time as well as a field that is becoming increasingly important for 
the future of our society and humanity as a whole.  I’ve worked in a wide variety of fields from 
Fortune 50 companies, to a self employed consultant, to an early stage startup.  In addition my 
roles in my professional experience have varied just as wildly. I enrolled in the OMSCS program 
for a few reasons.  The first of which is that I thoroughly wanted to learn and edify my informal 
computer science education as my undergrad was in business administration. The second of 
which was that I wanted to attend a top tier CS school, but didn’t want to pick up and relocate 
my family and start a new job.  As a result I had to look at a online offerings and the offerings 
were less than impressive until I found the OMSCS program. Plus it was reasonably priced! 
What more could I want? 
 
As I contemplate what I hope to achieve with this course specifically I reflect on the idea that 
throughout time as increases in the capabilities of technologies have come about there have 
been bold statements about how technology could never play an effective replacement or even 
role in certain fields.  One of them without a doubt has been education.  Obviously, as we stand 
today, technology is becoming an integral part of how we educate ourselves and our children. 
My experience with educational technology has been present since the mid 1990’s with early 
keyboarding classes, that taught basic computer skills but also had software for helping to teach 
math and spelling skills.  In retrospect I view that level of educational technology small in scale 
to what I’m experiencing with the OMSCS online program.  I’ve never developed, built, or 
participated in the creation or advancement of any educational technology.  So if I’m completely 
transparent I’m a bit lost in how to orient my thinking and applying my technical skills in an area 
I have no experience in.  It’s scary, I want to do well in this class.  I’m hoping that the 
participation in the class forums and reading the curated materials will help to spark the ideation 
of a project.  My initial thoughts would be something related to early childhood development with 
respect to language. 
 
I look forward to pushing myself outside of my comfort zone and seeing how I can use my 
skillset to be innovative in a field I have no experience in.  More so I hope to produce something 
that is useful to the public at large. 

