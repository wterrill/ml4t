Charles Nelson Hearn 

Personal Question 

What have you been able to find about the impact on test scores of different types of test settings? How 
well do students normally perform in a proctored situation compared to one where they're only bound by 

an honor code to not cheat? 

 

 
The future of education lies within technology. There have already been great strides into the 
realm of using technology for the advancement and production of high quality learning, teaching, and 
testing. Many major universities around the world have begun using online tools to allow for things like 
distance learning or resource availability in order to allow their students and teachers more freedom 
and access to a higher level education. Along with these advances, there have been issues that arise that 
hinder the growth or adoption of online learning into classrooms across America and the world. Mainly, 
there are worries of academic honesty and the validity of student test taking. 

Traditionally, students of the youngest age are proctored while taking a test in the United 

 
States. A teacher is normally present in the room during the test and is able to monitor the student’s 
actions in order to ensure an honest test environment. Americans are accustomed to this form of test 
taking and that process continues into higher levels of education. With the advent of online classes for 
higher learning, or even job interview applications, there is a worry that cheating on assignments and 
tests will be rampant and ruin the integrity of degrees from reputable institutions. 

 
There have been few studies regarding academic honesty in classrooms both online and 
physical. One such study, by Yasmine Kalkstein1, discusses the results of students participating in 
proctored and unproctored assignments. Her findings showed that: 

  A greater proportion of unproctored students reported that it would have been easy to cheat 
  Both bodies of students reported having cheated in another class at the university 
  A negligible higher proportion of students in the unproctored class reported that they felt 

responsible to report cheating 

  Both bodies of students felt equally nervous while taking a quiz or exam 
  Students being proctored felt less trusted by the instructor than unproctored students 

With these results, along with further reading of Kalkstein’s study, students that were subjected to test 
taking under an “honor code” felt that it would be easy to cheat but actually cheated at the same ratio 
as proctored students. Also, not being proctored gave the students a sense of trust from the instructor, 
which can lead to further understanding and willingness to learn in future courses/lectures. It is also 
worth noting that both student bodies felt nervous while taking an exam. This would seem to indicate, 
although the sample size is small, that being proctored or not has less effect on the anxiety of a student 
than the actual test. 

Along with small sample sizes, two other studies found similar results at two different 

 
universities regarding two different programs of study. One such study2 discussed that student groups 
that are proctored are more likely to learn more and be more engaged in the class than a unproctored 
group. While the two groups performed comparatively the same on the assessments, and pre-test 
(taken before the course) and post-test (taken after the course) showed that proctored students could 

more easily recall medical terminology from earlier modules than unproctored students. This study 
points out the four key areas that differ between proctored and unproctored students: 

“1. Supervision: In proctored testing, the instructor provides supervision of student activities that may 
influence the integrity of the examination process and reduces chances that students might collaborate 
without consent.  
2. Structure: The existence of a “time and place” where an exam will occur is consistent with the 
experience of most students at this time. This provides a degree of “face validity” and reinforcement of 
examination process.  
3. Environment: Traditional classroom examinations occur at a specific time and location; and provide a 
synchronicity with other students that may impose a degree of peer pressure or competition to perform 
that isn’t present for on-line test taking.   
4. Intra-Test Feedback: The presence of the instructor along side the student provides for technical 
troubleshooting, should problems occur; as well as allow for clarification of questions the student might 
have about the assessment instrument.” (pp. 36-37) 

These findings are helpful in allowing future technological advances in education to address these 
differences and either use them to more easily replicate a classroom setting or advance past the norms 
of standard classrooms to a level of education that exceeds that of past experiences.  

These four key areas lend to the differences that many studies have found in their results. 
Similar results were found in a study about high-stakes tests3. This study shows that students in an 
unproctored environment are more prone to academic dishonesty if the assignment is worth a higher 
amount towards the final grade. For instance, the study found that taking low-risk, low-stakes quizzes 
resulted in a non-significant difference in performance. However, when high-risk, high-stakes tests and 
exams were taken, the unproctored students tended to have a non-negligible surge in performance 
above that of the proctored students.  

Solving these discrepancies is no easy task. Oskar R. Harmon, et. al.4, have found that online 

programs had less problems with academic honesty if the class used a hybrid model. For instance, 
multiple test/quizzes of the same risk and weight would be split between proctored and unproctored 
versions evenly. The students would then have to be proctored for some of these tests and be under an 
honor code for the others. Whether the drop in cheating was simply due to the fact of being proctored 
is yet to be determined, but causation is implied. 

Going forward, the future of online classes and assessments is reliant on its ability to mimic the 

integrity of a traditional school model. Being able to know that students are participating in an honest 
manner while still remaining engaged and performing well on assignments is key to a successful 
program/reputable institution. This kind of result helps the student retain any key learned material and 
apply it in future classes or even future employment. While the majority of students in both types of 
environments reported that they do not cheat, the environments of test taking are fundamentally 
different, and the results of these studies show that the medium by which students are taking 
assessments and doing assignments is relevant in how the class is performing and retaining knowledge. 

 

 

 

References 

1 Kalkstein, Yasmine. “Comparison of Honor Code and Non-Honor Code Classrooms at a Non-Honor 

Code University”, Journal of College and Character 01/2008; 9(3). DOI: 10.2202/1940-1639.1115. Jan 

2008 

2 Wellman, Gregory. “Comparing Learning Style to Performance in On-Line Teaching: Impact of 

Proctored v. Un-Proctored Testing”, Journal of Interactive Online Learning, Volume 4, Number 1 

Summer 2005. 

3 Carstairs, Jane; Myors, Brett. “Internet testing: A natural experiment reveals test score inflation on a 
high-stakes, unproctored cognitive test”, Computers in Human Behavior, Volume 25, Issue 3, pp. 738-
742, May 2009. 

4 Harmon, Oskar, et. al. “Assessment Design and Cheating Risk in Online Instruction”, Online Journal of 
Distance Learning Administration, Volume XIII, Number III, Fall 2010. 

 

