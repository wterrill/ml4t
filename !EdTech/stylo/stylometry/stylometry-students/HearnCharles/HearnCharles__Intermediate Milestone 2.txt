Charles Hearn 

Intermediate Milestone #2 

BACKGROUND: 

I am currently doing a study into the pros and cons of online proctoring tools. I am studying the effects 
that the tools have on the students as well as the general consensus on these tools. For my first 
milestone, I created a survey (which you should totally take if you haven’t: 
http://goo.gl/forms/4tLelOJ8lj ) that tries to gather as much information as I can about the OMSCS 
student body’s perception of online proctoring. 

INTERMEDIATE MILESTONE 2: 

For this milestone, I recorded myself taking the survey, documenting my reasonings behind the 
questions and how I believe they are helpful to my research. This is a lengthy video of about 25 minutes, 
and I try to go in as much detail as possible on my purpose behind the survey and the questions within 
it. You can find the video here: https://t-square.gatech.edu/access/content/group/gtc-05a5-4f8e-5d77-
a312-ae8ea404668b/Student-Shared%20Files/Milestone_2_Hearn.mp4 (You may have to copy this link 
and paste it in your browser). 

FUTURE WORK: 

From here out, I will close the survey in about a week and begin compiling the information that I’ve 
gathered. My next milestone will be a PowerPivot of the data showing some of the major correlations 
that help/hinder my research goals. For the final portion of the project, I will be writing a formal open 
proposal to online proctoring companies on how to make their products better for future students and 
educators. 

 

