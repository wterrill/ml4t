Personal Statement:

CS 6460

Name: Jake Akers

My name is Jake Akers but people usually call me "Jakers." 
This is my first semester in the OMSCS program. I am taking CS 6460 
and CS 6210. I majored in history — not computer science — during my 
undergraduate years at UCLA. My first job out of school was a part-
time IT gig at a small publishing company in Irvine, California. I 
thoroughly enjoyed working with computers, so I decided to take night 
courses in computer science at Santiago Canyon College. 

After working part time and attending classes for about a 

year, I decided I wanted to take the next step and apply for graduate 
school. At the time, I was also looking for a full-time job in IT. I 
looked for programs that would allow me to complete a degree while 
employed. Of all the universities I researched, OMSCS seemed the best 
fit for my situation and I applied to Georgia Tech. Luckily, I found a 
full-time job at UCI's IT Department and was accepted into OMSCS 
around the same time.

As I looked through the course catalogue, CS 6210 interested 
me for two reasons. First, I wanted to try my hand at research, and 
educational technology seemed like a good starting point. Second, 
affordable and accessible education is the entire purpose of the OMSCS 
program. CS 6210 stands out as a course that advances this vision.
 Before OMSCS, I had taken a few EDX courses including an 

introduction to programming and an introduction to Linux. Beyond that, 
I have only taken traditional courses at Santiago Canyon and UCLA. 

Educators have always been interested in adapting courses to 
the individual needs of students. However, with advances in computer 
technology, we now have the ability to create interactive courses that 
can mold their outputs through the behaviors of students. At the 
present, technology cannot completely replace interaction with 
teaching staff, but the combination of both can help to deliver better 
courses to more students. It only makes sense that as technology 
continues to advance and techniques improve, education can be made 
more individualized and accessible in the future.

I am interested in adaptive learning technologies, and I am 

eager to read more about the research on the course website. Above all 
else, I want to complete a research paper. I have looked at many of 
the OMSCS Reddit pages to see if anyone has tried doing a PhD after 
the program but, so far, I have not found any examples. Though it is 
too early to decide on what I want to do, I want to explore all of my 
options. If I end up taking a liking to writing research papers, I can 
see if any of the professors at UCI are willing to allow me to take 
part in some projects.

Overall, I want to keep an open mind and utilize all the 

resources of the course. Most of all, I want to get better at 
programming and make my knowledge of computer science more adaptable 

to real-world projects. 

