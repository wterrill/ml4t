"	Define the area on which you want to focus for the next couple weeks. You might be interested most in a technology like intelligent 
 tutoring systems, or an audience like students with special needs, or a content area like foreign languages. Select an area and  
explore what's out there. What tools, theories, or communities exist? What are the general current problems? Who are the major players?  
(See https://www.udacity.com/wiki/ud915/Spring2016/Assignment_2.)"  

 

I ended my introductory paper from last week with this statement: Finally, I would hope to research 

and possibly develop an adaptive learning environment that comes with an adaptive and customizable curriculum – a 

curriculum which wouldn’t be set in stone like a generic, one-size-fits-all syllabus – but instead, is paced according to a 

student’s learning pattern, performance, scores, and other metrics. Add a human-centric layer (i.e., meetups) to this 

cognitive learning environment and you get an ideal educational technology to research and develop further in this course”.  

In  those  few  words,  I  have  unknowingly  defined  in  layman  terms,  an  intelligent  tutoring 

system. This came out of my personal experience with the first MOOCs I took and also, my personal 

“pain  points”  as  a  consumer  of  a  large  closed  online  classroom  (which  is  what  the  GT  OMSCS 

program is). In addition, how do you implement an intelligent tutoring system which can be deployed 

as an intervention for a student who is at risk (based on performance, late homework, low scores, and 

other metrics)? Currently, Udacity and other platforms assume you have a requisite level of knowledge 

(i.e., from a course prerequisite); and these platforms dismisses the fact that everybody is NOT on the 

same level of proficiency with the course material.  

So where would I start with this, you ask? To start with adaptive learning, I'd probably look at 

how you measure/assess current knowledge first. The assessment engine behind computer adaptive 

tests like the GMAT could be useful here. Once current knowledge of the material is assessed, the 

course content and knowledge graph would automatically adapt or be reconfigured to the learner's 

abilities and paced accordingly. 

Apparently from my research this week, some communities of practice are developing this. 

Carnegie  Mellon  University  has  developed  an  adaptive  and  intelligent  teaching  system  (Cognitive 

Tutor) which is widely implemented in several levels of math and science nationwide. A similar tool – 

the Andes Physics Tutor – has been developed at Arizona State University to support students in 

introductory physics courses. Additionally, the Knewton system provides custom learning experiences 

for students in K-12 and in higher ed and intelligent tutors for the GMAT and other CAT tests.  

As  MOOCs  mature,  these  smart  and  adaptive  tutoring  agents  can  be  programmed  and 

embedded in MOOCs to provide that missing conversational assistance and a sense of instructor 

interaction. With the student’s consent, the tutoring agents can collect data from student input which 

can be used to design improvements in the system. With this real-time data, instructors and developers 

can refine teaching methods. Because educational institutions cannot assign a human tutor to each 

student, these adaptive and smart teaching platforms will be useful alternatives to offer any student 

individual  help,  as  they  are  calibrated  to  students  own  learning  speed  and  specific  learning 

requirements. 

One of the current issues with building out smart adaptive learning systems is the complex 

effort involved. These systems are time-consuming to design and build, and they require considerable 

manual effort to load specific information for each course of study. It is also possible for students to 

trick a simple tutoring system by requesting assistance and ignoring each hint until the systems offers 

the  actual  answer.  So  the  challenge  here  is  developing  an  intelligent  and  adaptive  educational 

technology which is also cognitive and continually improvises/teaches itself based on data it is mining 

about former students, the current student, and the system itself. 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Bibliography 

1)  Educause	 Learning	 Initiative.	 7	 Things	 You	 Should	 Know	 About....	 July	 2013.	 Accessed	

January	24,	2016.	https://net.educause.edu/ir/library/pdf/ELI7098.pdf.	

