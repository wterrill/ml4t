Approach  

Motivation 
Active learning is a pedagogical approach to constructivism that is defined by shifting learning 
focus to the student instead of the lesson, class, or instructor (Felder & Brent, 2009).  
Numerous studies have shown that students who employ active learning perform better than 
those that used passive techniques only, such as listening to a lecture, watching a video, or 
reading a text book (Prince, 2004). 
 
However, active learning techniques may only help a student to internalize a concept for 
immediate use and may not help ensure that a student is able to recall and apply what she’s 
learned later in time (Glenda, 1996).  Active learning also presupposes an appropriate learning 
background and environment that promotes study, encourages growth, and cultivates 
collaboration (Grabinger & Dunlap, 1995). 
 
To overcome the first potential shortcoming, retrieval practice - distributed practice of varied 
material of interleaved context -  can be used to cement learned material in long term memory 
(Brown, Roediger III, & McDaniel, 2014).  Retrieval practice has shown consistent and significant 
increases in efficacy across a range of subjects, ages, and levels when compared to massed 
practice techniques such as rainbow writing (Jones, et al., 2015), cramming, or concept 
mapping (Karpicke & Blunt, 2011). 
 
To ensure an adequate learning environment, a professionally structured, collaborative 
environment, such as that found in certain MOOCs and offered by accredited institutions and 
professional educators, may be used. 
 
A combined active learning and a retrieval practice framework then presents the student a set 
of powerful tools to internalize knowledge and commit it to long term memory within a 
structured learning environment (Brown, Roediger III, & McDaniel, 2014). 
 
Problem Hypothesis  
At scale, a combined active learning/retrieval practice framework may serve any student, at any 
level, learning any new concept, within structured learning environments.  These students 
could be part of a traditional classroom, a MOOC (such Coursera), or a distance education 
program (such as the OMSCS). 
 
These students would achieve better learning outcomes, as defined by better verbal recall, 
inference, and problem solving for immediate and spaced retrieval, by using an active learning 
framework internalize material from educational content resources coupled with retrieval 
practice to commit learning to long term memory. 
 

Existing Online Learning Solutions 
Online Focus 
Online solutions provide ubiquitous access to learning materials to achieve common goals, a 
feature that facilitates the creation of a massive, open, and common active learning and 
retrieval practice framework.  Broad application of the tool, however, presupposes that the 
framework is extensible to a wide variety of disciplines and levels.   By focusing on online 
content, the latter presupposes that structured online education is representative of most 
structured education.  The latter assumption is borne out in a meta-analysis of over a thousand 
empirical studies that found that students of structured online programs, on average, 
performed the same or better than those in face-to-face programs (Means, Toyama, Murphy, 
Bakia, & Jones, 2010). 
 
As such, this proposal will focus on structured online learning solutions. 
 
Content and Practice Resources 
There are many open and free educational resources available to students at all levels for 
almost all conceivable subjects.  Some of this material is curated and there are some tools 
(Curriki, Schoozle) that are purpose-built to align the material to certain standards.  
 
On the testing side, numerous online apps (Anki, SuperMemo, Quizlet) implement some 
method of flash-card reinforcement system, often employing distributed practice and spaced 
repetition. 
 
Learning Platforms 
LMS’s and MOOCs add a curation layer to content through structured learning plans 
interwoven with reinforcement activities such as assignments, projects, tests, and quizzes.  
These solutions represent a holistic approach for online learning.  As an example, the OMSCS at 
Georgia Tech revolves around a set of YouTube videos, referenced reading material, and 
quizzes offered through Udacity (a public MOOC).  Instructors in the program are free to 
implement any pedagogical approach and some employ active learning techniques such as 
reflection and peer feedback.  
 
Potential Gap 
The online space today then contains many (often free) content resources and numerous 
spaced repetition framework, however, only closed, curated systems such as MOOCs and 
formal distance education programs tie content and practice together.  Even then, efficacy of 
learning is directly tied to the approaches an instructor chose to employ. 
 
There is currently no open, online framework for reflection and feedback that matches the 
breadth of open content available, nor is there a framework that provides guidance on how to 
create a varied, interleaved set of flash cards based on that content to ensure best learning 
outcomes.   
 

Solution  
The proposed solution will be an online app that contains an open interface to ingests content 
from free online resources into a defined, structured learning plan that facilitates reflection and 
feedback and fosters retrieval practice. 
 
The app will integrate with popular video and text repositories (YouTube, Vimeo, Bookboon) to 
import content into expert-created learning plans, bundles of content representing related 
concepts.  These learning plans can leverage existing institutional curricula (such as the lessons 
and mini courses in an OMSCS Udacity course), open curricula (such as Curriki), and user-
generated curricula. 
 
The app will foster active learning by asking the student to rephrase and summarize material 
they covered in a specific plan as well as provide a mechanism to solicit feedback on their 
summation from their peers.  
 
The app will then foster retrieval practice by asking a student to create a set of flash cards 
which they share publically for the same lesson plan, with a mechanism to export to existing 
spaced repetition programs 

Implementation Plan  
 
Key Assumptions 
Assumption 1 – One size framework fits all content 
The proposed solution provides a common framework to tie educational content resources 
together with testing plans.  As such, the app must easily ingest content from disparate sources 
covering varied material together into a common framework.  
 
Assumption 2 – Quality of collaboration and amplification 
Given the collaborative nature of the tool, the learning outcomes are likely directly correlated 
to the quality of feedback and crowd-sourced cards.  The solution assumes that feedback will 
be relevant and helpful. 
 
Assumption 3 – Investment and Commitment by Students 
Active learning and retrieval practice require increased time investment by students when 
compared to massed practice, and the proposed solution will only work students will invest this 
time to achieve better learning outcomes. 
 
Scope of Minimal Viable Product (MVP) 
Constrained by 100 hours and starting without any customers, the MVP will be based on 
OMSCS curriculum.  These courses will be identified as part of the first interim deliverable 
based on the following criteria: 

  Potential users interest in the course 

  Availability of video content on YouTube 

 
This approach reduces complexity of implementation to familiar courses, allowing me to act as 
an expert curator (essentially just copying the existing course structure) and helps to start to 
prove out assumption 1.  Using the OMSCS curriculum as a test board also allows for the 
flexibility of recruiting target users in a friendly environment – students comfortable with online 
learning and peer feedback in the 6460 forum.  
 
 
Success Criteria 
The MVP will primarily set out to prove aspects of each assumption listed in this proposal. 
 
Goal 1, informing assumption 1 
A structured list of content is ingested and presented to the student in the app 
At least one summary has been created for each ingested piece of content. 
At least once piece of feedback is given for each summary  
 
Goal 2, informing assumption 2 
More than one student participating as an active learner (creating summaries and flash cards) 
More than one piece of feedback is given for each summary  
 
Goal 3, informing assumption 3 
The percentage of active learners sticking to end of a course 
  
 
 

 

Tasks  
Task 
Set Up 

Solicit users 
Generate course list 
Set up domain and deploy boilerplate to devo serve 
Milestone Report 1 – Course and User list 

User Experience (UX) Mock Designs 

UX Mocks for module 1 - course setup and content ingestion  
UX Mocks for module 2 - soliciting and inputting reflection and 
gathering feedback  
UX Mocks for flash card creation and export 
Solicit feedback from users on UX mocks 
Milestone Report 2 – Complete UX Mocks, Revised Project Scope 

Development (dev)  

Module 1 – dev complete  
Module 2 – dev complete  
Module 3 – dev complete  
Milestone Report 3 – Working Prototype 

User Acceptance Testing (UAT) 

UAT 1 
Big Fixing  
UAT 2 
Bug Fixing 
Milestone 4 – Test and Usability Report 

Report 

Analyze user usage patterns   
Presentation and Paper  

Due 
 
1/21/2016 
1/28/2016 
1/28/2016 
3/6/2016 
 
3/10/2016 
3/14/2016 

3/18/2016 
3/18/2016 
3/20/2016 
 
3/27/2016 
3/31/2016 
4/3/2013 
4/3/2016 
 
4/7/2016 
4/14/2016 
4/15/2016 
4/16/2016 
4/17/2016 
 
5/2/2016 
5/2/2016 

 
 
Risks and Mitigation 
Scope  
The proposed solution will encompass content ingestion, reflection, feedback, and practice 
features.  This may prove to be too ambitious given the time constraints of the course.  As a 
mitigation, the retrieval practice module (creating and exploring crows source flash cards) may 
be descoped by interim deliverable 2 depending on progress to date. 
 
Lack of users and/or lack of commitment by users 
More users of the proposes MVP translates to stronger (validation or invalidation) of 
assumptions made.  While no surveys or human research will be performed, receiving informal 
feedback on usability and efficacy of the porotype would help inform design decisions and 
strategy for after the course.   

 
As a mitigation, students in 6460 will be reminded of the participation grade benefits associated 
with helping/testing other people’s projects as well as an offer for me to help critique, 
wireframe, and test their own projects.  Students outside the class will be presented an 
opportunity to use a tool that could, potentially, help them study better for their classes and 
achieve better marks. 
 

 

Works Cited 
Barnes, D. (1989). Active Learning. Leeds: Leeds University TVEI Support Projec. 

Brown, P., Roediger III, H. L., & McDaniel, M. A. (2014). Make it stick : the science of successful learning. 

Cambridge, Massachusetts: The Belknap Press of Harvard University Press. 

Cepeda, N. J., Pashler, H., Vul, E., Wixted, J. T., & Rohrer, D. (2004). Distributed practice in verbal recall 

tasks: A review and quantitative synthesis. Psychological Bulletin, 354-380. 

Donovan, J. J., & Radosevich, D. J. (1999). A Meta-Analytic Review of the Distribution of Practice Effect: 

Now You See It, Now You Don't. Journal of Applied Psychology, 795-805. 

Felder, R. M., & Brent, R. (2009). Active Learning: An Introduction. Raleigh: North Carolina State 

University. 

Freeman, S. F., Eddy, S. L., McDonough, M., Smith, M. K., Okoroafora, N., Jordta, H., & Pat, M. (2014, 

June 10). Active learning increases student performance in science, engineering, and 
mathematics. (1. S. Scott Freemana, Ed.) Proceedings of the National Academy of Sciences of the 
United States of America, 8410-8415. 

Glenda, A. (1996, December). Active Learning in a Constructivist Framework. Educational Studies in 

Mathematics, 349-369. 

Grabinger, R. S., & Dunlap, J. C. (1995). Rich environments for active learning: a definition. Research in 

Learning Technology, 3(2), 5-34. Retrieved from Research in Learning Technology: 
http://www.researchinlearningtechnology.net/index.php/rlt/article/viewFile/9606/11214 

Hein, G. E. (1991, October 15). Constructivist Learning Theory. Retrieved 02 11, 2016, from 

Exploratorium: http://www.exploratorium.edu/education/ifi/constructivist-learning 

Jones, A. C., Wardlow, L., Pan, S. C., Zepeda, C., Heyman, G. D., Dunlosky, J., & Rickard, T. C. (2015, July 

25). Beyond the Rainbow: Retrieval Practice Leads to Better Spelling than does Rainbow Writing. 
Educational Psychology Review, 1-16. 

Karpicke, J. D., & Blunt, J. R. (2011, February 11). Retrieval Practice Produces More Learning than 

Elaborative Studying with Concept Mapping. Science, 772-775. 

McCarthy, P. J., & Anderson, L. (2000, June). Active Learning Techniques Versus Traditional Teaching 
Styles: Two Experiments from History and Political Science. Innovative Higher Education, 279-
294. 

Means, B., Toyama, Y., Murphy, R., Bakia, M., & Jones, K. (2010). Evaluation of Evidence-Based Practices 
in Online Learning: A Meta-Analysis and Review of Online Learning Studies . U.S. Department of 
Education , Office of Planning, Evaluation, and Policy Development Policy and Program Studies 
Service . U.S. Department of Education . 

Prince, M. (2004, July). Does Active Learning Work? A Review of the Research. The Research Journal of 

Engnieering Education, 223-231. 

Royer, J. M., Tronskya, L. N., Chana, Y., & Jackson, S. J. (1999, July 1). Math-Fact Retrieval as the 

Cognitive Mechanism Underlying Gender Differences in Math Test Performance. Contemporary 
Educational Psychology, 181-266. 

 
 

