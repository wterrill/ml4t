var app = angular.module('sa-id', ['ngFileUpload', ]);

//# routing done here
//app.config(function () {
//    
//});

app.controller("main", function ($scope, $http, $location, Upload) {
    $scope.values = "";
    $scope.base_url = "http://52.11.104.178:5000";
    $scope.base_url = "http://localhost:5000";
    $scope.username = "test";
    $scope.progress = 0;
    $scope.corpus_data = {};
    $scope.corpus_active = {};
    $scope.uploading = false;
    $scope.working_status = null;
    $scope.error = null;
    $scope.identify_data = {};

    $scope.isEmpty = function (obj) {
        return Object.keys(obj).length === 0 && JSON.stringify(obj) === JSON.stringify({});
    };

    $scope.activate_corpus = function (key) {
        $scope.corpus_active = $scope.corpus_data[key];
        $scope.identify_data = {};
    };

    $scope.clear_error = function () {
        $scope.error = null;
    };

    $scope.go = function (path) {
        console.log("going to :" + path)
;        $location.path(path);
    };

    $scope.new_corpus = function (corpus) {
        url = $scope.base_url + '/new/' + corpus;
        $http.get(url)
            .then(function (response) {
                    $scope.identify_data = {};
                    $scope.corpus(corpus);
                },
                function (error) {
                    console.log(error);
                })
    }

    $scope.del_doc = function (corpus, doc) {
        url = $scope.base_url + '/del_doc/' + corpus + "/" + doc;
        $http.get(url)
            .then(function (response) {
                    $scope.corpus(corpus);
                },
                function (error) {
                    console.log(error);
                })
    }

    $scope.del_corpus = function (corpus) {
        url = $scope.base_url + '/del/' + corpus;
        $http.get(url)
            .then(function (response) {
                    $scope.corpus_active = {};
                    $scope.corpus();
                },
                function (error) {
                    console.log(error);
                })
    }

    $scope.refresh = function (corpus, method) {
        $scope.error = null;
        if (method === null || method === undefined)
            method = '/refresh/';
        url = $scope.base_url + method + corpus;
        if ($scope.working_status === null || $scope.working_status === '')
            $scope.working_status = "Refreshing corpus...";
        $http({
            method: 'POST',
            url: url,
            data: {
                corpus_data: corpus
            }
        }).then(function (response) {
            console.log(response.data);
            if (response.data !== null) {
                $scope.corpus_data = {};
                if (!$scope.isEmpty($scope.corpus_active))
                    old_key = $scope.corpus_active['corpus'];
            }
            for (var key in response.data) {
                $scope.corpus_data[key] = {
                    corpus: key,
                    data: response.data[key],
                    count: response.data[key].length,
                    status: ""
                };
                if ($scope.isEmpty($scope.corpus_active))
                    $scope.corpus_active = $scope.corpus_data[key];
            }
            if (old_key !== undefined)
                $scope.corpus_active = $scope.corpus_data[old_key];

            $scope.working_status = null;
        }, function (response) {
            console.log(response.data);
            console.log(response.data.message);
            $scope.error = response.data.message;
            $scope.working_status = null;
        });
    };

    $scope.process = function (corpus, method) {
        $scope.working_status = "Building authorship models...";
        if (method === null || method === undefined)
            method = '/process/';
        $scope.identify_data = {};
        $scope.refresh(corpus, method);
    };

    $scope.corpus = function (corpus_key) {
        url = $scope.base_url + '/corpus';
        $scope.working_status = "Loading data...";
        $http.get(url)
            .then(function (response) {
                console.log(response.data);
                $scope.corpus_data = {};
                for (var key in response.data) {
                    response.data[key]["status"] = "";
                    $scope.corpus_data[key] = {
                        corpus: key,
                        data: response.data[key],
                        count: response.data[key].length
                    };
                    if ((corpus_key == undefined || corpus_key == null) && $scope.isEmpty($scope.corpus_active))
                        $scope.corpus_active = $scope.corpus_data[key];
                    if (corpus_key != undefined)
                        $scope.corpus_active = $scope.corpus_data[corpus_key];
                }
                console.log($scope.corpus_data[key]);
                $scope.working_status = null;
            }, function (response) {
                console.log(response);
                $scope.error = "Cannot contact web service.";
                $scope.working_status = null;
            });

    };

    $scope.identifyFile = function (corpus, file) {
        url = $scope.base_url + '/identify/';
        $http({
            method: "POST",
            url: url,
            data: {
                doc: file,
                corpus: corpus
            }
        }).then(function (response) {
            console.log(response);
            $scope.identify_data = response.data;
            $scope.identify_data["file"] = file;
            console.log($scope.identify_data);
        }, function (response) {
            console.log(response);
        })
    }

    $scope.UploadIdentifyFile = function (file, corpus) {
        $scope.error = null;
        url = $scope.base_url + '/upload/';
        Upload.upload({
            url: url,
            method: 'POST',
            data: {
                file: file,
                corpus: corpus,
                identify: 1
            }
        }).then(function (response) {
            console.log(response.data);
            $scope.identify_data = response.data;
            $scope.identify_data["file"] = file.name;
        }, function (response) {
            console.log(response);
        });
    };

    // upload on file select or drop !!NOT USED
    $scope.upload = function (file) {
        Upload.upload({
            url: $scope.base_url + '/upload',
            data: {
                file: file,
                'username': $scope.username
            }
        }).then(function (resp) {
            console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });
    };

    // for multiple files:
    $scope.uploadFiles = function (files, corpus) {
        $scope.working_status = "Uploading...";
        $scope.upload_result = "Uploading...";
        $scope.error = null;
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                console.log(corpus);
                Upload.upload({
                    url: $scope.base_url + '/upload/',
                    data: {
                        file: files[i],
                        'username': $scope.username,
                        'corpus': corpus
                    }
                }).then(function (resp) {
                    console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                    $scope.upload_result = resp.data;
                    $scope.upload_alert = true;
                    $scope.upload_result = "";
                    $scope.working_status = null;
                    //if (i === files.length-1)
                    //$scope.corpus(corpus);
                }, function (resp) {
                    console.log('Error status: ' + resp.status);
                    $scope.working_status = null;
                }, function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    $scope.progress = progressPercentage + '%';
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                });
            }
            $scope.process(corpus);
            //$scope.refresh(corpus);
        }

    };


    $scope.compute_stdev = function (values) {
        url = $scope.base_url + '/stdev/' + values;
        console.log(url);
        $scope.results = "working...";

        $http.get(url)
            .then(function (response) {
                $scope.results = response.data.hasOwnProperty("result") //First function handles success
                    ? "Stdev: " + response.data.result : "";
            }, function (response) { //Second function handles error
                console.log(response);
                $scope.results = "Cannot contact the 'sa-id' web service";
            });
    };

    $scope.corpus();
});
