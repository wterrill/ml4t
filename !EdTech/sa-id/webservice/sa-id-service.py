""" Requirements & Notes
https://pypi.python.org/pypi/Flask-Cors
https://pypi.python.org/pypi/Flask/0.10.1
http://flask.pocoo.org/docs/0.10/quickstart/
http://flask.pocoo.org/docs/0.10/patterns/fileuploads/

pip install Flask
pip install -U flask-cors
"""

from flask import Flask, jsonify, request
from flask.ext.cors import CORS
from werkzeug import secure_filename
import json
import os
import shutil
import datetime
import stdev_code as s
import pandas as pd
from stylo.db import *
from stylo.stylo import *
from InvalidUsage import InvalidUsage

app = Flask(__name__)
CORS(app)
base_path = os.path.join( os.path.dirname(os.path.abspath(__file__)), "stylo","corpus")

# This is the path to the upload directory
app.config['UPLOAD_FOLDER'] = base_path
# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS'] = set(['txt', 'pdf'])

app.config['DATA'] = None

@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

#---helper functions -----

def allowed_file(filename):
    """For a given file, return whether it's an allowed type or not"""
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in app.config['ALLOWED_EXTENSIONS']

#---app routes----

@app.route("/")
def hello():
    return "Hello World!<br> from SA-ID"

@app.route("/identify/", methods=['POST'])
def identify():
    try:
        print datetime.datetime.now(), "Identify Started"
        data = request.json #<-- any other data passed from the front end eg. username
        print datetime.datetime.now(), data
        corpus = data['corpus'] if 'corpus' in data and data['corpus'] != '' else None
        doc = data['doc'] if 'doc' in data and data['doc'] != '' else None
        print datetime.datetime.now(), corpus, doc
        result = predict_from_file(doc,corpus)
        result['author'] = result.index
    except Exception as e:
        print str(e)
        raise InvalidUsage(e.message, status_code=410)
    return jsonify({"Upload": "success"}) if result is None else jsonify(result.to_dict(orient='index'))

@app.route("/new/<corpus>", methods=["POST","GET"])
def new_corpus(corpus):
    print datetime.datetime.now(), "creating corpus", corpus
    try:
        if not os.path.isdir(os.path.join(base_path,corpus)):
            os.makedirs(os.path.join(base_path,corpus))
            app.config['DATA'] = None
    except Exception as e:
        print str(e)
        raise InvalidUsage(e.message, status_code=410)
    return jsonify({"Upload": "success"})

@app.route("/del/<corpus>", methods=["POST","GET"])
def del_corpus(corpus):
    """Delete corpus by name from FS and database"""
    print datetime.datetime.now(), "removing corpus", corpus
    try:
        if os.path.isdir(os.path.join(base_path,corpus)):
            print datetime.datetime.now(), "removing from FS", corpus
            shutil.rmtree(os.path.join(base_path,corpus))
            app.config['DATA'] = None
        print datetime.datetime.now(), "removing from DB", corpus
        datastore = db()
        datastore.del_corpus(corpus)
    except Exception as e:
        print str(e)
        raise InvalidUsage(e.message, status_code=410)
    return jsonify({"Upload": "success"})

@app.route("/del_doc/<corpus>/<doc>", methods=["POST","GET"])
def del_doc(corpus,doc):
    print datetime.datetime.now(), "exploring corpus", corpus
    try:
        if os.path.isdir(os.path.join(base_path,corpus)):
            print datetime.datetime.now(), "removing from FS", doc
            os.remove(os.path.join(base_path,corpus,doc))
            app.config['DATA'] = None
        print datetime.datetime.now(), "removing from DB", corpus
        datastore = db()
        datastore.del_corpus_file(corpus,doc)
    except Exception as e:
        print str(e)
        raise InvalidUsage(e.message, status_code=410)
    return jsonify({"Upload": "success"})
    

@app.route("/process/<corpus>", methods=["POST", "GET"])
def process(corpus=None):
    try:
        process_corpus(corpus)
    except Exception as e:
        print str(e)
        raise InvalidUsage(e.message, status_code=410)
    app.config['DATA'] = None
    return get_corpus()

@app.route("/refresh/<corpus>", methods=["POST", "GET"])
def refresh(corpus=None):
    try:
        print datetime.datetime.now(), "Refresh Started"
        data = request.form #<-- any other data passed from the front end eg. username
        print datetime.datetime.now(), data
        corpus = data['corpus_data'] if 'corpus_data' in data and data['corpus_data'] != '' else corpus
        print datetime.datetime.now(), corpus
        refresh_corpus(corpus,True)
        print datetime.datetime.now(), "Refresh done"
    except Exception as e:
        print str(e)
        raise InvalidUsage(e.message, status_code=410)
    return get_corpus(from_database=True)

@app.route("/corpus")
def corpus():
    return get_corpus()

def get_corpus(corpus=None, from_database=False):
    print datetime.datetime.now(), "Getting corpus"
    try:
        print datetime.datetime.now(), "from app.config"
        data = app.config['DATA'] if from_database == False else None
        if data is None:
            data = {}
            print datetime.datetime.now(), "from Database"
            datastore = db()
            d = datastore.get_corpus_all()
            for c in d['corpus'].unique():
                data[c] = d[d.corpus==c].to_dict(orient='records')
            print datetime.datetime.now(), "from FS"
            for p in os.listdir(base_path):
                if p not in data:
                    data[p] = []
                    print "...",p
            data = jsonify(data)
            app.config['DATA'] = data
    except Exception as e:
        print str(e)
        raise InvalidUsage(e.message, status_code=410)
    print datetime.datetime.now(), "Done"
    return data

def corpus_old(corpus=None):
    d = None
    print "Start",  datetime.datetime.now()
    try:
        data = app.config['DATA']
        print data
        if data is None:
            print data
            data = {}
            datastore = db()
            d = datastore.get_corpus()
            for p in os.listdir(base_path):
                if os.path.isdir(os.path.join(base_path, p)):
                    file_count = len( filter(lambda file: os.path.isfile(os.path.join(base_path, p, file)) and os.path.splitext(file)[1] in ['.txt'] ,os.listdir(os.path.join(base_path, p))))
                    data[p] = {"corpus":p, "files":file_count, "processed":d[d.corpus==p].iloc[0].qty} if len(d[d.corpus==p]) > 0 else 0
            data = pd.DataFrame(data)
            app.config['DATA'] = data
    except Exception as e:
        print str(e)
        raise InvalidUsage(e.message, status_code=410)
    print "convert",  datetime.datetime.now()
    return data.to_json()

@app.route("/upload/", methods=["POST"])
def upload():
    """Handle uploaded files"""
    result = None
    file = request.files['file']
    data = request.form #<-- any other data passed from the front end eg. username
    corpus = data['corpus'] if len(data)> 0 and 'corpus' in data else None
    identify = data['identify'] if len(data) > 0 and 'identify' in data else None 
    if file and allowed_file(file.filename):
        # verify that the filename is secure
        filename = secure_filename(file.filename)
        #create upload folder if it does not exist
        if not os.path.exists(app.config['UPLOAD_FOLDER']):
            print "...creating upload directory"
            os.makedirs(os.path.join(app.config['UPLOAD_FOLDER'], corpus))
        #save upoaded file
        try:
            if (identify == '1' or identify == 1):
                if not os.path.exists(os.path.join(app.config['UPLOAD_FOLDER'], corpus, "identify")):
                    os.makedirs(os.path.join(app.config['UPLOAD_FOLDER'], corpus, "identify"))
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], corpus, "identify", filename))
                result = predict_from_file(filename,corpus)
                result['author'] = result.index
            else:
                print "...saving file", filename
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], corpus, filename))
                print "...file saved", os.path.join(app.config['UPLOAD_FOLDER'], corpus, filename)
                #extract_features_from_file(filename,corpus)
                app.config['DATA'] = None
        except Exception as e:
            print e
            raise InvalidUsage(e.message, status_code=410)
    return jsonify({"Upload": "success"}) if result is None else jsonify(result.to_dict(orient='index'))

@app.route("/classify")
def classify():
    """This is the web ntry point for classifying a document"""
    rr = st.classify()
    return rr



#--- testbed stuff ---
@app.route("/json")
def json():
    return jsonify(result=str({"test": [1, 2, 4, 4]}))

@app.route("/test")
def test():
    t = "[1,3,4,5,6]"
    return t

@app.route("/stdev/<values>")
def stdev(values):
    test = [float(x) for x in values.split(',')]
    ret = s.stdev_p(test)
    return jsonify(result=ret)

if __name__ == "__main__":
    app.run(host='0.0.0.0', threaded=True)
