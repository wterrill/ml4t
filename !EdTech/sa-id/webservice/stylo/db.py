"""This clss is used to manage database interactions for sa-id
It uses sqlite3 so a pip install of sqlite2 is required
>> pip install sqlite3
https://docs.python.org/2/library/sqlite3.html
"""

import sqlite3
import os
import pandas as pd
import collections

class db():
    """handles all database operations"""
    def __init__(self):
        self.script_path = os.path.dirname(os.path.realpath(__file__))
        print "Using database: ", os.path.join(self.script_path,'sa-id.db')
        self.conn = sqlite3.connect(os.path.join(self.script_path,'sa-id.db'), check_same_thread = False)
        self.features_table = 'features'
        self.authors_table = 'authors'

    def save_authors(self,authors,corpus):
        try:
            # pull authors from database and replace our current one then save
            try:
                table = pd.read_sql_query("select * from "+self.authors_table, self.conn, index_col='index')
                table = table[table.corpus != corpus]
                table = authors if table.empty else authors.append(table, ignore_index=True) #pd.concat([features,table])
                table = table[authors.columns] # this reorders the columns with corpus first!
            # exception will occur if the table does not exist
            except:
                table = authors
            # save dataframe to database
            table.to_sql(self.authors_table, self.conn, flavor='sqlite', if_exists='replace')
            self.conn.commit()
        except Exception as e:
            print "Error occurred",str(e)
        return True   
    
    def save_features(self, features, authors, corpus=None):
        try:
            corpus = features["corpus"][0] if corpus is None else corpus
            # pull features from database and replace our current one then save
            try:
                table = pd.read_sql_query("select * from "+self.features_table, self.conn, index_col='index')
                table = table[table.corpus != corpus]
                table = features if table.empty else features.append(table, ignore_index=True) #pd.concat([features,table])
                table = table[features.columns] # this reorders the columns with corpus first!
            # exception will occur if the table does not exist
            except:
                table = features
            # save dataframe to database
            table.to_sql(self.features_table, self.conn, flavor='sqlite', if_exists='replace')
            self.conn.commit()
        except Exception as e:
            print "Error occurred", str(e)
        self.save_authors(authors, corpus)
        return True

    def del_corpus(self, corpus):
        """Delete all records asscoiated with a corpus"""
        try:
            c = self.conn.cursor()
            c.execute ("delete from features where corpus = '"+corpus+"'")
            c.execute ("delete from authors where corpus = '"+corpus+"'")
            # Save (commit) the changes
            self.conn.commit()
            self.conn.close()
            return True
        except Exception as e:
            print "Error occured:", str(e)
            return False

    def del_corpus_file(self, corpus, doc):
        """Delete all records asscoiated with a file"""
        try:
            c = self.conn.cursor()
            c.execute ("delete from features where corpus = '"+corpus+"' and doc = '"+doc+"'")
            # Save (commit) the changes
            self.conn.commit()
            self.conn.close()
            return True
        except Exception as e:
            print "Error occured:", str(e)
            return False
            

    def get_features(self, corpus):
        data = None
        try:
            data = pd.read_sql_query("select * from "+self.features_table+" where corpus='"+corpus+"'",self.conn, index_col='index')
        except Exception as e:
            print "Error occured:", str(e)
        return data.ix[:,"corpus":]
    
    def get_corpus(self):
        data = None
        try:
            data = pd.read_sql_query("select corpus, count(*) as qty from "+self.features_table+" group by corpus", self.conn)
        except Exception as e:
            print "Error occured:", str(e)
        return data.ix[:,"corpus":]

    def get_corpus_all(self):
        data = None
        try:
            data = pd.read_sql_query("select * from "+self.features_table, self.conn)
        except Exception as e:
            print "Error occured:", str(e)
        return data.ix[:,"corpus":]


    def get_authors(self, corpus):
        authors = None
        try:
            authors = pd.read_sql_query("select * from "+self.authors_table+" where corpus='"+corpus+"'",self.conn, index_col='index')
            # authors = collections.OrderedDict((v,k) for k,v in dict(data[['author','target']].values).iteritems())
        except Exception as e:
            print"Error occured:", str(e)
        return authors
