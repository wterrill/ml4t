import os
from cStringIO import StringIO
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage

def pdfToText(fname, pages=None):
    if not pages:
        pagenums = set()
    else:
        pagenums = set(pages)

    output = StringIO()
    manager = PDFResourceManager()
    converter = TextConverter(manager, output, laparams=LAParams())
    interpreter = PDFPageInterpreter(manager, converter)

    infile = file(fname, 'rb')
    for page in PDFPage.get_pages(infile, pagenums):
        interpreter.process_page(page)
    infile.close()
    converter.close()
    text = output.getvalue()
    output.close
    text_file = open(fname[:-3]+"txt", "w")
    text_file.write(text)
    text_file.close()
    return fname[:-3]+"txt"

def convert(name, pages=None):
    if name[-3:]=="pdf":
        if os.path.exists(name[:-3]+"txt"):
            return name[:-3]+"txt"
        else:
            file = pdfToText(name,pages)
    else:
        for root, dirs, files in os.walk(name):
            path = root.split('/')
            print (len(path) - 1) *'---' , os.path.basename(root)
            for file in files:
                if file[-3:]=="pdf":
                    if not os.path.exists(name[:-3]+"txt"):
                        name = pdfToText(root+file)
                print len(path)*'---', file

    return file