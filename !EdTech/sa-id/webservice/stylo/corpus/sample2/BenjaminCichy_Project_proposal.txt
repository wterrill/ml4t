

                                                                            

  
                                                            Table of Contents 

Abstract .......................................................................................................................................... 3!

Introduction and Background ..................................................................................................... 3!

Objectives and Aims ..................................................................................................................... 4!

         Gender Bias ............................................................................................................................. 4!
         Economics Differences ............................................................................................................ 4!
Review of Literature ..................................................................................................................... 4!

         Team skills, younger children , outreach ................................................................................. 5!
         Quantification of Skills ............................................................................................................ 5!
Project and Study Design ............................................................................................................. 5!

      Setting and Resources ................................................................................................................. 6!



!                                                                            !
!                                                                            !

         Location of Group I ................................................................................................................. 6!
      Study Population ......................................................................................................................... 6!
         Group 1 .................................................................................................................................... 6!
         Group 2 .................................................................................................................................... 6!
      Sources of Data ........................................................................................................................... 6!
      Data Analysis .............................................................................................................................. 6!
      Quality ......................................................................................................................................... 7!
      Ethics and Human Subjects Protection ....................................................................................... 7!
      Budget ......................................................................................................................................... 7!
      Strengths and Weaknesses of the Study ...................................................................................... 7!
         Project Plan .............................................................................................................................. 7!
Conclusion ................................................................................................................................... 10!

References .................................................................................................................................... 11!

























                                                                     "#$%!!!&'!""!
!!



!                                                !
!                                                !

                                            Abstract 

 
       We know that sports can foster camaraderie among children while enhancing their spatial 

skills through games and competitions. This has also been brought into schools through various 

programs such as Math Olympiad("Math Olympiads for Elementary and Middle Schools," n.d.) , 

Odyssey of the Mind("Odyssey of the Mind -- Home Page," n.d.) and  Google Science 

Fair("Idea Springboard - Google Science Fair 2015," n.d.). All these contests have in common a 

theme of being competitive will other groups to see what unique ideas are brought forth as 

solutions. LEGO Mindstorms and the new LEGO Wedo 2.0 are ways to increase STEM learning 

at an early age. However, not many studies have been conducted for younger students and older 

adults.  The method of experimentation I am covering, is A/B testing, and generalized testing of 

incremental differences in experimental procedure. A/B testing (also known as split testing) is 

used for determining the completion or retention rate. Small differences such as additions and 

deletions of steps are compared and tested for a given metric such as time to completion. In this 

project we will track major and minor differences in learning for five age groups, divided into 

adults and children. Different economic backgrounds and genders also will be included and 

surveyed  

        

       Keywords:LEGO Wedo 2.0 , A/B testing, Split Testing,  gender differences, economic  

differences.

                                                  

                                 Introduction and Background 

A/B testing, and generalized testing of incremental differences in experimental procedure. 

Generally , A/B testing (also known as split testing) is used for determining the completion or 

                                           "#$%!#!&'!""!
!!



!                                                    !
!                                                    !

retention rate for a certain shown function. Small differences such as additions and deletions of 

steps are compared and tested for a given metric such as time to completion, and then adopted as 

the final solution. It's iterative in nature, and generally does not involve major differences 

between A and B versions.  

                                         Objectives and Aims 

!"#$"%&'()*&&
Regular participation is growing in terms of involvement by gender. Since 1970[5] participation 

in sports at best 1:4 girls to boys. It is now closer to 7:8, which shows a huge parity increase in 

what used to be a perceived set of gender roles. which are a set of more specific goals, supports 

that aim.  Aims and objectives are often written in bullet points as 'to' statements, such as, 'to' 

develop, 'to' identify, 'to' measure, 'to' explain, 'to' compare. 

+,-#-.(,*&/(00"%"#,"*&
 Children and adults come from many economic backgrounds. In today's technologically heavy 
world, just a small lack in terms of exposure to technology, could have repercussions in a skill 

set as applied to engineering and programming problems. This study also aims at quantifying 

these differences in terms of learning and success rates. The experiments will be adjusted A/B 

style to try to compensate for the differences.  

&
                                         Review of Literature 

LEGO Mindstorms is a STEM delivery vehicle disguised as a toy. Building with LEGO blocks 

improves visual-spatial skills. LEGO literally builds upon early successes by allowing for 

children to improve quickly and iterate their designs. Various recent papers confirm that the First 




                                               "#$%!$!&'!""!
!!



!                                                  !
!                                                  !

LEGO League has been successful in introducing a new generation of children to STEM skills 

and improving general problem solving.  

1").&*2(33*4&5-6#7"%&,8(3$%"#&4&-69%"),8&
A recent paper(Menekse, Schunn, Higashi, & Baehr, 2015) considered collaboration skills using 

LEGO Mindstorms in the First LEGO League. A Croatian(Sovi, Jagut, & Se, 2014) study took 

on the task of learning in the first grade. This study was more general, testing several methods of 

learning, and did not quantify results.  

A study for NXT Brick that underlies the Mindstorms kit  (Karp, Gale, Lowe, Medina, & 

Beutlich, 2010) followed children in West Texas regarding an outreach program called GEAR. 

This study tracked gender roles, engineering retention past the high school, and mentor factor.  

:6)#9(0(,)9(-#&-0&;2(33*&&
A study by  (Kazakoff, Sullivan, & Bers, 2013) used sequencing (ordering of pictures) as a way 

to quantify increased STEM skills. Aside: generally LEGO bricks come with instructions as a 

step by step process to building as set. Similarly, this study used to K-2nd grade children to test 

accuracy of sequencing pre and post LEGO Wedo. T scores and and ANOVA analysis was used 

to track significance of the results.  

 
                                      Project and Study Design 

Many conferences papers have covered success of learning STEM concepts through the vehicle 

of competition. In this project we cover the younger brother of the LEGO Mindstorms kit (the 

Wedo kit is too new for complete analysis of the K-3 sector). The study will require testing 

minute changes in a pre-existing curriculum design. Careful surveying and noting of learning 

speed will have to be kept, to validate the presumptions.  




                                             "#$%!%!&'!""!
!!



!                                                      !
!                                                      !

Setting and Resources 

        The groups tested will initially be adults, and the results compared to a larger roll out of 

K-7 aged children of various economic backgrounds as well as split of girls and boys.  

        Location of Group I 

San Diego, Ca.  
Study Population 

Adults: split into three groups.  

!%-6<&=&
        Older adults (50+ and also non-technical users) will be used as controls. Programmers 

and engineers will be added as alternate data subject points to validate outliers. Millennials (20-

29) will be used for another control point for a generally technologically savvier group.   

!%-6<&>&
        K-7 aged children split into two groups: Grades 2-3 and 5-7. This is to compare learning 

rates for early learning vs later learning.  

Sources of Data 

        The data sources are as follows: timed � testing, behavioral analysis, feedback questions, 

types by subject, solution validity of experiments, and post project surveys.  

Data Analysis 

        Fully address the statistical planning, if applicable, and the methods of collection such as 

surveys, interviews, or document analysis.  Confer with your advisor as to whether you should 

construct a data collection/analysis table or a table of evidence.  Refer to each table in the text, 

but put the table after the reference pages. 




                                                 "#$%!&!&'!""!
!!



!                                                       !
!                                                       !

Quality 

Surveys will be maintained offsite via Survey monkey, google sheets or similar service. Time 

will be kept via timer, and excel spreadsheets protected for anonymity of data. Published data 

will be fully anonymized, and only the goal of gender, economic and age differences will be 

shown. No personally identifiable information will be used.   

Ethics and Human Subjects Protection 

            Children as a subjects may not be a possible goal for this project. If permissions are 

obtained, a small pilot child study will be followed as a phase 2.  

Budget 

            The project has been allocated $200 for the LEGO Wedo 2.0 and possible survey fees.  

Strengths and Weaknesses of the Study 

Availability of testing children is a large unknown. A/B testing of experiments could be beneath 

the noise floor of the experiments. Significant changes in A/B tests could nullify the small 

change factor of split testing.  

 

?%-@",9&?3)#&
      Mileston Type           Due     Status        Link           Notes 
      e / Task 
      Week of 2/15/2016 
      Project      Delivera 2/21/     Done          http://legowedo.benjamincichy.com 
      Proposal  ble           16 
      LEGO         Design             Blocked  waiting on          waiting on delivery  
      Wedo 2.0                                      delivery  
      Week of 2/22/2016 
      Weekly       Delivera 2/28/     Not           http://...      
      Status       ble        16      Started 
      Check #1 

                                                  "#$%!'!&'!""!
!!



!                                                        !
!                                                        !

      Wedo Kit  Design                Not                           
      analysis                        Started 
      and run 
      through 
      Week of 2/29/2016 
      Weekly        Delivera 3/6/1    Not           http://...      
      Status        ble      6        Started 
      Check #2 
      Experime Design        3/6/1    Not           http://...      
      nt 1-                  6        Started 
      Basic 
      skills 
      (adult, K-
      3) 
      Experime Design                 Not                           
      nt 2-                           Started 
      Basic 
      Physics 
      skills 
      Experime Design                 Not                           
      nt 3 -                          Started 
      Basic 
      engineeri
      ng skills 
      Week of 3/7/2016 
      Weekly        Delivera 3/13/    Not           http://...      
      Status        ble      16       Started 
      Check #3 
      Experime Design                 Not                           
      nt 4 -                          Started 
      Gender 
      differenc
      es 
      Experime Design                 Not                           
      nt 5 -                          Started 
      Economi
      c 
      differenc
      es 
      Week of 3/14/2016 
      Weekly        Delivera 3/20/    Not           http://...      
      Status        ble      16       Started 
      Check #4 
      Intermedi Delivera 3/20/        Not           http://...      
      ate           ble      16       Started 


                                                  "#$%!(!&'!""!
!!



!                                                         !
!                                                         !

      Mileston
      e #2 
      Survey         Design            Not                           
      questions                        Started 
      /survey 
      setup 
      pre/exit 
      Week of 3/21/2016 
      Weekly         Delivera 3/27/    Not           http://...      
      Status         ble        16     Started 
      Check #5 
      Adult run  Experim               Not                           
      through        ent               Started 
      for 
      experime
      nt set 
      Week of 3/28/2016 
      Weekly         Delivera 4/3/1    Not           http://...      
      Status         ble        6      Started 
      Check #6 
      Intermedi Delivera 4/3/1         Not           http://...      
      ate            ble        6      Started 
      Mileston
      e #3 
      Adult          Experim           Not                           
      testing -      ent               Started 
      50+, 
      millenial 
      + 
      surveys 
      Week of 4/4/2016 
      Weekly         Delivera 4/10/    Not           http://...      
      Status         ble        16     Started 
      Check #7 
      Statistica Analysis              Not                           
      l analysis                       Started 
      of data.  
      Reconfig Design                  Not                           
      uration                          Started 
      of A/B 
      tests if 
      needed 
      Week of 4/11/2016 




                                                   "#$%!)!&'!""!
!!



!                                                       !
!                                                       !

     Weekly        Delivera 4/17/    Not           http://...       
     Status        ble        16     Started 
     Check #8 
     Intermedi Delivera 4/17/        Not           http://...       
     ate           ble        16     Started 
     Mileston
     e #4 
     K-7           Design            Not                            
     Testing (                       Started 
     if 
     possible_ 
     Paper +  Design                 Not                            
     ppt of                          Started 
     current 
     data 
     Week of 4/18/2016 
     Weekly        Delivera 4/24/    Not           http://...       
     Status        ble        16     Started 
     Check #9 
     Tabulatio Analysis              Not                            
     n of data                       Started 
     and 
     presentat
     ion 
     Week of 4/25/2016 
     Final         Delivera 5/1/1    Not           http://...       
     Project       ble        6      Started 
     Project       Delivera 5/1/1    Not           http://...       
     Presentat ble            6      Started 
     ion 
     Project       Delivera 5/1/1    Not           http://...       
     Paper         ble        6      Started 
 
                                                 Conclusion 

LEGO Mindstorms success in competition is not new. As far as I know, no studies have been 

done with LEGO Wedo or LEGO Wedo 2.0 for early and late adult testing of technological, 

engineer, and programmatic testing. This projects seeks to elucidate the basic processes through 

the and measure and collect metrics on learning rates, successes, as well as perception of 

experiments.    



                                                 "#$%!"*!&'!""!
!



!                                               !
!                                               !

                                           References 
Idea Springboard - Google Science Fair 2015. (n.d.). Retrieved February 14, 2016, from 
     https://www.googlesciencefair.com/springboard/en/ 
Karp, T., Gale, R., Lowe, L. A., Medina, V., & Beutlich, E. (2010). Generation NXT: Building 
     young engineers with LEGOs. IEEE Transactions on Education, 53(1), 80�87. 
     http://doi.org/10.1109/TE.2009.2024410 
Kazakoff, E. R., Sullivan, A., & Bers, M. U. (2013). The Effect of a Classroom-Based Intensive 
     Robotics and Programming Workshop on Sequencing Ability in Early Childhood. Early 
     Childhood Education Journal, 41(4), 245�255. http://doi.org/10.1007/s10643-012-0554-5 
Math Olympiads for Elementary and Middle Schools. (n.d.). Retrieved February 14, 2016, from 
     http://www.moems.org/ 
Menekse, M., Schunn, C., Higashi, R., & Baehr, E. (2015). An Investigation of the Relationship 
     between K-8 Robotics Teams ' Collaborative Behaviors and Their Performance in a 
     Robotics Tournament. 
Odyssey of the Mind -- Home Page. (n.d.). Retrieved February 14, 2016, from 
     http://www.odysseyofthemind.com/ 
Sovi??, A., Jagu??t, T., & Ser??i??, D. (2014). How to teach basic university-level programming 
     concepts to first graders? ISEC 2014 - 4th IEEE Integrated STEM Education Conference. 
     http://doi.org/10.1109/ISECon.2014.6891050 
 

 























                                          "#$%!""!&'!""!
!



