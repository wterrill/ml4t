
difficult to extend to new domains or scale up to large courses. In the best of worlds, a project should 

be able to re-use components of another project in a "plug-in" fashion. For example, the best Bayesian 

student modeler or the most fully functional graphing tool could be re-used by many ITS or ITS 

authoring tool design teams. Hodgins & Masie (2002) have identified a set of related issues, which I 

list below with additions from Roshelle et al. (1998).  

        First, ITS authoring tools should support interoperability. That is, can the system work with 

other systems? For applications to interoperate they need to, at the very least, be "inspectable", that 

is, one application can ask another for specific types of information. A higher level of interoperability 

involves "record-ability", in which an application is broadcasting data (for example student behaviors) 

to any application that cares to notice (for example a feedback agent). A further level of interoperability 

involves "script-ability" which allows one application to control another, for example a tutorial agent 

invoking a weather simulator with specific constraints on the simulation.  

        ITS and ITS authoring tools should also support manageability. That is, can the overall 

system track information about the learner and learning session? One method for this is record-ability, 

as mentioned above. But there are many ways to address this issue. Furthermore, in terms of re-



usability, can courseware (learning objects) be re-used by applications and by developers different 

from the context for it was originally designed? Also, in terms of durability and scalability, will the 

item remain usable as technology and standards evolve? Designing software so that it is extensible is 

one approach. Extensibility can be achieved by using open source coding or by having a plug- in 

architecture. Authoring tools that work with medium sized-domains or (in the case of on-line systems) 

with a few users, can experience performance problems when scaled up to large domains or many 

simultaneous users. Integration with off-the-shelf components such as robust database applications 

can  alleviate  this  problem.  Finally,  can  the  learner  or  content  developer  locate  and  download  the 

material in a reasonable time? Content repositories, mentioned below, are addressing this issue of 

accessibility. 

        In order to accomplish any of these objectives, more common standards are needed. In the 

area of application interoperability, several component software architectures have been developed 

including  OpenDoc  (no  longer  supported  by  Apple).  Though  these  architectures  may  make 

interoperability a reality, so far there have been only a few attempts. Roschelle et al. (1998) describe 

the EduObject project in which four research groups used OpenDoc to share components of their 

learning  environments.  Koedinger  et  al.  (1998)  describe  a  project  in  which  three  independently 

developed educational applications were combined using the "MOO communications protocol" as a 

communication  infrastructure.  The  applications  were  Active  Illustrations  Lab,  an  open-ended 

simulation  environment  (Forbus  &  Falkenhainer  1995);  Belvedere,  an  environment  supporting 

scientific dialoging and argumentation (Suthers et al. 1997); and a model tracing Tutor Agent that was 

able  to  give  feedback  to  students  (Ritter  &  Koedinger  1997).  These  two  projects  were  feasibility 

demonstrations that did not see extensive use. Others are working on large scale in-house component 

based  systems  (e.g.  Cheikes  1995),  but  a  major  bottleneck  to  interoperability  of  educational 

components  is  the  lack  of  shared  open  standards.  An  interoperability  framework  needs  "separate 



interface and data models which communicate through a fixed protocol [that] should express events 

in terms of their semantics, rather than user- interface implementation (Ritter, 2000)." 

        Although SCORM (sharable content object reference model) is becoming a de-facto reference 

model for integrating the various standards, there are still many unsolved issues including how to 

handle intellectual property rights in an environment of highly shared components and content, and 

how to include more pedagogically descriptive attributes in the standards. 

        Many  efforts  are  underway  to  develop  web-based  repositories  and  catalogues  that  give 

educators and learners easy access to a wide range of educational materials (including EDUTELLA 

(Nejdg et al. 2002); OCW & OKI (Kumar & Long 2002); GEM (Fitzgerald 2001); MERLOT (Wetzel 

2001)). These projects have begun to use meta-data standards to converge on common methods for 

describing  generic  attributes  of  learning  objects.  However,  this  software  is  currently  designed  to 

facilitates humans searching for educational material. The industry and the research communities are 

still far from having repositories of interoperable components that can be automatically retrieved and 

used by intelligent tutoring systems shells or authoring tools.  

        Given the advantages of ITS and adaptive learning systems, one would expect that this would 

not be the case � or that, at least, the standardization of sharable components and contents would be 

farther along than it is now. I will be looking further into how ITS authoring tools can leverage what 

is out there now to better support interoperability, manageability, usability, durability, scalability and 

accessibility in order to achieve the exciting gains and promise of truly intelligent tutoring systems.  

 

 

 

 

 



References 

Murray, T (2000). An Overview of Intelligent Tutoring System Authoring Tools, pp 493-542 

Ritter, S. & Blessing, S. (1998). Authoring tools for component-based learning environments. Journal 
of the Learning Sciences,. 7(1) pp. 107-132.  

Ritter, S. & Koedinger, K.R. (1997). An architecture for plug-in tutoring agents. In J. of Artificial 
Intelligence in Education, 7 (3/4) 315-347.  

Roschelle, J., Kaput, J., Stroup, W. & Kahn, T.M. (1998). Scaleable integration of educational 
software: Exploring the promise of component architectures. J. of Interactive Media in Education, 98 (6). 
[www-jime.open.ac.uk/96/6]  

Hodgins, W. & Massie, E. (2002). Making Sense of Learning Specifications and Standards: A 
decision makers guide to their adoption. MASIE Center technical report, Saratogy Springs, NS.  

Hodgins, W. et al. (2002). Making Sense of Learning Specifications & Standards: A Decision Maker's 
Guide to their Adoption. Industry Report by the MASIE Center: Saratoga Springs, NY. 

Murray, T (1998). A Model for Distributed Curriculum on the World Wide Web. J. of Interactive 
Media in Education 98(5). On-line journal at http://www-jime.open.ac.uk/. Murray, T. (1993).  

Formative Qualitative Evaluation for "Exploratory" ITS research. J. of AI in Education. 4(2/3), pp. 
179-207. Murray, T. (1996b). Toward a Conceptual Vocabulary for Intelligent Tutoring Systems. 



