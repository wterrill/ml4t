"""This is just a proof of concept of a slim stylo implementation
Using just the extract.py to extract features and scikit-learn to
do classfication and clustering

requirements:
    pip install -U scikit-learn
    pip install pandas
"""
import os
import pandas as pd
from db import *
from sklearn.cluster import KMeans
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.naive_bayes import GaussianNB
from sklearn.externals import joblib
import cPickle as pickle
from extract import *
import collections
import datetime

datastore = db()
feature_columns = ['corpus','models','author','target','doc','lex_diversity','mean_word_len',
                            'mean_sentence_len','std_sentence_len','mean_paragraph_len','document_len','commas',
                            'semicolons','quotations','exclamations','colons','hyphens','double_hyphens','and',
                            'but','however','if','that','more','must','might','this','very']

def extract_corpus_features(directory, corpus, from_documents=False):
    """Extracts features from all the files in a folder (corpus) to a list of features"""
    # retrive features from database
    directory = os.path.join(os.path.dirname(__file__),directory)
    data = []
    if from_documents == False:
        log( "[SA-ID] Checking database for features...")
        data = datastore.get_features(corpus)
        authors = datastore.get_authors(corpus)
        
    if data is not None and len(data) > 0:
        return data, authors
    features = []
    log( "[SA-ID] Processing corpus")
    for filename in [x for x in os.listdir(directory) if x.lower().endswith(".txt") or x.lower().endswith(".pdf")]:
        print "Processing", filename
        # extract features from document
        try:
            text = StyloDocument( os.path.join(directory,filename))
        except Exception as e:
            print str(e)
            continue
        if text.document_len < 1000:
            continue
        # convert extracted features to list
        feature_row = [a.replace(os.path.join(directory,filename),filename) for a in text.csv_output().split(',')]
        # append corpus identification to feature list
        feature_row = [corpus,0,0] + feature_row
        # append feature row from current document to larger list (essentially creating a table)
        if (filename.lower().endswith(".txt")):
            features.append(feature_row)
    features, authors = clean_features(pd.DataFrame(features))
    if (os.path.isfile(os.path.join(directory,"dtree.pkl")) and 
        os.path.isfile(os.path.join(directory,"etrees.pkl")) and
        os.path.isfile(os.path.join(directory,"gaussian.pkl")) and
        os.path.isfile(os.path.join(directory,"rforest.pkl"))):
        features.models = 1
    return features, authors;

def clean_features(features):
    """Used to convert the features to all floats"""
    if 'corpus' not in features.columns:
        # name columns
        features.columns = feature_columns

    # clean authors identification (student name)
    features['doc'] = features['doc'].astype(unicode).str.replace('"','')
    features['author'] = features['author'].astype(unicode).str.replace('"','')
    # extract authors from expected format <author>[_-__]<document>.txt
    for x in ['_','-','__']:    # list of author separators int he file name
        pattern = "(.+)\s?"+x+".+"
        features['author'] = np.where(features.doc.str.contains(x),
                                      features.doc.str.extract(pattern, expand=False).str.strip(),
                                      features.author)
    #features['author'] = features['target'].str.extract("(.+)\s?_.+", expand=False).str.strip()
    #features['author'] = features['target'].str.extract("(.+)\s?__.+", expand=False).str.strip() \
    #                        if features['author'].isnull().values.any() \
    #                        else features['author']
    #features['author'] = features['target'].str.extract("(.+)\s?-.+", expand=False).str.strip() \
    #                        if features['author'].isnull().values.any() \
    #                        else features['author']

    # make the authors categorical - will be used as target
    authors = pd.DataFrame(features['author'].unique(),columns=['author'])
    authors["corpus"] = features['corpus'][0]
    authors_dict = authors['author'].to_dict()
    category_lookup = dict((v,k) for k,v in authors_dict.iteritems())    
    # build numerical target by converting the categories to numeric
    features['target'] = np.nan
    features['target'] = features['author'].map(category_lookup)
    features['target'].astype('int64')
    authors['target'] = authors['author'].map(category_lookup)
    authors['target'].astype('int64')  
    #authors = features[['target','author']].to_dict()['author']
    return features, authors

def normalize_features(features):
    """Used to normalize the features to prevent bias"""
    pass

def select_features(features):
    """Used to do feature transformation and reduction""" 
    pass

def build_model_cluster(df):
    """Build and return model using kmeans clustring"""
    # normalize features
    features = (df - df.mean()) / (df.max() - df.min())
    km = KMeans(n_clusters=8, init='k-means++', n_init=10, verbose=0)
    return km.fit(features)

def build_model_dtree(features,labels):
    """Build and return decision tree classifier"""
    clf = DecisionTreeClassifier(max_depth=None, min_samples_split=1,random_state=0)
    clf = clf.fit(features, labels)
    return clf

def build_model_randomforest(features,labels):
    """Build and return decision tree classifier"""
    clf = RandomForestClassifier(n_estimators=10, max_depth=None,min_samples_split=1, random_state=0)
    clf = clf.fit(features, labels)
    return clf

def build_model_extratrees(features,labels):
    """Build and return decision tree classifier"""
    clf = ExtraTreesClassifier(n_estimators=10, max_depth=None,min_samples_split=1, random_state=0)
    clf = clf.fit(features, labels)
    return clf

def build_model_adaboost(features,labels):
    """Build and return decision tree classifier"""
    clf = AdaBoostClassifier()
    clf = clf.fit(features, labels)
    return clf

def build_model_NN(features,labels):
    """Build and return a neural Network classifier"""
    clf = MLPClassifier(algorithm='l-bfgs', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1)
    clf = clf.fit(features, labels)
    return clf

def build_model_MNB(features,labels):
    """Build and return a Naive Bayes classifier"""
    clf = MultinomialNB()
    clf = clf.fit(features, labels)
    return clf

def build_model_GNB(features,labels):
    """Build and return a Naive Bayes classifier"""
    clf = GaussianNB()
    clf = clf.fit(features, labels)
    return clf

def predict_proba(model,features):
    return model.predict(features),model.predict_proba(features)

def predict_cluster(model,features):
    model.predict(features), model.score(features)

def extract_features_from_file(filename, corpus):
    """extract features from a specific file and save to corpus"""
    print "[SA-ID] Extracting Features:",filename
    directory = os.path.join(os.path.dirname(__file__),"corpus",corpus)
    file = os.path.join(directory,filename)
    text = StyloDocument( file )
    print "[SA-ID] convert extracted features to list"
    feature_row = list([corpus,0,0])
    feature_row.extend([a.replace(file,filename) for a in text.csv_output().split(',')])
    print "[SA-ID] pull features from database"
    features = datastore.get_features(corpus)
    features.loc[len(features)] = feature_row
    features, authors = clean_features(pd.DataFrame(features))
    print "[SA-ID] save new feature set for corpus", corpus
    datastore.save_features(features,authors,corpus)

def predict_from_file(filename,corpus):
    """Run a file through the predictive models"""
    result = None
    print "[SA-ID] Identifying file:",filename
    directory = os.path.join(os.path.dirname(__file__),"corpus",corpus)
    try:
        file = os.path.join(directory,"identify",filename)
        text = StyloDocument( file )
    except:
        file = os.path.join(directory,filename)
        text = StyloDocument( file )
    # convert extracted features to list
    feature_row = [a.replace(file,filename) for a in text.csv_output().split(',')]
    file = None
    # extract train and labels
    train = np.array(feature_row[2:])
    train = train.astype(np.float).reshape(1,-1) #the reshape is needed because of a deprecation in scikit-learn
    # load models from disk
    try:
        dtree_model = pickle.load( open( os.path.join(directory,"dtree.pkl"), "rb" ) )
        rforest_model = pickle.load( open( os.path.join(directory,"rforest.pkl"), "rb" ) )
        etrees_model = pickle.load( open( os.path.join(directory,"etrees.pkl"), "rb" ) )
        gaussianNB = pickle.load( open( os.path.join(directory,"gaussian.pkl"), "rb" ) )
        # this anonymous function will be used to format the results
        format_results = lambda x,y : dict((x[ix], y[ix]) for ix in range(len(y)))
        # load real authors from database
        authors = datastore.get_authors(corpus)
        authors = collections.OrderedDict((v,k) for k,v in dict(authors[['author','target']].values).iteritems())
        authors = [y for x,y in sorted( authors.iteritems())]
        result = None
        if authors is not None:
            # do predictions on this file
            p,d = predict_proba(dtree_model,train)
            A = pd.DataFrame({"dtree":format_results( authors, d[0])} )
            p,d = predict_proba(rforest_model,train)
            B = pd.DataFrame({"rforest":format_results( authors, d[0])} )
            p,d = predict_proba(etrees_model,train)
            C = pd.DataFrame({"etrees":format_results( authors, d[0])} )
            p,d = predict_proba(gaussianNB,train)
            D = pd.DataFrame({"guassian":format_results( authors, d[0])} )
            # join results and display in table
            result = A.join(B)
            result = result.join(C)
            result = result.join(D)
        else:
            print "Cannot pull Authors"
        print result[result.sum(axis=1)>0.05]
    except Exception as e:
        print e
    return result[result.sum(axis=1)>0.05]

def refresh_corpus(corpus, from_documents = True):
    """This processes all the documents in a corpus(folder) and creates
       models which it saves in the same folders for later loading 
    """
    data, authors = extract_corpus_features(os.path.join("corpus",corpus), corpus , from_documents)
    if from_documents:
       datastore.save_features(data.ix[:,"corpus":],authors)

def log(text):
    print datetime.datetime.now(), text 

def process_corpus(corpus, from_documents = True):
    """This processes all the documents in a corpus(folder) and creates
       models which it saves in the same folders for later loading 
    """
    log("Extracting corpus features") 
    data, authors = extract_corpus_features(os.path.join("corpus",corpus), corpus , from_documents)
    
    # get training set and labels
    log("Training set and labels")
    train = data.ix[:,'lex_diversity':].astype(float).values
    labels = data['target'].astype(int).values
    # train models
    log("Building models")
    dtree_model = build_model_dtree(train,labels)
    rforest_model = build_model_randomforest(train,labels)
    etrees_model = build_model_extratrees(train,labels)
    gaussianNB = build_model_GNB(train,labels)
    # save models
    log("Saving models")
    directory = os.path.join(os.path.dirname(__file__),"corpus",corpus)
    pickle.dump( dtree_model, open( os.path.join(directory,"dtree.pkl"), "wb" ) )
    pickle.dump( rforest_model, open( os.path.join(directory,"rforest.pkl"), "wb" ) )
    pickle.dump( etrees_model, open( os.path.join(directory,"etrees.pkl"), "wb" ) )
    pickle.dump( gaussianNB, open( os.path.join(directory,"gaussian.pkl"), "wb" ) )
    data['models'] = 1
    data.columns = feature_columns
    if from_documents:
       datastore.save_features(data.ix[:,"corpus":],authors)
    log("done")

def testing():
    """This is used to test out shit!"""
    #clean up the features extracted
    data, authors = clean_features(data)
    print data
    # Store features DataFrame to database
    datastore.save_features(data.ix[:,'corpus':])
    # save features to .csv for external analysis (This is for you WILL :o))
    data.ix[:,'corpus':].to_csv(corpus+".csv",index=False)
    
    # extract training set, labels, test set
    train = data.ix[:,'lex_diversity':].astype(float).values
    labels = data['target'].astype(int).values
    train_set, test_set, labels_set = train[:-1],train[-1:],labels[:-1]
    
    # this anonymous function will be used to format the results
    format_results = lambda x,y : dict((x[ix], y[ix]) for ix in range(len(y)))
    
    #clust_model = build_model_cluster(train_set)
    #p,d = predict_cluster(clust_model,test_set)
    
    # build decision tree model, test using test set, and store results in data frame
    model = build_model_dtree(train_set,labels_set)
    p,d = predict_proba(model,test_set)
    A = pd.DataFrame({"dtree":format_results( authors, d[0])} )
    
    # build random forest model, test using test set, and store results in data frame
    model = build_model_randomforest(train_set,labels_set)
    p,d = predict_proba(model,test_set)
    B = pd.DataFrame({"randomforest":format_results( authors, d[0])} )
    
    # build extra tree model, test using test set, and store results in data frame
    model = build_model_extratrees(train_set,labels_set)
    p,d = predict_proba(model,test_set)
    C = pd.DataFrame({"extratrees":format_results( authors, d[0])} )

    # build ada boost model, test using test set, and store results in data frame
    model = build_model_adaboost(train_set,labels_set)
    p,d = predict_proba(model,test_set)
    G = pd.DataFrame({"adaboost":format_results( authors, d[0])} )

    model = build_model_MNB(train_set,labels_set)
    p,d = predict_proba(model,test_set)
    D = pd.DataFrame({"MNB":format_results( authors, d[0])} )

    model = build_model_GNB(train_set,labels_set)
    p,d = predict_proba(model,test_set)
    E = pd.DataFrame({"GNB":format_results( authors, d[0])} )
    
    # join results and display in table
    result = A.join(B)
    result = result.join(C)
    #result = result.join(D)
    result = result.join(E)
    result = result.join(G)
    print result

if __name__=='__main__':
    #the below needs to move to a function that's callable from the web service
    corpus = "sample2"
    process_corpus(corpus, False)
    predict_from_file("MatthewCohen _assignmentx.txt",corpus)
    predict_from_file("BeasleyMatthew_ProjectProposal.txt",corpus)
    predict_from_file("BSRKAditya_ProjectProposal.txt",corpus)
    predict_from_file("ChandaSandeep_FinalProposal.txt",corpus)
    predict_from_file("AslanBrooke_Assignment6.txt",corpus)
    predict_from_file("MatthewCohen _assignmentx.txt",corpus)
    corpus = "speeches"
    process_corpus(corpus, False)
    predict_from_file("bernie-speech1.txt",corpus)
    predict_from_file("hillary-speech2.txt",corpus)
    predict_from_file("trump-speech2.txt",corpus)
    predict_from_file("hillary-speech3.txt",corpus)
    

    
