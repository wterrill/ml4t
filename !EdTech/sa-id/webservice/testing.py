import os
import pandas as pd
from stylo.db import *
import json

base_path = os.path.join( os.path.dirname(os.path.abspath(__file__)), "stylo\corpus")

if __name__ == "__main__":
    data = {}
    datastore = db()
    d = datastore.get_corpus_all()
    for c in d['corpus'].unique():
        data[c] = d[d.corpus==c].to_dict(orient='records')
    for p in os.listdir(base_path):
        if os.path.isdir(os.path.join(base_path, p)):
            file_count = len( filter(lambda file: os.path.isfile(os.path.join(base_path, p, file)) and os.path.splitext(file)[1] in ['.txt'] ,os.listdir(os.path.join(base_path, p))))
            data[p] = {"files":file_count, "processed":d[d.corpus==p].iloc[0].qty} if len(d[d.corpus==p]) > 0 else 0
    data = pd.DataFrame(data)
    print data
