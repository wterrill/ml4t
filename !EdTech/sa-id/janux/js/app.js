var app = angular.module('sa-id', ['ngFileUpload' ]);

//# routing done here
//app.config(function () {
//    
//});

app.controller("main", function ($scope, Upload, $http) {
    $scope.values = "";
    $scope.base_url = "http://52.11.104.178:5000";
    $scope.base_url = "http://localhost:5000";
    $scope.username = "test";
    $scope.progress = 0;
    $scope.corpus_data = {};
    $scope.username = "will";
    $scope.upload_alert = false;

    $scope.corpus = function () {
        url = $scope.base_url + '/corpus';
        $http.get(url)
                .then(function (response) {
                    console.log(response.data);
                    for (var key in response.data)
                    {
                        console.log(key);
                        console.log(response.data[key]["files"]);
                        response.data[key]["message"] = "";
                    }
                    $scope.corpus_data = response.data;
                }, function (response) {
                    console.log(response);
                });
    };

    // upload on file select or drop !!NOT USED
    $scope.upload = function (file) {
        Upload.upload({
            url: $scope.base_url + '/upload',
            data: {file: file, 'username': $scope.username}
        }).then(function (resp) {
            console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });
    };

    // for multiple files:
    $scope.uploadFiles = function (files, corpus) {
        $scope.upload_result = "Uploading...";
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                console.log(corpus)
                Upload.upload({
                    url: $scope.base_url + '/upload',
                    data: {file: files[i], 'username': $scope.username, 'corpus':corpus}
                }).then(function (resp) {
                    console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                    $scope.upload_result = resp.data;
                    $scope.upload_alert = true;
                    $scope.upload_result = "";
                }, function (resp) {
                    console.log('Error status: ' + resp.status);
                }, function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    $scope.progress = progressPercentage + '%';
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                });
            }
        }
    };


    $scope.compute_stdev = function (values) {
        url = $scope.base_url + '/stdev/' + values;
        console.log(url);
        $scope.results = "working...";

        $http.get(url)
                .then(function (response) {
                    $scope.results = response.data.hasOwnProperty("result")     //First function handles success
                            ? "Stdev: " + response.data.result
                            : "";
                }, function (response) {                                        //Second function handles error
                    console.log(response);
                    $scope.results = "Cannot contact the 'sa-id' web service";
                });
    };
    
    $scope.corpus();
});
