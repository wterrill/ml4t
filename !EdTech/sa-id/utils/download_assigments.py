"""This program is used to download the assignments from edtech
http://zevross.com/blog/2014/05/16/using-the-python-library-beautifulsoup-to-extract-data-from-a-webpage-applied-to-world-cup-rankings/

requirements
pip install mechanize
pip install cookielib
pip install BeautifulSoup
"""

from BeautifulSoup import BeautifulSoup
import mechanize
import cookielib
import os

cj = cookielib.CookieJar()
br = mechanize.Browser()
br.set_cookiejar(cj)
br.set_handle_refresh(False)
br.open("https://login.gatech.edu/cas/login")
    
br.select_form(nr=0)
br.form['username'] = raw_input("Enter GT username:")
br.form['password'] = raw_input("Password:")
br.submit()

#print br.response().read()

baseurl = 'https://files.t-square.gatech.edu/access/content/group/gtc-05a5-4f8e-5d77-a312-ae8ea404668b/Course%20Assignment%20Library/'
page = br.open(baseurl)
#page = urllib2.urlopen(url)
soup = BeautifulSoup(page.read())
#print soup
listings = soup.findAll("li", {"class": "folder"})
if len(listings) > 0:
    for listing in listings:
        folder_url = baseurl + listing.a['href']
        corpus = listing.a.contents[0]
        corpus = corpus.replace(",","").replace(" ","")
        page = br.open(folder_url)
        soup = BeautifulSoup(page.read())
        assignments = soup.findAll("li", {"class": "file"})
        print "Downloading", corpus
        for assignment in assignments:
            file_url = folder_url + assignment.a['href']
            filename = corpus+"/"+assignment.a.contents[0]
            if not os.path.exists(corpus):
                os.makedirs(corpus)
            print " ...",assignment.a.contents[0]
            br.retrieve(file_url,filename)
else:
    print "Cannot access corpus"
