

import numpy as np
import random as rand

class QLearner(object):
    # set dyna back to zero for straight qlearning
    def __init__(self, \
        num_states = 100, \
        num_actions = 4, \
        alpha = 0.2, \
        gamma = 0.9, \
        rar = 0.5, \
        radr = 0.99, \
        dyna = 200, \
        verbose = False):

        self.verbose = verbose
        self.num_actions = num_actions
        self.num_states = num_states
        self.s = 0
        self.a = 0
        self.rar = rar
        self.radr = radr
        self.gamma = gamma
        self.alpha = alpha
        self.dyna = dyna

        # initialize structures
        self.T = []
        self.Q = np.random.normal(loc=0,scale=1.0,size=(num_states,num_actions))
        self.R = np.zeros(shape=(num_states,num_actions))

    def querysetstate(self, s):
        """
        @summary: Update the state without updating the Q-table
        @param s: The new state
        @returns: The selected action
        """
        self.s = s
        #action = rand.randint(0, self.num_actions-1)
        action = np.argmax(self.Q[s,:], axis=0)
        if self.verbose:
            print "s =", s,"a =",action
        return action

    def query(self,s_prime,r):
        """
        @summary: Update the Q table and return an action
        @param s_prime: The new state
        @param r: The ne state
        @returns: The selected action
        """
        s = self.s
        a = self.a
        sp = s_prime
        alpha = self.alpha
        gamma = self.gamma
        num_states = self.num_states
        num_actions = self.num_actions

        self.Q[s,a] = (1-self.alpha)*self.Q[s,a] + alpha * (r + self.gamma * self.Q[sp,np.argmax(self.Q[sp])])

        x = np.random.multinomial(9,[self.rar,(1-self.rar)],size=1).argmax()
        self.rar = self.rar * self.radr

        if x == 0:
            action = rand.randint(0, self.num_actions-1)
        else:
            action = np.argmax(self.Q[sp], axis=0)

        self.s = s_prime
        self.a = action

        if self.dyna == 0:
            return action
        self.R[s,a] = (1-self.alpha)*self.R[s,a] + self.alpha * r
        self.T.append((s,a,sp,self.R[s,a]))
        self.T = self.T[-self.dyna:]
        h = len(self.T)
        for i in xrange(self.dyna):
            select = self.T[np.random.randint(0,h)]
            s= select[0]
            a= select[1],
            sp = select[2],
            r =  select[3]
            self.Q[s,a] = (1-self.alpha)*self.Q[s,a] + alpha * (r + gamma * self.Q[sp,np.argmax(self.Q[sp])])

        if self.verbose: print "s =", s_prime,"a =",action,"r =",r
        return action

if __name__=="__main__":
    print "Remember Q from Star Trek? Well, this isn't him"
