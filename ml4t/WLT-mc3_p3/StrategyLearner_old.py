import numpy as np
import random as rand
import time
import math
import QLearner as ql
import os
import pandas as pd
import datetime as dt


def get_data(symbols, dates, addSPY=True):
    """Read stock data (adjusted close) for given symbols from CSV files."""
    df = pd.DataFrame(index=dates)
    if addSPY and 'SPY' not in symbols:  # add SPY for reference, if absent
        symbols = ['SPY'] + symbols

    for symbol in symbols:
        df_temp = pd.read_csv(symbol_to_path(symbol), index_col='Date',
                parse_dates=True, usecols=['Date', 'Adj Close'], na_values=['nan'])
        df_temp = df_temp.rename(columns={'Adj Close': symbol})
        df = df.join(df_temp)
        if symbol == 'SPY':  # drop dates SPY did not trade
            df = df.dropna(subset=["SPY"])
    return df

def print_full(x):
    pd.set_option('display.max_rows', len(x))
    print(x)
    pd.reset_option('display.max_rows')

def initializedata(stocks,name,rolling_window, n_days):
        stocks['SMA'] = pd.rolling_mean(stocks[name[0]],rolling_window, min_periods=1)
        stocks['stdev'] = pd.rolling_std(stocks[name[0]],rolling_window, min_periods=1)
        stocks = stocks.dropna()
        stocks['bb_value'] = (stocks[name[0]] - stocks['SMA'])/(2 * stocks['stdev'])
        stocks['momentum'] = (stocks[name[0]]/stocks[name[0]].shift(n_days))-1
        stocks['daily_return'] = (stocks[name[0]]/stocks[name[0]].shift(1))-1
        stocks['volat'] = pd.rolling_std(stocks['daily_return'],rolling_window,min_periods=1)
        stocks['P/SMA'] = stocks[name[0]]/stocks['SMA']
        stocks = stocks.dropna()
        del stocks['SMA']
        del stocks['bb_value']
        del stocks['volat']
        return stocks

def symbol_to_path(symbol, base_dir=os.path.join("..", "data")):
    """Return CSV file path given ticker symbol."""
    return os.path.join(base_dir, "{}.csv".format(str(symbol)))

def discretize(data,steps):
    threshold = np.zeros(steps)
    stepsize = len(stocks)/steps
    sorted = data.sort_values(axis=0, ascending=True, inplace=False, kind='quicksort', na_position='last')
    for i in range(0,steps):
        threshold[i]=sorted[(i+1)*stepsize]
    j=0
    for i in range(0,len(data)):
        beer = data.ix[i]
        while data.ix[i] > threshold[j]:
            ice = threshold[j]
            j += 1
            if j > 9:
                j=9
                break
        data.ix[i] = j
        j=0

    return data



# find where the robot is in the map
def getstockpos(date,data,columns,holding):
    i = 0
    j = np.zeros(len(stocks))
    k = 1
    for column in columns:
        i = data.ix[date,column] * k + i
        k = k*10
    return i + (holding * k)




# move the robot according to the action and the map

def movestock(stocks,action,state,date,stock_amount,columns,cash):
    #actions -> 0 do nothing 1 = buy 2 = sell

    # update the test location
    if action == 0: #do nothing
        if state > 999:
            holding = 1
            stock_equity = stock_amount * stocks.ix[date]['IBM']
        else:
            holding = 0
            stock_equity = stock_amount * stocks.ix[date]['IBM']

    elif action == 1 and state <1000: #enter sell
        holding = 1
        stock_amount += 100
        stock_equity = stock_amount * stocks.ix[date]['IBM']
        cash -= 100 * stocks.ix[date]['IBM']

    elif action == 1 and stock_amount == -100: #exit from sell
        holding = 0
        stock_amount += 100
        stock_equity = stock_amount * stocks.ix[date]['IBM']
        cash -= 100 * stocks.ix[date]['IBM']

    elif action == 2 and state <1000: #enter sell
        holding = 1
        stock_amount -= 100
        stock_equity = stock_amount * stocks.ix[date]['IBM']
        cash += 100 * stocks.ix[date]['IBM']

    elif action == 2 and stock_amount == 100: #exit from buy
        holding = 0
        stock_amount -= 100
        stock_equity = stock_amount * stocks.ix[date]['IBM']
        cash += 100 * stocks.ix[date]['IBM']

    elif state > 999:
        holding = 1
        stock_equity = stock_amount * stocks.ix[date]['IBM']


    newstate = getstockpos(date,stocks,columns,holding)
    return stock_equity,cash,holding, stock_amount, newstate

# convert the location to a single integer


# run the code to test a learner
if __name__=="__main__":

    verbose = False #print lots of debug stuff if True

    # read in the stock
    name = ['IBM']
    sd = dt.datetime(2008,1,1)
    ed = dt.datetime(2009,1,1)

    stocks = get_data(name, pd.date_range(sd, ed), addSPY=True)
    del stocks['SPY']

    rolling_window = 20
    n_days = 5
    stocks = initializedata(stocks,name,rolling_window,n_days)

    steps = 10
    stocks['momentum'] = discretize(stocks['momentum'],steps)
    stocks['stdev'] = discretize(stocks['stdev'],steps)
    stocks['P/SMA'] = discretize(stocks['P/SMA'],steps)

    #positions = discretize(stocks,10) #create gameboard

    #if verbose: printmap(stocks)

    rand.seed(5)

    learner = ql.QLearner(num_states=2000,\
        num_actions = 3, \
        rar = 0.98, \
        radr = 0.9999, \
        verbose=verbose) #initialize the learner

    #each iteration involves one trip to the goal
    stocks['trade'] = 0

    for iteration in range(0, 1000):
        steps = 0
        #data = originalmap.copy()
        #robopos = startpos
        columns = ['momentum','stdev', 'P/SMA']
        current_date = pd.Timestamp(stocks.index.get_values()[0]).date()
        holding = 0
        stock_amount = 0
        cumulative = 0
        state = getstockpos(current_date,stocks,columns,holding) #convert the location to a state
        action = learner.querysetstate(state) #set the state and get first action
        port_val = 10000
        cash = 10000
        stock_equity = 0

        if action == 1:
            stock_amount = 100
            stock_equity = stock_amount * stocks.ix[0]['IBM']
            cash = cash - stock_equity
            state = state + 1000

        if action == 2:
            stock_amount = -100
            stock_equity = stock_amount * stocks.ix[0]['IBM']
            cash = cash - stock_equity
            state = state + 1000

        port_val = cash + stock_equity
        x=0
        stocks.ix[x,'cash'] = cash
        stocks.ix[x,'stock_equity'] = stock_equity
        stocks.ix[x,'stock_amount'] = stock_amount
        stocks.ix[x, 'port_val']=port_val
        stocks.ix[x,'trade'] = action

        for x in range(0,len(stocks)):
            #actions -> 0 do nothing 1 = buy 2 = sell

            current_date = pd.Timestamp(stocks.index.get_values()[x]).date()

            stock_equity, cash, holding,stock_amount,state = movestock(stocks,action,state,current_date,stock_amount,columns,cash)

            r = ((cash + stock_equity)/port_val)-1
            port_val = cash + stock_equity

            action = learner.query(state,r)
            steps += 1
            stocks.ix[x,'trade'] = action
            stocks.ix[x,'cum_return']=r
            stocks.ix[x,'cash'] = cash
            stocks.ix[x,'stock_equity'] = stock_equity
            stocks.ix[x,'stock_amount'] = stock_amount
            stocks.ix[x, 'port_val']=port_val

        print iteration, "," , steps
        print port_val
        #print stocks
    #printmap(data)
