"""
Test a Q Learner in a navigation problem.  (c) 2015 Tucker Balch
"""

import numpy as np
import random as rand
import time
import math
import QLearner as ql
import pandas as pd
import os
import StrategyLearner_old as sl
import datetime as dt

def run():
 learner = sl.StrategyLearner(verbose = False) # constructor
 learner.addEvidence(symbol="IBM", sd=dt.datetime(2008,1,1), ed=dt.datetime(2009,1,1), sv = 10000) # training step
 df_trades = learner.testPolicy(symbol = "IBM", sd=dt.datetime(2009,1,1), ed=dt.datetime(2010,1,1), sv = 10000) # testing step

 # put together some charts and analytics here

# run the code to test a learner
if __name__=="__main__":
 run()