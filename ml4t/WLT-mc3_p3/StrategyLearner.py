"""
Template for implementing StrategyLearner  (c) 2016 Tucker Balch
"""

import datetime as dt
import QLearner as ql
import pandas as pd
import util as ut
import StrategyLearner as sl
import numpy as np
import datetime
import os


def print_full(x):
    pd.set_option('display.max_rows', len(x))
    print(x)
    pd.reset_option('display.max_rows')

def initializedata(stocks,name,rolling_window, n_days):
        stocks['SMA'] = pd.rolling_mean(stocks[name[0]],rolling_window, min_periods=1)
        stocks['stdev'] = pd.rolling_std(stocks[name[0]],rolling_window, min_periods=1)
        stocks = stocks.dropna()
        stocks['bb_value'] = (stocks[name[0]] - stocks['SMA'])/(2 * stocks['stdev'])
        stocks['momentum'] = (stocks[name[0]]/stocks[name[0]].shift(n_days))-1
        stocks['daily_return'] = (stocks[name[0]]/stocks[name[0]].shift(1))-1
        stocks['volat'] = pd.rolling_std(stocks['daily_return'],rolling_window,min_periods=1)
        stocks['P/SMA'] = stocks[name[0]]/stocks['SMA']
        stocks = stocks.dropna()
        del stocks['SMA']
        del stocks['bb_value']
        del stocks['volat']
        return stocks

def discretize(data,steps):

    threshold = np.zeros(steps)
    stepsize = len(data)/steps
    sorted = data.sort_values(axis=0, ascending=True, inplace=False, kind='quicksort', na_position='last')
    for i in range(0,steps):
        threshold[i]=sorted[(i+1)*stepsize]

    bin = []
    for x in range(1,steps):
        bin.append(threshold[x])

    data = np.digitize(data,bin, right = False)


    return data

def movestock(stocks,action,state,date,stock_amount,columns,cash,symbol):
    #actions -> 0 do nothing 1 = buy 2 = sell

    # update the test location
    if action == 0: #do nothing
        if state > 999:
            holding = 1
            stock_equity = stock_amount * stocks.ix[date][symbol]
        else:
            holding = 0
            stock_equity = stock_amount * stocks.ix[date][symbol]

    elif action == 1 and state <1000: #enter sell
        stock_amount += 100
        stock_equity = stock_amount * stocks.ix[date][symbol]
        cash -= 100 * stocks.ix[date-1][symbol]
        holding = 1


    elif action == 1 and stock_amount == -100: #exit from sell
        stock_amount += 100
        stock_equity = stock_amount * stocks.ix[date][symbol]
        cash -= 100 * stocks.ix[date-1][symbol]
        holding = 0



    elif action == 2 and state <1000: #enter sell
        stock_amount -= 100
        stock_equity = stock_amount * stocks.ix[date][symbol]
        cash += 100 * stocks.ix[date-1][symbol]
        holding = 1


    elif action == 2 and stock_amount == 100: #exit from buy
        stock_amount -= 100
        stock_equity = stock_amount * stocks.ix[date][symbol]
        cash += 100 * stocks.ix[date-1][symbol]
        holding = 0



    elif state > 999:
        holding = 1
        stock_equity = stock_amount * stocks.ix[date][symbol]


    newstate = getstockpos(date,stocks,columns,holding)
    return stock_equity,cash,holding, stock_amount, newstate

def getstockpos(date,data,columns,holding):
    i = 0
    j = np.zeros(len(data))
    k = 1
    for column in columns:
        i = data.ix[date,column] * k + i
        k = k*10
    return i + (holding * k)


class StrategyLearner(object):

    # constructor
    def __init__(self, verbose = False):
        self.verbose = verbose

        self.learner = ql.QLearner(num_states=2000,\
        num_actions = 3, \
        rar = 0.98, \
        radr = 0.9999,\
        dyna=0,\
        verbose=verbose)


    # this method should create a QLearner, and train it for trading
    def addEvidence(self, symbol = "IBM", \
        sd=dt.datetime(2008,1,1), \
        ed=dt.datetime(2009,1,1), \
        sv = 10000): 


        # add your code to do learning here

        syms=[symbol]
        dates = pd.date_range(sd, ed)
        prices_all = ut.get_data(syms, dates)  # automatically adds SPY
        stocks = prices_all[syms]  # only portfolio symbols
        #prices_SPY = prices_all['SPY']  # only SPY, for comparison later
        if self.verbose: print stocks

        #del stocks['SPY']

        rolling_window = 20
        n_days = 5
        stocks = initializedata(stocks,syms,rolling_window,n_days)
        iteration = 0
        steps = 10
        stocks['momentum'] = discretize(stocks['momentum'],steps)
        stocks['stdev'] = discretize(stocks['stdev'],steps)
        stocks['P/SMA'] = discretize(stocks['P/SMA'],steps)

    #each iteration involves one trip to the goal
        stocks['trade'] = 0

        conv = [10000,20000,300000,400000,500000]

        while np.std(conv) > 500:
            #for iteration in range(0, 500):
            steps = 0
            #data = originalmap.copy()
            #robopos = startpos
            columns = ['momentum','stdev', 'P/SMA']

            #current_date = pd.Timestamp(stocks.index.get_values()[0]).date()
            x = 0
            holding = 0
            stock_amount = 0
            cumulative = 0

            state = getstockpos(0,stocks,columns,holding) #convert the location to a state

            action = 1 #self.learner.querysetstate(state) #set the state and get first action
            port_val = sv
            cash = sv
            stock_equity = 0

            stock_amount = 100
            stock_equity = stock_amount * stocks.ix[0][symbol]

            cash = cash - stock_equity
            state = state + 1000

            port_val = cash + stock_equity
            #stocks.ix[x,'cash'] = cash
            #stocks.ix[x,'stock_equity'] = stock_equity
            #stocks.ix[x,'stock_amount'] = stock_amount
            #stocks.ix[x, 'port_val']=port_val
            #stocks.ix[x,'trade'] = action



            #start =  datetime.datetime.now()
            for x in range(1,len(stocks)):
                #actions -> 0 do nothing 1 = buy 2 = sell

                #current_date = pd.Timestamp(stocks.index.get_values()[x]).date()

                stock_equity, cash, holding,stock_amount,state = movestock(stocks,action,state,x,stock_amount,columns,cash,symbol)

                r = ((cash + stock_equity)/port_val)-1
                if r < 0:
                    r = r - 2
                port_val = cash + stock_equity

                action = self.learner.query(state,r)
                steps += 1
                #stocks.ix[x,'trade'] = action
                #stocks.ix[x,'cum_return']=r
                #stocks.ix[x,'cash'] = cash
                #stocks.ix[x,'stock_equity'] = stock_equity
                #stocks.ix[x,'stock_amount'] = stock_amount
                #stocks.ix[x, 'port_val']=port_val
                #print " time"
                #print datetime.datetime.now() - start
            conv.append(port_val)
            conv = conv[-5:]
            iteration += 1

            print iteration, "," , steps
            print port_val
            self.model = stocks



  
        # example use with new colname 
        #volume_all = ut.get_data(syms, dates, colname = "Volume")  # automatically adds SPY
        #volume = volume_all[syms]  # only portfolio symbols
        #volume_SPY = volume_all['SPY']  # only SPY, for comparison later
        #if self.verbose: print volume

    # this method should use the existing policy and test it against new data
    def testPolicy(self, symbol = "IBM", \
        sd=dt.datetime(2009,1,1), \
        ed=dt.datetime(2010,1,1), \
        sv = 10000):

        # here we build a fake set of trades
        # your code should return the same sort of data
        dates = pd.date_range(sd, ed)
        stocks = ut.get_data([symbol], dates)  # automatically adds SPY
        trades = stocks[[symbol,]]  # only portfolio symbols
        trades_SPY = stocks['SPY']  # only SPY, for comparison later

        rolling_window = 20
        n_days = 5
        syms = [symbol]
        stocks = initializedata(stocks,syms,rolling_window,n_days)

        steps = 10
        stocks['momentum'] = discretize(stocks['momentum'],steps)
        stocks['stdev'] = discretize(stocks['stdev'],steps)
        stocks['P/SMA'] = discretize(stocks['P/SMA'],steps)

        columns = ['momentum','stdev', 'P/SMA']


        holding = 0
        stock = 0
        cash = sv
        stock_equity = 0
        trades.values[:,:]=0


        for x in range(0,len(stocks)):
            current_date = pd.Timestamp(stocks.index.get_values()[x]).date()
            price = stocks.ix[x,symbol]
            state = getstockpos(x,stocks,columns,holding) #convert the location to a state
            action = self.learner.querysetstate(state) #set the state and get first action
            if x == 0:
                action = 1
            if action == 1 and stock == 0 and holding == 0: #enter buy
                holding = 1
                stock = 100
                trade = 100
                cash = cash - 100*price

            elif action == 1 and stock == -100 and holding == 1: # exit sell

                holding = 0
                stock = 0
                trade = 100
                cash = cash - 100*price


            elif action == 2 and stock == 0 and holding == 0: #enter sell

                holding = 1
                stock = -100
                trade = -100
                cash = cash + 100*price


            elif action == 2 and stock == 100 and holding == 1: # exit buy

                holding = 0
                stock = 0
                trade = -100
                cash = cash + 100*price


            elif action == 0:
                #print "change nothing"
                trade = 0
            elif action > 0 and holding == 1:
                trade = 0

            else:
                print "there has been an error"

            if x == len(stocks)-1:
                if stock < 0:
                    trade = 100

                if stock > 0:
                    trade = -100

            trades.values[x+n_days+1,:] = trade # set them all to nothing #the +6 is for the fa
            #print x


            #print "cash =", cash
            #print "stock equity", price*stock
        cum = cash + stock * price
        #print cum


        if self.verbose: print type(trades) # it better be a DataFrame!
        if self.verbose: print trades
        if self.verbose: print stocks

        return trades

if __name__=="__main__":


    verbose = False #print lots of debug stuff if True

    learner = StrategyLearner()

    StrategyLearner.addEvidence(learner, symbol = "IBM", sd=dt.datetime(2008,1,1),  ed=dt.datetime(2009,1,1), sv = 10000)

    df_trades = StrategyLearner.testPolicy(learner, symbol = "IBM", sd=dt.datetime(2008,1,1), ed=dt.datetime(2009,1,1), sv = 10000) # testing step
    print df_trades

    #orders=[]
    #for x in range(0,len(df_trades)):
    #    print "hello"

    #    if df_trades.value[x] == 100:
    #        orders.append(pd.Timestamp(df_trades.index.get_values()[x]).date())
    #        orders.append("BUY")
    #    if df_trades.value[x] == -100:
    #        orders.append(pd.Timestamp(df_trades.index.get_values()[x]).date())
    #        orders.append("BUY")


    #finalOrder=np.array(orders)
    #finalOrder = finalOrder.reshape(len(finalOrder)/2,2)
    #finalOrder = pd.DataFrame(finalOrder)
    #finalOrder.insert(1,'Symbol',name)
    #finalOrder.insert(3,'Shares',100)
    #finalOrder.columns=['Date','Symbol','Order', 'Shares']
    #finalOrder.set_index('Date',drop=True, inplace=True)
    #create orders folder if it does not exist
    #if not os.path.exists("orders"):
    #    print "...creating orders directory"
    #    os.makedirs("orders")
    #finalOrder.to_csv('./orders/orders.csv')

    df_trades = StrategyLearner.testPolicy(learner, symbol = "IBM", sd=dt.datetime(2009,1,1), ed=dt.datetime(2010,1,1), sv = 10000)
    print df_trades



