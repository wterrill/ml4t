# Figuring out transpositions from 7-Optimizer.py
import numpy as np

array1 = np.arange(4.0)
print array1

array2 = np.arange(4.0) + 10
print array2

array3 = np.asarray([array1, array2])
print array3
print array3.T

array4 = [array1, array2]
print array4

array5 = np.asarray(array4)
print array5
print array5.T

print array5.T[:,0]
print array5.T[:,1]


