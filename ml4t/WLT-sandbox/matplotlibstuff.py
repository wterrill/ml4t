# Everything below follows this page: http://matplotlib.org/users/pyplot_tutorial.html

import matplotlib.pyplot as plt
import numpy as np

if(False):
    plt.plot([1,2,3,4])  # matplotlib assumes these are y values.
    plt.ylabel('some numbers')
    plt.show()

####################################

if(False):
    plt.plot([1,2,3,4], [1,4,9,16], 'ro')  #the x and y values are spelled out. ro = red dots
    plt.axis([0, 6, 0, 20]) #axis is x min, max and then y min, max
    plt.show()

####################################


if(False):
    # evenly sampled time at 200ms intervals
    t = np.arange(0., 5., 0.2)

    # red dashes, blue squares and green triangles
    plt.plot(t, t, 'r--', t, t**2, 'bs', t, t**3, 'g^')
    plt.show()

####################################

if(False):
    x=[1,2,3,6]
    y=[3,4,5,2]
    plt.plot(x, y, linewidth=2.0)
    plt.show()

    line, = plt.plot(x, y, '-')
    line.set_antialiased(False) # turn off antialising
    plt.show()

    x1=[1,2,3,6]
    y1=[3,4,5,2]
    x2=[11,12,13,16]
    y2=[13,14,15,12]
    lines = plt.plot(x1, y1, x2, y2)
    # use keyword args
    plt.setp(lines, color='r', linewidth=12.0)
    plt.show()
    # or MATLAB style string value pairs
    plt.setp(lines, 'color', 'b', 'linewidth', 2.0)
    plt.show()

    plt.setp(lines)  # This shows all of the different things that you can set for the line.
####################################

if (False):
    def f(t):
        return np.exp(-t) * np.cos(2*np.pi*t)

    t1 = np.arange(0.0, 5.0, 0.1)
    t2 = np.arange(0.0, 5.0, 0.02)

    plt.figure(1)
    plt.subplot(211)    # The subplot() command specifies numrows, numcols, fignum where fignum ranges
                        # from 1 to numrows*numcols. The commas in the subplot command are optional if
                        # numrows*numcols<10. So subplot(211) is identical to subplot(2, 1, 1)
    plt.plot(t1, f(t1), 'bo', t2, f(t2), 'k')

    plt.subplot(212)
    plt.plot(t2, np.cos(2*np.pi*t2), 'r--')
    plt.show()

####################################
if(False):
    plt.figure(1)                # the first figure
    plt.subplot(211)             # the first subplot in the first figure
    plt.plot([1, 2, 3])
    plt.subplot(212)             # the second subplot in the first figure
    plt.plot([4, 5, 6])


    plt.figure(2)                # a second figure (this is created in a second window from the first.
    plt.plot([4, 5, 6])          # creates a subplot(111) by default

    plt.figure(1)                # figure 1 current; subplot(212) still current
    plt.subplot(211)             # make subplot(211) in figure1 current
    plt.title('Easy as 1, 2, 3') # subplot 211 title
    plt.show()

####################################
if(False):
    mu, sigma = 100, 15
    x = mu + sigma * np.random.randn(10000)

    # the histogram of the data
    n, bins, patches = plt.hist(x, 50, normed=1, facecolor='g', alpha=0.75)

    plt.xlabel('Smarts',fontsize=14, color='red')
    plt.ylabel('Probability')
    plt.title('Histogram of IQ')
    plt.text(60, .025, r'$\mu=100,\ \sigma=15$')
    plt.axis([40, 160, 0, 0.03])
    plt.grid(True)
    plt.show()

####################################
if(False):
    ax = plt.subplot(111)

    t = np.arange(0.0, 5.0, 0.01)
    s = np.cos(2*np.pi*t)
    line, = plt.plot(t, s, lw=2)

    plt.annotate('local max', xy=(2, 1), xytext=(3, 1.5),  #this puts an annotation on the chart
                arrowprops=dict(facecolor='black', shrink=0.05),
                )

    plt.ylim(-2,2)
    plt.show()

####################################
if(True):
    # make up some data in the interval ]0, 1[
    y = np.random.normal(loc=0.5, scale=0.4, size=1000)
    y = y[(y > 0) & (y < 1)]
    y.sort()
    x = np.arange(len(y))

    # plot with various axes scales
    plt.figure(1)

    # linear
    plt.subplot(221)
    plt.plot(x, y)
    plt.yscale('linear')
    plt.title('linear')
    plt.grid(True)


    # log
    plt.subplot(222)
    plt.plot(x, y)
    plt.yscale('log')
    plt.title('log')
    plt.grid(True)


    # symmetric log
    plt.subplot(223)
    plt.plot(x, y - y.mean())
    plt.yscale('symlog', linthreshy=0.05)
    plt.title('symlog')
    plt.grid(True)

    # logit
    plt.subplot(224)
    plt.plot(x, y)
    plt.yscale('logit')
    plt.title('logit')
    plt.grid(True)

    plt.show()