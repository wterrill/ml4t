import scipy.optimize as spo
fun = lambda x: (x[0] - 1)**2 + (x[1] - 2.5)**2

# There are three constraints defined as:

cons = ({'type': 'ineq', 'fun': lambda x:  x[0] - 2 * x[1] + 2},
         {'type': 'ineq', 'fun': lambda x: -x[0] - 2 * x[1] + 6},
         {'type': 'ineq', 'fun': lambda x: -x[0] + 2 * x[1] + 2})

# And variables must be positive, hence the following bounds:

bnds = ((0, 1), (0, 1))

#The optimization problem is solved using the SLSQP method as:

res = spo.minimize(fun, (2, 0), method='SLSQP', bounds=bnds, constraints=cons)
print res