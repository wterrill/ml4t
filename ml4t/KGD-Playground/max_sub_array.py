"""Python program to find maximum contiguous subarray
Example:
    max_sub_array_sum([1, 2, -5, 4, -3, 2, 6, -5, -1])
    should yeild (9, [3, 4, 5, 6]) as 9 is the max sum and the suporting sub array 
"""

def max_sub_array_sum(numbers):
    """find the maximum sum of any contiguous subarray
    and the index values of this subarray
    Returns max sum, indices of max sum sub array
    """
    
    max_current = max_real = 0
    max_curr_indices = max_real_indices = []
    if len(numbers) < 1:
        raise ValueError('Input list should not be empty')
    
    #enumerate the list and find max
    for ix, num in enumerate(numbers):
        max_current += num

        #reset counters if current max is less than 0
        if max_current <= 0:
            max_real_indices = max_real_indices \
                               if len(max_curr_indices) < 1 \
                               else max_curr_indices
            max_curr_indices = []
            max_current = 0

        #update real max if smaller than current max
        if max_real < max_current:
            max_real = max_current
            max_curr_indices.append(ix)

    #determine which max is truely max
    mci = sum([numbers[a] for a in max_curr_indices])
    mri = sum([numbers[a] for a in max_real_indices])
    max_real_indices = max_real_indices \
                       if mri > mci \
                       else max_curr_indices
    
    return max_real, [g for g in range(max_real_indices[0], max_real_indices[-1]+1)]

if __name__ == "__main__":
    print "--TESTS--"
    #print max_sub_array_sum([])
    print max_sub_array_sum([1, 2, 5, 4, -3, 2, -6, -5, 13]) == (13, [8])
    print max_sub_array_sum([12, -2, -5, 4, -3, 2, -6, -5, 1]) == (12, [0])
    print max_sub_array_sum([12, -2, -5, 8, -3, 2, -6, -5, 1]) == (13, [0, 1, 2, 3])
    print max_sub_array_sum([1, 2, 5, 4, -3, 2, 6, -5, -1]) == (17, [0, 1, 2, 3, 4, 5, 6])
    print max_sub_array_sum([1, 2, -5, 4, -3, 2, 6, -5, -1]) == (9, [3, 4, 5, 6])
    print max_sub_array_sum([-2, -3, 4, -1, -2, 1, 5, -3]) == (7, [2, 3, 4, 5, 6])
    print max_sub_array_sum([-2, -3, 4, -9, -2, 1, -11, -3]) == (4, [2])
    print max_sub_array_sum([-2, -3, 2, -1, 3, -9, -2, 1, -11, -3]) == (4, [2, 3, 4])
    print max_sub_array_sum([-2, -3, -90, 3, -9, -2, 1, -11, -3]) == (3, [3])
    print max_sub_array_sum([2, 3, 4, -9, -2, 11, -11, -3]) == (11, [5])
    print max_sub_array_sum([-2, -3, 1, 1, -9, 2, -1, -11, -3]) == (2, [2, 3])
    
