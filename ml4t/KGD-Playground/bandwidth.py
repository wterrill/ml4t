import pandas as pd
import matplotlib.pyplot as plt
import os

scales = {'Kbps':1024,'Mbps':1,'ms':1}
dir = os.path.dirname(os.path.realpath(__file__))
data = pd.read_csv(dir+"/bandwidth.txt",header=None)

scale = lambda x: pd.Series([float(x.split(' ')[0]) / scales[x.split(' ')[1]]])
data[5] = data[2].apply(scale)
data[6] = data[3].apply(scale)
data[7] = data[4].apply(scale)

newdata = data[[0,6,7]]
newdata[0] = pd.to_datetime(data[0])
newdata = newdata.set_index([0])
newdata.columns = ['down', 'up']


# need to plot column 6 and 7 indexed on col 0
lines = newdata.plot(title= "Bandwidth" ,fontsize=12 )
lines.set_xlabel("Date")
lines.set_ylabel("Speed")
plt.show()




