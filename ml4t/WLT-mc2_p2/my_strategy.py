"""Here are the essential things you need to know and to implement them. In addition to the price of a stock over time, Bollinger Bands include:

    20 day simple moving average (SMA) line.
    IBM-upper Band = SMA + 2 * 20 day standard deviation.
   'IBM-lower' Band = SMA - 2 * 20 day standard deviation.

There are two potential entries, long and short. The long entry is made when the price transitions from below the'IBM-lower' band to above the'IBM-lower' band.
This indicates that the stock price has moved substantially away from the moving average, but is now moving back towards the moving average.
When this entry signal criteria is met, buy the stock and hold it until the exit. The exit signal occurs when the price moves from below the SMA to above it.

The short entry and exit are mirrors of the long entry and exit: The short entry is made when the price transitions from above the IBM-upper band to below
the IBM-upper band. This indicates that the stock price has moved substantially away from the moving average, but is now moving back towards the moving average.
When this entry signal criteria is met, short the stock and hold it until the exit. The exit signal occurs when the price moves from above the SMA to below it.

You should create a chart that shows:
    Stock price (adjusted close)
    SMA
    IBM-upper Band
   'IBM-lower' Band
    Long entries as a vertical green line at the time of entry.
    Long exits as a vertical black line at the time of exit.
    Short entries as a vertical RED line at the time of entry.
    Short exits as a vertical black line at the time of exit.

URL: https://www.quantstart.com/articles/Backtesting-a-Moving-Average-Crossover-in-Python-with-pandas
"""
#from util import *
import datetime
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os

def print_full(x):
    pd.set_option('display.max_rows', len(x))
    print(x)
    pd.reset_option('display.max_rows')

def symbol_to_path(symbol, base_dir=os.path.join("..", "data")):
    """Return CSV file path given ticker symbol."""
    return os.path.join(base_dir, "{}.csv".format(str(symbol)))

def plot_strategy(df, title="Stock prices", xlabel="Date", ylabel="Price"):
    """Plot stock prices"""


    ax = df[['IBM','IBM-SMA','meanslope', 'zero']].plot(title=title, fontsize=12)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    #ax = df.plot(figsize=(14,6))
    ymin, ymax = ax.get_ylim()

    buy = df.ix[df.order == "BUY"].index
    sell = df.ix[df.order == "SELL"].index
    exitbuy = df.ix[df.order == 'SELL-exit'].index
    exitsell = df.ix[df.order == 'BUY-exit'].index


    plt.vlines(sell,ymin=ymin, ymax=ymax,color='r')
    plt.vlines(buy,ymin=ymin, ymax=ymax,color='g')
    plt.vlines(exitsell,ymin=ymin, ymax=ymax,color='k')
    plt.vlines(exitbuy,ymin=ymin, ymax=ymax,color='k')
    #plt.vlines('2008-12-11',ymin=ymin, ymax=ymax,color='b')

    plt.show()

def get_data(symbols, dates, addSPY=True):
    """Read stock data (adjusted close) for given symbols from CSV files."""
    df = pd.DataFrame(index=dates)
    if addSPY and 'SPY' not in symbols:  # add SPY for reference, if absent
        symbols = ['SPY'] + symbols

    for symbol in symbols:
        df_temp = pd.read_csv(symbol_to_path(symbol), index_col='Date',
                parse_dates=True, usecols=['Date', 'Adj Close'], na_values=['nan'])
        df_temp = df_temp.rename(columns={'Adj Close': symbol})
        df = df.join(df_temp)
        if symbol == 'SPY':  # drop dates SPY did not trade
            df = df.dropna(subset=["SPY"])

    return df

def strategy(stocks,start,end):
    rolling_window=20
    n_days = 5
    stocks = get_data(stocks, pd.date_range(start, end), addSPY=True)

    stocks['sell'] = 0
    stocks['exit_sell']=0
    stocks['buy'] = 0
    stocks['exit_buy']=0
    stocks['slope'] = 0

    stocks['order']=0

    stocks['IBM-SMA'] = pd.rolling_mean(stocks['IBM'],rolling_window, min_periods=20)

    stocks['slope'] = stocks['IBM-SMA'].diff()
    stocks['meanslope'] = 50*pd.rolling_mean(stocks['slope'],rolling_window*2, min_periods=20)
    stocks['zero'] = 0
    stocks['trend']=stocks['meanslope']>0

    del stocks['slope']
    #del stocks['meanslope']


    trigger_sell = 0
    trigger_buy = 0
    quantity = 0

    stocks.to_csv("before.csv")

    orders = []
    for x in range(40,len(stocks)):

        current_date = pd.Timestamp(stocks.index.get_values()[x]).date()
        #if stocks.index[x] == datetime.datetime(2008, 10, 30, 0, 0):
        #    print "testing data"

        #signal a buy
        if stocks.ix[x,'trend'] and stocks.ix[x-1,'trend'] and stocks.ix[x-2,'trend'] \
                and quantity <100 \
                and not trigger_sell \
                and not trigger_buy:

            orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            orders.append("BUY")
            stocks.ix[x,'order']="BUY"
            quantity = quantity + 100
            stocks.ix[x,'quant'] = quantity
            trigger_buy = 1

        #signal a buy exit
        if not stocks.ix[x,'trend'] and stocks.ix[x-1,'trend'] and stocks.ix[x-2,'trend'] \
            and quantity > -100:

            orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            orders.append("SELL")
            stocks.ix[x,'order']="SELL-exit"
            quantity = quantity - 100
            stocks.ix[x,'quant'] = quantity
            trigger_buy = 0

        #signal a sell
        if not stocks.ix[x,'trend'] and not stocks.ix[x-1,'trend'] and not stocks.ix[x-2,'trend'] \
                and not quantity < 0 \
                and not trigger_sell \
                and not trigger_buy:

            orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            orders.append("SELL")
            stocks.ix[x,'order']="SELL"
            quantity = quantity - 100
            stocks.ix[x,'quant'] = quantity
            trigger_sell = 1

        #signal a sell  exit
        if stocks.ix[x,'trend'] and not stocks.ix[x-1,'trend'] and not stocks.ix[x-2,'trend'] \
            and not quantity > 0:

            orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            orders.append("BUY")
            stocks.ix[x,'order']="BUY-exit"
            quantity = quantity + 100
            stocks.ix[x,'quant'] = quantity
            trigger_sell = 0

        #if last day, get rid of holdings
        if x==len(stocks)-1 and quantity > 0:
            orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            orders.append("SELL")
            stocks.ix[x,'order']="SELL-exit"
            quantity = quantity - 100
            stocks.ix[x,'quant'] = quantity

    stocks.to_csv("stocks.csv")

    print_full(stocks)
    plot_strategy(stocks)


    final = np.array(orders)
    final = final.reshape(len(final)/2,2)
    final = pd.DataFrame(final)

    final.insert(1,'Symbol','IBM')
    final.insert(3,'Shares',100)

    final.columns=['Date','Symbol','Order', 'Shares']
    final.set_index('Date',drop=True, inplace=True)
    final.to_csv('orders.csv')

    return

if __name__ == "__main__":
    #strategy(['IBM'],'2007-12-31','2009-12-31')
    strategy(['IBM'],'2009-12-31','2011-12-31')