"""MC2-P1: Market simulator."""

import pandas as pd
import numpy as np
import datetime as dt
import os
from util import get_data, plot_data
import matplotlib.pyplot as plt


def compute_portvals(orders_file = "./orders/orders.csv", start_val = 1000000):
    # this is the function the autograder will call to test your code

    pd.set_option('max_rows', 400) #This sets the number of rows that a print DATAFRAME will print. useful for debug.

    #Get orders and put them into the dataframe "orders"
    df_temp = pd.read_csv(orders_file, index_col='Date',parse_dates=True, na_values=['nan'])
    orders = df_temp.sort_index(ascending=1) #sort by index
    df_temp = None;
    sym = orders['Symbol'].unique().tolist() #get all the unique symbols in the order
    start_date = orders.index.values[0] #since it's sorted, the start and end dates will be at the beginning and end
    end_date = orders.index.values[-1]

    #Get stocks associated with the previous orders.
    stocks = get_data(sym, pd.date_range(start_date,end_date), addSPY=True)
    stocks.dropna(subset=['SPY'],inplace=True)   # I don't *actually* need to do this. due to the addSPY=True... but, what the heck
    stocks.drop(['SPY'],axis=1,inplace=True)  #SPY was used to get rid of Nan... but we don't need it right now.
    stocks['Cash'] = np.ones(stocks.shape[0]) #create 'Cash' column with a price of 1.0 for each date.

    #Make trades frame that shows the date things traded, and the fluctuations in cash based on the price for that day
    trades = make_trades(stocks,sym,orders)


    #Create holdings frame that has the stocks and cash held every day.
    holdings = make_holdings(trades,start_val)

    #create value frame that takes the stock prices and multiplies them by the holdings for every single day.
    value = stocks * holdings

    #Sum those puppies to get your portfolio.
    portvals = value.sum(axis=1)

    #check if overleveraged
    leverages = []
    for transaction in orders.iterrows():
        if transaction[0] in stocks.index:

            temp = value.ix[transaction[0],:]
            numerator = sum(np.abs(temp[0:len(temp)-1]))

            denominator = sum(value.ix[transaction[0],:])
            leverage = numerator/denominator
            print "leverage", leverage
            if (leverage > 2.0):
                orders.drop([transaction[0]],inplace=True)
                print ("dropped {} {}, {} on {} due to leverage > 2.0. leverage = {}").format(transaction[1]['Order'],\
                      transaction[1]['Shares'],transaction[1]['Symbol'],transaction[0], leverage)
                #stocks = stocks[orders.index[0]:orders.index[-1]]
                trades = make_trades(stocks,sym,orders)
                holdings = make_holdings(trades,start_val)
                #create value frame that takes the stock prices and multiplies them by the holdings for every single day.
                value = stocks * holdings
                #Sum those puppies to get your portfolio.
                portvals = value.sum(axis=1)
        else:
            print "Order made on an invalid trading date:", transaction[0]


    return portvals

def assess_portfolio(portfolio, rfr=0.0, sf=252.0, gen_plot=False):

    # Get portfolio statistics (note: std_daily_ret = volatility)
    cr = portfolio.values[-1]/portfolio.values[0] - 1     #This divides the last value by the first
    daily_returns = portfolio.copy()          #copy to make a dataframe match shape and size
    daily_returns[1:] = \
        ((portfolio.values[1:])/(portfolio.values[:-1])) - 1  #This divides the daily value by the one before it.
    daily_returns[0] = 0                        #not sure why this matters, but it was in the lecture.
    adr = daily_returns[1:].mean()              #Take the mean from all the values except for the first one
    sddr = np.std(daily_returns[1:],ddof=1)
    sr = ((adr - rfr)/ sddr) * (sf ** 0.5)
    ev = portfolio.values[-1]

    sd = daily_returns.index.tolist()[0]
    ed = daily_returns.index.tolist()[-1]
    dates = pd.date_range(daily_returns.index.tolist()[0], ed)
    SPX = get_data(['$SPX'], dates, addSPY=True)  # automatically adds SPY
    del SPX['SPY']
    SPX_Norm = SPX.ix[:,0]/SPX.ix[0][0]


    #prices = prices_all[syms]  # only portfolio symbols
    #prices_SPY = prices_all['SPY']  # only SPY, for comparison later



        # Compare daily portfolio value with SPY using a normalized plot
    if gen_plot:
        df = pd.DataFrame(portfolio/portfolio[0],portfolio.index.tolist())
        df.columns = ['IBM']
        df1 = pd.concat([df,SPX_Norm],axis=1)
        ax = df1.plot(title="Normalized Portfolio versus Normalized SPY",fontsize=12)
        ax.set_xlabel("Date")
        ax.set_ylabel("Normalized Price")
        #plt.savefig('output/plot.png')
        plt.show()
        pass

    return cr, adr, sddr, sr, ev

def make_trades(stocks,sym,orders):
    trades_temp = np.zeros((stocks.shape[0], len(sym)+1)) #create initial numpy array with zeros
    trades = pd.DataFrame(trades_temp, index=stocks.index, columns=stocks.columns) #...and then put it in a panda.
    for transaction in orders.iterrows(): #iterate through all rows. (I wonder if there is an easier way to do this?)d
        if transaction[1]['Order']=="SELL":
            change = -1 * transaction[1]['Shares']

        else:
            change = transaction[1]['Shares']
        if transaction[0] in stocks.index:
            price = stocks.ix[transaction[0],transaction[1]['Symbol']]
            trades.ix[transaction[0],transaction[1]['Symbol']]+= change
            trades.ix[transaction[0],'Cash'] += -1.0 * price * change
    return trades

def make_holdings(trades, start_val):
    holdings = trades.copy()
    start = np.zeros(holdings.shape[1])
    start[-1] = start_val
    for day in holdings.iterrows():
        start += day[1].values
        holdings.ix[day[0]] = start
    return holdings

def test_code():
    # this is a helper function you can use to test your code
    # note that during autograding his function will not be called.
    # Define input parameters

    of = "orders.csv"
    sv = 10000

    # Process orders
    portvals = compute_portvals(orders_file = of, start_val = sv)
    if isinstance(portvals, pd.DataFrame):
        portvals = portvals[portvals.columns[0]] # just get the first column
    else:
        "warning, code did not return a DataFrame"
    
    # Get portfolio stats
    # Here we just fake the data. you should use your code from previous assignments.

    start_date = portvals.index[0]
    end_date = portvals.index[-1]

    cum_ret, avg_daily_ret, std_daily_ret, sharpe_ratio, ev = assess_portfolio(portvals, gen_plot=True)
    print portvals[-1]

    benchmark = '$SPX'  #even though everything says "SPY" below, the wiki is using $SPX.
    port_SPY = get_data([benchmark], pd.date_range(start_date,end_date), addSPY=False)
    port_SPY = port_SPY.dropna(subset=[benchmark])

    cum_ret_SPY, avg_daily_ret_SPY, std_daily_ret_SPY, sharpe_ratio_SPY, ev_SPY = assess_portfolio(port_SPY[benchmark])

    # Compare portfolio against $SPX
    print "Date Range: {} to {}".format(start_date, end_date)
    print
    print "Sharpe Ratio of Fund: {}".format(sharpe_ratio)
    print "Sharpe Ratio of SPY : {}".format(sharpe_ratio_SPY)
    print
    print "Cumulative Return of Fund: {}".format(cum_ret)
    print "Cumulative Return of SPY : {}".format(cum_ret_SPY)
    print
    print "Standard Deviation of Fund: {}".format(std_daily_ret)
    print "Standard Deviation of SPY : {}".format(std_daily_ret_SPY)
    print
    print "Average Daily Return of Fund: {}".format(avg_daily_ret)
    print "Average Daily Return of SPY : {}".format(avg_daily_ret_SPY)
    print
    print "Final Portfolio Value: {}".format(portvals[-1])

if __name__ == "__main__":
    test_code()

