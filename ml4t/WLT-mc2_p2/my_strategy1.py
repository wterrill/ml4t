"""Here are the essential things you need to know and to implement them. In addition to the price of a stock over time, Bollinger Bands include:

    20 day simple moving average (SMA) line.
    IBM-upper Band = SMA + 2 * 20 day standard deviation.
   'IBM-lower' Band = SMA - 2 * 20 day standard deviation.

There are two potential entries, long and short. The long entry is made when the price transitions from below the'IBM-lower' band to above the'IBM-lower' band.
This indicates that the stock price has moved substantially away from the moving average, but is now moving back towards the moving average.
When this entry signal criteria is met, buy the stock and hold it until the exit. The exit signal occurs when the price moves from below the SMA to above it.

The short entry and exit are mirrors of the long entry and exit: The short entry is made when the price transitions from above the IBM-upper band to below
the IBM-upper band. This indicates that the stock price has moved substantially away from the moving average, but is now moving back towards the moving average.
When this entry signal criteria is met, short the stock and hold it until the exit. The exit signal occurs when the price moves from above the SMA to below it.

You should create a chart that shows:
    Stock price (adjusted close)
    SMA
    IBM-upper Band
   'IBM-lower' Band
    Long entries as a vertical green line at the time of entry.
    Long exits as a vertical black line at the time of exit.
    Short entries as a vertical RED line at the time of entry.
    Short exits as a vertical black line at the time of exit.

URL: https://www.quantstart.com/articles/Backtesting-a-Moving-Average-Crossover-in-Python-with-pandas
"""
#from util import *
import datetime
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os

def print_full(x):
    pd.set_option('display.max_rows', len(x))
    print(x)
    pd.reset_option('display.max_rows')

def symbol_to_path(symbol, base_dir=os.path.join("..", "data")):
    """Return CSV file path given ticker symbol."""
    return os.path.join(base_dir, "{}.csv".format(str(symbol)))

def plot_strategy(df, title="Stock prices", xlabel="Date", ylabel="Price"):
    """Plot stock prices"""
    df['SPY'] = df['SPY'] + 100
    df['SPY-SMA'] = df['SPY-SMA'] + 100
    df['SPY-upper'] = df['SPY-upper'] + 100
    df['SPY-lower'] = df['SPY-lower'] + 100

    ax = df[['IBM','IBM-SMA','IBM-upper','IBM-lower','SPY', 'SPY-SMA', 'SPY-upper', 'SPY-lower']].plot(title=title, fontsize=12)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    #ax = df.plot(figsize=(14,6))
    ymin, ymax = ax.get_ylim()

    buy = df.ix[df.buy == -1].index
    sell = df.ix[df.sell == -2].index
    exitbuy = df.ix[df.exit_buy == 1].index
    exitsell = df.ix[df.exit_sell == 2].index

    buy_SPY = df.ix[df.buy_SPY == -1].index
    sell_SPY = df.ix[df.sell_SPY == -2].index
    exitbuy_SPY = df.ix[df.exit_buy_SPY == 1].index
    exitsell_SPY = df.ix[df.exit_sell_SPY == 2].index


    plt.vlines(sell,ymin=ymin, ymax=ymax,color='r')
    plt.vlines(buy,ymin=ymin, ymax=ymax,color='g')
    plt.vlines(exitsell,ymin=ymin, ymax=ymax,color='k')
    plt.vlines(exitbuy,ymin=ymin, ymax=ymax,color='k')
    #plt.vlines('2008-12-11',ymin=ymin, ymax=ymax,color='b')

    plt.show()

def get_data(symbols, dates, addSPY=True):
    """Read stock data (adjusted close) for given symbols from CSV files."""
    df = pd.DataFrame(index=dates)
    if addSPY and 'SPY' not in symbols:  # add SPY for reference, if absent
        symbols = ['SPY'] + symbols

    for symbol in symbols:
        df_temp = pd.read_csv(symbol_to_path(symbol), index_col='Date',
                parse_dates=True, usecols=['Date', 'Adj Close'], na_values=['nan'])
        df_temp = df_temp.rename(columns={'Adj Close': symbol})
        df = df.join(df_temp)
        if symbol == 'SPY':  # drop dates SPY did not trade
            df = df.dropna(subset=["SPY"])

    return df

def test_code(stocks,start,end, rolling_window=20):
    n_days = 5
    stocks = get_data(stocks, pd.date_range(start, end), addSPY=True)

    stocks['sell'] = 0
    stocks['exit_sell']=0
    stocks['buy'] = 0
    stocks['exit_buy']=0
    stocks['sell_SPY'] = 0
    stocks['exit_sell_SPY']=0
    stocks['buy_SPY'] = 0
    stocks['exit_buy_SPY']=0
    stocks['order']=0


    stocks['IBM-SMA'] = pd.rolling_mean(stocks['IBM'],rolling_window, min_periods=1)
    stocks['IBM-upper'] = stocks['IBM-SMA'] + 2 * pd.rolling_std(stocks['IBM'],rolling_window)
    stocks['IBM-lower'] = stocks['IBM-SMA'] - 2 * pd.rolling_std(stocks['IBM'],rolling_window)


    stocks['SPY-SMA'] = pd.rolling_mean(stocks['SPY'],rolling_window, min_periods=1)
    stocks['SPY-upper'] = stocks['SPY-SMA'] + 2 * pd.rolling_std(stocks['SPY'],rolling_window)
    stocks['SPY-lower'] = stocks['SPY-SMA'] - 2 * pd.rolling_std(stocks['SPY'],rolling_window)



    stocks['sell'][rolling_window:] = np.where(stocks['IBM'][rolling_window:] > stocks['IBM-upper'][rolling_window:],1,0)
    stocks['buy'][rolling_window:] = np.where(stocks['IBM'][rolling_window:] < stocks['IBM-lower'][rolling_window:],1,0)
    stocks['exit_sell'][rolling_window:] = np.where(stocks['IBM'][rolling_window:] > stocks['IBM-SMA'][rolling_window:],1,0)
    stocks['exit_buy'][rolling_window:] = np.where(stocks['IBM'][rolling_window:] < stocks['IBM-SMA'][rolling_window:],1,0)

    stocks['sell_SPY'][rolling_window:] = np.where(stocks['SPY'][rolling_window:] > stocks['SPY-upper'][rolling_window:],1,0)
    stocks['buy_SPY'][rolling_window:] = np.where(stocks['SPY'][rolling_window:] < stocks['SPY-lower'][rolling_window:],1,0)
    stocks['exit_sell_SPY'][rolling_window:] = np.where(stocks['SPY'][rolling_window:] > stocks['SPY-SMA'][rolling_window:],1,0)
    stocks['exit_buy_SPY'][rolling_window:] = np.where(stocks['SPY'][rolling_window:] < stocks['SPY-SMA'][rolling_window:],1,0)




    stocks['buy'] = stocks['buy'].diff()
    stocks['sell'] = stocks['sell'].diff()
    stocks['exit_buy'] = stocks['exit_buy'].diff()
    stocks['exit_sell'] = stocks['exit_sell'].diff()

    stocks['buy']=-1*(stocks['buy']<0)
    stocks['exit_buy']=1*(stocks['exit_buy']<0)
    stocks['sell']=-2*(stocks['sell']<0)
    stocks['exit_sell']=2*(stocks['exit_sell']<0)

    stocks['buy_SPY'] = stocks['buy_SPY'].diff()
    stocks['sell_SPY'] = stocks['sell_SPY'].diff()
    stocks['exit_buy_SPY'] = stocks['exit_buy_SPY'].diff()
    stocks['exit_sell_SPY'] = stocks['exit_sell_SPY'].diff()

    stocks['buy_SPY']=-1*(stocks['buy_SPY']<0)
    stocks['exit_buy_SPY']=1*(stocks['exit_buy_SPY']<0)
    stocks['sell_SPY']=-2*(stocks['sell_SPY']<0)
    stocks['exit_sell_SPY']=2*(stocks['exit_sell_SPY']<0)

    trigger_sell = 0
    trigger_buy = 0

    #stocks.to_csv("before.csv")

    orders = []
    for x in range(20,len(stocks)):
        #take care of the random exit_buys and sells that occur outside of entrances
        if not trigger_buy:
            stocks.ix[x,'exit_buy'] = 0

        if not trigger_sell:
            stocks.ix[x,'exit_sell'] = 0

        #take care of sell entrances and exits

        #in between an extrance and exit
        if trigger_sell and not stocks.ix[x,'exit_sell']==2 and not trigger_buy:
            stocks.ix[x,'buy']=0
            stocks.ix[x,'exit_buy']=0
            stocks.ix[x,'sell']=0

        # exit a sell
        if trigger_sell and stocks.ix[x,'exit_sell']==2 and not trigger_buy:
            trigger_sell = 0
            #orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            #orders.append("BUY")

        # enter a sell
        if stocks.ix[x,'sell']==-2 and not trigger_sell and not trigger_buy:
            trigger_sell = 1
            #orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            #orders.append("SELL")

        #take care of buy entraces and exits

        # in between an entrance and an exit
        if trigger_buy and not stocks.ix[x,'exit_buy']==1 and not trigger_sell:
            stocks.ix[x,'sell']=0
            stocks.ix[x,'exit_sell']=0
            stocks.ix[x,'buy']=0

        # exit a buy
        if trigger_buy and stocks.ix[x,'exit_buy']==1 and not trigger_sell:
            trigger_buy = 0
            #orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            #orders.append("SELL")

        # enter a buy
        if stocks.ix[x,'buy']==-1 and not trigger_buy and not trigger_sell:
            trigger_buy = 1
            #orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            #orders.append("BUY")


        #if x == stocks.index[-1]:
        #    if trigger_buy:
                #orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
                #orders.append("BUY")
        #    if trigger_sell:
                #orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
                #orders.append("SELL")




    trigger_sell = 0
    trigger_buy = 0


    ordersSPY = []
    for x in range(20,len(stocks)):
        #take care of the random exit_buy_SPYs and sells that occur outside of entrances
        if not trigger_buy:
            stocks.ix[x,'exit_buy_SPY'] = 0

        if not trigger_sell:
            stocks.ix[x,'exit_sell_SPY'] = 0

        #take care of sell entrances and exits

        #in between an extrance and exit
        if trigger_sell and not stocks.ix[x,'exit_sell_SPY']==2 and not trigger_buy:
            stocks.ix[x,'buy_SPY']=0
            stocks.ix[x,'exit_buy_SPY']=0
            stocks.ix[x,'sell_SPY']=0

        # exit a sell
        if trigger_sell and stocks.ix[x,'exit_sell_SPY']==2 and not trigger_buy:
            trigger_sell = 0
            #orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            #orders.append("BUY")

        # enter a sell
        if stocks.ix[x,'sell_SPY']==-2 and not trigger_sell and not trigger_buy:
            trigger_sell = 1
            #orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            #orders.append("SELL")

        #take care of buy entraces and exit

        # in between an entrance and an exit
        if trigger_buy and not stocks.ix[x,'exit_buy_SPY']==1 and not trigger_sell:
            stocks.ix[x,'sell_SPY']=0
            stocks.ix[x,'exit_sell_SPY']=0
            stocks.ix[x,'buy_SPY']=0

        # exit a buy
        if trigger_buy and stocks.ix[x,'exit_buy_SPY']==1 and not trigger_sell:
            trigger_buy = 0
            #orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            #orders.append("SELL")

        # enter a buy
        if stocks.ix[x,'buy_SPY']==-1 and not trigger_buy and not trigger_sell:
            trigger_buy = 1
            #orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            #orders.append("BUY")


        #if x == stocks.index[-1]:
        #    if trigger_buy:
                #orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
                #orders.append("BUY")
        #    if trigger_sell:
                #orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
                #orders.append("SELL")



    # make the orders file.

    trigger_buy_IBM = 0
    trigger_sell_IBM = 0
    trigger_but_SPY = 0
    trigger_buy_SPY = 0
    trigger_buy = 0
    trigger_sell = 0
    trigger_exit = 0



    for x in range(20,len(stocks)):

        current_date = pd.Timestamp(stocks.index.get_values()[x]).date()
        if stocks.index[x] == datetime.datetime(2008, 10, 30, 0, 0):
            print "yup"


        # exit a sell
        if trigger_sell and (stocks.ix[x,'exit_sell_SPY']==2 or stocks.ix[x,'buy_SPY']== -1 or stocks.ix[x,'exit_sell']==2 ) and not trigger_buy:
            if trigger_sell == 1:

                orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
                orders.append("BUY")
                stocks.ix[x,'order']="BUY-exit"

            trigger_sell = 0




        elif trigger_sell == 2 and (stocks.ix[x,'exit_sell_SPY']==2 or stocks.ix[x,'sell_SPY']== -2 or stocks.ix[x,'exit_sell']==2 ) and not trigger_buy:
                trigger_sell = trigger_sell - 1

                if stocks.ix[x,'exit_sell_SPY']==2 and stocks.ix[x,'sell_SPY']== 2:
                    orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
                    orders.append("BUY")
                    stocks.ix[x,'order']="BUY-exit"
                    trigger_sell = 0
                    trigger_buy = 0





        #trigger a sell if any single one is triggered and the other doesn't disagree
        elif (stocks.ix[x,'sell']==-2 or stocks.ix[x,'sell_SPY']==-2) and  trigger_sell < 2  and not trigger_buy:
                trigger_sell = trigger_sell + 1


                orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
                orders.append("SELL")
                stocks.ix[x,'order']="SELL"



        # exit a buy
        elif trigger_buy == 1 and (stocks.ix[x,'exit_buy_SPY']==1 or stocks.ix[x,'sell_SPY']== -1 or stocks.ix[x,'exit_buy']==1 or stocks.ix[x,'sell']== -2) and not trigger_sell:
                trigger_buy = 0
                trigger_sell = 0


                orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
                orders.append("SELL")
                stocks.ix[x,'order']="SELL-exit"




        elif trigger_buy == 2                                   \
                            and                                 \
                            (                                   \
                            stocks.ix[x,'exit_buy_SPY']==1      \
                            or stocks.ix[x,'buy_SPY']== -1      \
                            or stocks.ix[x,'exit_buy']==1       \
                            )                                   \
                            and not trigger_sell:
                trigger_buy = trigger_buy - 1

                if stocks.ix[x,'exit_buy_SPY']==1 and stocks.ix[x,'exit_buy']== 1:
                    orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
                    orders.append("SELL")
                    stocks.ix[x,'order']="SELL-exit"
                    trigger_sell = 0
                    trigger_buy = 0





        #trigger a buy if any single one is triggered and the other doesn't disagree
        else:
            if (stocks.ix[x,'buy']==-1 or stocks.ix[x,'buy_SPY']==-1) and trigger_buy < 2  and not trigger_sell:
                trigger_buy = trigger_buy + 1

                if trigger_buy == 1:
                    orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
                    orders.append("BUY")
                    stocks.ix[x,'order']="BUY"

        if trigger_buy < 0:
            trigger_buy = 0
        if trigger_sell < 0:
            trigger_sell = 0









    stocks.to_csv("stocks.csv")

    print_full(stocks)
    plot_strategy(stocks)


    final = np.array(orders)
    final = final.reshape(len(final)/2,2)
    final = pd.DataFrame(final)

    final.insert(1,'Symbol','IBM')
    final.insert(3,'Shares',100)

    final.columns=['Date','Symbol','Order', 'Shares']
    final.set_index('Date',drop=True, inplace=True)
    final.to_csv('orders.csv')



    return

if __name__ == "__main__":
    test_code(['IBM'],'2007-12-31','2009-12-31')