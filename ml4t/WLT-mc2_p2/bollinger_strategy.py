"""Here are the essential things you need to know and to implement them. In addition to the price of a stock over time, Bollinger Bands include:

    20 day simple moving average (SMA) line.
    Upper Band = SMA + 2 * 20 day standard deviation.
    Lower Band = SMA - 2 * 20 day standard deviation.

There are two potential entries, long and short. The long entry is made when the price transitions from below the lower band to above the lower band.
This indicates that the stock price has moved substantially away from the moving average, but is now moving back towards the moving average. 
When this entry signal criteria is met, buy the stock and hold it until the exit. The exit signal occurs when the price moves from below the SMA to above it.

The short entry and exit are mirrors of the long entry and exit: The short entry is made when the price transitions from above the upper band to below 
the upper band. This indicates that the stock price has moved substantially away from the moving average, but is now moving back towards the moving average. 
When this entry signal criteria is met, short the stock and hold it until the exit. The exit signal occurs when the price moves from above the SMA to below it.

You should create a chart that shows:
    Stock price (adjusted close)
    SMA
    Upper Band
    Lower Band
    Long entries as a vertical green line at the time of entry.
    Long exits as a vertical black line at the time of exit.
    Short entries as a vertical RED line at the time of entry.
    Short exits as a vertical black line at the time of exit.

URL: https://www.quantstart.com/articles/Backtesting-a-Moving-Average-Crossover-in-Python-with-pandas
"""
#from util import *
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os

def print_full(x):
    pd.set_option('display.max_rows', len(x))
    print(x)
    pd.reset_option('display.max_rows')

def symbol_to_path(symbol, base_dir=os.path.join("..", "data")):
    """Return CSV file path given ticker symbol."""
    return os.path.join(base_dir, "{}.csv".format(str(symbol)))

def plot_strategy(df, title="Stock prices", xlabel="Date", ylabel="Price"):
    """Plot stock prices"""
    ax = df[['IBM','SMA','upper','lower']].plot(title=title, fontsize=12)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    #ax = df.plot(figsize=(14,6))
    ymin, ymax = ax.get_ylim()
    buy = df.ix[df.buy == -1].index
    sell = df.ix[df.sell == -2].index
    exitbuy = df.ix[df.exit_buy == 1].index
    exitsell = df.ix[df.exit_sell == 2].index
    plt.vlines(sell,ymin=ymin, ymax=ymax,color='r')
    plt.vlines(buy,ymin=ymin, ymax=ymax,color='g')
    plt.vlines(exitsell,ymin=ymin, ymax=ymax,color='k')
    plt.vlines(exitbuy,ymin=ymin, ymax=ymax,color='k')
    #plt.vlines('2008-12-11',ymin=ymin, ymax=ymax,color='b')
    plt.show()

def get_data(symbols, dates, addSPY=True):
    """Read stock data (adjusted close) for given symbols from CSV files."""
    df = pd.DataFrame(index=dates)
    if addSPY and 'SPY' not in symbols:  # add SPY for reference, if absent
        symbols = ['SPY'] + symbols

    for symbol in symbols:
        df_temp = pd.read_csv(symbol_to_path(symbol), index_col='Date',
                parse_dates=True, usecols=['Date', 'Adj Close'], na_values=['nan'])
        df_temp = df_temp.rename(columns={'Adj Close': symbol})
        df = df.join(df_temp)
        if symbol == 'SPY':  # drop dates SPY did not trade
            df = df.dropna(subset=["SPY"])

    return df

def test_code(stocks,start,end, rolling_window=20):
    n_days = 5
    stocks = get_data(stocks, pd.date_range(start, end), addSPY=True)
    del stocks['SPY']
    stocks['sell'] = 0
    stocks['exit_sell']=0
    stocks['buy'] = 0
    stocks['exit_buy']=0
    stocks['SMA'] = pd.rolling_mean(stocks['IBM'],rolling_window, min_periods=1)
    stocks['upper'] = stocks['SMA'] + 2 * pd.rolling_std(stocks['IBM'],rolling_window)
    stocks['lower'] = stocks['SMA'] - 2 * pd.rolling_std(stocks['IBM'],rolling_window)
    #stocks[stocks.IBM > stocks.upper] = 1
    #stocks[stocks.IBM > stocks.upper] = 1
    stocks['sell'][rolling_window:] = np.where(stocks['IBM'][rolling_window:] > stocks['upper'][rolling_window:],1,0)
    stocks['buy'][rolling_window:] = np.where(stocks['IBM'][rolling_window:] < stocks['lower'][rolling_window:],1,0)
    stocks['exit_sell'][rolling_window:] = np.where(stocks['IBM'][rolling_window:] > stocks['SMA'][rolling_window:],1,0)
    stocks['exit_buy'][rolling_window:] = np.where(stocks['IBM'][rolling_window:] < stocks['SMA'][rolling_window:],1,0)
    stocks['buy'] = stocks['buy'].diff()
    stocks['sell'] = stocks['sell'].diff()
    stocks['exit_buy'] = stocks['exit_buy'].diff()
    stocks['exit_sell'] = stocks['exit_sell'].diff()

    stocks['buy']=-1*(stocks['buy']<0)
    stocks['exit_buy']=1*(stocks['exit_buy']<0)
    stocks['sell']=-2*(stocks['sell']<0)
    stocks['exit_sell']=2*(stocks['exit_sell']<0)

    trigger_sell = 0
    trigger_buy = 0

    stocks.to_csv("before.csv")

    orders = []
    for x in range(20,len(stocks)):
        #take care of the random exit_buys and sells that occur outside of entrances
        if not trigger_buy:
            stocks.ix[x,'exit_buy'] = 0

        if not trigger_sell:
            stocks.ix[x,'exit_sell'] = 0

        #take care of sell entrances and exits

        #in between an extrance and exit
        if trigger_sell and not stocks.ix[x,'exit_sell']==2 and not trigger_buy:
            stocks.ix[x,'buy']=0
            stocks.ix[x,'exit_buy']=0
            stocks.ix[x,'sell']=0

        # exit a sell
        if trigger_sell and stocks.ix[x,'exit_sell']==2 and not trigger_buy:
            trigger_sell = 0
            orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            orders.append("BUY")

        # enter a sell
        if stocks.ix[x,'sell']==-2 and not trigger_sell and not trigger_buy:
            trigger_sell = 1
            orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            orders.append("SELL")

        #take care of buy entraces and exits

        # in between an entrance and an exit
        if trigger_buy and not stocks.ix[x,'exit_buy']==1 and not trigger_sell:
            stocks.ix[x,'sell']=0
            stocks.ix[x,'exit_sell']=0
            stocks.ix[x,'buy']=0

        # exit a buy
        if trigger_buy and stocks.ix[x,'exit_buy']==1 and not trigger_sell:
            trigger_buy = 0
            orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            orders.append("SELL")

        # enter a buy
        if stocks.ix[x,'buy']==-1 and not trigger_buy and not trigger_sell:
            trigger_buy = 1
            orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            orders.append("BUY")


        if x == stocks.index[-1]:
            if trigger_buy:
                orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
                orders.append("BUY")
            if trigger_sell:
                orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
                orders.append("SELL")




    '''
    #check to see what the first trade is:
    end = stocks.index[0]
    for x in range(0,10):
        if stocks[stocks['sell']==-2].index[0] < stocks[stocks['buy']==-1].index[0]: #figure out if it's a buy or sell
            start = stocks.ix[stocks['sell']==-2].index[0] # it's a sell.  get the first one.
            stocks.ix[start,'sell'] = -200 #set it to -100 so the we can get more -2' in the future
            stocks.ix[end:start,'exit_sell']=0 #get rid of anything before the original sell

            #now work on the exit
            end = stocks.ix[stocks['exit_sell']==2].index[0] #get the exit date

    '''


    stocks.to_csv("stocks.csv")

    print_full(stocks)
    plot_strategy(stocks)

    final = np.array(orders)
    final = final.reshape(len(final)/2,2)
    final = pd.DataFrame(final)

    final.insert(1,'Symbol','IBM')
    final.insert(3,'Shares',100)

    final.columns=['Date','Symbol','Order', 'Shares']
    final.set_index('Date',drop=True, inplace=True)
    final.to_csv('orders.csv')


    
    return

if __name__ == "__main__":
    test_code(['IBM'],'2007-12-31','2009-12-31')