import math as m

# calculate the population standard deviation
def stdev_p(data):
    avg = mean(data)
    sum = 0
    #get the sum of squared deviations from the mean.
    for value in data:
        sum += m.pow(value - avg,2)
    #variance is the above value divided by the number of values
    var = sum/len(data)
    #standard deviation is the square root of the variance
    return m.sqrt(var)

# calculate the sample standard deviation
def stdev_s(data):
    avg = mean(data)
    sum = 0
    #get the sum of squared deviations from the mean.
    for value in data:
        sum += m.pow(value - avg,2)
    #variance is the above value divided by the number of values
    var = sum/(len(data)-1)  #the subtraction of -1 is the Bessel's correction
    #standard deviation is the square root of the variance
    return m.sqrt(var)

def mean(data):
    sum = 0
    for value in data:
        sum = sum + value
    return sum/len(data)






if __name__ == "__main__":
    test = [2.0, 4.0, 4.0, 4.0, 5.0, 5.0, 7.0, 9.0]
    print "the population stdev is", stdev_p(test)
    print "the sample stdev is", stdev_s(test)
    import time
    import numpy as np
    import random
    
    print "------------TESTS-----------"
    print "Test stdev_p -> ", "Passed!" if np.std(test,ddof=0) or 2 == stdev_p(test) else "Failed"
    print "Test stdev_s -> ", "Passed!" if np.std(test,ddof=1) or 2.1380899353 == stdev_s(test) else "Failed"
    
    print "-----------SECONDS-----------"
    #Test 1000 iterations with the test variable
    start = time.time()
    for b in range(1000):
        stdev_p(test)
        stdev_s(test)
    print "[Test data] -> ",time.time()-start
    
    #Test 1000 iterations with a list of random ints
    start = time.time()
    for b in range(1000):
        stdev_p([random.randint(1,10) for r in range(10)])
        stdev_s([random.randint(1,10) for r in range(10)])
    print "[Random ints] -> ",time.time()-start
