import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
import LinRegLearner as lrl
import datetime
from util import *
import marketsim
import math

debug = False;

def createDataSet(stocks, dates, rolling_window):
    stock = stocks[0]
    data = get_data(stocks, pd.date_range(dates[0], dates[1]), addSPY=True)
    data.drop('SPY', axis=1, inplace=True)
    # extract features
    try:
        data['SMA'] = data[stock].rolling(min_periods=1, window=rolling_window,center=False).mean()
        data['stdev'] = data[stock].rolling(min_periods=1, window=rolling_window,center=False).std()
    except:
        data['SMA'] = pd.rolling_mean(data[stock],rolling_window, min_periods=1)
        data['stdev'] = pd.rolling_std(data[stock],rolling_window, min_periods=1)
    data['bb_value'] = (data[stock] - data['SMA'])/(2 * data['stdev'])
    data['momentum'] = (data[stock]/data[stock].shift(5))-1
    data['daily_return'] = (data[stock]/data[stock].shift(1))-1
    try:
        data['volatility'] = data['daily_return'].rolling(min_periods=1, window=rolling_window,center=False).std() 
    except:
        data['volatility'] = pd.rolling_std(data['daily_return'],window=rolling_window,min_periods=1)
    # normalize
    data['volatility'] = normalize(data.momentum)
    data['momentum'] = normalize(data['momentum'])
    data['bb_value'] = normalize(data['bb_value'])
    data['Y'] = data[stock].shift(-5)/data[stock] - 1.0
    data = data.dropna()
    return data[[stock,'bb_value','momentum','volatility','Y']]

def normalize(data):
    return (data - data.mean()) / (data.max() - data.min())

def plot_strategy(name, df, title="Stock prices", xlabel="Date", ylabel="Price", sample_type="in"):
    """Plot stock prices"""
    if not os.path.exists("output"):
            print "...creating output directory"
            os.makedirs("output")
    title = name + " Stock prices"
    ax = df[[name,'Y','Ypred']].plot(title=title, fontsize=12)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax = df[[name,'Y','Ypred']].plot(figsize=(14,6))
    plt.savefig('./output/'+name+'_'+sample_type+'-sample_prices.png')
    ymin, ymax = ax.get_ylim()
    buy = df[(df.buy == 1) & (df.exit == 0)].index
    sell = df[(df.sell == 1) & (df.exit == 0)].index
    exit = df[df.exit == 1].index
    plt.vlines(sell,ymin=ymin, ymax=ymax,color='r')
    plt.vlines(buy,ymin=ymin, ymax=ymax,color='g')
    plt.vlines(exit,ymin=ymin, ymax=ymax,color='k')
    plt.savefig('./output/'+name+'_'+sample_type+'-sample_trades.png')
    if debug:
        plt.show()

def runSimulation(stocks,train_range, test_range, sample_type="in", rolling_window=20):
    stock = stocks[0]
    train = createDataSet(stocks, train_range, rolling_window)
    test = createDataSet(stocks, test_range, rolling_window)
    # create dependent and independent variable datasets
    trainX = train.iloc[:,1:-1]
    trainY = train.iloc[:,-1]
    testX = test.iloc[:,1:-1]

    # using linreg learner
    log("...Using linreg learner")
    learner = lrl.LinRegLearner(verbose = True)
    learner.addEvidence(trainX, trainY)
    Ypred = learner.query(testX)

    rmse = math.sqrt(((trainY - Ypred) ** 2).sum()/trainY.shape[0])
    print "..RMSE: ", rmse
    #c = np.corrcoef(Ypred, y=trainY)
    #print "..corr: ", c[0,1]

    # set up the stage for orders generation
    test['Ypred'] = test[stock] + test[stock] * Ypred[:test.shape[0]]       # scale Ypred to the level of price
    test['Ypred%'] = Ypred[:test.shape[0]] * 100.                           # create a Ypred percentage
    test['bs'] = 0
    test['exit'] = 0
    
    # strategy
    # if YPred% > 1 then buy 100, then go forward 5 days and sell (to exit)
    # if YPred% < 1 then sell 100, then go forward 5 days and buy (to exit)

    x = 4
    threshold = 1
    while x < len(test)-1:
        x+=1
        if x > len(test) or x+5 > len(test):
            break
        elif test.ix[x,'Ypred%'] > threshold:
            test.ix[x,'bs'] = 1
            x += 5 if x+5 < len(test) else 2
            test.ix[x,'bs'] = -1
            test.ix[x,'exit'] = 1
            continue
        elif test.ix[x,'Ypred%'] < -threshold:
            test.ix[x,'bs'] = -1
            x += 5 if x+5 < len(test) else 2
            test.ix[x,'bs'] = 1
            test.ix[x,'exit'] = 1

    test['Order'] = np.where(test.bs==1,'BUY', '')
    test['Order'] = np.where(test.bs==-1,'SELL', test.Order)
    test['Shares'] = abs(test.bs)*100
    test['Symbol'] = stock
    test.index.names = ['Date']
    if not os.path.exists("orders"):
        log( "...creating orders directory")
        os.makedirs("orders")
    test[test.Shares!=0][['Symbol','Order','Shares']].to_csv("./orders/orders.csv")
    #if debug:
    plotData = test[[stock,'Y','Ypred']].copy()
    plotData.index.names = ['Date']
    plotData['Y'] = test[stock] + test[stock] * test['Y']               # scale Y to the level of price
    plotData.ix[:,'buy'] = np.where(test.bs == 1,1,0)
    plotData.ix[:,'sell'] = np.where(test.bs == -1,1,0)
    plotData.ix[:,'exit'] = test['exit']
    plot_strategy(stock, plotData, title="Stock prices", xlabel="Date", ylabel="Price", sample_type=sample_type)
    



def log(x):
    if debug:
        print (x)


if __name__ == "__main__":
    print "ML4T-220"
    print "..in-sample learning"
    runSimulation(['ML4T-220'],('2007-12-31','2009-12-31'),('2007-12-31','2009-12-31'),"in")
    print "...market simulator"
    marketsim.test_code('ML4T-220', sample_type="in")
    print "..out-sample learning"
    runSimulation(['ML4T-220'],('2007-12-31','2009-12-31'),('2009-12-31','2011-12-31'),"out")
    print "...market simulator"
    marketsim.test_code('ML4T-220', sample_type="out")

    print "IBM"
    print "..in-sample learning"
    runSimulation(['IBM'],('2007-12-31','2009-12-31'),('2007-12-31','2009-12-31'),"in")
    print "...market simulator"
    marketsim.test_code('IBM', sample_type="in")
    print "..out-sample learning"
    runSimulation(['IBM'],('2007-12-31','2009-12-31'),('2009-12-31','2011-12-31'),"out")
    print "...market simulator"
    marketsim.test_code('IBM', sample_type="out")