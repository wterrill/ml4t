# bollinger_strategy.py: This code should generate a .png chart that illustrates the bollinger bands,
# and entry and exit points for a bollinger-based strategy.
# It should also generate an orders.txt file that you feed into your market simulator to backtest the strategy.

import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import datetime

def symbol_to_path(symbol, base_dir=os.path.join("..", "data")):
    """Return CSV file path given ticker symbol."""
    return os.path.join(base_dir, "{}.csv".format(str(symbol)))

def get_data(symbols, dates, addSPY=True):
    """Read stock data (adjusted close) for given symbols from CSV files."""
    df = pd.DataFrame(index=dates)
    if addSPY and 'SPY' not in symbols:  # add SPY for reference, if absent
        symbols = ['SPY'] + symbols

    for symbol in symbols:
        df_temp = pd.read_csv(symbol_to_path(symbol), index_col='Date',
                parse_dates=True, usecols=['Date', 'Adj Close'], na_values=['nan'])
        df_temp = df_temp.rename(columns={'Adj Close': symbol})
        df = df.join(df_temp)
        if symbol == 'SPY':  # drop dates SPY did not trade
            df = df.dropna(subset=["SPY"])

    return df


def plot_data(df, buy,sell,exit, title="Stock prices", xlabel="Date", ylabel="Price"):
    """Plot stock prices with a custom title and meaningful axis labels."""
    ax = df.plot(title=title, fontsize=12)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ymin, ymax = ax.get_ylim()
    plt.vlines(sell,ymin=ymin, ymax=ymax,color='r')
    plt.vlines(buy,ymin=ymin, ymax=ymax,color='g')
    plt.vlines(exit,ymin=ymin, ymax=ymax,color='k')
    plt.show()

def print_full(x):
    pd.set_option('display.max_rows', len(x))
    print(x)
    pd.reset_option('display.max_rows')

def test_code(stocks,start,end):
    stocks = get_data(stocks, pd.date_range(start, end), addSPY=True) #Get the stock data
    del stocks['SPY']

    stocks['SMA'] = pd.rolling_mean(stocks['IBM'],20) #Calculate the rolling mean over twenty days
    stocks['slope'] = 20*(stocks['SMA'] - stocks['SMA'].shift(1))
    stocks['meanslope'] = pd.rolling_mean(stocks['slope'],10)
    del stocks['slope']
    stocks['zero'] = 0
    #ok... We have enough data to start going through and making buy/sell decisions.

    trigger_long = False
    trigger_short = False
    waiting_exit_long = True
    waiting_exit_short = True
    last_trade = ""
    orders=[]
    buy=[]
    sell=[]
    exit=[]
    first = True
    amount = 0
    shorting = 0


    for x in range(20,len(stocks)):

        currentday = stocks.index[x]

        if stocks.index[x] == datetime.datetime(2009, 6, 26, 0, 0):
            print ""
        stocks.ix[x,'amount'] = amount
        if stocks.ix[x,'meanslope']<-0.7 and stocks.ix[x-2,'meanslope']>0.7  and not stocks.ix[x-1,'meanslope']=='NaN' and waiting_exit_long:
            orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            orders.append("SELL")
            sell.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            stocks.ix[x, 'Decision'] = "SELL"
            waiting_exit_long = False
            waiting_exit_short = True
            amount = amount - 100

        if stocks.ix[x,'meanslope']>0.7 and stocks.ix[x-2,'meanslope']<-0.7 and not stocks.ix[x-1,'meanslope']=='NaN' and waiting_exit_short:
            orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            orders.append("BUY")
            buy.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            stocks.ix[x, 'Decision'] = "BUY"
            waiting_exit_long = True
            waiting_exit_short = False
            amount = amount + 100
            if shorting == 1:
                orders.append(pd.Timestamp(stocks.index.get_values()[x+1]).date())
                orders.append("BUY")
                buy.append(pd.Timestamp(stocks.index.get_values()[x+1]).date())
                stocks.ix[x+1, 'Decision'] = "BUY"
                waiting_exit_long = True
                waiting_exit_short = False
                amount = amount + 100
                shorting = 0

        if stocks.ix[x, 'meanslope'] >5 and waiting_exit_short:
            orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            orders.append("BUY")
            buy.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            stocks.ix[x, 'Decision'] = "BUY"
            waiting_exit_long = True
            waiting_exit_short = False
            amount = amount + 100



        if stocks.ix[x, 'meanslope'] < -9 and amount == 0 and not stocks.ix[x-1,'meanslope']=='NaN':
            orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            orders.append("SELL")
            sell.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            stocks.ix[x, 'Decision'] = "SELL-short"
            waiting_exit_long = False
            waiting_exit_short = True
            amount = amount - 100
            shorting = 1


        if stocks.ix[x, 'meanslope'] > 0 and amount == 200:
            orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            orders.append("BUY")
            buy.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            stocks.ix[x, 'Decision'] = "BUY"
            waiting_exit_long = True
            waiting_exit_short = False
            amount = amount - 100


        if x==len(stocks)-1 and waiting_exit_long:
            orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            orders.append("SELL")
            sell.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            stocks.ix[x, 'Decision'] = "SELL"
            amount = amount + 100

    finalOrder = np.array(orders)
    finalOrder = finalOrder.reshape(len(finalOrder)/2,2)
    finalOrder = pd.DataFrame(finalOrder)
    finalOrder.insert(1,'Symbol','IBM')
    finalOrder.insert(3,'Shares',100)
    finalOrder.columns=['Date','Symbol','Order', 'Shares']
    finalOrder.set_index('Date',drop=True, inplace=True)
    #create orders folder if it does not exist
    if not os.path.exists("orders"):
        print "...creating orders directory"
        os.makedirs("orders")
    finalOrder.to_csv('./orders/orders.csv')
    print_full(stocks)
    del stocks['amount']
    plot_data(stocks,buy,sell,exit)

    return

if __name__ == "__main__":
    #test_code(['IBM'],'2008-12-31','2011-12-31')
    test_code(['IBM'],'2007-12-31','2009-12-31')
