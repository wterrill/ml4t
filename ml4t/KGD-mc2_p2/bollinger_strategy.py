# bollinger_strategy.py: This code should generate a .png chart that illustrates the bollinger bands,
# and entry and exit points for a bollinger-based strategy.
# It should also generate an orders.txt file that you feed into your market simulator to backtest the strategy.

import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

def symbol_to_path(symbol, base_dir=os.path.join("..", "data")):
    """Return CSV file path given ticker symbol."""
    return os.path.join(base_dir, "{}.csv".format(str(symbol)))

def get_data(symbols, dates, addSPY=True):
    """Read stock data (adjusted close) for given symbols from CSV files."""
    df = pd.DataFrame(index=dates)
    if addSPY and 'SPY' not in symbols:  # add SPY for reference, if absent
        symbols = ['SPY'] + symbols

    for symbol in symbols:
        df_temp = pd.read_csv(symbol_to_path(symbol), index_col='Date',
                parse_dates=True, usecols=['Date', 'Adj Close'], na_values=['nan'])
        df_temp = df_temp.rename(columns={'Adj Close': symbol})
        df = df.join(df_temp)
        if symbol == 'SPY':  # drop dates SPY did not trade
            df = df.dropna(subset=["SPY"])

    return df

def plot_data(df, buy,sell,exit, title="Stock prices", xlabel="Date", ylabel="Price"):
    """Plot stock prices with a custom title and meaningful axis labels."""
    ax = df.plot(title=title, fontsize=12)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ymin, ymax = ax.get_ylim()
    plt.vlines(sell,ymin=ymin, ymax=ymax,color='r')
    plt.vlines(buy,ymin=ymin, ymax=ymax,color='g')
    plt.vlines(exit,ymin=ymin, ymax=ymax,color='k')
    plt.show()

def print_full(x):
    pd.set_option('display.max_rows', len(x))
    print(x)
    pd.reset_option('display.max_rows')

def test_code(stocks,start,end):
    n_days = 5
    stocks = get_data(stocks, pd.date_range(start, end), addSPY=True) #Get the stock data
    del stocks['SPY']

    stocks['SMA'] = pd.rolling_mean(stocks['IBM'],20) #Calculate the rolling mean over twenty days
    #print_full(stocks)
    stocks['bolinger-top'] = stocks['SMA'] + 2 * pd.rolling_std(stocks['IBM'],20) # two standard deviations up.
    #print_full(stocks)
    stocks['bolinger-bot'] = stocks['SMA'] - 2 * pd.rolling_std(stocks['IBM'],20) # two standard deviations down
    #print_full(stocks)

    trigger_long = False
    trigger_short = False
    waiting_exit_long = False
    waiting_exit_short = False
    last_trade = ""
    orders=[]
    buy=[]
    sell=[]
    exit=[]
    for x in range(20,len(stocks)):

        if waiting_exit_short and stocks.ix[x,'IBM'] < stocks.ix[x,'SMA']:
            stocks.ix[x, 'Decision'] = "Buy-Exit"
            orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            orders.append("BUY")
            exit.append(stocks.index.get_values()[x])
            waiting_exit_short = False
            trigger_short = False
            last_trade = "Buy"

        if stocks.ix[x,'IBM'] > stocks.ix[x,'bolinger-top'] and not trigger_short:
            trigger_short = True

        if stocks.ix[x,'IBM'] < stocks.ix[x,'bolinger-top'] and trigger_short and not waiting_exit_short:
            orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            orders.append("SELL")
            sell.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            stocks.ix[x,'Decision']= "Sell"
            waiting_exit_short = True
            last_trade = "Sell"



        if  waiting_exit_long and stocks.ix[x,'IBM'] > stocks.ix[x,'SMA']:
            orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            orders.append("SELL")
            exit.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            stocks.ix[x, 'Decision'] = "Sell-Exit"
            waiting_exit_long = False
            trigger_long = False
            last_trade = "Sell"

        if stocks.ix[x,'IBM'] < stocks.ix[x,'bolinger-bot'] and not trigger_long:
            trigger_long = True

        if stocks.ix[x,'IBM'] > stocks.ix[x,'bolinger-bot'] and trigger_long and not waiting_exit_long:
            orders.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            orders.append("BUY")
            buy.append(pd.Timestamp(stocks.index.get_values()[x]).date())
            stocks.ix[x,'Decision']= "Buy"
            waiting_exit_long = True
            last_trade = "Buy"


    finalOrder=np.array(orders)
    finalOrder = finalOrder.reshape(len(finalOrder)/2,2)
    finalOrder = pd.DataFrame(finalOrder)
    finalOrder.insert(1,'Symbol','IBM')
    finalOrder.insert(3,'Shares',100)
    finalOrder.columns=['Date','Symbol','Order', 'Shares']
    finalOrder.set_index('Date',drop=True, inplace=True)
    #create orders folder if it does not exist
    if not os.path.exists("orders"):
        print "...creating orders directory"
        os.makedirs("orders")
    finalOrder.to_csv('./orders/orders.csv')
    print stocks
    plot_data(stocks,buy,sell,exit)

    return

if __name__ == "__main__":
    test_code(['IBM'],'2007-12-31','2009-12-31')

