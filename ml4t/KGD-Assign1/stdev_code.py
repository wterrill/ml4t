import math as m

# calculate the population standard deviation
def stdev_s(data):
    """
    Computes the sample standard deviation
    """
    n = len(data)
    mean = sum(data)/n
    var = 0
    for x in data:
        var+=(x-mean)**2
    return m.sqrt(var/(n-1))

# calculate the sample standard deviation
def stdev_p(data):
    """
    Computes the population standard deviation
    """
    n = len(data)
    mean = sum(data)/n
    var = 0
    for x in data:
        var+=(x-mean)**2
    return m.sqrt(var/(n))

if __name__ == "__main__":
    import time
    import numpy as np
    import random
    test = [2.0, 4.0, 4.0, 4.0, 5.0, 5.0, 7.0, 9.0]
    print "Test data -> ", str(test)
    print "The population stdev is", stdev_p(test)
    print "The sample stdev is", stdev_s(test)
    
    print "------------TESTS-----------"
    print "Test stdev_p -> ", "Passed!" if np.std(test,ddof=0) or 2 == stdev_p(test) else "Failed"
    print "Test stdev_s -> ", "Passed!" if np.std(test,ddof=1) or 2.1380899353 == stdev_s(test) else "Failed"
    
    print "-----------SECONDS-----------"
    #Test 1000 iterations with the test variable
    start = time.time()
    for b in range(1000):
        stdev_p(test)
        stdev_s(test)
    print "[Test data] -> ",time.time()-start
    
    #Test 1000 iterations with a list of random ints
    start = time.time()
    for b in range(1000):
        stdev_p([random.randint(1,10) for r in range(10)])
        stdev_s([random.randint(1,10) for r in range(10)])
    print "[Random ints] -> ",time.time()-start
        
