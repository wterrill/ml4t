"""MC1-P2: Optimize a portfolio."""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import datetime as dt
from util import get_data, plot_data
import scipy.optimize as spo

# This is the function that will be tested by the autograder
# The student must update this code to properly implement the functionality
def optimize_portfolio(sd=dt.datetime(2008,1,1), ed=dt.datetime(2009,1,1), \
    syms=['GOOG','AAPL','GLD','XOM'], gen_plot=False):

    # Read in adjusted closing prices for given symbols, date range
    dates = pd.date_range(sd, ed)
    prices_all = get_data(syms, dates)  # automatically adds SPY
    prices = prices_all[syms]  # only portfolio symbols
    prices_SPY = prices_all['SPY']  # only SPY, for comparison later

    # Get daily portfolio value, normalize them and find the optimal allocation
    nd_port = prices.values
    nd_port =  nd_port/nd_port[0]
    opt_alloc = fit_alloc(nd_port, error_neg_sharpe) #calling parameters: normalized portfolio and error function

    # Compute statistics using the optimal allocation
    nd_port = nd_port * opt_alloc
    cr, adr, sddr, sr = stats_from_allocated(nd_port)
    prt_dly_val = nd_port.sum(axis = 1)


    # Compare daily portfolio value with SPY using a normalized plot
    if gen_plot:
        # add code to plot here
        df = pd.DataFrame(prt_dly_val/prt_dly_val[0], prices.index.tolist(), ["Portfolio"])
        df_temp = pd.concat([df, prices_SPY/prices_SPY[0]], axis=1)
        print syms
        title = syms[0]
        itersyms = iter(syms)
        next(itersyms)
        for sym in itersyms:
            title = title +", "+ sym
        ax = df_temp.plot(title= "Optimized allocations of: "+ title + " versus SPY" ,fontsize=12)
        ax.set_xlabel("Date")
        ax.set_ylabel("Normalized Price")
        plt.legend(loc='upper left' )
        plt.savefig('output/comparison_optimal.png')
        plt.show()

    return opt_alloc, cr, adr, sddr, sr

def stats_from_allocated(nd_port):
    '''
    :param nd_port: The normalized portfolio multiplied by some allocation
    :return: cr = cumulative return
             adr = average daily return
             sddr = standard deviation of daily return
             sr = Sharpe's Ratio.
    '''
    rfr = 0  # The risk free rate for the entire period
    sf = 252 # Sampling frequency per year

    # Create the daily return array
    prt_dly_val = nd_port.sum(axis = 1)
    daily_returns = prt_dly_val.copy()
    daily_returns[1:] = (prt_dly_val[1:]/prt_dly_val[:-1]) - 1
    daily_returns[0] = 0

    # Calculate stats
    adr = daily_returns[1:].mean()
    sddr = np.std(daily_returns[1:],ddof=1)
    sr = ((adr - rfr)/ sddr) * (sf ** 0.5)
    cr = prt_dly_val[-1]/prt_dly_val[0] - 1

    return cr, adr, sddr, sr


def error_neg_sharpe(allocs, data):
    '''
    :param allocs: This is the guess that the optimizer is making for any given iteration
    :param data: This is the normalized portfolio values
    :return: return the negative of SR in order to maximize it.
    '''
    # error function - allocs here is the guess that the optimizer is making.

    nd_port = data * allocs
    cr, adr, sddr, sr = stats_from_allocated(nd_port)
    # the -1 below is since we are trying to maximize SR, which is the same as minimizing negative SR.
    return -1 * sr


def fit_alloc(data, error_func):
    '''

    :param data: This is the normalized portfolio of all the stocks being investigated
    :param error_func: This is the calculation of the negative sharpe value
    :return: result.x = optimal allocation of stocks for that time period.
    '''

    #initial allocation guess is a ndarray of equally allocations
    allocs = np.zeros(data.shape[1])
    allocs = allocs + float(1.0/data.shape[1])

    # boundary for each allocation
    bnds = []
    for num in range(data.shape[1]):
        bnds.append((0,1))

    # constraint across all allocations
    #cons = ({'type': 'eq', 'fun': lambda x:  x[0]+x[1]+x[2]+x[3] - 1})
    cons = ({'type': 'eq', 'fun': lambda x:  np.sum(x) - 1})

    #Call optimizer to minimize error function
    result = spo.minimize(error_func, allocs, args=(data,), method='SLSQP', options={'disp':True}, bounds=bnds, constraints=cons)

    return result.x

if __name__ == "__main__":

    start_date = dt.datetime(2010,1,1)
    end_date = dt.datetime(2010,12,31)
    symbols = ['GOOG', 'AAPL', 'GLD', 'XOM']

    # Assess the portfolio
    allocations, cr, adr, sddr, sr = optimize_portfolio(sd = start_date, ed = end_date,\
        syms = symbols, \
        gen_plot = True)

    # Print statistics
    print"************************"
    print "Start Date:", start_date
    print "End Date:", end_date
    print "Symbols:", symbols
    print "Allocations:", allocations
    print "Sharpe Ratio:", sr
    print "Volatility (stdev of daily returns):", sddr
    print "Average Daily Return:", adr
    print "Cumulative Return:", cr


    '''
    Start Date: 2010-01-01
    End Date: 2010-12-31
    Symbols: ['GOOG', 'AAPL', 'GLD', 'XOM']
    Optimal allocations: [  5.38105153e-16   3.96661695e-01   6.03338305e-01  -5.42000166e-17]
    Sharpe Ratio: 2.00401501102
    Volatility (stdev of daily returns): 0.0101163831312
    Average Daily Return: 0.00127710312803
    Cumulative Return: 0.360090826885
    '''

    start_date = dt.datetime(2004,1,1)
    end_date = dt.datetime(2006,1,1)
    symbols = ['AXP', 'HPQ', 'IBM', 'HNZ']

    # Assess the portfolio
    allocations, cr, adr, sddr, sr = optimize_portfolio(sd = start_date, ed = end_date,\
        syms = symbols, \
        gen_plot = True)

    # Print statistics
    print"************************"
    print "Start Date:", start_date
    print "End Date:", end_date
    print "Symbols:", symbols
    print "Allocations:", allocations
    print "Sharpe Ratio:", sr
    print "Volatility (stdev of daily returns):", sddr
    print "Average Daily Return:", adr
    print "Cumulative Return:", cr

    '''

    Start Date: 2004-01-01
    End Date: 2006-01-01
    Symbols: ['AXP', 'HPQ', 'IBM', 'HNZ']
    Optimal allocations: [  7.75113042e-01   2.24886958e-01  -1.18394877e-16  -7.75204553e-17]
    Sharpe Ratio: 0.842697383626
    Volatility (stdev of daily returns): 0.0093236393828
    Average Daily Return: 0.000494944887734
    Cumulative Return: 0.255021425162

    '''

    start_date = dt.datetime(2004,12,1)
    end_date = dt.datetime(2006,5,31)
    symbols = ['YHOO', 'XOM', 'GLD', 'HNZ']

    # Assess the portfolio
    allocations, cr, adr, sddr, sr = optimize_portfolio(sd = start_date, ed = end_date,\
        syms = symbols, \
        gen_plot = False)

    # Print statistics
    print"************************"
    print "Start Date:", start_date
    print "End Date:", end_date
    print "Symbols:", symbols
    print "Allocations:", allocations
    print "Sharpe Ratio:", sr
    print "Volatility (stdev of daily returns):", sddr
    print "Average Daily Return:", adr
    print "Cumulative Return:", cr

    '''


    Start Date: 2004-12-01
    End Date: 2006-05-31
    Symbols: ['YHOO', 'XOM', 'GLD', 'HNZ']
    Optimal allocations: [ -3.84053467e-17   7.52817663e-02   5.85249656e-01   3.39468578e-01]
    Sharpe Ratio: 1.5178365773
    Volatility (stdev of daily returns): 0.00797126844855
    Average Daily Return: 0.000762170576913
    Cumulative Return: 0.315973959221

    '''

    start_date = dt.datetime(2005,12,1)
    end_date = dt.datetime(2006,5,31)
    symbols = ['YHOO', 'HPQ', 'GLD', 'HNZ']

    # Assess the portfolio
    allocations, cr, adr, sddr, sr = optimize_portfolio(sd = start_date, ed = end_date,\
        syms = symbols, \
        gen_plot = False)

    # Print statistics
    print"************************"
    print "Start Date:", start_date
    print "End Date:", end_date
    print "Symbols:", symbols
    print "Allocations:", allocations
    print "Sharpe Ratio:", sr
    print "Volatility (stdev of daily returns):", sddr
    print "Average Daily Return:", adr
    print "Cumulative Return:", cr

    '''

    Start Date: 2005-12-01
    End Date: 2006-05-31
    Symbols: ['YHOO', 'HPQ', 'GLD', 'HNZ']
    Optimal allocations: [ -1.67414005e-15   1.01227499e-01   2.46926722e-01   6.51845779e-01]
    Sharpe Ratio: 3.2334265871
    Volatility (stdev of daily returns): 0.00842416845541
    Average Daily Return: 0.00171589132005
    Cumulative Return: 0.229471589743

    Plot comparing the optimal portfolio with SPY as comparison_optimal.png using the following parameters:
    Start Date: 2010-01-01, End Date: 2010-12-31, Symbols: ['GOOG', 'AAPL', 'GLD', 'HNZ']
    '''
    start_date = dt.datetime(2009,1,1)
    end_date = dt.datetime(2010,12,31)
    symbols = ['IBM', 'AAPL', 'HNZ', 'XOM', 'GLD']



    # Assess the portfolio
    allocations, cr, adr, sddr, sr = optimize_portfolio(sd = start_date, ed = end_date,\
        syms = symbols, \
        gen_plot = True)

    # Print statistics
    print"************************"
    print"************************"
    print"************************"
    print "Start Date:", start_date
    print "End Date:", end_date
    print "Symbols:", symbols
    print "Allocations:", allocations
    print "Sharpe Ratio:", sr
    print "Volatility (stdev of daily returns):", sddr
    print "Average Daily Return:", adr
    print "Cumulative Return:", cr
    print"************************"
    print"************************"
    print"************************"