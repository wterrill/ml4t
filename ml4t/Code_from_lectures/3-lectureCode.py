"""Plot High prices for IBM"""

import pandas as pd
import matplotlib.pyplot as plt

def test_run():
    df = pd.read_csv("../data/IBM.csv")
    # Your code here

    df[['High','Low']].plot()
    plt.xlabel('x label')
    plt.ylabel('y label')
    plt.title('put title here')
    #plt.text(60, .025, r'$\mu=100,\ \sigma=15$')
    #plt.axis([40, 160, 0, 0.03])
    plt.grid(True)
    plt.show()  # must be called to show plots


if __name__ == "__main__":
    test_run()



