"""MC1-P2: Optimize a portfolio."""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import datetime as dt
from util import get_data, plot_data
import scipy.optimize as spo

# This is the function that will be tested by the autograder
# The student must update this code to properly implement the functionality
def optimize_portfolio(sd=dt.datetime(2008,1,1), ed=dt.datetime(2009,1,1), \
    syms=['GOOG','AAPL','GLD','XOM'], gen_plot=False):

    # Read in adjusted closing prices for given symbols, date range
    dates = pd.date_range(sd, ed)
    prices_all = get_data(syms, dates)  # automatically adds SPY
    prices = prices_all[syms]  # only portfolio symbols
    prices_SPY = prices_all['SPY']  # only SPY, for comparison later

    # Get daily portfolio value, normalize them and find the optimal allocation
    nd_port = prices.values
    nd_port =  nd_port/nd_port[0]
    opt_alloc = fit_alloc(nd_port, error_neg_sharpe) #calling parameters: normalized portfolio and error function

    # Compute statistics using the optimal allocation
    nd_port = nd_port * opt_alloc
    cr, adr, sddr, sr = stats_from_allocated(nd_port)
    prt_dly_val = nd_port.sum(axis = 1)


    # Compare daily portfolio value with SPY using a normalized plot
    if gen_plot:
        # add code to plot here
        df = pd.DataFrame(prt_dly_val/prt_dly_val[0], prices.index.tolist(), ["Portfolio"])
        df_temp = pd.concat([df, prices_SPY/prices_SPY[0]], axis=1)
        print syms
        title = syms[0]
        itersyms = iter(syms)
        next(itersyms)
        for sym in itersyms:
            title = title +", "+ sym
        ax = df_temp.plot(title= "Optimized allocations of: "+ title + " versus SPY" ,fontsize=12)
        ax.set_xlabel("Date")
        ax.set_ylabel("Normalized Price")
        plt.legend(loc='upper left' )
        plt.savefig('output/comparison_optimal.png')
        plt.show()

    return opt_alloc, cr, adr, sddr, sr

def stats_from_allocated(nd_port):
    """
    :param nd_port: The normalized portfolio multiplied by some allocation
    :return: cr = cumulative return
             adr = average daily return
             sddr = standard deviation of daily return
             sr = Sharpe's Ratio.
    """
    rfr = 0  # The risk free rate for the entire period
    sf = 252 # Sampling frequency per year

    # Create the daily return array
    prt_dly_val = nd_port.sum(axis = 1)
    daily_returns = prt_dly_val.copy()
    daily_returns[1:] = (prt_dly_val[1:]/prt_dly_val[:-1]) - 1
    daily_returns[0] = 0

    # Calculate stats
    adr = daily_returns[1:].mean()
    sddr = np.std(daily_returns[1:],ddof=1)
    sr = ((adr - rfr)/ sddr) * (sf ** 0.5)
    cr = prt_dly_val[-1]/prt_dly_val[0] - 1

    return cr, adr, sddr, sr


def error_neg_sharpe(allocs, data):
    """
    :param allocs: This is the guess that the optimizer is making for any given iteration
    :param data: This is the normalized portfolio values
    :return: return the negative of SR in order to maximize it.
    """
    # error function - allocs here is the guess that the optimizer is making.

    nd_port = data * allocs
    cr, adr, sddr, sr = stats_from_allocated(nd_port)
    # the -1 below is since we are trying to maximize SR, which is the same as minimizing negative SR.
    return -1 * sr


def fit_alloc(data, error_func):
    """
    :param data: This is the normalized portfolio of all the stocks being investigated
    :param error_func: This is the calculation of the negative sharpe value
    :return: result.x = optimal allocation of stocks for that time period.
    """

    #initial allocation guess is a ndarray of equally allocations
    allocs = np.zeros(data.shape[1])
    allocs = allocs + float(1.0/data.shape[1])

    # boundary for each allocation
    bnds = []
    for num in range(data.shape[1]):
        bnds.append((0,1))

    # constraint across all allocations
    cons = ({'type': 'eq', 'fun': lambda x:  np.sum(x) - 1})

    #Call optimizer to minimize error function
    result = spo.minimize(error_func, allocs, args=(data,), method='SLSQP', options={'disp':True}, bounds=bnds, constraints=cons)

    return result.x

if __name__ == "__main__":
    
    arguments = [
    [dt.datetime(2010,1,1),dt.datetime(2010,12,31),['GOOG', 'AAPL', 'GLD', 'XOM']],
     [dt.datetime(2004,1,1),dt.datetime(2006,1,1),['AXP', 'HPQ', 'IBM', 'HNZ']],
      [dt.datetime(2004,12,1),dt.datetime(2006,5,31),['YHOO', 'XOM', 'GLD', 'HNZ']],
       [dt.datetime(2005,12,1),dt.datetime(2006,5,31),['YHOO', 'HPQ', 'GLD', 'HNZ']],
        [dt.datetime(2009,1,1),dt.datetime(2010,12,31),['IBM', 'AAPL', 'HNZ', 'XOM', 'GLD']],
         ]
    
    for s, e, sym in arguments:
        optimize_portfolio(sd = s, ed = e, syms = sym, gen_plot = True) 
        
