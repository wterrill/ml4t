"""MC1-P1: Analyze a portfolio."""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import datetime as dt
from util import get_data, plot_data
import datetime as dt

# This is the function that will be tested by the autograder
# The student must update this code to properly implement the functionality
def assess_portfolio(sd = dt.datetime(2008,1,1), ed = dt.datetime(2009,1,1), \
    syms = ['GOOG','AAPL','GLD','XOM'], \
    allocs=[0.1,0.2,0.3,0.4], \
    sv=1000000, rfr=0.0, sf=252.0, \
    gen_plot=False):
    """
    Access a portfolio by returning various portfolio statistics
    :param sd: A datetime object that represents the start date
    :param ed: A datetime object that represents the end date
    :param syms: A list of symbols that make up the portfolio
    :param allocs: A list of allocations to the stocks, must sum to 1.0
    :param sv: Start value of the portfolio
    :param rfr: The risk free rate for the entire period
    :param sf: Sampling frequency per year
    :param gen_plot: If True, create a plot named plot.png
    """
    # Check inputs
    if sum(allocs)<>1:
        raise ValueError("Allocs must add up to 1")
    if sd>ed:
        raise ValueError("Start date cannot be greater than end date")
    if len(syms) < 1:
        raise ValueError("Symbols cannot be empty")
    
    
    
    # Read in adjusted closing prices for given symbols, date range
    dates = pd.date_range(sd, ed)
    prices_all = get_data(syms, dates)          # automatically adds SPY
    prices_all = prices_all.fillna(method='ffill') #forward fill na's
    prices_all = prices_all.fillna(method='bfill') #back fill na's
    prices = prices_all[syms]                   # only portfolio symbols
    prices_SPY = prices_all['SPY']              # only SPY, for comparison later

    # Compute daily portfolio values
    nd1 = prices_all.values                     
    nd_port = nd1[:,1:5]                        #get rid of SPY
    nd_port = nd_port/nd_port[0]                #normalize prices
    nd_port = nd_port * allocs                  #convert prices to allocated performance
    nd_port = nd_port * sv                      #multiply by the starting value of overall portfolio
    np_oirt_dly_val = nd_port.sum(axis = 1)     #sum the values of the 4 stocks for every day to get the daily value.

    # Get portfolio statistics 
    cr = np_oirt_dly_val[-1]/np_oirt_dly_val[0] - 1     #divide the last value by the first

    daily_returns = np_oirt_dly_val.copy()      
    daily_returns[1:] = \
        (np_oirt_dly_val[1:]/np_oirt_dly_val[:-1]) - 1  #divide the daily value by the one before it.
    daily_returns[0] = 0                        #since here is no period before the first row there is no returns on first row.
    adr = daily_returns[1:].mean()              #Get the mean from all the values except for the first one
    sddr = np.std(daily_returns[1:],ddof=1)     #Standard deviation of daily return
    sr = ((adr - rfr)/ sddr) * (sf ** 0.5)      #sharpe ratio

    # Compare daily portfolio value with SPY using a normalized plot
    if gen_plot:
        #TODO add code to plot here
        df = pd.DataFrame(np_oirt_dly_val/np_oirt_dly_val[0], prices.index.tolist(), ["Portfolio"])
        df_temp = pd.concat([df, prices_SPY/prices_SPY[0]], axis=1)
        ax = df_temp.plot(title="Normalized Portfolio versus Normalized SPY",fontsize=12)
        ax.set_xlabel("Date")
        ax.set_ylabel("Normalized Price")
        plt.show()
        pass


    # TODO Add code here to properly compute end value
    ev = np.around(np_oirt_dly_val[-1], decimals=2)
    return cr, adr, sddr, sr, ev

if __name__ == "__main__":
    # This code WILL NOT be tested by the auto grader
    # It is only here to help you set up and test your code
    
    print "Test1", (0.25564678453350465, 0.00095736623423814133, 0.010010402800015371, \
                    1.5181924364126345, 1255646.78) == \
                    assess_portfolio(dt.datetime(2010,1,1),dt.datetime(2010,12,31),\
                    ['GOOG', 'AAPL', 'GLD', 'XOM'],[0.2, 0.3, 0.4, 0.1],\
                    1000000,0.0,252)
                    
    print "Test2", (0.19810596365497823, 0.00076310615267202893, 0.0092615312876845723, \
                    1.3079839874416057, 1198105.96) == \
                    assess_portfolio(dt.datetime(2010,1,1),dt.datetime(2010,12,31),\
                    ['AXP', 'HPQ', 'IBM', 'HNZ'],[0.0, 0.0, 0.0, 1.0],\
                    1000000,0.0,252)
                    
    print "Test3", (0.091629275658909437, 0.00065159405233020313, 0.011264100192701125, \
                    0.91829216115368417, 1091629.28) == \
                    assess_portfolio(dt.datetime(2010,06,1),dt.datetime(2010,12,31),\
                    ['AXP', 'HPQ', 'IBM', 'HNZ'],[0.2, 0.3, 0.4, 0.1],\
                    1000000,0.0,252)
                    
    print "Test4", (0.11412851097387788, 0.00078093355518605114, 0.010560182755967731, \
                    1.1739319433578221, 1114128.51) == \
                    assess_portfolio(dt.datetime(2010,06,1),dt.datetime(2010,12,31),\
                    ['AXP', 'HPQ', 'IBM', 'HNZ'],[0.2, 0.2, 0.4, 0.2],\
                    1000000,0.0,252,True)