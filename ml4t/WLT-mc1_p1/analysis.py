"""MC1-P1: Analyze a portfolio."""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import datetime as dt
from util import get_data, plot_data
import datetime as dt

# This is the function that will be tested by the autograder
# The student must update this code to properly implement the functionality
def assess_portfolio(sd = dt.datetime(2008,1,1), ed = dt.datetime(2009,1,1), \
    syms = ['GOOG','AAPL','GLD','XOM'], \
    allocs=[0.1,0.2,0.3,0.4], \
    sv=1000000, rfr=0.0, sf=252.0, \
    gen_plot=False):
    """
    Access a portfolio by returning various portfolio statistics
    :param sd: A datetime object that represents the start date
    :param ed: A datetime object that represents the end date
    :param syms: A list of symbols that make up the portfolio
    :param allocs: A list of allocations to the stocks, must sum to 1.0
    :param sv: Start value of the portfolio
    :param rfr: The risk free rate for the entire period
    :param sf: Sampling frequency per year
    :param gen_plot: If True, create a plot named plot.png
    """

    # Read in adjusted closing prices for given symbols, date range
    dates = pd.date_range(sd, ed)
    prices_all = get_data(syms, dates)  # automatically adds SPY
    prices = prices_all[syms]  # only portfolio symbols
    prices_SPY = prices_all['SPY']  # only SPY, for comparison later

    # Get daily portfolio value
    port_val = prices_SPY
    # TODO add code here to compute daily portfolio values

    nd1 = prices_all.values                     #convert to numpy
    nd_port = nd1[:,1:5]                        #This gets rid of SPY
    nd_port= nd_port/nd_port[0]                 #this normalizes the prices
    nd_port = nd_port * allocs                  #this gets the allocations set for the portfolio
    nd_port = nd_port * sv                      #multiplying by the starting amount gets the positional value.
    prt_dly_val = nd_port.sum(axis = 1)         #This sums up the values of the 4 stocks for every day to get the daily value.

    # Get portfolio statistics (note: std_daily_ret = volatility)
    cr = prt_dly_val[-1]/prt_dly_val[0] - 1     #This divides the last value by the first

    daily_returns = prt_dly_val.copy()          #copy to make a dataframe match shape and size
    daily_returns[1:] = \
        (prt_dly_val[1:]/prt_dly_val[:-1]) - 1  #This divides the daily value by the one before it.
    daily_returns[0] = 0                        #not sure why this matters, but it was in the lecture.
    adr = daily_returns[1:].mean()              #Take the mean from all the values except for the first one
    sddr = np.std(daily_returns[1:],ddof=1)     #******??? what the heck???****** why does this give an "almost" value?
    sr = ((adr - rfr)/ sddr) * (sf ** 0.5)

    # Compare daily portfolio value with SPY using a normalized plot
    if gen_plot:
        #TODO add code to plot here
        df = pd.DataFrame(prt_dly_val/prt_dly_val[0], prices.index.tolist(), ["Portfolio"])
        df_temp = pd.concat([df, prices_SPY/prices_SPY[0]], axis=1)
        ax = df_temp.plot(title="Normalized Portfolio versus Normalized SPY",fontsize=12)
        ax.set_xlabel("Date")
        ax.set_ylabel("Normalized Price")
        plt.savefig('output/plot.png')
        #plt.show()
        pass

    ev = prt_dly_val[-1]
    return cr, adr, sddr, sr, ev

if __name__ == "__main__":
    # This code WILL NOT be tested by the auto grader
    # It is only here to help you set up and test your code

    # Define input parameters
    # Note that ALL of these values will be set to different values by
    # the autograder!
    start_date = dt.datetime(2010,1,1)
    end_date = dt.datetime(2010,12,31)
    symbols = ['GOOG', 'AAPL', 'GLD', 'XOM']
    allocations = [0.2, 0.3, 0.4, 0.1]
    start_val = 1000000  
    risk_free_rate = 0.0
    sample_freq = 252

    # Assess the portfolio
    cr, adr, sddr, sr, ev = assess_portfolio(sd = start_date, ed = end_date,\
        syms = symbols, \
        allocs = allocations,\
        sv = start_val, \
        gen_plot = True)

    # Print statistics
    print "Start Date:", start_date
    print "End Date:", end_date
    print "Symbols:", symbols
    print "Allocations:", allocations
    print "Sharpe Ratio:", sr
    print "Volatility (stdev of daily returns):", sddr
    print "Average Daily Return:", adr
    print "Cumulative Return:", cr
    print "Ending value:", ev

    print ""
    print "*****Correct Answers******"
    print "Start Date: 2010-01-01"
    print "End Date: 2010-12-31"
    print "Symbols: ['GOOG', 'AAPL', 'GLD', 'XOM']"
    print "Allocations: [0.2, 0.3, 0.4, 0.1]"
    print "Sharpe Ratio: 1.51819243641"
    print "Volatility (stdev of daily returns): 0.0100104028"
    print "Average Daily Return: 0.000957366234238"
    print "Cumulative Return: 0.255646784534"

    start_date = dt.datetime(2010,1,1)
    end_date = dt.datetime(2010,12,31)
    symbols = ['AXP', 'HPQ', 'IBM', 'HNZ']
    allocations = [0.0, 0.0, 0.0, 1.0]
    start_val = 1000000
    risk_free_rate = 0.0
    sample_freq = 252

    # Assess the portfolio
    cr, adr, sddr, sr, ev = assess_portfolio(sd = start_date, ed = end_date,\
        syms = symbols, \
        allocs = allocations,\
        sv = start_val, \
        gen_plot = True)

    # Print statistics
    print "Start Date:", start_date
    print "End Date:", end_date
    print "Symbols:", symbols
    print "Allocations:", allocations
    print "Sharpe Ratio:", sr
    print "Volatility (stdev of daily returns):", sddr
    print "Average Daily Return:", adr
    print "Cumulative Return:", cr
    print "Ending value:", ev

    print ""
    print "*****Correct Answers******"
    print "Start Date: 2010-01-01"
    print "End Date: 2010-12-31"
    print "Symbols: ['AXP', 'HPQ', 'IBM', 'HNZ']"
    print "Allocations: [0.0, 0.0, 0.0, 1.0]"
    print "Sharpe Ratio: 1.30798398744"
    print "Volatility (stdev of daily returns): 0.00926153128768"
    print "Average Daily Return: 0.000763106152672"
    print "Cumulative Return: 0.198105963655"
    print ""

    start_date = dt.datetime(2010,6,1)
    end_date = dt.datetime(2010,12,31)
    symbols = ['GOOG', 'AAPL', 'GLD', 'XOM']
    allocations = [0.2, 0.3, 0.4, 0.1]
    start_val = 1000000
    risk_free_rate = 0.0
    sample_freq = 252

    # Assess the portfolio
    cr, adr, sddr, sr, ev = assess_portfolio(sd = start_date, ed = end_date,\
        syms = symbols, \
        allocs = allocations,\
        sv = start_val, \
        gen_plot = True)

    # Print statistics
    print "Start Date:", start_date
    print "End Date:", end_date
    print "Symbols:", symbols
    print "Allocations:", allocations
    print "Sharpe Ratio:", sr
    print "Volatility (stdev of daily returns):", sddr
    print "Average Daily Return:", adr
    print "Cumulative Return:", cr
    print "Ending value:", ev
    print ""
    print "*****Correct Answers******"
    print "Start Date: 2010-06-01"
    print "End Date: 2010-12-31"
    print "Symbols: ['GOOG', 'AAPL', 'GLD', 'XOM']"
    print "Allocations: [0.2, 0.3, 0.4, 0.1]"
    print "Sharpe Ratio: 2.21259766672"
    print "Volatility (stdev of daily returns): 0.00929734619707"
    print "Average Daily Return: 0.00129586924366"
    print "Cumulative Return: 0.205113938792"


    start_date = dt.datetime(2010,1,1)
    end_date = dt.datetime(2010,12,31)
    symbols = ['GOOG', 'AAPL', 'GLD', 'XOM']
    allocations = [0.2, 0.2, 0.4, 0.2]
    start_val = 1000000
    risk_free_rate = 0.0
    sample_freq = 252

    # Assess the portfolio
    cr, adr, sddr, sr, ev = assess_portfolio(sd = start_date, ed = end_date,\
        syms = symbols, \
        allocs = allocations,\
        sv = start_val, \
        gen_plot = True)
