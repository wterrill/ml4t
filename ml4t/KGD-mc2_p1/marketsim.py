"""MC2-P1: Market simulator."""

import numpy as np
import pandas as pd
from util import get_data, plot_data

def assess_portfolio(portfolio, rfr=0.0, days=252.0, gen_plot=False):
    cumret = portfolio.values[-1]/portfolio.values[0] - 1     
    daily_returns = portfolio.copy()          
    daily_returns[1:] = ((portfolio.values[1:])/(portfolio.values[:-1])) - 1  
    daily_returns[0] = 0                        
    avg = daily_returns[1:].mean()              
    risk = np.std(daily_returns[1:], ddof=1)
    sratio = ((avg - rfr)/ risk) * (days ** 0.5)
    ev = portfolio.values[-1]
    return cumret, avg, risk, sratio, ev

def compute_trades(df_stocks,symbol,df_orders):
    sp = df_stocks.shape[0] if df_stocks.shape[0] > 0 else 1
    trades = pd.DataFrame(np.zeros((sp, len(symbol)+1)), index = df_stocks.index, columns=df_stocks.columns)
    
    for order in df_orders.iterrows(): 
        x = -1 * order[1]['Shares'] if order[1]['Order']=="SELL" else order[1]['Shares']
        if order[0] in df_stocks.index:
            price = df_stocks.ix[order[0],order[1]['Symbol']]
            trades.ix[order[0],order[1]['Symbol']]+= x
            trades.ix[order[0],'Cash'] += -1.0 * price * x           #reduce cash
    return trades

def compute_holdings(trades, cash):
    holdings = trades.copy()
    sp = holdings.shape[1] if holdings.shape[1] > 0 else 1
    start = np.zeros(sp)
    start[-1] = cash
    for x in holdings.iterrows():
        start += x[1].values
        holdings.ix[x[0]] = start
    return holdings

def compute_portvals(orders_file = "./orders/orders-leverage-2.csv", start_val = 1000000):
    df_orders = pd.read_csv(orders_file, index_col='Date',parse_dates=True, na_values=['nan']).sort_index(ascending=1)
    
    #get unique symbols
    sym = df_orders['Symbol'].unique().tolist()
    start, end  = df_orders.index.values[0],df_orders.index.values[-1]

    df_stocks = get_data(sym, pd.date_range(start, end), addSPY=True)
    df_stocks.dropna(subset=['SPY'], inplace=True) 
    df_stocks.drop(['SPY'], axis=1, inplace=True)
    sp = df_stocks.shape[0] if df_stocks.shape[0] > 0 else 1
    df_stocks['Cash'] = np.ones(sp) 

    trades = compute_trades(df_stocks, sym, df_orders)
    holdings = compute_holdings(trades,start_val)
    total = df_stocks * holdings
    portvals = total.sum(axis=1)

    for order in df_orders.iterrows():
        if order[0] in df_stocks.index:
            temp = total.ix[order[0],:]
            x, y = sum(np.abs(temp[0:len(temp)-1])),sum(total.ix[order[0],:])
            leverage = x / y
            if (leverage > 2):
                df_orders.drop([order[0]],inplace=True)
                trades = compute_trades(df_stocks,sym,df_orders)
                holdings = compute_holdings(trades,start_val)
                total = df_stocks * holdings
                portvals = total.sum(axis=1)
    return portvals

def test_code():
    # this is a helper function you can use to test your code
    # note that during autograding his function will not be called.
    # Define input parameters

    of = "./orders/orders-leverage-2.csv"
    sv = 1000000

    # Process orders
    portvals = compute_portvals(orders_file = of, start_val = sv)
    if isinstance(portvals, pd.DataFrame):
        portvals = portvals[portvals.columns[0]] # just get the first column
    else:
        "warning, code did not return a DataFrame"
    
    # Get portfolio stats
    # Here we just fake the data. you should use your code from previous assignments.
    
    start, end = portvals.index[0], portvals.index[-1]
    cum_ret, avg_daily_ret, std_daily_ret, sharpe_ratio, ev = assess_portfolio(portvals)
    print portvals[-1]

    benchmark = '$SPX'  #even though everything says "SPY" below, the wiki is using $SPX.
    port_SPY = get_data([benchmark], pd.date_range(start, end), addSPY=False)
    port_SPY = port_SPY.dropna(subset=[benchmark])

    cum_ret_SPY, avg_daily_ret_SPY, std_daily_ret_SPY, sharpe_ratio_SPY, ev_SPY = assess_portfolio(port_SPY[benchmark])

    # Compare portfolio against $SPX
    print "Date Range: {} to {}".format(start, end)
    print
    print "Sharpe Ratio of Fund: {}".format(sharpe_ratio)
    print "Sharpe Ratio of SPY : {}".format(sharpe_ratio_SPY)
    print
    print "Cumulative Return of Fund: {}".format(cum_ret)
    print "Cumulative Return of SPY : {}".format(cum_ret_SPY)
    print
    print "Standard Deviation of Fund: {}".format(std_daily_ret)
    print "Standard Deviation of SPY : {}".format(std_daily_ret_SPY)
    print
    print "Average Daily Return of Fund: {}".format(avg_daily_ret)
    print "Average Daily Return of SPY : {}".format(avg_daily_ret_SPY)
    print
    print "Final Portfolio Value: {}".format(portvals[-1])

if __name__ == "__main__":
    test_code()

