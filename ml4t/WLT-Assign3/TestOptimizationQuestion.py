import scipy.optimize as spo
def fun(x):
    print ("This iteration guess is: {} "),x
    value = x[0]**2 + x[1]
    print ("The value of this iteration is: {}"), value
    return -1 *  value
bnds = ((-3, 2), (-3, 2))
res = spo.minimize(fun, (-1, 0), method='SLSQP', bounds=bnds)
print ("final guess: {}").format(res.x)
print ("final value: {}".format(fun(res.x)))


