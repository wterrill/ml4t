"""Implements a bag learner with boosting feature for a dataset with ONLY 2 features and 1 label
    Expected feature row - [X1,X2,Y]
"""
__author__ = "kdaniel38"

import numpy as np
import math

class BagLearner(object):
    """Implements Bootstrap Aggregating learner"""

    def __init__(self, learner, kwargs, bags, boost, verbose = False):
        self.learner = learner
        self.kwargs = kwargs
        self.k = kwargs['k']
        self.bags = bags
        self.boost = boost
        self.verbose = verbose

    def addEvidence(self,dataX,dataY):
        """
        @summary: add data to the learner
        @param dataX: X values of data {x1,x2}
        @param dataY: Y training lables {y}
        """
        self.dataX = dataX
        self.dataY = dataY
        self.learners = []                                      # learners collection
        size = len(dataX)
        kwargs = {"k":self.k, "verbose": self.verbose}
        p = np.array([1./size]*size)                            # build uniform distribution, each observation is equally weighted
        for i in range(self.bags):         
            rnd_ix = np.random.choice(
                range(size),size,replace=True,p=p)              # random indices for the bag based on the probability distribution
            dataX,dataY = self.dataX[[rnd_ix]], self.dataY[[rnd_ix]]        # get new "bagged" data based the random indices
            self.learners.append(self.learner(**kwargs))        
            self.learners[i].addEvidence(dataX,dataY)           # train a learner with the new random "bagged" data
            if(self.boost):                                     # if bosting set to True then apply boosting              
                predictedY = np.average(                        # Query all the leaners with original data and average the result
                        np.array([L.query(self.dataX) for L in self.learners])
                        , axis=0) 
                error = np.square(np.subtract(self.dataY,predictedY))       # find the error residual (real - predicted)
                p += error                                      # add error to wieghted distribution
                p[p>0] /= np.sum(p)                             # normalize the error weighted proba distribution 
                # rinse and repeat :) 

    def query(self,points):
        """
        @summary: Estimate a set of test points given the model
        @param points: numpy array with each row corresponding to a specific query
        @returns the estimated values based on the computed model
        """
        return np.average(np.array([self.learners[i].query(points) for i in range(self.bags)]), axis=0)
        