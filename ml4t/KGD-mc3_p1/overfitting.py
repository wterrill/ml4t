import math
import LinRegLearner as lrl
import KNNLearner as knn
import BagLearner as bl
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os

k = 10
def evaluate(learner,trainX,trainY,testX,testY,k,bags=0):   
    # evaluate in sample
    predY = learner.query(trainX) # get the predictions
    rmse = math.sqrt(((trainY - predY) ** 2).sum()/trainY.shape[0])
    c = np.corrcoef(predY, y=trainY)
    rmseIS = rmse
    corrIS = c[0,1]

    # evaluate out of sample
    predY = learner.query(testX) # get the predictions
    rmse = math.sqrt(((testY - predY) ** 2).sum()/testY.shape[0])
    c = np.corrcoef(predY, y=testY)
    rmseOS = rmse
    corrOS = c[0,1]

    return k,rmseIS, corrIS, rmseOS, corrOS, bags

def runKNNOverfittingExploration(trainX,trainY,testX,testY):
    #k = 10
    results = []
    for i in range(1,k+1):
        kwargs = {"k":i}
        learner = knn.KNNLearner(verbose = False, **kwargs)
        learner.addEvidence(trainX, trainY) # training step
        print "k = ", i
        results.append(evaluate(learner,trainX,trainY,testX,testY,i))
    return results

def runBagOverfittingExploration(trainX,trainY,testX,testY, boost = False):
    #k = 10
    results = []
    learnerKNN = knn.KNNLearner(verbose = False, k=3)
    for i in range(1,k+1):
        learner = bl.BagLearner(learner = knn.KNNLearner, kwargs = {"k":3}, bags = i, boost = boost, verbose = False)
        learner.addEvidence(trainX, trainY) # training step
        if boost:
            print "bags & boost = ", i
        else:
            print "bags = ", i
        results.append(evaluate(learner,trainX,trainY,testX,testY,i,3))
    return results

def runBagKOverfittingExploration(trainX,trainY,testX,testY, boost = False):
    #k = 10
    results = []
    learnerKNN = knn.KNNLearner(verbose = False, k=3)
    for x in range(1,6):
        print "K = ", x
        for i in [10,50,100]:
            learner = bl.BagLearner(learner = knn.KNNLearner, kwargs = {"k":x}, bags = i, boost = boost, verbose = False)
            learner.addEvidence(trainX, trainY) # training step
            if boost:
                print "bags & boost = ", i
            else:
                print "bags = ", i
            results.append(evaluate(learner,trainX,trainY,testX,testY,x,i))
    return results

if __name__ == '__main__':
    inf = open('Data/best4linreg.csv')
    data = np.array([map(float,s.strip().split(',')) for s in inf.readlines()])
    # compute how much of the data is training and testing
    train_rows = math.floor(0.6* data.shape[0])
    test_rows = data.shape[0] - train_rows

    # separate out training and testing data
    trainX = data[:train_rows,0:-1]
    trainY = data[:train_rows,-1]
    testX = data[train_rows:,0:-1]
    testY = data[train_rows:,-1]

    if not os.path.exists("./results/"):
        os.makedirs("./results/")

    #r = np.array(runKNNOverfittingExploration(trainX,trainY,testX,testY))
    #k = str(int(r[-1:,0][0]))
    #pd.DataFrame(r,columns=['K', 'rmseIS', 'corrIS', 'rmseOS', 'corrOS']).to_csv('./results/overfittingKNN_'+k+'.csv',index=False)
    #plt.plot(r[:,0], r[:,1], 'g-', r[:,0], r[:,3], 'r-',)
    #plt.xlabel = "k"
    #plt.ylabel = "RMSE"
    #plt.title = "OVERFITTING Exploration"
    #plt.savefig("./results/overfittingKNN_"+k+".png")
    #plt.clf()
    ##plt.show()

    #r = np.array(runBagOverfittingExploration(trainX,trainY,testX,testY))
    #k = str(int(r[-1:,0][0]))
    #pd.DataFrame(r,columns=['K', 'rmseIS', 'corrIS', 'rmseOS', 'corrOS']).to_csv('./results/overfittingBag_'+k+'.csv',index=False)
    ##plot results
    #plt.plot(r[:,0], r[:,1], 'g-', r[:,0], r[:,3], 'r-')
    #plt.savefig("./results/overfittingBag_"+k+".png")
    #plt.clf()
    ##plt.show()

    #r = np.array(runBagKOverfittingExploration(trainX,trainY,testX,testY,False))
    #k = str(int(r[-1:,0][0]))
    #pd.DataFrame(r,columns=['K', 'rmseIS', 'corrIS', 'rmseOS', 'corrOS', 'bags']).to_csv('./results/overfittingBagK.csv',index=False)
    #plt.plot(r[:,0], r[:,1], 'g-', r[:,0], r[:,3], 'r-')
    #plt.savefig("./results/overfittingBagK_.png")
    #plt.clf()
    ##plt.show()
    #print r

    #for x in [10,20]:
    #    k = x
    #    r = np.array(runBagOverfittingExploration(trainX,trainY,testX,testY))
    #    pd.DataFrame(r,columns=['bags', 'rmseIS', 'corrIS', 'rmseOS', 'corrOS','k']).to_csv('./results/3_groups_overfittingBag_'+str(k)+'.csv',index=False)
    #    plt.plot(r[:,0], r[:,1], 'g-', r[:,0], r[:,3], 'r-')
    #    plt.savefig("./results/3_groups_overfittingBag_"+str(k)+".png")
    #    plt.clf()

    for x in [10,20]:   
        r = np.array(runBagOverfittingExploration(trainX,trainY,testX,testY,True))
        #pd.DataFrame(r,columns=['bags', 'rmseIS', 'corrIS', 'rmseOS', 'corrOS', 'k']).to_csv('./results/best4KNN_overfittingBagBoost_'+str(k)+'.csv',index=False)
        plt.plot(r[:,0], r[:,1], 'g-', r[:,0], r[:,3], 'r-')
        #plt.savefig("./results/best4KNN_overfittingBagBoost_"+str(k)+".png")
        plt.show()
        plt.clf()
        
