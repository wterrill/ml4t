"""Generates data that performs better with a linear regression learner
    Performance is measured by RMSE and Correlation
"""
__author__ = "kdaniel38"

import numpy as np
import matplotlib.pyplot as plt
try:
    from tester import test
except:
    pass

def my_data(size):
    data = np.ones((size,3))
    m,c = .3,5                                              # m and c for line equation y=mx+c
    data[:,1] = np.random.normal(0, scale=1,size=1000)      # 1000 samples from a normal distribution
    data[:,0] = np.linspace(-9.0,9.0,num=1000)              # 1000 samples of monotonicly increasing numbers
    data[:,2] = m*(data[:,0]+data[:,1]) + c                 # calculate the real Y using the line equation
    np.savetxt("./Data/best4linreg.csv", data, delimiter=",")
    return data

def scatterPlot(a,b):
    plt.scatter(a, b, s=5, c='b', alpha=0.5)
    plt.show()

if __name__ == "__main__":
    graph = False
    print "------Best 4 LinReg ------"
    data = my_data(1000)
    try:
        test(data)
        if graph:
            scatterPlot(data[:,0],data[:,1])
    except:
        pass