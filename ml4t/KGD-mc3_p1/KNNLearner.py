"""Implements a KNN learner for a dataset with ONLY 2 features and 1 label
    Expected feature row - [X1,X2,Y]
"""
__author__ = "kdaniel38"

import numpy as np
import math

class KNNLearner(object):
    """Implements a KNN learner using numpy"""

    def __init__(self, k, verbose = False):
        self.k = k
        self.verbose = verbose
        
    def addEvidence(self,dataX,dataY):
        """
        @summary: add data to the learner
        @param dataX: X values of data to add
        @param dataY: the Y training values
        """
        self.dataX = dataX
        self.dataY = dataY

    def query(self,points):
        """
        @summary: query a list of points.
        @param points: should be a numpy array with each row corresponding to a specific query.
        @returns the estimated values according to the saved model.
        """
        self.log("Computing nearest Y")
        return np.array([self.findY(x,y) for x,y in points])

    def findY(self,p0,p1):
        """Find the closest average label - Y based on k neighbors"""
        neighbors = np.argsort(np.sqrt(np.square(self.dataX[:,0]-p0)+np.square(self.dataX[:,1]-p1)))[:self.k]
        return np.average(self.dataY[[neighbors]])

    def log(self,text):
        if self.verbose:
            print text


