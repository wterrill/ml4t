"""Generates data that performs better with a KNN regression learner
    Performance is measured by RMSE and Correlation
"""
__author__ = "kdaniel38"

import numpy as np
import matplotlib.pyplot as plt
try:
    from tester import test
except:
    pass

def my_data(size):
    data = np.ones((size,3))
    m,c = .3,5  # m and c from line equation y=mx+c
    # range from -1 to 1 with SLIGHT variability using normal dist of mu=0, std=0.01 for X1 and X2
    data[:,0] = np.arange(-1,1,0.002)                       # range of -1 to 0 with VERY small increase
    data[:,1] = np.random.normal(0, scale=0.1,size=1000)    # 1000 samples from a normal distribution
    data[:,2] = np.random.normal(0, scale=0.01,size=1000)   # add NOISE to Y
    random_ix = np.random.choice(range(size),size,replace=True) # randomize points
    data = data[[random_ix]]
    data[:,2] += m*np.square(data[:,0]-data[:,1]) + c       #calculate y using a convex equation and add to the noise
    np.savetxt("./Data/best4KNN.csv", data, delimiter=",")
    return data

def scatterPlot(a,b):
    plt.scatter(a, b, s=5, c='b', alpha=0.5)
    plt.show()

if __name__ == "__main__":
    graph = False
    print "------Best 4 KNN ------"
    data = my_data(1000)
    try:
        test(data)
        if graph:
            scatterPlot(data[:,0],data[:,1])
    except:
        pass