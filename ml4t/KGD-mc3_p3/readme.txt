Adapted from: https://piazza.com/class/ij9yiif53l27fs?cid=1474
https://gist.githubusercontent.com/anonymous/cf836cfd3575c9888c9b656295415e23/raw/71c5831d7edd0f4c8c52b549d69dda558d71f1a2/maze.py
https://gist.github.com/anonymous/cf836cfd3575c9888c9b656295415e23

$ python testqlearners.py -h

Usage: maze.py [options]

Options:
  -h, --help            show this help message and exit
  -t, --trace           print verbose message and map from driver code
  -1, --rubric1         run test suite for rubric1
  -2, --rubric2         run test suite for rubric2
  -w WORLD, --world=WORLD   comma separated numbers
  -a ALPHA, --alpha=ALPHA
  -g GAMMA, --gamma=GAMMA
  -r RAR, --rar=RAR     
  -d RADR, --radr=RADR  
  -y DYNA, --dyna=DYNA  
  -i ITERATION, --iteration=ITERATION
  -v, --verbose         print verbose message from user code



$ python testqlearners.py --world 1,2,3,4,5,6,7,8,9,10
world01 best: 15 time: 4.13899993896
world02 best: 16 time: 4.28400015831
world03 best: 58 time: 13.3020000458
world04 best: 26 time: 7.40199995041
world05 best: 18 time: 4.45700001717
world06 best: 18 time: 4.16100001335
world07 best: 16 time: 4.25099992752
world08 best: 16 time: 3.25699996948
world09 best: 17 time: 5.77999997139
world10 best: 30 time: 7.242000103