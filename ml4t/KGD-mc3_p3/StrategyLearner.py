"""
Template for implementing StrategyLearner  (c) 2016 Tucker Balch
"""

import datetime as dt
import QLearner as ql
import pandas as pd
import numpy as np
import util as ut

class StrategyLearner(object):

    # constructor
    def __init__(self, verbose = True):
        self.verbose    = verbose
        self.steps      = 9
        self.actions    = [0,100,-100] #actions -> 0 do nothing 1 = buy 2 = sell
        #<position,action,reward>
        self.transitions = np.array([[0,100,-100], 
                                     [0,0  ,-200],
                                     [0,200,0   ]])
        #self.transitions = np.array([[0,100,-100], [0,0  ,-100],[0,100,0   ]])
        self.features = ['bb_value','volatility','daily_return']
        self.n = 3

    # this method should create a QLearner, and train it for trading
    def addEvidence(self, symbol = "IBM", \
        sd=dt.datetime(2007,12,31), \
        ed=dt.datetime(2009,12,31), \
        sv = 10000): 

        if self.verbose: print "Building model"
        self.learner = ql.QLearner(num_actions = 3, num_states=20000, verbose=self.verbose)
        # add your code to do learning here
        data = self.createDataSet([symbol,],(sd,ed),20)
        data.ix[:,1:] = data.ix[:,1:].apply(self.discretize,axis=0) 
        conv = [x for x in range(self.n)]
        comr = [x for x in range(self.n)]
        port_val = sv
        cumulative = 0
        steps   = 0

        while np.std(comr) > 0.5 and np.std(conv) > 0.5:
            state   = self.getstockpos(0,data,self.features,0)
            #action  = self.learner.querysetstate(state) #set the state and get first action
            action  = 1 #Benchmark: Buy 100 shares on the first trading day
            port_val = sv
            cash = sv
            cumulative = 0
            position = 1

            x = 0
            equity = self.actions[action] * data.ix[x][symbol]
            cash -= equity
            state   = self.getstockpos(0,data,self.features,1)
            port_val = cash + equity

            #data.ix[x,'trade']      = action
            #data.ix[x,'position']   = position
            #data.ix[x,'cum_return'] = cumulative
            #data.ix[x,'cash']       = cash
            #data.ix[x,'shares']     = self.actions[action]
            #data.ix[x,'port_val']   = port_val
            final = False
            for x in xrange(1,len(data)):
                if x == len(data) - 1:
                    action = self.actions.index((self.actions[position] * -1))
                    final = True
                new_port_val, cash, state, position, shares = self.movestock(x,data,action,state,cash,position,symbol,final)

                r = ((new_port_val)/port_val)-1
                r = r if r > 0 else r - 1
                cumulative += r
                port_val = new_port_val

                #data.ix[x,'trade']      = action
                #data.ix[x,'position']   = position
                #data.ix[x,'cum_return'] = cumulative
                #data.ix[x,'cash']       = cash
                #data.ix[x,'shares']     = shares
                #data.ix[x,'port_val']   = port_val

                action = self.learner.query(state,r)
            steps += 1    
            conv.append(port_val)
            conv = conv[-self.n:]
            comr.append(cumulative)
            comr = comr[-self.n:]
            if self.verbose: print steps, port_val, cumulative, np.std(conv), np.std(comr)
        if self.verbose: print "Converged [port_val:", port_val, "r: " , cumulative, "iterations: ", steps, "]"

    # this method should use the existing policy and test it against new data
    def testPolicy(self, symbol = "IBM", \
        sd=dt.datetime(2009,12,31), \
        ed=dt.datetime(2011,12,31), \
        sv = 10000):

        if self.verbose: print "Testing model"
        # here we build a fake set of trades
        # your code should return the same sort of data
        dates = pd.date_range(sd, ed)
        data = self.createDataSet([symbol,],(sd,ed),20)
        data.ix[:,1:] = data.ix[:,1:].apply(self.discretize,axis=0)
        data['trades'] = 0
        holding = position = 0
        for x in xrange(0,len(data)):
            if x == len(data) - 1:
                action = self.actions.index((self.actions[position] * -1))
                data.ix[x,'trades'] = self.actions[position] * -1
                position  = holding = 0
            else:
                state = self.getstockpos(x,data,self.features,holding)
                action  = self.learner.querysetstate(state)
                shares = self.transitions[position,action]
                position = action if action > 0 else position
                holding = abs(self.actions[position]/100)
                data.ix[x,'trades'] = shares
                position = position

        data['IBM'] = data['trades']
        prices_all = ut.get_data([symbol], dates)  # automatically adds SPY
        trades = prices_all[[symbol,]]  # only portfolio symbols
        trades_SPY = prices_all['SPY']  # only SPY, for comparison later
        trades.values[:,:] = 0 # set them all to nothing
        trades.values[3,:] = 100 # add a BUY at the 4th date
        trades.values[5,:] = -100 # add a SELL at the 6th date 
        trades.values[6,:] = -100 # add a SELL at the 7th date 
        trades.values[8,:] = -100 # add a SELL at the 9th date
        if self.verbose: print type(trades) # it better be a DataFrame!
        if self.verbose: print trades
        if self.verbose: print prices_all
        return pd.DataFrame(data.ix[:,0])

    def discretize(self, series):
        steps = self.steps
        stepsize = len(series)/steps
        sorted = series.sort_values(axis=0, ascending=True, inplace=False, kind='quicksort', na_position='last')
        threshold =[sorted[(i+1)*stepsize] for i in xrange(steps)]
        return np.digitize(series,threshold, right = False)
        #return ts
        #j=0
        #for i in range(0,len(series)):
        #    beer = series.ix[i]
        #    while series.ix[i] > threshold[j]:
        #        ice = threshold[j]
        #        j += 1
        #        if j > 9:
        #            j=9
        #            break
        #    series.ix[i] = j
        #    j=0
        #return ts

    def movestock(self, ix, data,action,state,cash,position, symbol, final):
        '''Move the stock position'''
        shares = self.transitions[position,action]
        equity = shares * data.ix[ix,symbol]
        cash -= shares * data.ix[ix-1,symbol]
        position = action if action > 0 else position
        holding = abs(self.actions[position]/100)
        new_port_val = cash + (self.actions[position]*data.ix[ix,symbol])
        new_state = self.getstockpos(ix,data,self.features,holding) if not final else state
        return new_port_val, cash, new_state, position, shares

    def getstockpos(self,ix,data,columns,holding):
        '''find the stock in the MDP'''
        a = np.append(holding,data.ix[ix,columns].values)
        b = ''.join(a.astype(int).astype(str).tolist())
        return int(b)

    def createDataSet(self, symbols, dates, rolling_window):
        stock = symbols[0]
        data = ut.get_data(symbols, pd.date_range(dates[0], dates[1]), addSPY=True)
        data.drop('SPY', axis=1, inplace=True)
        # extract features
        try:
            data['SMA'] = data[stock].rolling(min_periods=1, window=rolling_window,center=False).mean()
            data['stdev'] = data[stock].rolling(min_periods=1, window=rolling_window,center=False).std()
        except:
            data['SMA'] = pd.rolling_mean(data[stock],rolling_window, min_periods=1)
            data['stdev'] = pd.rolling_std(data[stock],rolling_window, min_periods=1)
        data['bb_value'] = (data[stock] - data['SMA'])/(2 * data['stdev'])
        #data['momentum'] = (data[stock]/data[stock].shift(5))-1
        data['daily_return'] = (data[stock]/data[stock].shift(1))-1
        data['close/SMA'] = data[stock]/data['SMA']
        try:
            data['volatility'] = data['daily_return'].rolling(min_periods=1, window=rolling_window,center=False).std() 
        except:
            data['volatility'] = pd.rolling_std(data['daily_return'],window=rolling_window,min_periods=1)
        # normalize
        #data['volatility'] = normalize(data.momentum)
        #data['momentum'] = normalize(data['momentum'])
        #data['bb_value'] = normalize(data['bb_value'])
        #data['Y'] = data[stock].shift(-5)/data[stock] - 1.0
        data.bfill(inplace=True)
        data = data.dropna()
        self.features = ['bb_value','volatility','daily_return','close/SMA']
        return data[[stock,'bb_value','volatility','daily_return','close/SMA']]

if __name__=="__main__":
    print "One does not simply think up a strategy"
