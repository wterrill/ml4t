"""
Template for implementing QLearner  (c) 2015 Tucker Balch
http://quantsoftware.gatech.edu/MC3-Project-3
http://mnemstudio.org/path-finding-q-learning-tutorial.htm
"""

import numpy as np
import random as rand

class QLearner(object):
    # set dyna back to zero for straight qlearning
    def __init__(self, \
        num_states = 100, \
        num_actions = 4, \
        alpha = 0.2, \
        gamma = 0.9, \
        rar = 0.98, \
        radr = 0.999, \
        dyna = 0, \
        verbose = False):

        self.verbose = verbose
        self.num_actions = num_actions
        self.num_states = num_states
        self.s = 0
        self.a = 0
        self.rar = rar
        self.radr = radr
        self.gamma = gamma 
        self.alpha = alpha
        self.dyna = dyna

        # initialize structures
        self.Ta = []
        self.Q = np.random.normal(loc=0,scale=1.0,size=(num_states,num_actions))
        self.R = np.zeros(shape=(num_states,num_actions))

    def querysetstate(self, s):
        """
        @summary: Update the state without updating the Q-table
        @param s: The new state
        @returns: The selected action
        """
        self.s = s
        action = np.argmax(self.Q[s,:], axis=0)
        if self.verbose: print "s =", s,"a =",action
        return action

    def query(self,s_prime,r):
        """
        @summary: Update the Q table and return an action
        @param s_prime: The new state
        @param r: The ne state
        @returns: The selected action

        The formula for computing Q for any state-action pair <s, a>, given an experience tuple <s, a, s', r>, is:
        Q'[s, a] = (1 - alpha) * Q[s, a] + alpha * (r + gamma * Q[s', argmaxa'(Q[s', a'])])
        Q[state, action] = (1-alpha)Q[state,action]+ alpha * (R(state, action) + Gamma * Max[Q(next state, all actions)])
        r = R[s, a] is the immediate reward for taking action a in state s
        s' is the resulting next state,
        argmaxa'(Q[s', a']) is the action that maximizes the Q-value among all possible actions a' from s', and,
        alpha is the learning rate
        gamma is the discount rate
        We're also randomly choosing the next action with a decaying probability of rar
        """
        s = self.s
        a = self.a
        sp = s_prime
        alpha = self.alpha
        gamma = self.gamma
        alpha_c = 1-self.alpha
        
        num_states = self.num_states
        num_actions = self.num_actions

        # discount non-progressive movements
        #r = -abs(r)*2 if s == sp else r

        self.Q[s,a] = alpha_c*self.Q[s,a] + alpha * (r + gamma * self.Q[sp,np.argmax(self.Q[sp])])
        #self.Q[s,a] = self.Q[s,a] + alpha * (r + gamma * self.Q[sp,:].max() - self.Q[s,a])
        #self.Q[s,a] = (1-alpha)*self.Q[s,a] + alpha * (r + gamma * self.Q[sp].max())

        # choose a random action with probability rar
        x = np.random.multinomial(9,[self.rar,(1-self.rar)],size=1).argmax()        
        action = rand.randint(0, self.num_actions-1) if x == 0 else np.argmax(self.Q[sp], axis=0)


        self.s = s_prime
        self.a = action

        # decay rar with radr
        self.rar = self.rar * self.radr

        if self.dyna < 1:      
            return action

        # ------------ Dyna ---------------
        # Tc[] = 0.00001
        # increment Tc[s,a,s'] everytime we observe <s,a,s'>
        # T[s,a,s'] = Tc[s,a,s'] / Tc[s, a, :].sum()
        # R[s,a] = (1-alpha)R[s,a] + alpha*r .... alpha = 0.2
        #
        # s,a = random or random from seen examples
        # s' = infer from T[]
        # r = R[s,a]
        # ---------------------------------  

        # update the transition model
        self.R[s,a] = alpha_c*self.R[s,a] + alpha * r
       
        self.Ta.append((s,a,sp,self.R[s,a]))
        self.Ta = self.Ta[-self.dyna:]
        if self.verbose:
            print
            for ta in self.Ta:
                print ta      

        h = len(self.Ta)
        # loop for Dyna
        for i in xrange(self.dyna):
            # hallucinate an experience 
            g = self.Ta[np.random.randint(0,h)]
            s,a,sp,r = g[0],g[1],g[2],g[3]
            
            # udpate Q table
            #self.Q[s,a] = (1-alpha)*self.Q[s,a] + alpha * (r + gamma * self.Q[sp,:].max())
            self.Q[s,a] = alpha_c*self.Q[s,a] + alpha * (r + gamma * self.Q[sp,np.argmax(self.Q[sp])])
            #self.Q[s,a] = self.Q[s,a] + alpha * (r + gamma * self.Q[sp,:].max() - self.Q[s,a])
            
        if self.verbose: print "s =", s_prime,"a =",action,"r =",r
        return action

if __name__=="__main__":
    print "Remember Q from Star Trek? Well, this isn't him"
