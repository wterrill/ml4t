"""
Test a Strategy Learner.  (c) 2016 Tucker Balch
"""

import pandas as pd
import datetime as dt
import util as ut
import StrategyLearner as sl
import marketsim
import os

def test_code(verb = True):

    # instantiate the strategy learner
    learner = sl.StrategyLearner(verbose = verb)

    # set parameters for training the learner
    sym = "GOOG"
    stdate =dt.datetime(2007,12,31)
    enddate =dt.datetime(2009,12,31) # just a few days for "shake out"
    start = dt.datetime.now()
    # train the learner
    learner.addEvidence(symbol = sym, sd = stdate, \
        ed = enddate, sv = 10000) 
    print "training time: ", dt.datetime.now() - start
    # set parameters for testing
    sym = "IBM"
    stdate =dt.datetime(2009,12,31)
    enddate =dt.datetime(2011,12,31) 

    # get some data for reference
    syms=[sym]
    dates = pd.date_range(stdate, enddate)
    prices_all = ut.get_data(syms, dates)  # automatically adds SPY
    prices = prices_all[syms]  # only portfolio symbols
    if verb: print prices

    # test the learner
    df_trades = learner.testPolicy(symbol = sym, sd = stdate, \
        ed = enddate, sv = 10000)

    # a few sanity checks
    # df_trades should be a single column DataFrame (not a series)
    # including only the values 100, 0, -100
    if isinstance(df_trades, pd.DataFrame) == False:
        print "Returned result is not a DataFrame"
    if prices.shape != df_trades.shape:
        print "Returned result is not the right shape"
    tradecheck = abs(df_trades.cumsum()).values
    tradecheck[tradecheck<=100] = 0
    tradecheck[tradecheck>0] = 1
    if tradecheck.sum(axis=0) > 0:
        print "Returned result violoates holding restrictions (more than 100 shares)"

    if verb: print df_trades
    # we will add code here to evaluate your trades
    df_trades['Symbol'] = ''
    df_trades['Order'] = ''
    df_trades['Shares'] = 0
    for x in range(len(df_trades)):
        if df_trades.ix[x,0] == 0:
            continue
        df_trades.ix[x,'Symbol'] = sym
        df_trades.ix[x,'Order']  = 'BUY' if df_trades.ix[x,0] > 0 and df_trades.ix[x,'Order'] == '' else df_trades.ix[x,'Order']
        df_trades.ix[x,'Order']  = 'SELL' if df_trades.ix[x,0] < 0 and df_trades.ix[x,'Order'] == '' else df_trades.ix[x,'Order']
        df_trades.ix[x,'Shares'] = df_trades._ix[x,0]
    
        if not os.path.exists("orders"):
            print "...creating orders directory"
            os.makedirs("orders")
    df_trades.index.names = ['Date']
    df_trades[df_trades.IBM <> 0][[1,2,3]].to_csv("./orders/orders.csv")
    print df_trades[df_trades.IBM <> 0]
    marketsim.test_code('IBM', sample_type="in", plot = True)
    

if __name__=="__main__":
    test_code(verb = False)
