
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
import KNNLearner as knn
import LinRegLearner as lrl
import datetime
import marketsim
import math

def print_full(x):
    pd.set_option('display.max_rows', len(x))
    print(x)
    pd.reset_option('display.max_rows')

def symbol_to_path(symbol, base_dir=os.path.join("..", "data")):
    """Return CSV file path given ticker symbol."""
    return os.path.join(base_dir, "{}.csv".format(str(symbol)))

def plot_strategy(name,df, title="Stock prices", xlabel="Date", ylabel="Price"):
    """Plot stock prices"""
    title = name + " Stock prices"
    #ax = df[[name,'Y','Ypred2', 'zero', 'buy limit', 'sell limit']].plot(title=title, fontsize=12)
    ax = df[[name,'Y','Ypred']].plot(title=title, fontsize=12)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    #ax = df.plot(figsize=(14,6))
    ymin, ymax = ax.get_ylim()
    buy = df.ix[df.buy == 1].index
    sell = df.ix[df.sell == 1].index
    exit = df.ix[df.exit == 1].index
    #exitsell = df.ix[df.exit_sell == 1].index
    ax.vlines(sell,ymin=ymin, ymax=ymax,color='r')
    ax.vlines(buy,ymin=ymin, ymax=ymax,color='g')
    ax.vlines(exit,ymin=ymin, ymax=ymax,color='k')
    #ax.vlines(exitbuy,ymin=ymin, ymax=ymax,color='k')

    #colors = ['blue','red','green','black', 'black', 'black']
    #df.plot(color=colors)

    #plt.vlines('2008-12-11',ymin=ymin, ymax=ymax,color='b')
    plt.show()

def get_data(symbols, dates, addSPY=True):
    """Read stock data (adjusted close) for given symbols from CSV files."""
    df = pd.DataFrame(index=dates)
    if addSPY and 'SPY' not in symbols:  # add SPY for reference, if absent
        symbols = ['SPY'] + symbols

    for symbol in symbols:
        df_temp = pd.read_csv(symbol_to_path(symbol), index_col='Date',
                parse_dates=True, usecols=['Date', 'Adj Close'], na_values=['nan'])
        df_temp = df_temp.rename(columns={'Adj Close': symbol})
        df = df.join(df_temp)
        if symbol == 'SPY':  # drop dates SPY did not trade
            df = df.dropna(subset=["SPY"])

    return df

def print_full(x):
    pd.set_option('display.max_rows', len(x))
    print(x)
    pd.reset_option('display.max_rows')

def test_code(level,k, stocks,train_start, train_end, test_start, test_end, Useknn=True, graph=True, rolling_window=20 ):

#####################################################################################################################
    name = stocks[0]
    extra = 0
    level = level
    k = k
    n_days = 5
    wait_to_trade = 10
    rolling_window = 20

#####################################################################################################################



    stocks_train = get_data(stocks, pd.date_range(train_start, train_end), addSPY=True)
    del stocks_train['SPY']
    stocks_test = get_data(stocks, pd.date_range(test_start, test_end), addSPY=True)
    del stocks_test['SPY']

    stocks_train['SMA'] = pd.rolling_mean(stocks_train[name],rolling_window, min_periods=1)
    stocks_train['stdev'] = pd.rolling_std(stocks_train[name],rolling_window, min_periods=1)
    stocks_train['bb_value'] = (stocks_train[name] - stocks_train['SMA'])/(2 * stocks_train['stdev'])
    stocks_train['momentum'] = (stocks_train[name]/stocks_train[name].shift(n_days))-1
    #stocks_train['daily_return'] = (stocks_train[name]-stocks_train[name].shift(1))
    stocks_train['daily_return'] = (stocks_train[name]/stocks_train[name].shift(1))-1
    stocks_train['volat'] = pd.rolling_std(stocks_train['daily_return'],rolling_window,min_periods=1)
    stocks_train['Y'] = stocks_train[name].shift(-5)/stocks_train[name] - 1.0
    del stocks_train['SMA']
    del stocks_train['stdev']
    del stocks_train['daily_return']
    stocks_train = stocks_train.dropna()

    stocks_test['SMA'] = pd.rolling_mean(stocks_test[name],rolling_window, min_periods=1)
    stocks_test['stdev'] = pd.rolling_std(stocks_test[name],rolling_window, min_periods=1)
    stocks_test['bb_value'] = (stocks_test[name] - stocks_test['SMA'])/(2 * stocks_test['stdev'])
    stocks_test['momentum'] = (stocks_test[name]/stocks_test[name].shift(n_days))-1
    #stocks_test['daily_return'] = (stocks_test[name]-stocks_test[name].shift(1))
    stocks_test['daily_return'] = (stocks_test[name]/stocks_test[name].shift(1))-1

    stocks_test['volat'] = pd.rolling_std(stocks_test['daily_return'],rolling_window,min_periods=1)
    stocks_test['Y'] = stocks_test[name].shift(-5)/stocks_test[name] - 1.0
    del stocks_test['SMA']
    del stocks_test['stdev']
    del stocks_test['daily_return']
    stocks_test = stocks_test.dropna()




    bb_value_train = stocks_train['bb_value'].values
    momentum_train = stocks_train['momentum'].values
    vol_train = stocks_train['volat'].values
    Y_train = stocks_train['Y'].values

    del stocks_train['bb_value']
    del stocks_train['momentum']
    del stocks_train['volat']

    length_train = len(bb_value_train)
    bb_value_train = bb_value_train.reshape(length_train,1)
    momentum_train = momentum_train.reshape(length_train,1)
    vol_train = vol_train.reshape(length_train,1)
    Y_train = Y_train.reshape(length_train,1)

    bb_value_train = (bb_value_train - bb_value_train.mean())/(bb_value_train.max()-bb_value_train.min())
    momentum_train = (momentum_train - momentum_train.mean())/(momentum_train.max()-momentum_train.min())
    vol_train = (vol_train - vol_train.mean())/(vol_train.max()-vol_train.min())


    temp = np.concatenate([bb_value_train,momentum_train],axis=1)
    temp = np.concatenate([temp,vol_train],axis=1)
    final_train = np.concatenate([temp,Y_train],axis=1)

    bb_value_test = stocks_test['bb_value'].values
    momentum_test = stocks_test['momentum'].values
    vol_test = stocks_test['volat'].values
    Y_test = stocks_test['Y'].values

    del stocks_test['bb_value']
    del stocks_test['momentum']
    del stocks_test['volat']

    length_test = len(bb_value_test)
    bb_value_test = bb_value_test.reshape(length_test,1)
    momentum_test = momentum_test.reshape(length_test,1)
    vol_test = vol_test.reshape(length_test,1)
    Y_test = Y_test.reshape(length_test,1)

    bb_value_test = (bb_value_test - bb_value_test.mean())/(bb_value_test.max()-bb_value_test.min())
    momentum_test = (momentum_test - momentum_test.mean())/(momentum_test.max()-momentum_test.min())
    vol_test = (vol_test - vol_test.mean())/(vol_test.max()-vol_test.min())

    temp = np.concatenate([bb_value_test,momentum_test],axis=1)
    temp = np.concatenate([temp,vol_test],axis=1)
    final_test = np.concatenate([temp,Y_test],axis=1)


    # compute how much of the data is training and testing
    #train_rows = math.floor(0.6* final.shape[0])
    #test_rows = final.shape[0] - train_rows

    # separate out training and testing data

    trainX = final_train[:,0:-1]
    trainY = final_train[:,-1]
    testX = final_test[:,0:-1]
    testY = final_test[:,-1]

    if Useknn:
        #print "KNN Learner ********************************************"
        learner = knn.KNNLearner(k = k, verbose = False) #  constructor
    else:
        #print "LinReg Learner *****************************************"
        learner = lrl.LinRegLearner(verbose = True)
    learner.addEvidence(trainX, trainY) # training step
    Ypred = learner.query(testX) # query
    results = "In sample results"
    if test_start != train_start:
        trainY = trainY[0:Ypred.shape[0]]
        results = "Out of sample results"


    rmse = math.sqrt(((trainY - Ypred) ** 2).sum()/trainY.shape[0])
    print
    print results
    print "RMSE: ", rmse
    c = np.corrcoef(Ypred, y=trainY)
    print "corr: ", c[0,1]

    stocks2 = stocks_test.copy()
    stocks2['Y'] = stocks2[name] + stocks2[name] * stocks2['Y']
    stocks2['Ypred'] = stocks2[name] + stocks2[name] * Ypred


    stocks2['Ypred2'] = 100*Ypred + extra
    stocks2['buy limit'] = level + extra
    stocks2['sell limit'] = -1 * level + extra
    stocks2['zero'] = extra

    orders=[]
    x = wait_to_trade
    done = False
    stocks2['buy'] = 0
    stocks2['sell'] = 0
    stocks2['exit_buy'] = 0
    stocks2['exit_sell'] = 0

    while x < len(stocks2):
        yup = stocks2.ix[x,'Ypred2']
        current_date = pd.Timestamp(stocks2.index.get_values()[x]).date()
        #print current_date
        #if current_date == datetime.date(2009, 12, 17):
            #print "testing data"

        if stocks2.ix[x,'Ypred2']> (level + extra):
            stocks2.ix[x,'buy'] = 1
            orders.append(pd.Timestamp(stocks2.index.get_values()[x]).date())
            orders.append("BUY")
            x += 5
            if x > len(stocks2)-1:
                x = len(stocks2)-1
                done = True
            current_date = pd.Timestamp(stocks2.index.get_values()[x]).date()
            #print current_date
            stocks2.ix[x,'exit_buy'] = 1
            orders.append(pd.Timestamp(stocks2.index.get_values()[x]).date())
            orders.append("SELL")

        elif stocks2.ix[x,'Ypred2']<(-1 * level + extra):
            stocks2.ix[x,'sell'] = 1
            orders.append(pd.Timestamp(stocks2.index.get_values()[x]).date())
            orders.append("SELL")
            x += 5
            if x > len(stocks2)-1:
                x = len(stocks2)-1
                done = True
            current_date = pd.Timestamp(stocks2.index.get_values()[x]).date()
            #print current_date
            stocks2.ix[x,'exit_sell'] = 1
            orders.append(pd.Timestamp(stocks2.index.get_values()[x]).date())
            orders.append("BUY")

        x += 1
        if done == True:
            x+=6

    #stocks2.drop(stocks2.columns[[2,4,5]], axis=1, inplace=True)
    stocks2['exit'] = stocks2['exit_sell'] + stocks2['exit_buy']
    del stocks2['exit_sell']
    del stocks2['exit_buy']
    if graph:
        plot_strategy(name,stocks2, title="Stock prices", xlabel="Date", ylabel="Price")

    finalOrder=np.array(orders)
    finalOrder = finalOrder.reshape(len(finalOrder)/2,2)
    finalOrder = pd.DataFrame(finalOrder)
    finalOrder.insert(1,'Symbol',name)
    finalOrder.insert(3,'Shares',100)
    finalOrder.columns=['Date','Symbol','Order', 'Shares']
    finalOrder.set_index('Date',drop=True, inplace=True)
    #create orders folder if it does not exist
    if not os.path.exists("orders"):
        print "...creating orders directory"
        os.makedirs("orders")
    finalOrder.to_csv('./orders/orders.csv')

    return


if __name__ == "__main__":

    k=3
    # return rate examined = 1.1, k=1, stock = ml4t-399, train start, train end, test start, test end, knn? = True, graph = True)
    test_code(1, k, ['ML4T-220'],'2007-12-31','2009-12-31', '2007-12-31', '2009-12-31')
    marketsim.test_code("Ml4T-220" ,4)

    test_code(1, k, ['ML4T-220'],'2007-12-31','2009-12-31', '2009-12-31', '2011-12-31')
    marketsim.test_code("Ml4T-220", 4)
    #print "Insample"
    #for k in range(1,50):

    test_code(1, k, ['IBM'],'2007-12-31','2009-12-31', '2007-12-31', '2009-12-31',True, True)
    returns = marketsim.test_code('IBM',10,True)
    #print k, returns

    #print "Outsample"
    #for k in range(1,50):
    test_code(1,k,['IBM'],'2007-12-31','2009-12-31', '2009-12-31', '2011-12-31',True, True)
    returns = marketsim.test_code('IBM', 10,True)  # the 6 is the maximum amount of leverage that you can have.
    #print k, returns

        #test_code(1,1,['IBM'],'2007-12-31','2009-12-31', '2009-12-31', '2011-12-31',False)
        #marketsim.test_code('IBM',10)

