

import numpy as np
import math
import LinRegLearner as lrl
import pandas as pd
import KNNLearner as knn
import BagLearner as bl
import datetime
import matplotlib.pyplot as plt

def print_full(x):
    pd.set_option('display.max_rows', len(x))
    print(x)
    pd.reset_option('display.max_rows')

def scatterGraph(a,b):
    plt.scatter(a, b, s=0.5, c='b', alpha=0.5)
    plt.show()

if __name__=="__main__":
    graph = False
    inf = open('Data/ripple.csv')
    data = np.array([map(float,s.strip().split(',')) for s in inf.readlines()])
    start = datetime.datetime.now()
    # compute how much of the data is training and testing
    train_rows = math.floor(0.6* data.shape[0])
    test_rows = data.shape[0] - train_rows

    # separate out training and testing data
    trainX = data[:train_rows,0:-1]
    trainY = data[:train_rows,-1]
    testX = data[train_rows:,0:-1]
    testY = data[train_rows:,-1]

    print testX.shape
    print testY.shape

    print "linear regression ********************************************"
    # create a learner and train it
    learner = lrl.LinRegLearner(verbose = True) # create a LinRegLearner
    learner.addEvidence(trainX, trainY) # train it


    # evaluate in sample
    predY = learner.query(trainX) # get the predictions
    rmse = math.sqrt(((trainY - predY) ** 2).sum()/trainY.shape[0])
    print
    print "In sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=trainY)
    print "corr: ", c[0,1]
    if graph:
        scatterGraph(predY, trainY)


    # evaluate out of sample
    predY = learner.query(testX) # get the predictions
    rmse = math.sqrt(((testY - predY) ** 2).sum()/testY.shape[0])
    print
    print "Out of sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=testY)
    print "corr: ", c[0,1]
    if graph:
        scatterGraph(predY, testY)

    print ""
    print "linear regression with bagging ********************************************"
    learner = bl.BagLearner(learner = lrl.LinRegLearner, kwargs = {}, bags = 20, boost = False, verbose = False)
    learner.addEvidence(trainX, trainY)
    Y = learner.query(testX)

    # evaluate in sample
    predY = learner.query(trainX) # get the predictions
    rmse = math.sqrt(((trainY - predY) ** 2).sum()/trainY.shape[0])
    print
    print "In sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=trainY)
    print "corr: ", c[0,1]
    if graph:
        scatterGraph(predY, trainY)


    # evaluate out of sample
    predY = learner.query(testX) # get the predictions
    rmse = math.sqrt(((testY - predY) ** 2).sum()/testY.shape[0])
    print
    print "Out of sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=testY)
    print "corr: ", c[0,1]
    if graph:
        scatterGraph(predY, testY)


    print ""
    print "KNN Learner ********************************************"
    learner = knn.KNNLearner(k = 3, verbose = False) # constructor
    learner.addEvidence(trainX, trainY) # training step
    Y = learner.query(testX) # query

    # evaluate in sample
    predY = learner.query(trainX) # get the predictions
    rmse = math.sqrt(((trainY - predY) ** 2).sum()/trainY.shape[0])
    print
    print "In sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=trainY)
    print "corr: ", c[0,1]
    if graph:
        scatterGraph(predY, trainY)


    # evaluate out of sample
    predY = learner.query(testX) # get the predictions
    rmse = math.sqrt(((testY - predY) ** 2).sum()/testY.shape[0])
    print
    print "Out of sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=testY)
    print "corr: ", c[0,1]
    if graph:
        scatterGraph(predY, testY)

    print ""
    print "KNN Learner with bagging ********************************************"
    learner = bl.BagLearner(learner = knn.KNNLearner, kwargs = {"k":3}, bags = 20, boost = False, verbose = False)
    learner.addEvidence(trainX, trainY)
    Y = learner.query(testX)

    # evaluate in sample
    predY = learner.query(trainX) # get the predictions
    rmse = math.sqrt(((trainY - predY) ** 2).sum()/trainY.shape[0])
    print
    print "In sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=trainY)
    print "corr: ", c[0,1]
    if graph:
        scatterGraph(predY, trainY)


    # evaluate out of sample
    predY = learner.query(testX) # get the predictions
    rmse = math.sqrt(((testY - predY) ** 2).sum()/testY.shape[0])
    print
    print "Out of sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=testY)
    print "corr: ", c[0,1]
    if graph:
        scatterGraph(predY, testY)

    '''
    learners = []
    for i in range(0,10):
        kwargs = {"k":i}
        learners.append(lrl.LinRegLearner(**kwargs))
    '''

    bags = 5
    learners = []
    #kwargs = {"k":i}
    for i in range(1,bags):
        kwargs = {"k":i}
        learners.append(knn.KNNLearner(verbose = False, **kwargs))
    print "KNN Learner with boosting??? ********************************************"
    learner = bl.BagLearner(learner = learners, kwargs = {}, bags = 5, boost = True, verbose = False)
    learner.addEvidence(trainX, trainY)
    Y = learner.query(testX)

    # evaluate in sample
    predY = learner.query(trainX) # get the predictions
    rmse = math.sqrt(((trainY - predY) ** 2).sum()/trainY.shape[0])
    print
    print "In sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=trainY)
    print "corr: ", c[0,1]
    if graph:
        scatterGraph(predY, trainY)


    # evaluate out of sample
    predY = learner.query(testX) # get the predictions
    rmse = math.sqrt(((testY - predY) ** 2).sum()/testY.shape[0])
    print
    print "Out of sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=testY)
    print "corr: ", c[0,1]
    if graph:
        scatterGraph(predY, testY)



    print datetime.datetime.now()-start

