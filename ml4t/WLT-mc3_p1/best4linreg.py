'''
Create your own dataset generating code (call it best4linreg.py) that creates data that performs significantly
better with LinRegLearner than KNNLearner. Explain your data generating algorithm, and explain why LinRegLearner
performs better. Your data should include at least 2 dimensions in X, and at least 1000 points.
(Don't use bagging for this section).
'''

import numpy as np

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import random
try:
    from tester import test
except:
    pass



def threeD_plot(X1,X2,Y):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(X1, X2, Y, c='r', marker='o')
    ax.set_xlabel('X1')
    ax.set_ylabel('X2')
    ax.set_zlabel('Y')
    ax.view_init(elev=50., azim=75)
    plt.show()




def my_data():
    array = np.zeros((1000,3))

    x = np.arange(-1,1,0.001)
    X1 = x.reshape(2000,1)
    Y = np.zeros((2000,1))
    for i in range(0,2000):
        Y[i] = 3*X1[i] + 2 + 0.1*random.random()
    X2 = np.ones((2000,1))

    threeD_plot(X1,X2,Y)
    temp = np.concatenate([X1,X2],axis=1)
    final = np.concatenate([temp,Y],axis=1)
    #points = pd.DataFrame(final,columns=['X1', 'X2', 'Y'])
    np.random.shuffle(final)
    np.savetxt("./Data/linreg.csv", final, delimiter=",")
    return final

if __name__ == "__main__":
    data = my_data()
    try:
        test(data)
    except:
        pass
