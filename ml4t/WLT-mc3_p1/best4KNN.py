'''
Create your own dataset generating code (call it best4KNN.py) that creates data that performs significantly better
with KNNLearner than LinRegLearner. Explain your data generating algorithm, and explain why KNNLearner performs
better.
Your data should include at least 2 dimensions in X, and at least 1000 points. (Don't use bagging for this section).
'''

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def threeD_plot(X1,X2,Y):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(X1, X2, Y, c='r', marker='o')
    ax.set_xlabel('X1')
    ax.set_ylabel('X2')
    ax.set_zlabel('Y')
    ax.view_init(elev=50., azim=45)
    plt.show()

def my_data():
    i = -1
    array=np.zeros((2500,4))
    for xi in range (-25,25):
        x1 = float(xi)/100
        for xii in range(-25,25):
            x2=float(xii)/100
            y = x1**2 + x2**2
            i = i+1
            array[i,:] = [i,x1,x2,y]

    X1=array[:,1]
    X2=array[:,2]
    Y=array[:,3]
    threeD_plot(X1,X2,Y)
    X1 = X1.reshape(2500,1)
    X2 = X2.reshape(2500,1)
    Y = Y.reshape(2500,1)
    temp = np.concatenate([X1,X2],axis=1)
    final = np.concatenate([temp,Y],axis=1)
    np.random.shuffle(final)
    np.savetxt("./Data/knn.csv", final, delimiter=",")

if __name__ == "__main__":
    my_data()
