

import numpy as np
import math
import LinRegLearner as lrl
import pandas as pd
import KNNLearner as knn
import BagLearner as bl
import datetime
import matplotlib.pyplot as plt

def print_full(x):
    pd.set_option('display.max_rows', len(x))
    print(x)
    pd.reset_option('display.max_rows')

def scatterGraph(a,b):
    plt.scatter(a, b, s=0.5, c='b', alpha=0.5)
    plt.show()

def evaluate(learner,trainX,trainY,testX,testY):   # evaluate in sample
    predY = learner.query(trainX) # get the predictions
    rmse = math.sqrt(((trainY - predY) ** 2).sum()/trainY.shape[0])
    print
    print "In sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=trainY)
    print "corr: ", c[0,1]
    if graph:
        scatterGraph(predY, trainY)
    rmseIS = rmse
    corrIS = c[0,1]

    # evaluate out of sample
    predY = learner.query(testX) # get the predictions
    rmse = math.sqrt(((testY - predY) ** 2).sum()/testY.shape[0])
    print
    print "Out of sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=testY)
    print "corr: ", c[0,1]
    if graph:
        scatterGraph(predY, testY)
    rmseOS = rmse
    corrOS = c[0,1]

    return rmseIS, corrIS, rmseOS, corrOS

if __name__=="__main__":
    rmseIS=[]
    corrIS=[]
    rmseOS=[]
    corrOS=[]
    graph = False
    inf = open('Data/ripple.csv')
    data = np.array([map(float,s.strip().split(',')) for s in inf.readlines()])
    start = datetime.datetime.now()
    # compute how much of the data is training and testing
    train_rows = math.floor(0.6* data.shape[0])
    test_rows = data.shape[0] - train_rows

    # separate out training and testing data
    trainX = data[:train_rows,0:-1]
    trainY = data[:train_rows,-1]
    testX = data[train_rows:,0:-1]
    testY = data[train_rows:,-1]

    print testX.shape
    print testY.shape


    bags = 20
    learners = []
    #kwargs = {"k":i}
    for i in range(1,bags):
        kwargs = {'k':3}
        learner = bl.BagLearner(learner = knn.KNNLearner, boost = False, verbose = False, bags = bags, kwargs = kwargs)
        learner.addEvidence(trainX, trainY) # training step
        print "bags = ", i
        rIS,cIS,rOS,cOS = evaluate(learner,trainX,trainY,testX,testY)
        rmseIS.append(rIS)
        corrIS.append(cIS)
        rmseOS.append(rOS)
        corrOS.append(cOS)

    print rmseIS
    print corrIS
    print rmseOS
    print corrOS

    rmseIS = np.array(rmseIS).reshape(len(rmseIS),1)
    corrIS = np.array(corrIS).reshape(len(rmseIS),1)
    rmseOS = np.array(rmseOS).reshape(len(rmseIS),1)
    corrOS = np.array(corrOS).reshape(len(rmseIS),1)

    IS = np.concatenate((rmseIS,corrIS),axis=1)
    OS = np.concatenate((rmseOS,corrOS),axis=1)
    final = np.concatenate((IS,OS),axis=1)

    print final

    print datetime.datetime.now()-start

