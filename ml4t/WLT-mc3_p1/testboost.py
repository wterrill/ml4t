import LinRegLearner as lrl
import KNNLearner as knn
import numpy as np
import math
import BagLearner as bl

def test(data):  
    # compute how much of the data is training and testing
    train_rows = math.floor(0.6* data.shape[0])
    test_rows = data.shape[0] - train_rows

    # separate out training and testing data
    trainX = data[:train_rows,0:-1]
    trainY = data[:train_rows,-1]
    testX = data[train_rows:,0:-1]
    testY = data[train_rows:,-1]

    print testX.shape
    print testY.shape
    print


    bags = 5
    learners = []
    #kwargs = {"k":i}
    for i in range(1,bags):
        kwargs = {"k":i}
        learners.append(knn.KNNLearner(verbose = False, **kwargs))
    print "KNN Learner with boosting??? ********************************************"
    learner = bl.BagLearner(learner = learners, kwargs = {}, bags = 5, boost = True, verbose = False)
    learner.addEvidence(trainX, trainY)
    Y = learner.query(testX)

    # evaluate in sample
    predY = learner.query(trainX) # get the predictions
    rmse = math.sqrt(((trainY - predY) ** 2).sum()/trainY.shape[0])
    print
    print "In sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=trainY)
    print "corr: ", c[0,1]
    if graph:
        scatterGraph(predY, trainY)


    # evaluate out of sample
    predY = learner.query(testX) # get the predictions
    rmse = math.sqrt(((testY - predY) ** 2).sum()/testY.shape[0])
    print
    print "Out of sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=testY)
    print "corr: ", c[0,1]
    if graph:
        scatterGraph(predY, testY)