'''
Your KNNLearner class should be implemented in the file KNNLearner.py. It should implement EXACTLY the API defined below.
DO NOT import any modules besides allowed below. You should implement the following functions/methods:

import KNNLearner as knn
learner = knn.KNNLearner(k = 3, verbose = False) # constructor
learner.addEvidence(Xtrain, Ytrain) # training step
Y = learner.query(Xtest) # query

Where "k" is the number of nearest neighbors to find. Xtrain and Xtest should be ndarrays (numpy objects) where each row
represents an X1, X2, X3... XN set of feature values. The columns are the features and the rows are the individual
example instances. Y and Ytrain are single dimension ndarrays that indicate the value we are attempting to predict
with X.

If "verbose" is True, your code can print out information for debugging. Otherwise your code should not generate
ANY output.

Use Euclidean distance. Take the mean of the closest k points' Y values to make your prediction. If there are
multiple equidistant points on the boundary of being selected or not selected, you may use whatever method you
like to choose among them.

This code should not generate statistics or charts. Modify testlearner.py to generate statistics and charts.

'''

import numpy as np
import scipy as sci
import pandas as pd
from scipy.spatial import distance


class KNNLearner(object):

    def __init__(self, k, verbose = False):
        self.k = k # for this one, all we need is the k, so, save k
        self.verbose = verbose


    def addEvidence(self,dataX,dataY):
        """
        @summary: Add training data to learner
        @param dataX: X values of data to add
        @param dataY: the Y training values
        """

        #keeping it in numpy array with first column as index
        index = np.arange(dataX.shape[0]).reshape(dataX.shape[0],1)
        temp= np.concatenate((index, dataX[:,0:2]), axis=1)
        self.model = np.concatenate((temp,dataY.reshape(dataY.shape[0],1)), axis=1)

    def query(self,points):
        """
        @summary: Estimate a set of test points given the model we built.
        @param points: should be a numpy array with each row corresponding to a specific query.
        @returns the estimated values according to the saved model.
        """

        #Create the what we are going to return that has the same length as what we were passed in.
        Y =np.zeros(len(points))
        # loop through each one of the points. (I think this loop is unavoidable)
        for x in range(0,len(points)):
            #Get the euclidean distance between each points[x] (reshaped to make it the same shape as the model)
            # and the columns TrainX1 and TrainX2 in the model (columns 0 and 1)
            i = self.model[:,1:self.model.shape[1]]
            j = points[x].reshape(1,points.shape[1])
            dist = distance.cdist(i,j,'euclidean')
            #make a transformation of the matrix in prep for the argsort
            dist = dist.reshape(1,len(dist))
            # dist[0] to use the write size... [:self.k] to return the indices of the lowest three values
            indices = dist[0].argsort()[:self.k]
            #prep to make an average of the three points
            sum = 0
            #I couldn't figure out how to avoid this loop either and allow for a generic "k"
            for y in range(0,self.k):
                #find the values in the model with the same indices as the lowest three distances and get the mean value
                sum += self.model[indices[y]][3]
            #calulate the mean
            Y[x] = sum / self.k
        return Y

if __name__=="__main__":
    print ""