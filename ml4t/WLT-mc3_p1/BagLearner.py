'''
Implement Bootstrap Aggregating as a Python class named BagLearner. Your BagLearner class should be implemented in
the file BagLearner.py. It should implement EXACTLY the API defined below.
You should implement the following functions/methods:

import BagLearner as bl
learner = bl.BagLearner(learner = knn.KNNLearner, kwargs = {"k":3}, bags = 20, boost = False, verbose = False)
learner.addEvidence(Xtrain, Ytrain)
Y = learner.query(Xtest)

Where learner is the learning class to use with bagging. kwargs are keyword arguments to be passed on to the learner's
constructor and they vary according to the learner (see hints below). "bags" is the number of learners you should
train using Bootstrap Aggregation. If boost is true, then you should implement boosting.

If verbose is True, your code can generate output. Otherwise it should be silent.
Notes: See hints section below for example code you might use to instantiate your learners. Boosting is an optional
topic and not required. There's a citation below in the Resources section that outlines a method of implementing
bagging. If the training set contains n data items, each bag should contain n items as well.
Note that because you should sample with replacement, some of the data items will be repeated.

This code should not generate statistics or charts. Modify testlearner.py to generate statistics and charts.




Extra Credit (3%)
Implement boosting as part of BagLearner. How does boosting affect performance for ripple and 3_groups data?
Does overfitting occur for either of these datasets as the number of bags with boosting increases?
Create your own dataset for which overfitting occurs as the number of bags with boosting increases.
Describe and assess your boosting code in a separate report.pdf. Your report should focus only on boosting.
It should be submitted separately to the "extra credit" assignment on t-square.


    def query(self, points):
        predictions
        for learner in self.learners:
            predictions += learner.query(points)
        return average(predictions)
'''

import numpy as np
import scipy as sci
import pandas as pd
from scipy.spatial import distance


class BagLearner(object):

    def __init__(self, learner, kwargs, bags, boost, verbose):
        self.learner = learner
        self.kwargs = kwargs
        self.k = kwargs.get('k', -1)
        self.bags = bags #np.empty([bags,1])
        self.boost = boost
        self.verbose = verbose



    def print_full(x):
        pd.set_option('display.max_rows', len(x))
        print(x)
        pd.reset_option('display.max_rows')


    def addEvidence(self,dataX,dataY):
        """
        @summary: Add training data to learner
        @param dataX: X values of data to add
        @param dataY: the Y training values
        """

        #Let's convert all this data into a pandas dataframe
        #Create a 0 - 600 index so that we can find things by index later on.
        index1= np.arange(dataX.shape[0]).tolist()

        #using that index, make a series out of each column of data
        a = pd.Series(dataX[:,0], index=index1)
        b = pd.Series(dataX[:,1], index=index1)
        c = pd.Series(dataY, index=index1)

        #Make a dictionary to add column names
        d = {'TrainX1' : a, 'TrainX2' : b, 'TrainY':c}
        #save the dataframe as the model
        self.model = pd.DataFrame(d)

        def print_full(x):
            pd.set_option('display.max_rows', len(x))
            print(x)
            pd.reset_option('display.max_rows')

        #build bags
        size = self.bags
        self.bags = np.empty([size,dataX.shape[0],3])
        for x in range(0, size):
            self.bags[x] = self.model.sample(n=None, frac=1, replace=True, weights=None, random_state=None, axis=None)


    def query(self,points):
        """
        @summary: Estimate a set of test points given the model we built.
        @param points: should be a numpy array with each row corresponding to a specific query.
        @returns the estimated values according to the saved model.
        """
        Z=np.zeros(points.shape[0])
        if self.boost == False:
            for x in range(0,self.bags.shape[0]):
                learner = self.learner(verbose = False, **self.kwargs)
                learner.addEvidence(self.bags[x][:,0:2], self.bags[x][:,2]) # training step
                Y = learner.query(points) # queryY=[]
                Z += Y
                #calulate the mean
            Y = Z / self.bags.shape[0]
        else:
            final = np.zeros(points.shape[0])
            for learner in self.learner:
                for x in range(0,self.bags.shape[0]):
                    learner.addEvidence(self.bags[x][:,0:2], self.bags[x][:,2]) # training step
                    Y = learner.query(points) # queryY=[]
                    Z += Y
                Y = Z / self.bags.shape[0]
                final += Y
            Y = final / len(self.learner)


        return Y