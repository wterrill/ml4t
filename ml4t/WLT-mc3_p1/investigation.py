'''
Create your own dataset generating code (call it best4KNN.py) that creates data that performs significantly better
with KNNLearner than LinRegLearner. Explain your data generating algorithm, and explain why KNNLearner performs
better.
Your data should include at least 2 dimensions in X, and at least 1000 points. (Don't use bagging for this section).
'''



import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import math



def threeD_plot(X1,X2,Y):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(X1, X2, Y, c='r', marker='o')
    ax.set_xlabel('X1')
    ax.set_ylabel('X2')
    ax.set_zlabel('Y')
    ax.view_init(elev=50., azim=45)
    plt.show()

def ripple_graph():
    inf = open('Data/ripple.csv')
    data = np.array([map(float,s.strip().split(',')) for s in inf.readlines()])
    Y=data[:,2]
    X1=data[:,0]
    X2=data[:,1]

    threeD_plot(X1,X2,Y)

def simple_graph():
    inf = open('Data/simple.csv')
    data = np.array([map(float,s.strip().split(',')) for s in inf.readlines()])
    Y=data[:,2]
    X1=data[:,0]
    X2=data[:,1]

    threeD_plot(X1,X2,Y)

def three_groups():
    inf = open('Data/3_groups.csv')
    data = np.array([map(float,s.strip().split(',')) for s in inf.readlines()])
    Y=data[:,2]
    X1=data[:,0]
    X2=data[:,1]

    threeD_plot(X1,X2,Y)

def my_data():
    Y = np.arange(0,6.2,0.006201)
    Y = Y.reshape(1000,1)
    X1 = np.sin(Y)
    X2 = np.cos(Y)

    threeD_plot(X1,X2,Y)
    temp = np.concatenate([X1,X2],axis=1)
    final = np.concatenate([temp,Y],axis=1)
    #points = pd.DataFrame(final,columns=['X1', 'X2', 'Y'])
    np.random.shuffle(final)
    np.savetxt("./Data/linreg.csv", final, delimiter=",")

    #points.values.to_csv("./Data/linreg.csv")

def my_data1():
    i = -1
    array=np.zeros((2500,4))
    for xi in range (-25,25):
        x1 = float(xi)/100
        for xii in range(-25,25):
            x2=float(xii)/100
            y = x1**2 + x2**2
            i = i+1
            array[i,:] = [i,x1,x2,y]



    X1=array[:,1]
    X2=array[:,2]
    Y=array[:,3]


    threeD_plot(X1,X2,Y)
    X1 = X1.reshape(2500,1)
    X2 = X2.reshape(2500,1)
    Y = Y.reshape(2500,1)
    temp = np.concatenate([X1,X2],axis=1)
    final = np.concatenate([temp,Y],axis=1)
    #points = pd.DataFrame(final,columns=['X1', 'X2', 'Y'])
    np.random.shuffle(final)
    np.savetxt("./Data/knn.csv", final, delimiter=",")


def my_data2():
    array = np.zeros((1000,3))

    x = np.arange(-1,1,0.001)
    X1 = x.reshape(2000,1)
    Y = 3*X1 + 2
    X2 = np.ones((2000,1))

    threeD_plot(X1,X2,Y)
    temp = np.concatenate([X1,X2],axis=1)
    final = np.concatenate([temp,Y],axis=1)
    #points = pd.DataFrame(final,columns=['X1', 'X2', 'Y'])
    np.random.shuffle(final)
    np.savetxt("./Data/linreg.csv", final, delimiter=",")

if __name__ == "__main__":
    ripple_graph()
    simple_graph()
    three_groups()
    #my_data()
    #my_data1()
    #my_data2()





    #points.to_csv("linreg.csv")
